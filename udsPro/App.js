/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import { Navigation } from "react-native-navigation";
import { StatusBar, YellowBox } from 'react-native';
StatusBar.setBarStyle('light-content', true);

// Hide Warnig messages
YellowBox.ignoreWarnings(['Require cycle: node_modules/', 'Virtualized', 'Unable to find module for UIManager']);

import LoginScreen from "./src/Screens/Login/LoginScreen";
import CaseListing from "./src/Screens/Case/Listing/Listing";
import CompareCaseListingConflictsScreen from './src/Screens/Documents/CompareCaseListingConflictsScreen';
import ErrorLogsScreen from "./src/Screens/Logs/ErrorLogsScreen";


// Register Screens
Navigation.registerComponent("LoginScreen", () => LoginScreen);
Navigation.registerComponent("CaseListing", () => CaseListing);
Navigation.registerComponent("CompareCaseListingConflictsScreen", () => CompareCaseListingConflictsScreen);
Navigation.registerComponent("ErrorLogsScreen", () => ErrorLogsScreen);

// Starts App
Navigation.events().registerAppLaunchedListener(() => {
    Navigation.setDefaultOptions({
        animations: {
            push: {
                waitForRender: true
            },
            setRoot: {
                waitForRender: true
            }
        }
    });

    Navigation.setRoot({
        root: {
            stack: {
                id: 'udsPro',
                options: {
                    topBar: {
                        visible: false,
                        animate: true,
                    }
                },
                children: [
                    {
                        component: {
                            name: "LoginScreen",
                        }
                    }
                ]
            }
        }
    });
});