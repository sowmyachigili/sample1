/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */
 
# Steps for react native setup
https://medium.com/@randerson112358/setup-react-native-environment-for-ios-97bf7faadf77


## IOS Build Steps

cd cssi-uds/udsPro

npm install

yarn add react-native-navigation

react-native link

replace appDelegate.m filw with react-native-navigation appdelegate.m file
https://github.com/wix/react-native-navigation/blob/master/example/ios/example/AppDelegate.m

To open project in xcode - cd udsPro->ios-> double click on - PROi App.xcodeproj

Xcode -> project -> project Settings -> build -> legacy build system

Product->Build

Run from Xcode

If screen is opening only half in ipad
https://github.com/facebook/react-native/issues/11102

For adding custom fonts in ios,android react native
https://medium.com/react-native-training/react-native-custom-fonts-ccc9aacf9e5e


For adding toaster message with icon
https://github.com/prscX/react-native-toasty


If fetch method failing ios like network request failed
https://stackoverflow.com/questions/31254725/transport-security-has-blocked-a-cleartext-http

# Login Credentials
{
  "UserName": "Jayeesha",
  "FacilityCode": "m999",
  "Password": "cssi@123"
}

# Realm Support
npm install --save realm@2.16.0
react-native link realm

For reference check this link
https://realm.io/docs/javascript/latest/

Error
Cannot read property 'debugHosts' of undefined 

Solution
npm i -g rnpm
rnpm link realm

//Animations

# Animations

npm install react-native-animatable --save
https://github.com/oblador/react-native-animatable

# Third party modules

https://wix.github.io/react-native-navigation/#/installation-android?id=android-installation Used for navigation

https://github.com/oblador/react-native-vector-icons For displaying icons

https://nativebase.io/  It includes all controls

https://github.com/oblador/react-native-animatable For applying animations (used in icd popup screen)

https://github.com/xgfe/react-native-datepicker Date picker in all screens

https://react-native-training.github.io/react-native-elements/docs/getting_started.html Provides some controls

https://www.npmjs.com/package/react-native-modal Used for all popups

https://www.npmjs.com/package/react-native-modest-checkbox For displaying checkbox with images used in all tabs

https://www.npmjs.com/package/react-native-picker-select For dropdown

https://www.npmjs.com/package/react-native-table-component For displaying tables . Used in pre-admit document only

https://realm.io/blog/introducing-realm-react-native/ Used for saving data in offline mode

https://www.npmjs.com/package/react-native-simple-popover for displaying popover ( installed and commented code )

https://github.com/steffeydev/react-native-popover-view for displaying popover ( used in preadmission listing page )

https://github.com/master-atul/react-native-exception-handler for handling erros

https://github.com/avishayil/react-native-restart - for restarting app incase of crash

Screen keep awake
https://www.npmjs.com/package/react-native-keep-awake

Use keyboard manager for auto tabing
https://github.com/douglasjunior/react-native-keyboard-manager

<!-- Cocoapods Installation steps in IOS(For react-native-keyboard-manager)-->

--> open ios folder in terminal
$ sudo gem install cocoapods
$ pod init 
  --> it will creates a pod file in your project IOS folder and then remove all data in podFile and paste below code     in pod file and save

      platform :ios, '9.0'

      target 'PROi App' do
      pod 'ReactNativeKeyboardManager', :path => '../node_modules/react-native-keyboard-manager'

      pod 'React', :path => '../node_modules/react-native', :subspecs => [
      'Core',
      'CxxBridge',
      'DevSupport',
      # the following ones are the ones taken from "Libraries" in Xcode:
      'RCTAnimation',
      'RCTActionSheet',
      'RCTBlob',
      'RCTGeolocation',
      'RCTImage',
      'RCTLinkingIOS',
      'RCTNetwork',
      'RCTSettings',
      'RCTText',
      'RCTVibration',
      'RCTWebSocket'
      ]

      pod 'yoga', :path => '../node_modules/react-native/ReactCommon/yoga'
      pod 'DoubleConversion', :podspec => '../node_modules/react-native/third-party-podspecs/DoubleConversion.podspec'
      pod 'Folly', :podspec => '../node_modules/react-native/third-party-podspecs/Folly.podspec'
      pod 'glog', :podspec => '../node_modules/react-native/third-party-podspecs/GLog.podspec'

      end

$ pod install


# Pod errors
# error: 
The sandbox is not in sync with the Podfile.lock. Run 'pod install' or update your CocoaPods installation.

Solution
https://stackoverflow.com/questions/21366549/errorthe-sandbox-is-not-in-sync-with-the-podfile-lock-after-installing-res



#Firebase crashlytics
 - Gmail Credentials
proidev.contact@gmail.com
cssi@123

1. https://console.firebase.google.com/
2. Create new project and go to Quality/Crashlytics in sidemenu.
3. Follow the steps in firebase.


Document (Steps for Firebase):
https://firebase.google.com/docs/ios/setup?authuser=0


