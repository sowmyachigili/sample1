/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import { Alert } from 'react-native';
import RNExitApp from 'react-native-exit-app';
import DeviceInfo from 'react-native-device-info';
import configMessage from '../config/configMessages';

// check passcode is enable or disable in ipad
checkPasscodeSetOrNot = () => {
    return new Promise((resolve, reject) => {
        DeviceInfo.isPinOrFingerprintSet()(isSet => {
            if (isSet) {
                resolve(true);
            }
            else {
                resolve(false);
            }
        });
    });
};

export default {
    displayPasscodeErrorAlert: (isActive) => {
        if (isActive) {
            checkPasscodeSetOrNot().then(async (isPasscodeEnable) => {
                if (!isPasscodeEnable) {
                    setTimeout(async () => {
                        Alert.alert(configMessage.passcodeAlertMessage, '', [{
                            text: 'OK',
                            onPress: () => {
                                RNExitApp.exitApp()
                            }
                        }]);
                    }, 350);
                }
            });
        }
    }
};
