/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import config from '../config/config';
import commonFun from './commonFun';

calculateMotorScoreIndexForV3 = (fim) => {
    if (fim) {
        let ProjectedMotorScoreIndex = commonFun.calculateMotorScoreIndexForV3(fim);
        return ProjectedMotorScoreIndex;
    }
}

export default {
    // FIM tab isMiniFim fields checked values calculated for these fields
    // (TotalFIM, TotalCognition and TotalMotor)
    checkedFIMStatusCalculation: (FIM) => {
        //TotalMotor
        let TotalMotor = 0;
        if (FIM.Toileting || FIM.Bathing || FIM.Bladder || FIM.WalkWheelChair || FIM.BedChair) {
            let toileting = 0, bathing = 0, bladder = 0, walkWheelChair = 0, BedChair = 0;
            if (FIM.Toileting) {
                toileting = parseInt(FIM.Toileting) * config.fimToilet;
                TotalMotor += toileting;
            }
            if (FIM.Bathing) {
                bathing = parseInt(FIM.Bathing) * config.fimBathing;
                TotalMotor += bathing;
            }
            if (FIM.Bladder) {
                bladder = parseInt(FIM.Bladder) * config.fimBladder;
                TotalMotor += bladder;
            }
            if (FIM.WalkWheelChair) {
                walkWheelChair = parseInt(FIM.WalkWheelChair) * config.fimWalkWheelChair;
                TotalMotor += walkWheelChair;
            }

            if (FIM.BedChair) {
                BedChair = parseInt(FIM.BedChair) * config.fimBedChair;
            }
        }

        //TotalCognition
        let ProjectedTotalCognition = 0
        if (FIM.ProblemSolving && FIM.ProblemSolving > 0) {
            ProjectedTotalCognition = ((parseInt(FIM.ProblemSolving)) * config.fimProblemSolving) + 5.65;
        }

        // Projected Tub/Shower Transfer score
        let ProjectedTotalTubShowerTransferScore = 0, Bathing = 0, Bladder = 0, Toileting = 0, WalkWheelChair = 0, BedChair = 0;
        if (FIM.Bathing) {
            Bathing = parseInt(FIM.Bathing) * config.tubShowerBathing;
        }
        if (FIM.Bladder) {
            Bladder = parseInt(FIM.Bladder) * config.tubShowerBladder;
        }
        if (FIM.Toileting) {
            Toileting = parseInt(FIM.Toileting) * config.tubShowerToileting;
        }
        if (FIM.WalkWheelChair) {
            WalkWheelChair = parseInt(FIM.WalkWheelChair) * config.tubShowerWalkWheelChair;
        }
        if (FIM.BedChair) {
            BedChair = parseInt(FIM.BedChair) * config.tubShowerBedChair;
        }
        ProjectedTotalTubShowerTransferScore = Bathing + Toileting + BedChair + Bladder + WalkWheelChair - 0.10;

        // TotalFIM
        let projectedMoterScoreForCMG = calculateProjectedMotorScore(FIM);
        let ProjectedTotalFimScore = Math.round(ProjectedTotalCognition + projectedMoterScoreForCMG + ProjectedTotalTubShowerTransferScore);
        ProjectedTotalFimScore = (ProjectedTotalFimScore < 18) ? 18 : ProjectedTotalFimScore;

        projectedMoterScoreForCMG = (projectedMoterScoreForCMG < 12) ? 12 : projectedMoterScoreForCMG;
        projectedMoterScoreForCMG = Math.round(projectedMoterScoreForCMG);
        ProjectedTotalCognition = Math.round(ProjectedTotalCognition);

        //MotorScoreIndex
        let ProjectedMotorScoreIndex = 0;
        ProjectedMotorScoreIndex = calculateMotorScoreIndexForV3(FIM);

        return {
            TotalFIM: ProjectedTotalFimScore,
            TotalCognition: ProjectedTotalCognition,
            TotalMotor: projectedMoterScoreForCMG,
            MotorScoreIndex: ProjectedMotorScoreIndex
        }
    },

    // FIM tab isMiniFim fields unchecked values calculated for these fields
    // (TotalFIM, TotalCognition and TotalMotor)
    uncheckedFIMStatusCalculation: (FIM) => {
        //TotalFIM
        let TotalFIM = 0
        config.uncheckedTotalFIMArray.forEach(function (element) {
            if (FIM[element] && FIM[element] > 0) {
                TotalFIM += parseInt(FIM[element])
            } else if (FIM[element] == 0) {
                if (element.name == 'Toileting') {
                    TotalFIM += 2;
                } else {
                    TotalFIM += 1;
                }
            }
        });
        //TotalCognition
        let TotalCognition = 0
        config.uncheckedTotalCognitionArray.forEach(function (element) {
            if (FIM[element] && FIM[element] > 0) {
                TotalCognition += parseInt(FIM[element])
            } else if (FIM[element] == 0) {
                if (element.name == 'Toileting') {
                    TotalCognition += 2;
                } else {
                    TotalCognition += 1;
                }
            }
        });
        // TotalMotor
        let TotalMotor = parseInt(TotalFIM) - parseInt(TotalCognition);
        if (FIM.TubShower && FIM.TubShower > 0) {
            TotalMotor = parseInt(TotalMotor) - FIM.TubShower;
        } else if (FIM.TubShower == 0) {
            TotalMotor = parseInt(TotalMotor) - 1;
        }

        //MotorScoreIndex
        let TotalMotorScoreIndex = 0;
        TotalMotorScoreIndex = calculateMotorScoreIndexForV3(FIM);
        return {
            TotalFIM: TotalFIM,
            TotalCognition: TotalCognition,
            TotalMotor: TotalMotor,
            MotorScoreIndex: TotalMotorScoreIndex
        }

    },

    // calculation projected motor score for CMG/MiniFim
    calculateProjectedMotorScore: (fim) => {
        let ProjectedEatingScore, ProjectedGroomingScore, ProjectedDressingUpperScore,
            ProjectedBowelScore, ProjectedToiletTransferScore, ProjectedDressingLowerScore,
            ProjectedStairsScore, ProjectedMotorScore, ProjectedTubShowerSCore;
        if (fim) {
            let Bathing = 0, Bladder = 0, Toileting = 0, ProblemSolving = 0, WalkWheelChair = 0, BedChair = 0;
            if (fim.Bathing) {
                Bathing = parseInt(fim.Bathing);
            }
            if (fim.Bladder) {
                Bladder = parseInt(fim.Bladder);
            }
            if (fim.Toileting) {
                Toileting = parseInt(fim.Toileting);
            }
            if (fim.WalkWheelChair) {
                WalkWheelChair = parseInt(fim.WalkWheelChair);
            }
            if (fim.ProblemSolving) {
                ProblemSolving = parseInt(fim.ProblemSolving);
            }
            if (fim.BedChair) {
                BedChair = parseInt(fim.BedChair);
            }

            // Projected Eating Score
            ProjectedEatingScore = ((Bathing * 0.18) + (Toileting * 0.08) +
                (BedChair * 0.06) + (Bladder * 0.08) +
                (WalkWheelChair * 0.04) + (ProblemSolving * 0.30) + 2.50);

            ProjectedEatingScore = (ProjectedEatingScore > 7) ? 7 : ProjectedEatingScore;

            // Projected Grooming Score
            ProjectedGroomingScore = ((Bathing * 0.35) + (Toileting * 0.06) + (BedChair * 0.09) + (Bladder * 0.05) +
                (WalkWheelChair * 0.03) + (ProblemSolving * 0.20) + 1.52);

            // Projected Dressing Upper Score
            ProjectedDressingUpperScore = ((Bathing * 0.35) + (Toileting * 0.15) + (BedChair * 0.14) + (Bladder * 0.07) +
                (WalkWheelChair * 0.03) + (ProblemSolving * 0.14) + 0.52);
            ProjectedDressingUpperScore = ProjectedDressingUpperScore ? parseInt(ProjectedDressingUpperScore) : null;

            // Projected Bowel Score
            ProjectedBowelScore = ((Bathing * 0.09) + (Toileting * 0.12) + (BedChair * 0.08) + (Bladder * 0.39) +
                (WalkWheelChair * 0.01) + (ProblemSolving * 0.06) + 1.24);

            // Projected Toileting Transfer Score
            ProjectedToiletTransferScore = ((Bathing * 0.09) + (Toileting * 0.25) + (BedChair * 0.53) + (Bladder * 0.03) +
                (WalkWheelChair * 0.03) + (ProblemSolving * 0.01) + 0.20);

            // Projected Dressing Lower Score
            ProjectedDressingLowerScore = ((Bathing * 0.25) + (Toileting * 0.31) + (BedChair * 0.22) + (Bladder * 0.05) +
                (WalkWheelChair * 0.08) - 0.04);
            //  *Note: problem solving is not included for this item 
            // **Note: the constant is subtracted from the equation (opposed to added)

            // Projected Stairs Score
            ProjectedStairsScore = ((Bathing * 0.06) + (Toileting * 0.14) + (BedChair * 0.20) + (Bladder * 0.02) +
                (WalkWheelChair * 0.30) - 0.50);
            // *Note: problem solving is not included for this item 
            // **Note: the constant is subtracted from the equation (opposed to added)

            ProjectedTubShowerSCore = ((Bathing * 0.36) + (Toileting * 0.14) + (BedChair * 0.24) + (Bladder * 0.03) + (WalkWheelChair * 0.10) - 0.10);

            // Projected Motor Score for CMG/MiniFIM
            ProjectedMotorScore = ProjectedEatingScore + ProjectedGroomingScore + ProjectedDressingUpperScore +
                ProjectedBowelScore + ProjectedToiletTransferScore + ProjectedDressingLowerScore + ProjectedStairsScore +
                Bathing + Toileting + Bladder + BedChair + WalkWheelChair;
            ProjectedMotorScore = Math.round(ProjectedMotorScore);

            return ProjectedMotorScore;
        }
    }
}