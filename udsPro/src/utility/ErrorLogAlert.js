/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import { Alert } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import RNRestart from 'react-native-restart';
import realmController from "../Realm/realmController";
import schema from "../Realm/schemaNames";
import dateFormats from './formatDate';
let networkErrMessage = '';
export default ErrorLogHandler = (e, isFatal, preadmissionServerObj, hideAlert, errorFromServer) => {
    if (isFatal) {
        if (hideAlert && hideAlert == 'Hide Alert') { // comes from login page
            let exceptionDetails = {};
            exceptionDetails.ExceptionHandlingID = realmController.getMaxPrimaryKeyId(schema.EXCEPTION_HANDLING_SCHEMA, 'ExceptionHandlingID');
            exceptionDetails.Description = e.message;
            exceptionDetails.InsertedOn = dateFormats.formatDate('todayDate');
            exceptionDetails.FacilityCode = e.FacilityCode ? e.FacilityCode : '';
            exceptionDetails.InsertedBy = e.UserID ? parseInt(e.UserID) : null;
            realmController.insertOrUpdateRecord(schema.EXCEPTION_HANDLING_SCHEMA, exceptionDetails);
        } else {
            if (e.message) {
                // Insert exception details into realm
                AsyncStorage.getItem('loggedInUserDetails').then(userData => {
                    userData = JSON.parse(userData);
                    if (userData && userData.UserID) {
                        let exceptionDetails = {};
                        exceptionDetails.ExceptionHandlingID = realmController.getMaxPrimaryKeyId(schema.EXCEPTION_HANDLING_SCHEMA, 'ExceptionHandlingID');
                        if (preadmissionServerObj) {
                            exceptionDetails.Description = e.message + ', ## MESSAGELog: ' + JSON.stringify(preadmissionServerObj);
                        } else if (errorFromServer) {
                            exceptionDetails.Description = errorFromServer + ' error Message' + e.message;
                        } else {
                            if (e.message == 'Network request failed') {
                                networkErrMessage = e.message;
                            }
                            exceptionDetails.Description = 'CrashError - ' + e.message + ' ' + errorFromServer;
                        }
                        exceptionDetails.InsertedOn = dateFormats.formatDate('todayDate');
                        exceptionDetails.FacilityCode = userData.FacilityCode;
                        exceptionDetails.InsertedBy = userData.UserID;
                        realmController.insertOrUpdateRecord(schema.EXCEPTION_HANDLING_SCHEMA, exceptionDetails);
                    }
                });
            }
            if (!hideAlert) { // don't display alerts for some exceptions
                setTimeout(() => {
                    if (networkErrMessage) {
                        Alert.alert(
                            `${networkErrMessage}...`,
                            'Please check your internet connection',
                            [{
                                text: 'Restart',
                                onPress: () => {
                                    RNRestart.Restart();
                                }
                            }]
                        );
                    } else {
                        Alert.alert(
                            'Something went wrong...',
                            `We have reported this to our team ! Please restart the app.`,
                            [{
                                text: 'Restart',
                                onPress: () => {
                                    RNRestart.Restart();
                                }
                            }]
                        );
                    }
                }, 500);
            }
        }
    }
};
