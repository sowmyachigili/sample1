/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import React from 'react';
import {
  Text,
  View,
  processColor,
} from 'react-native';

import { RadarChart } from 'react-native-charts-wrapper';
import styles from '../components/styles/styles';

class RadarChartScreen extends React.Component {

  constructor(props) {
    super(props);
    this.inputRefs = {};

    this.state = {
      legend: {
        enabled: false,
        textSize: 14,
        form: 'CIRCLE',
        wordWrapEnabled: true
      }
    };
  }

  render() {
    return (
      <View style={{ flex: 1 }} pointerEvents={'none'}>
        <View style={[styles.radraChartContainer]}>
          <RadarChart
            style={[styles.radarChartheight]}
            data={this.props.data}
            xAxis={{
              valueFormatter: this.props.xAxisValues,
              fontWeight: 'bold',
              textSize: 13

            }}
            yAxis={{
              drawLabels: true,
              axisMaximum: 7,
              axisMinimum: 0,
              labelCount: 8,
              labelCountForce: true,
              fontWeight: 'bold',
            }}
            chartDescription={{ text: '' }}
            legend={this.state.legend}
            drawWeb={true}

            webLineWidth={2}
            webLineWidthInner={2}
            webAlpha={255}
            webColor={processColor("rgb(211,211,211)")}
            webColorInner={processColor("grey")}
            skipWebLineCount={0}
          />
        </View>

      </View>
    );
  }
}

export default RadarChartScreen;