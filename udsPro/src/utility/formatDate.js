/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import Moment from 'moment';
import moment from 'moment-timezone';
import config from '../config/config';

removeZFromDate = (specificDate) => {
    let newDate = JSON.stringify(specificDate);
    return JSON.parse(newDate.replace('Z', ''));
}

export default {
    formatDate: (specificDate, expectedformat = config.dbDateFormat) => {
        if (specificDate === 'todayDate') {
            return Moment().tz(config.ESTTimezone).format(expectedformat); // est time
        } else if (specificDate === 'UTCTimeNow') {
            return Moment.utc().format(expectedformat); // utc time
        } else {
            if (specificDate) {
                let newDate = removeZFromDate(specificDate);
                return Moment(newDate).format(expectedformat); // selected date time
            } else {
                return null;
            }
        }
    },
    formatWithTimeZone: (specificDate, expectedformat = config.dbDateFormat) => {
        if (specificDate === 'todayDate') {
            return Moment().tz(config.ESTTimezone).format(expectedformat);
        } else {
            if (specificDate) {
                let newDate = removeZFromDate(specificDate);
                let dateAfterFormat = Moment.tz(newDate, config.ESTTimezone).format(expectedformat);
                return dateAfterFormat;
            } else {
                return null;
            }
        }
    },
    addMinutesToDate: (minutes) => {
        let dateAfterMinutes = Moment().tz(config.ESTTimezone).add(minutes, 'minutes').format('YYYY-MM-DD HH:mm:ss');
        return dateAfterMinutes;
    },
    addDaysToDate: (date, days) => {
        if (date) {
            let newDate = removeZFromDate(date);
            let dateAfterDays = Moment(newDate).add(days, 'days').format('YYYY-MM-DD HH:mm:ss');
            return dateAfterDays;
        } else {
            return
        }
    },
    addSecondsToDate: (date, seconds) => {
        if (date) {
            let newDate = removeZFromDate(date);
            let dateAfterDays = Moment(newDate).add(seconds, 'seconds').format('YYYY-MM-DD HH:mm:ss');
            return dateAfterDays;
        } else {
            return
        }
    },
    subtractDaysFromDate: (date, days) => {
        if (date) {
            let newDate = removeZFromDate(date);
            return Moment(newDate).subtract(days, 'days').format('YYYY-MM-DD');
        } else {
            return null;
        }
    },
    subtractHoursFromDate: (date, hours) => {
        if (date) {
            let newDate = removeZFromDate(date);
            return Moment(newDate).subtract(hours, 'hours').format('YYYY-MM-DD');
        } else {
            return null;
        }
    },
    datesComparisionWithSameOrAfter: (newDate, currentDate) => {
        if (newDate && currentDate) {
            let newDateModified = removeZFromDate(newDate);
            let currentDateModified = removeZFromDate(currentDate);
            return Moment(currentDateModified).isSameOrAfter(newDateModified);
        } else {
            return null;
        }
    },
    datesComparisionBefore: (newDate, currentDate) => {
        if (newDate && currentDate) {
            let newDateModified = removeZFromDate(newDate);
            let currentDateModified = removeZFromDate(currentDate);
            return Moment(newDateModified).isBefore(currentDateModified);
        } else {
            return null;
        }
    },
    datesComparisionAfter: (newDate, currentDate) => {
        if (newDate && currentDate) {
            let newDateModified = removeZFromDate(newDate);
            let currentDateModified = removeZFromDate(currentDate);
            return Moment(newDateModified).isAfter(currentDateModified);
        } else {
            return null;
        }
    },
    datesComparisionSameOrBefore: (newDate, currentDate) => {
        if (newDate && currentDate) {
            let newDateModified = removeZFromDate(newDate);
            let currentDateModified = removeZFromDate(currentDate);
            return Moment(newDateModified).isSameOrBefore(currentDateModified);
        } else {
            return null;
        }
    },
    datesComparisionSame: (newDate, currentDate) => {
        if (newDate && currentDate) {
            let newDateModified = removeZFromDate(newDate);
            let currentDateModified = removeZFromDate(currentDate);
            return Moment(newDateModified).isSame(currentDateModified);
        } else {
            return null;
        }
    },
    differenceBetweenDatesInHours: (date1, date2) => {
        if (date1 && date2) {
            let modifiedDate1 = removeZFromDate(date1);
            let modifiedDate2 = removeZFromDate(date2);
            modifiedDate1 = Moment(modifiedDate1).format('YYYY-MM-DD HH:mm:ss');
            modifiedDate2 = Moment(modifiedDate2).format('YYYY-MM-DD HH:mm:ss');
            let hours = Moment(modifiedDate1).diff(Moment(modifiedDate2), 'hours');
            return Math.abs(hours);
        } else {
            return null;
        }
    },
    currentDateTime: () => {
        return Moment().tz(config.ESTTimezone).format('YYYY-MM-DD HH:mm:ss');
    },
    addTimeToDate: (date, hours, minutes) => {
        if (date) {
            let modifiedDate = removeZFromDate(date);
            return Moment(modifiedDate)
                .add(hours, 'hours')
                .add(minutes, 'minutes')
                .format(config.dbDateFormat);
        } else {
            return null;
        }
    },
    differenceBetweenDatesInYears: (date1, date2) => {
        let age = Moment(date1).diff(Moment(date2), 'years');
        return age;
    },
    getCurrentYear: () => {
        var year = moment().format('YYYY');
        year = moment().year();
        return year;
    },
    getYearsFromDate: (date) => {
        let age = moment().diff(date, 'years')
        return age.toString();
    },
    // For adding or subtracting hours to utc time
    addOrSubtractHoursToUTCTime: (hours) => {
        if (hours) {
            let newDate;
            // = Moment.utc();
            if (hours > 0) {
                newDate = Moment.utc().add(Math.abs(hours), 'hours').format(config.fullDateFormat);
            } else {
                newDate = Moment.utc().subtract(Math.abs(hours), 'hours').format(config.fullDateFormat);
            }
            return newDate;
        } else {
            return null;
        }
    }
};
