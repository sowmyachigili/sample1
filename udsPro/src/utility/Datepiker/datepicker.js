/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import React, { Component } from 'react';
import configImage from '../../config/configImages';

import PropTypes from 'prop-types';
import {
    View,
    Text,
    Image,
    Modal,
    TouchableHighlight,
    DatePickerAndroid,
    TimePickerAndroid,
    DatePickerIOS,
    Platform,
    Animated,
    Keyboard, TouchableOpacity, TextInput,
    StyleSheet, TouchableWithoutFeedback
} from 'react-native';
import Style from './style';
import Moment from 'moment';

const FORMATS = {
    'date': 'YYYY-MM-DD',
    'datetime': 'YYYY-MM-DD HH:mm',
    'time': 'HH:mm'
};

const SUPPORTED_ORIENTATIONS = ['portrait', 'portrait-upside-down', 'landscape', 'landscape-left', 'landscape-right'];

class DatePicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
            date: this.getDate(),
            modalVisible: false,
            animatedHeight: new Animated.Value(0),
            allowPointerEvents: true
        };

        this.getDate = this.getDate.bind(this);
        this.getDateStr = this.getDateStr.bind(this);
        this.datePicked = this.datePicked.bind(this);
        this.onPressDate = this.onPressDate.bind(this);
        this.onPressCancel = this.onPressCancel.bind(this);
        this.onPressConfirm = this.onPressConfirm.bind(this);
        this.onDateChange = this.onDateChange.bind(this);
        this.onPressMask = this.onPressMask.bind(this);
        this.onDatePicked = this.onDatePicked.bind(this);
        this.onTimePicked = this.onTimePicked.bind(this);
        this.onDatetimePicked = this.onDatetimePicked.bind(this);
        this.onDatetimeTimePicked = this.onDatetimeTimePicked.bind(this);
        this.setModalVisible = this.setModalVisible.bind(this);
        this.onUpArrow = this.onUpArrow.bind(this);
        this.onDownArrow = this.onDownArrow.bind(this);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.date !== this.props.date) {
            this.setState({ date: this.getDate(prevProps.date) });
        }
    }

    setModalVisible(visible) {
        const { height, duration } = this.props;

        // slide animation
        if (visible) {
            this.setState({ modalVisible: visible });
            return Animated.timing(
                this.state.animatedHeight,
                {
                    toValue: height,
                    duration: duration
                }
            ).start();
        } else {
            return Animated.timing(
                this.state.animatedHeight,
                {
                    toValue: 0,
                    duration: duration
                }
            ).start(() => {
                this.setState({ modalVisible: visible });
            });
        }
    }

    onStartShouldSetResponder(e) {
        return true;
    }

    onMoveShouldSetResponder(e) {
        return true;
    }

    // these timeouts were a hacky first pass at ensuring the callback triggered after the modal animation
    // TODO: find a better approach
    onUpArrow() {
        const { onUpArrow } = this.props;
        this.onPressConfirm();

        this.setModalVisible(false);
        setTimeout(onUpArrow);
    }

    onDownArrow() {
        const { onDownArrow } = this.props;
        this.onPressConfirm();
        setTimeout(onDownArrow, 500);
    }

    onPressMask() {
        if (typeof this.props.onPressMask === 'function') {
            this.props.onPressMask();
        } else {
            this.onPressCancel();
        }
    }

    onPressCancel() {
        this.setModalVisible(false);

        if (typeof this.props.onCloseModal === 'function') {
            this.props.onCloseModal();
        }
    }

    onPressConfirm() {
        this.datePicked();
        this.setModalVisible(false);

        if (typeof this.props.onCloseModal === 'function') {
            this.props.onCloseModal();
        }
    }

    getDate(date = this.props.date) {
        const { mode, minDate, maxDate, format = FORMATS[mode] } = this.props;

        // date默认值
        if (!date) {
            let now = new Date();
            if (minDate) {
                let _minDate = this.getDate(minDate);

                if (now < _minDate) {
                    return _minDate;
                }
            }

            if (maxDate) {
                let _maxDate = this.getDate(maxDate);

                if (now > _maxDate) {
                    return _maxDate;
                }
            }

            return now;
        }

        if (date instanceof Date) {
            return date;
        }

        return Moment(date, format).toDate();
    }

    getDateStr(date = this.props.date) {
        const { mode, format = FORMATS[mode] } = this.props;

        const dateInstance = date instanceof Date
            ? date
            : this.getDate(date);

        if (typeof this.props.getDateStr === 'function') {
            return this.props.getDateStr(dateInstance);
        }

        return Moment(dateInstance).format(format);
    }

    datePicked() {
        if (typeof this.props.onDateChange === 'function') {
            this.props.onDateChange(this.getDateStr(this.state.date), this.state.date);
        }
    }

    getTitleElement() {
        const { date, placeholder, customStyles, allowFontScaling } = this.props;

        if (!date && placeholder) {
            return (
                <Text allowFontScaling={allowFontScaling} style={[Style.placeholderText, customStyles.placeholderText]}>
                    {placeholder}
                </Text>
            );
        }
        return (
            <Text allowFontScaling={allowFontScaling} style={[Style.dateText, customStyles.dateText]}>
                {this.getDateStr()}
            </Text>
        );
    }

    onDateChange(date) {
        this.setState({
            allowPointerEvents: false,
            date: date
        });
        const timeoutId = setTimeout(() => {
            this.setState({
                allowPointerEvents: true
            });
            clearTimeout(timeoutId);
        }, 200);
    }

    onDatePicked({ action, year, month, day }) {
        if (action !== DatePickerAndroid.dismissedAction) {
            this.setState({
                date: new Date(year, month, day)
            });
            this.datePicked();
        } else {
            this.onPressCancel();
        }
    }

    onTimePicked({ action, hour, minute }) {
        if (action !== DatePickerAndroid.dismissedAction) {
            this.setState({
                date: Moment().hour(hour).minute(minute).toDate()
            });
            this.datePicked();
        } else {
            this.onPressCancel();
        }
    }

    onDatetimePicked({ action, year, month, day }) {
        const { mode, androidMode, format = FORMATS[mode], is24Hour = !format.match(/h|a/) } = this.props;

        if (action !== DatePickerAndroid.dismissedAction) {
            let timeMoment = Moment(this.state.date);

            TimePickerAndroid.open({
                hour: timeMoment.hour(),
                minute: timeMoment.minutes(),
                is24Hour: is24Hour,
                mode: androidMode
            }).then(this.onDatetimeTimePicked.bind(this, year, month, day));
        } else {
            this.onPressCancel();
        }
    }

    onDatetimeTimePicked(year, month, day, { action, hour, minute }) {
        if (action !== DatePickerAndroid.dismissedAction) {
            this.setState({
                date: new Date(year, month, day, hour, minute)
            });
            this.datePicked();
        } else {
            this.onPressCancel();
        }
    }

    onPressDate() {
        if (this.props.disabled) {
            return true;
        }

        Keyboard.dismiss();

        // reset state
        this.setState({
            date: this.getDate()
        });
        if (Platform.OS === 'ios') {
            this.setModalVisible(true);
        }

        if (typeof this.props.onOpenModal === 'function') {
            this.props.onOpenModal();
        }

        if (this.inputRef) { // to move datepicker to upward 
            this.inputRef.focus();
            this.inputRef.blur();
        }
    }

    setInputRef = (ref) => {
        this.inputRef = ref;
    }

    _renderIcon() {
        const {
            showIcon,
            iconSource,
            iconComponent,
            customStyles,
            onClickCancelIcon,
            disabled
        } = this.props;

        if (showIcon) {
            if (iconComponent) {
                return iconComponent;
            }

            return (
                <View style={[customStyles.dateIcon, { flexDirection: 'row', alignItems: 'center' }]}>
                    <TouchableWithoutFeedback onPress={this.onPressDate} disabled={disabled ? true : false}>
                        <Image style={customStyles.onlyDateIcon} source={iconSource} />
                    </TouchableWithoutFeedback>

                    <View style={customStyles.closeIconView} disabled={disabled ? true : false}>
                        <TouchableOpacity onPress={() => onClickCancelIcon ? onClickCancelIcon() : null}>
                            <Image style={[customStyles.clearIcon, { marginLeft: 8 }]} source={configImage.cancelIcon} />
                        </TouchableOpacity>
                    </View>
                </View>
            );
        }

        return null;
    }

    renderDoneBar() {
        const {
            TouchableComponent,
            allowFontScaling,
            confirmBtnText,
            onDownArrow,
            customStyles
        } = this.props;

        return (
            <View
                style={[defaultStyles.modalViewMiddle, customStyles.modalViewMiddle]}
                testID="done_bar">
                <View style={defaultStyles.nextView}>
                    <TouchableWithoutFeedback
                        activeOpacity={onDownArrow ? 0.5 : 1}
                        onPress={onDownArrow ? this.onDownArrow : null}>
                        <View>
                            <Text style={[defaultStyles.done, onDownArrow ? customStyles.btnTextConfirm : customStyles.btnTextNext]} allowFontScaling={allowFontScaling}>Next</Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
                {/* For displaying selected dropdown names */}
                <View style={defaultStyles.displayView}>
                    <Text style={[defaultStyles.done, customStyles.btnTextConfirm, defaultStyles.labelText]} numberOfLines={1}>
                        {this.props.displayName}
                    </Text>
                </View>
                <View style={defaultStyles.doneView}>
                    <TouchableWithoutFeedback
                        onPress={this.onPressConfirm}
                        hitSlop={{ top: 2, right: 2, bottom: 2, left: 2 }}
                    >
                        <View>
                            <Text style={[defaultStyles.done, customStyles.btnTextConfirm]} allowFontScaling={allowFontScaling}>{confirmBtnText}</Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            </View>
        );
    }

    render() {
        const {
            mode,
            style,
            customStyles,
            disabled,
            minDate,
            maxDate,
            minuteInterval,
            timeZoneOffsetInMinutes,
            TouchableComponent,
            testID,
            locale,
        } = this.props;

        const dateInputStyle = [
            Style.dateInput, customStyles.dateInput,
            disabled && Style.disabled,
            disabled && customStyles.disabled, {
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'flex-start',
                borderRadius: 5
            }
        ];

        return (
            <View style={[Style.dateTouch, style]}>
                <TouchableComponent
                    underlayColor={'transparent'}
                    onPress={this.onPressDate}
                    testID={testID}
                >
                    <View style={[Style.dateTouchBody, customStyles.dateTouchBody]} pointerEvents='box-only'>

                        {
                            !this.props.hideText ?
                                <View style={dateInputStyle}>
                                    {this.getTitleElement()}
                                    <TextInput
                                        style={defaultStyles.textInputStyle}
                                        ref={this.setInputRef}
                                    />
                                </View>
                                :
                                <View ><TextInput
                                    style={defaultStyles.textInputStyle}
                                    ref={this.setInputRef}
                                /></View>
                        }
                    </View>
                </TouchableComponent>
                {this._renderIcon()}

                {/* Modal starts */}
                {Platform.OS === 'ios' && <Modal
                    transparent={true}
                    animationType="fade"
                    visible={this.state.modalVisible}
                    supportedOrientations={SUPPORTED_ORIENTATIONS}
                >
                    {/* Modal body */}

                    <TouchableOpacity
                        style={[defaultStyles.modalViewTop]}
                    />

                    {this.renderDoneBar()}
                    <View style={[defaultStyles.modalViewBottom]}>
                        <DatePickerIOS
                            date={this.state.date}
                            mode={mode}
                            minimumDate={minDate && this.getDate(minDate)}
                            maximumDate={maxDate && this.getDate(maxDate)}
                            onDateChange={this.onDateChange}
                            minuteInterval={minuteInterval}
                            timeZoneOffsetInMinutes={timeZoneOffsetInMinutes ? timeZoneOffsetInMinutes : null}
                            style={[Style.datePicker, customStyles.datePicker]}
                            locale={locale}
                        />
                    </View>
                </Modal>}
            </View>
        );
    }
}

DatePicker.defaultProps = {
    mode: 'date',
    androidMode: 'default',
    date: '',
    // component height: 216(DatePickerIOS) + 1(borderTop) + 42(marginTop), IOS only
    height: 259,

    // slide animation duration time, default to 300ms, IOS only
    duration: 300,
    confirmBtnText: '确定',
    cancelBtnText: '取消',
    iconSource: require('./date_icon.png'),
    customStyles: {},

    // whether or not show the icon
    onUpArrow: null,
    onDownArrow: null,
    showIcon: true,
    disabled: false,
    allowFontScaling: true,
    hideText: false,
    placeholder: '',
    TouchableComponent: TouchableHighlight,
    modalOnResponderTerminationRequest: e => true,
    displayName: null,
    onClickCancelIcon: null
};

DatePicker.propTypes = {
    mode: PropTypes.oneOf(['date', 'datetime', 'time']),
    androidMode: PropTypes.oneOf(['clock', 'calendar', 'spinner', 'default']),
    date: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date), PropTypes.object]),
    format: PropTypes.string,
    minDate: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)]),
    maxDate: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)]),
    height: PropTypes.number,
    duration: PropTypes.number,
    confirmBtnText: PropTypes.string,
    cancelBtnText: PropTypes.string,
    iconSource: PropTypes.oneOfType([PropTypes.number, PropTypes.object]),
    iconComponent: PropTypes.element,
    customStyles: PropTypes.object,
    showIcon: PropTypes.bool,
    disabled: PropTypes.bool,
    allowFontScaling: PropTypes.bool,
    onDateChange: PropTypes.func,
    onOpenModal: PropTypes.func,
    onCloseModal: PropTypes.func,
    onPressMask: PropTypes.func,
    placeholder: PropTypes.string,
    modalOnResponderTerminationRequest: PropTypes.func,
    is24Hour: PropTypes.bool,
    getDateStr: PropTypes.func,
    locale: PropTypes.string,
    onUpArrow: PropTypes.func,
    onDownArrow: PropTypes.func,
    displayName: PropTypes.string,
    onClickCancelIcon: PropTypes.func,
};
const defaultStyles = StyleSheet.create({

    chevronContainer: {
        flexDirection: "row",
        marginLeft: 15
    },
    chevron: {
        fontSize: 22,
        color: '#D0D4DB'
    },
    chevronActive: {
        color: "black"
    },

    modalViewTop: {
        flex: 1
    },
    modalViewMiddle: {
        height: 44,
        zIndex: 2,
        flexDirection: "row",
        alignItems: "center",
        backgroundColor: "#EFF1F2",
        borderTopWidth: 0.5,
        borderTopColor: "#919498"
    },
    modalViewBottom: {
        height: 215, // 215
        justifyContent: "center",
        backgroundColor: "#D0D4DB"
    },
    done: {
        color: "#007AFE",
        fontWeight: "bold",
        padding: 10,
        fontSize: 18
    },
    labelText: {
        fontWeight: 'normal',
        color: '#007AFE'
    },
    nextView: {
        width: '10%',
        alignItems: 'flex-start',
        marginLeft: 5
    },
    doneView: {
        width: '10%',
        alignItems: 'flex-start'
    },
    displayView: {
        width: '80%',
        alignItems: 'center'
    },
    textInputStyle: {
        width: 0,
        height: 0
    }
});

export default DatePicker;
