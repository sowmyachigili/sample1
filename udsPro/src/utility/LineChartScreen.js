

/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    processColor
} from "react-native";

import { LineChart } from "react-native-charts-wrapper";
// styles
import newstyles from '../components/styles/styles';
class LineChartScreen extends Component {
    constructor(props) {
        super(props);
        this.inputRefs = {};
        this.state = {
            data: {},
            yAxis: {
                right: { enabled: false },
                left: {
                    enabled: true,
                    granularityEnabled: true,
                    granularity: 1,
                    axisMaximum: 7.8,
                    axisMinimum: 0,
                    labelCount: 9
                }
            },
        };
    }


    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.container}>
                    <View style={{ flex: 1, height: 40 }}>
                        <Text style={[newstyles.tabHeading, { paddingLeft: 80 }]} >FIM® Instrument Trending Report</Text>
                    </View>

                    <LineChart
                        style={this.props.type && this.props.type == 'graph1' ? styles.chart1 : styles.chart2}
                        data={{ dataSets: this.props.dataSets }}
                        chartDescription={{ text: " " }}
                        legend={{
                            enabled: true
                        }}
                        marker={{
                            enabled: true,
                            markerColor: processColor("white"),
                            textColor: processColor("black")
                        }}
                        xAxis={{
                            enabled: true,
                            granularityEnabled: true,
                            granularity: 1,
                            axisMaximum: (this.props.type && this.props.type == 'graph1') ?
                                20 : this.props.xaxisValues && this.props.xaxisValues.length > 10 ? this.props.xaxisValues.length + 2 : 10,
                            axisMinimum: 0,
                            labelCount: (this.props.type && this.props.type == 'graph1') ? 20 : 20,
                            drawLabels: true,
                            centerAxisLabels: false,
                            keepPositionOnRotation: true,
                            position: "BOTTOM",
                            drawAxisLine: true,
                            drawGridLines: true,
                            fontFamily: "HelveticaNeue-Medium",
                            fontWeight: "bold",
                            textSize: 10,
                            textColor: processColor("gray"),
                            valueFormatter: this.props.xaxisValues && (this.props.xaxisValues.length) ? this.props.xaxisValues : [],
                            labelRotationAngle: this.props.type ? -80 : 0,
                        }}
                        yAxis={this.state.yAxis}
                        autoScaleMinMaxEnabled={false}
                        animation={{
                            durationX: 0,
                            durationY: 1500,
                            easingY: "EaseInOutQuart"
                        }}

                        drawGridBackground={false}
                        drawBorders={false}
                        touchEnabled={false}
                        dragEnabled={false}
                        scaleEnabled={false}
                        scaleXEnabled={false}
                        scaleYEnabled={false}
                        pinchZoom={false}
                        doubleTapToZoomEnabled={false}
                        dragDecelerationEnabled={true}
                        dragDecelerationFrictionCoef={0.99}
                        keepPositionOnRotation={false}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 20
    },
    chart1: {
        height: 500,
        width: '100%',
    },
    chart2: {
        height: 400,
        width: '100%',
    }
});

export default LineChartScreen;

