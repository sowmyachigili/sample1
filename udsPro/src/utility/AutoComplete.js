/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */


// Realm
import realmController from '../Realm/realmController';
import schema from '../Realm/schemaNames';

import dateFormats from './formatDate';

export default {

    // check new category list value in category details
    checkAndSaveNewListValuesInCategoryDetails: (loggedInUserFacilityId, Notes, categoryName) => {

        let dropdownOptions = realmController.getAllcategoriesDataFromRealm(loggedInUserFacilityId, categoryName, '', 'string');

        let isRecordExists = dropdownOptions.findIndex(option => option.Notes && option.Notes === Notes);
        if (isRecordExists != -1) {
            var valueOfproperty = dropdownOptions.find(obj => {
                return obj.Notes === Notes
            })
            let value = valueOfproperty.value
            if (value && typeof value == 'number') {
                return value.toString();
            } else {
                return value;
            }
        } else {
            let filterQuery = 'CategoryFacCode ==[c] ' + '"' + loggedInUserFacilityId + '" AND Name = ' + '"' + categoryName + '"';
            let facilityCategoryData = realmController.getAllRecordsWithFiltering(schema.DROPDOWNS_SCHEMA, filterQuery);

            if (facilityCategoryData && facilityCategoryData.length && facilityCategoryData.length == 1) {
                let categoryInfo = { ...facilityCategoryData[0] };

                // Insert record into dropdown table
                let categorydetails = {};
                categorydetails.CategoryIPADID = categoryInfo.CategoryIPADID;
                categorydetails.CategoryDetailIPADId = realmController.getMaxPrimaryKeyId(schema.DROPDOWN_OPTIONS_SCHEMA, 'CategoryDetailIPADId');
                categorydetails.CategoryDetailID = null;
                categorydetails.DBName = categoryInfo.DBName;
                categorydetails.CategoryID = categoryInfo.CategoryID;
                categorydetails.SequenceNo = dropdownOptions.length + 1;
                categorydetails.Item = ''; //this.state.listItemCode;
                categorydetails.Notes = Notes;
                categorydetails.DoNotUse = 0;
                categorydetails.DoNotShow = 0;
                categorydetails.DoNotDelete = 0;
                categorydetails.DoNotEdit = 0;
                categorydetails.InsertedOn = dateFormats.formatDate('todayDate');
                // categorydetails.InsertedBy=this.props.loggedInUserDetails.UserID;

                let newCategoryDetails = {};
                newCategoryDetails.CategoryIPADID = categoryInfo.CategoryIPADID;
                newCategoryDetails.CategoryDetails = [];

                if (categoryInfo.CategoryDetails && categoryInfo.CategoryDetails.length
                    && categoryInfo.CategoryDetails.length > 0) {
                    categoryInfo.CategoryDetails.forEach(category => {
                        newCategoryDetails.CategoryDetails.push(category);
                    });
                }
                newCategoryDetails.CategoryDetails.push(categorydetails)
                realmController.insertOrUpdateRecord(schema.DROPDOWNS_SCHEMA, newCategoryDetails, true);

                if (categorydetails.CategoryDetailIPADId && typeof categorydetails.CategoryDetailIPADId == 'number') {
                    return categorydetails.CategoryDetailIPADId.toString();
                } else {
                    return categorydetails.CategoryDetailIPADId;
                }
            }
        }
    },

}