/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import { Linking, Keyboard, } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import DeviceInfo from 'react-native-device-info';

// config files
import config from '../config/config';
import configMessage from '../config/configMessages';

// Realm
import realmController from '../Realm/realmController';
import schema from '../Realm/schemaNames';
import realm from '../Realm/realm';

// Components
import dateFormats from './formatDate';

// get category detail IPAD ID
getCategoryIPADID = (categoryDetailId) => {
    let filterQuery = ' CategoryDetailID = ' + categoryDetailId;
    let categoryDetails = realmController.getAllRecordsWithFiltering(schema.DROPDOWN_OPTIONS_SCHEMA, filterQuery);
    if (categoryDetails && categoryDetails.length && categoryDetails.length == 1) {
        if (categoryDetails[0].CategoryDetailIPADId) {
            return categoryDetails[0].CategoryDetailIPADId;
        }
    } else {
        return null;
    }
}

// get category detail ID
getCategoryDetailID = (categoryDetailIPADId) => {
    let filterQuery = ' CategoryDetailIPADId = ' + categoryDetailIPADId;
    let categoryDetails = realmController.getAllRecordsWithFiltering(schema.DROPDOWN_OPTIONS_SCHEMA, filterQuery);
    if (categoryDetails && categoryDetails.length && categoryDetails.length == 1) {
        if (categoryDetails[0].CategoryDetailIPADId) {
            return categoryDetails[0].CategoryDetailID;
        }
    } else {
        return null;
    }
};

// Check if category exists if yes return IPADID
CheckRecordExists = (schemaname, recordId, fieldname, fieldnameIPAD) => {
    let filterQuery;
    if (recordId) {
        filterQuery = fieldname + ' = ' + recordId;
    }
    let obj = realmController.getAllRecordsWithFiltering(schemaname, filterQuery);
    return obj && obj.length > 0 ? obj[0][fieldnameIPAD] : 0;
}

// Prepare validation message with invalid icd code
prepareValidationMessagesWithInvalidCode = (codeType, screenName, fieldIndex, codesInfo) => {

    if (codesInfo[fieldIndex]) {
        let message = '';
        let confirmInfo = {};

        confirmInfo.type = 'Clear Changes';
        confirmInfo.fieldName = codesInfo[fieldIndex].fieldName;
        confirmInfo.fieldIndex = fieldIndex;
        confirmInfo.label = codesInfo[fieldIndex].label;
        confirmInfo.value = codesInfo[fieldIndex].value ? codesInfo[fieldIndex].value.trim() : codesInfo[fieldIndex].value;

        if (codeType == 'Comorbid') {
            confirmInfo.icdCodeType = 'Comorbid';
            if (screenName == 'Case') {
                message = `${configMessage.selectValidComorbidICd} (24${confirmInfo.label}).`;
            } else {
                message = `${configMessage.selectValidComorbidICd} 72(${confirmInfo.label}).`;
            }
            codesInfo[fieldIndex].value = null;
        } else if (codeType == 'Etiologic') {
            confirmInfo.icdCodeType = 'Etiologic';
            message = `${configMessage.selectValidEtiologicIcd} (67b ${confirmInfo.label}).`;
            codesInfo[fieldIndex].value = null;
        } else if (codeType == 'Additional') {
            confirmInfo.icdCodeType = 'Additional';
            if (screenName == 'Case') {
                message = `${configMessage.selectValidComorbidICd} (24.${confirmInfo.label}).`;
                codesInfo[fieldIndex].value = null;
            }
        }

        if (message) {
            confirmInfo.validationMsg = message;
            confirmInfo.codesInfo = codesInfo;
            return confirmInfo;
        } else {
            return null;
        }
    }
};

// Prepare validation message with invalid icd code
prepareValidationMessageWithSinlgeInvalidCode = (codeDetails, codeType, screenName) => {
    if (codeDetails && codeDetails.fieldName) {
        let message = '';
        let confirmInfo = {};

        if (codeType == 'Comorbid') {
            if (screenName == 'Case') {
                message = `${configMessage.selectValidComorbidICd} (24${codeDetails.label}).`;
            } else {
                message = `${configMessage.selectValidComorbidICd} 72(${codeDetails.label}).`;
            }
        } else if (codeType == 'Etiologic') {
            message = `${configMessage.selectValidEtiologicIcd} (67b ${codeDetails.label}).`;
        } else if (codeType == 'Additional') {
            if (screenName == 'Case') {
                message = `${configMessage.selectValidComorbidICd} (24.${codeDetails.label}).`;
            }
        }
        if (message) {
            confirmInfo.validationMsg = message;
            confirmInfo.isValid = false;
            confirmInfo.value = null;
            confirmInfo.type = 'Clear Changes';
            confirmInfo.fieldName = codeDetails.fieldName;
            return confirmInfo;
        } else {
            return null;
        }
    }
};

// Check string with special char format
checkSpecialCharFormat = (string, specialChars) => {
    if (string) {
        for (i = 0; i < specialChars.length; i++) {
            if (string.indexOf(specialChars[i]) > -1) {
                return true
            }
        }
        return false;
    }
}

// Get details from configuration to check app version and session exp time
getConfigurationDetails = (configKeyName, type) => {
    let filterCriteria = 'Property = ' + '"' + configKeyName + '"';
    let configKeys = realmController.getAllRecordsWithFiltering(schema.PROIAPP_SETTINGS_SCHEMA, filterCriteria);
    configKeys = [...configKeys];
    if (configKeys && configKeys.length && configKeys.length == 1) {
        if (configKeys[0] && configKeys[0].Value) {
            if (type == 'URL' || type == 'DBUniqueID' || type == 'versions') {
                return configKeys[0].Value;
            } else {
                return parseInt(configKeys[0].Value);
            }
        } else {
            return null;
        }
    } else {
        return null;
    }
};

// get facility expiration details based on logged in user facility
export function getFacilityExpDetails(facilityCode) {
    let filterQuery = 'FacilityCode ==[c] ' + '"' + facilityCode + '"';
    let facilityDetails = realmController.getAllRecordsWithFiltering(schema.Facility_SCHEMA, filterQuery);
    if (facilityDetails && facilityDetails.length && facilityDetails.length == 1) {
        return facilityDetails[0];
    } else {
        return null;
    }
}

// for setting logged in user details in asyncstorage
setLoggedInUserDetailsInAsyncStore = (userDetails, type) => {
    AsyncStorage.setItem('loggedInUserDetails', JSON.stringify(userDetails));
    if (!type) {
        AsyncStorage.setItem('checkOffline', JSON.stringify('offline'));
    } else {
        AsyncStorage.setItem('checkOffline', JSON.stringify('online'));

    }

}

// check patient assessmentDate or Reassessment date is with in 48hrs or not and change pending signature labels in dashboard and grid
checkAssessmentDateOrReassessmentDateWithSignatureLabels = (preadmission, PreSignature) => {
    let signatureLabelsStatus = {}, PreAdmissionSignature = false, PhysicianSignature = false;
    if (preadmission.DoNotAdmitStatus || preadmission.patIPADID) {
        if (preadmission.DoNotAdmitStatus) {
            PreAdmissionSignature = true;
            PhysicianSignature = true;
        } else {
            // screener signature
            if (preadmission.PreAdmissionSignature) {
                PreAdmissionSignature = true; // labels not visible
            } else {
                PreAdmissionSignature = false;
            }
            // physician signature
            if (preadmission.PhysicianSignature) {
                PhysicianSignature = true; // labels not visible
            } else {
                PhysicianSignature = false;
            }
        }
    } else {
        let assOrReassDate = '';
        if (preadmission.ReAssessmentDateTime) {
            assOrReassDate = preadmission.ReAssessmentDateTime;
        } else {
            assOrReassDate = preadmission.AssessmentDateTime;
        }

        // for findng record in signature
        if (PreSignature && PreSignature.length && PreSignature.length > 0) {
            for (let i = 0; i < PreSignature.length; i++) {
                let signature = {};
                signature = PreSignature[i];
                let currentDate = dateFormats.formatDate('todayDate', 'YYYY-MM-DD HH:mm:ss');

                if (signature) {
                    // Screener signature
                    if (signature.SignatureType == "Physician&Screener" || signature.SignatureType == 'Screener') {
                        let hours = dateFormats.differenceBetweenDatesInHours(assOrReassDate, currentDate);
                        if (signature.InsertedOn && dateFormats.datesComparisionWithSameOrAfter(assOrReassDate, signature.InsertedOn)
                            && (hours && hours <= 48)) {
                            PreAdmissionSignature = true; // labels not visible
                        } else {
                            PreAdmissionSignature = false;
                        }
                    }

                    // Physician signature
                    if ((signature.SignatureType == "Physician&Screener" || signature.SignatureType == 'Physician')) {
                        let hours = dateFormats.differenceBetweenDatesInHours(assOrReassDate, currentDate);

                        if (signature.InsertedOn && dateFormats.datesComparisionWithSameOrAfter(assOrReassDate, signature.InsertedOn)
                            && (hours && hours <= 48)) {
                            PhysicianSignature = true; // labels not visible
                        } else {
                            PhysicianSignature = false;
                        }
                    }
                }
            }
        }
    }
    signatureLabelsStatus = {
        PreAdmissionSignature: PreAdmissionSignature,
        PhysicianSignature: PhysicianSignature
    }
    return signatureLabelsStatus;
}

PAIRoundNumber = (aStrNumber, aStrRound) => {
    var pNumber = aStrNumber
    pNumber = Math.round(pNumber * Math.pow(10, aStrRound)) / Math.pow(10, aStrRound)
    return pNumber
}

export default {
    // Social Security Number 123-12-1234

    generateSSN: (val) => {
        var newVal = '';
        if (val.length > 4) {
            this.Value = val;
        }

        if ((val.length > 3) && (val.length < 6)) {
            newVal += val.substr(0, 3) + '-';
            val = val.substr(3);
        }

        if (val.length > 5) {
            newVal += val.substr(0, 3) + '-';
            newVal += val.substr(3, 2) + '-';
            val = val.substr(5);
        }

        newVal += val;
        return newVal
    },
    isValidateSSN: (ssnVal) => {
        var validationRegex = /^[0-9-]+$/; // 0 to 9 and -
        if (!ssnVal.match(validationRegex)) {
            return false;
        } else {
            let value = ssnVal;
            value = ssnVal.replace(/\D/g, '');
            if (value.length == 9) {
                if ((value == '000000000') || (value == '111111111') ||
                    (value == '222222222') || (value == '333333333') ||
                    (value == '444444444') || (value == '555555555') ||
                    (value == '666666666') || (value == '777777777') ||
                    (value == '888888888') || (value == '999999999') ||
                    (value == '123456789')
                ) {
                    return false;
                } else {
                    return true
                }
            } else {
                return false;
            }
        }
    },
    formatPhoneNumber: (phone) => {
        return formatted = phone.substr(0, 3) + '-' + phone.substr(3, 3) + '-' + phone.substr(6, 4)
    },

    // Get details from configuration to check app version and session exp time
    getConfigurationDetails: (configKeyName, type) => {
        return getConfigurationDetails(configKeyName, type);
    },

    // get category detail IPAD ID
    getCategoryIPADID: (categoryDetailId, type) => {
        let filterQuery;
        if (type == true) {
            filterQuery = ' CategoryDetailID == ' + categoryDetailId;
        } else {
            filterQuery = ' CategoryDetailIPADId == ' + categoryDetailId;
        }
        let categoryDetails = realmController.getAllRecordsWithFiltering(schema.DROPDOWN_OPTIONS_SCHEMA, filterQuery);
        if (categoryDetails && categoryDetails.length && categoryDetails.length == 1) {
            if (categoryDetails[0].CategoryDetailIPADId) {
                return categoryDetails[0].CategoryDetailIPADId;
            }
        } else {
            return null;
        }

    },

    // Save session expiration time in asyncstorage: ex: present time + min
    activateSession: async () => {
        let expTime = await AsyncStorage.getItem('sessionExpTimeInMin');
        let expiryTime = await dateFormats.addMinutesToDate(expTime);
        try {
            await AsyncStorage.setItem('sessionExpTime', JSON.stringify(expiryTime));
            return expiryTime;
        } catch (error) {
            return false;
        }
    },

    // get IRFCompliance from category filter by category detail id
    getIRFComplianceFromCategory: (facilityCode, complianceValue, screenName) => {
        let compliance;
        if (screenName == 'caseListing') {
            compliance = realmController.getcategoryNotesByCategoryNameRealm(facilityCode, schema.IRF_CASELISTING_SCHEMA, '', 'Item', complianceValue);
        } else {
            compliance = realmController.getcategoryNotesByCategoryNameRealm(facilityCode, schema.IRF_PREADMISSION_SCHEMA, '', 'Item', complianceValue);
        }
        return compliance;
    },

    // get dropdown item filter by igcode from impairmentgroup
    getImpairMentGroupItemByValue: (igCode) => {
        let filterQuery = "IGCode == " + '"' + igCode + '"';
        let impairMentDescription = realmController.getAllRecordsWithFiltering(schema.IMPAIRMENTGROUP_SCHEMA, filterQuery);
        if (impairMentDescription && impairMentDescription.length && impairMentDescription.length > 0) {
            if (impairMentDescription[0].IGDescription) {
                return igCode + ' - ' + impairMentDescription[0].IGDescription;
            }
        }
    },
    // get dropdown item value filter by siteId from site
    getSiteItemByValue: (siteId) => {
        let filterQuery = '(DeletedBy == null) AND SiteID = ' + '"' + siteId + '"';
        let siteData = realm.objects(schema.SITE_SCHEMA).filtered(filterQuery);
        if (siteData && siteData.length && siteData.length > 0) {
            if (siteData[0].SiteDescription) {
                return siteData[0].SiteCode + ' - ' + siteData[0].SiteDescription;
            }
        }

    },
    // get dropdown item  value filter by  siteId,unitcode from unit
    getUnitItemByValue: (facilityDetails, siteId, UnitCode) => {
        if (facilityDetails && facilityDetails.HasSite && facilityDetails.HasUnit) {
            if (siteId && UnitCode) {
                let filterQuery = '(DeletedBy == null) AND SiteID = ' + '"' + siteId + '" AND UnitCode = ' + '"' + UnitCode + '"';
                let unitData = realm.objects(schema.UNIT_SCHEMA).filtered(filterQuery);
                if (unitData && unitData.length && unitData.length > 0) {
                    if (unitData[0].UnitDescription) {
                        return unitData[0].UnitCode + ' - ' + unitData[0].UnitDescription;
                    }
                }
            } else {
                return UnitCode;
            }

        }

    },

    // get category value filter by category id and item
    getCategoryValueByCategoryID: (categoryId, item) => {
        let filterQuery = 'Item == ' + '"' + item + '" AND CategoryID==' + categoryId;
        let categoryDetails = realmController.getAllRecordsWithFiltering(schema.DROPDOWN_OPTIONS_SCHEMA, filterQuery);
        if (categoryDetails && categoryDetails.length && categoryDetails.length > 0) {
            if (categoryDetails[0].Notes) {
                return categoryDetails[0].Notes;
            }
        }
    },

    // get selected reason based do not admit status
    getReasonBasedOnDonotAdmitStatus: (DoNotAdmitStatus) => {
        let categoryOptionsDetails = realmController.getSelectedCategoryDetailsBasedOnIPADId(DoNotAdmitStatus);
        if (categoryOptionsDetails) {
            return categoryOptionsDetails.Item + ' - ' + categoryOptionsDetails.Notes;
        } else {
            return '';
        }
    },

    // get user name filter by userId
    getUserNameFilterByUserID: (userId) => {
        let filterCriteria = 'UserID == ' + userId;
        let users = realmController.getAllRecordsWithFiltering(schema.EMAIL_ADDRESSES, filterCriteria);
        if (users && users.length && users.length === 1) {
            if (users[0] && users[0].RealName) {
                return users[0].RealName;
            }
        }

    },


    // Preadmission Format data to save in local realm
    formatPreadmissionData: (preadmissionServerObj, action) => {
        // insert or update
        let preAdmissionIPADID = CheckRecordExists(schema.PREADMISSION_SCHEMA, preadmissionServerObj.PreAdmissionID, 'PreAdmissionID', 'PreAdmissionIPADID');
        preadmissionServerObj.PreAdmissionIPADID = preAdmissionIPADID == 0 ? realmController.getMaxPrimaryKeyId(schema.PREADMISSION_SCHEMA, 'PreAdmissionIPADID') : preAdmissionIPADID;
        if (action) {
            let patIPADid;
            if (preadmissionServerObj.PatID) {
                patIPADid = CheckRecordExists(schema.CASELISTING_SCHEMA, preadmissionServerObj.PatID, 'PatID', 'patIPADID');
            }
            preadmissionServerObj.patIPADID = patIPADid ? patIPADid : null;
        }
        if (preadmissionServerObj.DoNotAdmitStatus != null && preadmissionServerObj.DoNotAdmitStatus > 0) {
            preadmissionServerObj.DoNotAdmitStatus = getCategoryIPADID(preadmissionServerObj.DoNotAdmitStatus);
        }
        if (preadmissionServerObj.DischargeDestination != null && preadmissionServerObj.DischargeDestination > 0) {
            preadmissionServerObj.DischargeDestination = getCategoryIPADID(preadmissionServerObj.DischargeDestination);
        }

        // Insert icd version based on IRFPAI_Version
        if (preadmissionServerObj.IRFPAI_Version) {
            let IRFVersionDetails = realmController.getICDVersionBasedOnIRFPAIVersion(preadmissionServerObj.IRFPAI_Version);
            if (IRFVersionDetails && IRFVersionDetails.ICDVersion) {
                preadmissionServerObj.ICDVersion = IRFVersionDetails.ICDVersion;
            }
        }

        preadmissionServerObj.AssessmentDateTime = dateFormats.formatDate(preadmissionServerObj.AssessmentDate, config.dbOnlyDateFormat);


        if (preadmissionServerObj.PatientInfo.length > 0) {
            let pre_PatientInfoIPADID = 0;
            if (action == 'update') {
                pre_PatientInfoIPADID = CheckRecordExists(schema.PATIENTINFO_SCHEMA, preadmissionServerObj.PatientInfo[0].PRE_PatientInfoID, 'PRE_PatientInfoID', 'PRE_PatientInfoIPADID');
            }
            preadmissionServerObj.PatientInfo[0].PRE_PatientInfoIPADID = pre_PatientInfoIPADID == 0 ? realmController.getMaxPrimaryKeyId(schema.PATIENTINFO_SCHEMA, 'PRE_PatientInfoIPADID') : pre_PatientInfoIPADID;
            if (preadmissionServerObj.PatientInfo[0].PrimaryInsuranceType && (preadmissionServerObj.PatientInfo[0].PrimaryInsuranceType == '02' ||
                preadmissionServerObj.PatientInfo[0].PrimaryInsuranceType == '51' || preadmissionServerObj.PatientInfo[0].PrimaryInsuranceType == '99')) {
                preadmissionServerObj.PatientInfo[0].PrimaryInsuranceTypeDetail = '';
            }
            if (preadmissionServerObj.PatientInfo[0].PrimaryInsuranceType && preadmissionServerObj.PatientInfo[0].PrimaryInsuranceType != '02' &&
                preadmissionServerObj.PatientInfo[0].PrimaryInsuranceType != '51' && preadmissionServerObj.PatientInfo[0].PrimaryInsuranceType != '99') {
                //assign detail value
                preadmissionServerObj.PatientInfo[0].PrimaryInsuranceTypeDetail = preadmissionServerObj.PatientInfo[0].PrimaryInsuranceType;
                preadmissionServerObj.PatientInfo[0].PrimaryInsuranceType = '99';
            }
            if (preadmissionServerObj.PatientInfo[0].SecondaryInsuranceType && (preadmissionServerObj.PatientInfo[0].SecondaryInsuranceType == '02' ||
                preadmissionServerObj.PatientInfo[0].SecondaryInsuranceType == '51' || preadmissionServerObj.PatientInfo[0].SecondaryInsuranceType == '99')) {
                preadmissionServerObj.PatientInfo[0].SecondaryInsuranceTypeDetail = '';
            }
            if (preadmissionServerObj.PatientInfo[0].SecondaryInsuranceType && preadmissionServerObj.PatientInfo[0].SecondaryInsuranceType != '02' &&
                preadmissionServerObj.PatientInfo[0].SecondaryInsuranceType != '51' && preadmissionServerObj.PatientInfo[0].SecondaryInsuranceType != '99') {
                //assign detail value
                preadmissionServerObj.PatientInfo[0].SecondaryInsuranceTypeDetail = preadmissionServerObj.PatientInfo[0].SecondaryInsuranceType;
                preadmissionServerObj.PatientInfo[0].SecondaryInsuranceType = '99';
            }
            if (preadmissionServerObj.PatientInfo[0].PrimaryCarePhysician) {
                let primarycarePhysician = getCategoryIPADID(preadmissionServerObj.PatientInfo[0].PrimaryCarePhysician);
                if (primarycarePhysician) {
                    preadmissionServerObj.PatientInfo[0].PrimaryCarePhysician = primarycarePhysician.toString();
                }
            }
            if (preadmissionServerObj.PatientInfo[0].AttendingPhysician) {
                let attendingPhysician = getCategoryIPADID(preadmissionServerObj.PatientInfo[0].AttendingPhysician);
                if (attendingPhysician) {
                    preadmissionServerObj.PatientInfo[0].AttendingPhysician = attendingPhysician.toString();
                }
            }
            if (preadmissionServerObj.PatientInfo[0].ConsultingPhysician) {
                let consultingPhysician = getCategoryIPADID(preadmissionServerObj.PatientInfo[0].ConsultingPhysician);
                if (consultingPhysician) {
                    preadmissionServerObj.PatientInfo[0].ConsultingPhysician = consultingPhysician.toString();
                }
            }
            if (preadmissionServerObj.PatientInfo[0].ReferringPhysician) {
                let referringPhysician = getCategoryIPADID(preadmissionServerObj.PatientInfo[0].ReferringPhysician);
                if (referringPhysician) {
                    preadmissionServerObj.PatientInfo[0].ReferringPhysician = referringPhysician.toString();
                }
            }
            if (preadmissionServerObj.PatientInfo[0].ReferringFacility) {
                let referringFacility = getCategoryIPADID(preadmissionServerObj.PatientInfo[0].ReferringFacility);
                if (referringFacility) {
                    preadmissionServerObj.PatientInfo[0].ReferringFacility = referringFacility.toString();
                }
            }

            // For inserting assessment date time in to new column
            if (preadmissionServerObj.PatientInfo[0].AssessmentTime) {
                let assesmentTime = preadmissionServerObj.PatientInfo[0].AssessmentTime;
                let time = assesmentTime.split(':');
                if (time && time.length && time.length == 2) {
                    preadmissionServerObj.AssessmentDateTime = dateFormats.addTimeToDate(preadmissionServerObj.AssessmentDate, parseInt(time[0]), parseInt(time[1]));
                }
            }
        }

        if (preadmissionServerObj.PREPriorLiving.length > 0) {
            let PREPriorLivingIPADID = 0
            if (action == 'update') {
                PREPriorLivingIPADID = CheckRecordExists(schema.PRIOR_LIVING_SCHEMA, preadmissionServerObj.PREPriorLiving[0].PRE_PriorLivingID, 'PRE_PriorLivingID', 'PRE_PriorLivingIPADID');
            }
            preadmissionServerObj.PREPriorLiving[0].PRE_PriorLivingIPADID = PREPriorLivingIPADID == 0 ? realmController.getMaxPrimaryKeyId(schema.PRIOR_LIVING_SCHEMA, 'PRE_PriorLivingIPADID') : PREPriorLivingIPADID;
        }

        if (preadmissionServerObj.PREDiagnosis.length > 0) {
            let pre_DiagnosisIPADID = 0;
            if (action == 'update') {
                pre_DiagnosisIPADID = CheckRecordExists(schema.DIAGNOSIS_SCHEMA, preadmissionServerObj.PREDiagnosis[0].PRE_Diagnosis_ID, 'PRE_Diagnosis_ID', 'PRE_DiagnosisIPADID');
            }
            preadmissionServerObj.PREDiagnosis[0].PRE_DiagnosisIPADID = pre_DiagnosisIPADID == 0 ? realmController.getMaxPrimaryKeyId(schema.DIAGNOSIS_SCHEMA, 'PRE_DiagnosisIPADID') : pre_DiagnosisIPADID;
        }

        if (preadmissionServerObj.CurrentMedical.length > 0) {
            let pre_CurrentMedicalIPADID = 0;
            if (action == 'update') {
                pre_CurrentMedicalIPADID = CheckRecordExists(schema.MEDICALINFO_SCHEMA, preadmissionServerObj.CurrentMedical[0].PRE_CurrentMedicalID, 'PRE_CurrentMedicalID', 'PRE_CurrentMedicalIPADID');
            }
            preadmissionServerObj.CurrentMedical[0].PRE_CurrentMedicalIPADID = pre_CurrentMedicalIPADID == 0 ? realmController.getMaxPrimaryKeyId(schema.MEDICALINFO_SCHEMA, 'PRE_CurrentMedicalIPADID') : pre_CurrentMedicalIPADID;
            if (preadmissionServerObj.CurrentMedical[0].Lab1_Type) {
                let lab1Type = getCategoryIPADID(preadmissionServerObj.CurrentMedical[0].Lab1_Type);
                if (lab1Type) {
                    preadmissionServerObj.CurrentMedical[0].Lab1_Type = lab1Type.toString();
                }
            }
            if (preadmissionServerObj.CurrentMedical[0].Lab2_Type) {
                let lab2Type = getCategoryIPADID(preadmissionServerObj.CurrentMedical[0].Lab2_Type);
                if (lab2Type) {
                    preadmissionServerObj.CurrentMedical[0].Lab2_Type = lab2Type.toString();
                }
            }
            if (preadmissionServerObj.CurrentMedical[0].Lab3_Type) {
                let lab3Type = getCategoryIPADID(preadmissionServerObj.CurrentMedical[0].Lab3_Type);
                if (lab3Type) {
                    preadmissionServerObj.CurrentMedical[0].Lab3_Type = lab3Type.toString();
                }
            }
            if (preadmissionServerObj.CurrentMedical[0].Lab4_Type) {
                let lab4Type = getCategoryIPADID(preadmissionServerObj.CurrentMedical[0].Lab4_Type);
                if (lab4Type) {
                    preadmissionServerObj.CurrentMedical[0].Lab4_Type = lab4Type.toString();
                }
            }
            if (preadmissionServerObj.CurrentMedical[0].Lab5_Type) {
                let lab5Type = getCategoryIPADID(preadmissionServerObj.CurrentMedical[0].Lab5_Type);
                if (lab5Type) {
                    preadmissionServerObj.CurrentMedical[0].Lab5_Type = lab5Type.toString();
                }
            }
            if (preadmissionServerObj.CurrentMedical[0].Lab6_Type) {
                let lab6Type = getCategoryIPADID(preadmissionServerObj.CurrentMedical[0].Lab6_Type);
                if (lab6Type) {
                    preadmissionServerObj.CurrentMedical[0].Lab6_Type = lab6Type.toString();
                }
            }
            if (preadmissionServerObj.CurrentMedical[0].Lab7_Type) {
                let lab7Type = getCategoryIPADID(preadmissionServerObj.CurrentMedical[0].Lab7_Type);
                if (lab7Type) {
                    preadmissionServerObj.CurrentMedical[0].Lab7_Type = lab7Type.toString();
                }
            }
            if (preadmissionServerObj.CurrentMedical[0].Lab8_Type) {
                let lab8Type = getCategoryIPADID(preadmissionServerObj.CurrentMedical[0].Lab8_Type);
                if (lab8Type) {
                    preadmissionServerObj.CurrentMedical[0].Lab8_Type = lab8Type.toString();
                }
            }
            if (preadmissionServerObj.CurrentMedical[0].Lab9_Type) {
                let lab9Type = getCategoryIPADID(preadmissionServerObj.CurrentMedical[0].Lab9_Type);
                if (lab9Type) {
                    preadmissionServerObj.CurrentMedical[0].Lab9_Type = lab9Type.toString();
                }
            }
            if (preadmissionServerObj.CurrentMedical[0].Lab10_Type) {
                let lab10Type = getCategoryIPADID(preadmissionServerObj.CurrentMedical[0].Lab10_Type);
                if (lab10Type) {
                    preadmissionServerObj.CurrentMedical[0].Lab10_Type = lab10Type.toString();
                }
            }
            if (preadmissionServerObj.CurrentMedical[0].Study1_Type) {
                let study1Type = getCategoryIPADID(preadmissionServerObj.CurrentMedical[0].Study1_Type);
                if (study1Type) {
                    preadmissionServerObj.CurrentMedical[0].Study1_Type = study1Type.toString();
                }
            }
            if (preadmissionServerObj.CurrentMedical[0].Study2_Type) {
                let study2Type = getCategoryIPADID(preadmissionServerObj.CurrentMedical[0].Study2_Type);
                if (study2Type) {
                    preadmissionServerObj.CurrentMedical[0].Study2_Type = study2Type.toString();
                }
            }
            if (preadmissionServerObj.CurrentMedical[0].Study3_Type) {
                let study3Type = getCategoryIPADID(preadmissionServerObj.CurrentMedical[0].Study3_Type);
                if (study3Type) {
                    preadmissionServerObj.CurrentMedical[0].Study3_Type = study3Type.toString();
                }
            }
            if (preadmissionServerObj.CurrentMedical[0].Study4_Type) {
                let study4Type = getCategoryIPADID(preadmissionServerObj.CurrentMedical[0].Study4_Type);
                if (study4Type) {
                    preadmissionServerObj.CurrentMedical[0].Study4_Type = study4Type.toString();
                }
            }
            if (preadmissionServerObj.CurrentMedical[0].Study5_Type) {
                let study5Type = getCategoryIPADID(preadmissionServerObj.CurrentMedical[0].Study5_Type);
                if (study5Type) {
                    preadmissionServerObj.CurrentMedical[0].Study5_Type = study5Type.toString();
                }
            }
        }
        if (preadmissionServerObj.PREFIMCurrentStatus.length > 0) {
            let pre_FIMCurrent_StatusIPADID = 0;
            if (action == 'update') {
                pre_FIMCurrent_StatusIPADID = CheckRecordExists(schema.PREFIMCURRENTFIM_SCHEMA, preadmissionServerObj.PREFIMCurrentStatus[0].PRE_FIMCurrent_StatusID, 'PRE_FIMCurrent_StatusID', 'PRE_FIMCurrent_StatusIPADID');
            }
            preadmissionServerObj.PREFIMCurrentStatus[0].PRE_FIMCurrent_StatusIPADID = pre_FIMCurrent_StatusIPADID == 0 ? realmController.getMaxPrimaryKeyId(schema.PREFIMCURRENTFIM_SCHEMA, 'PRE_FIMCurrent_StatusIPADID') : pre_FIMCurrent_StatusIPADID;
        }

        if (preadmissionServerObj.FIM.length > 0) {
            let fimIPADID = 0;
            if (action == 'update') {
                fimIPADID = CheckRecordExists(schema.FIM_SCHEMA, preadmissionServerObj.FIM[0].FIMID, 'FIMID', 'FIMIPADID');
            }
            preadmissionServerObj.FIM[0].FIMIPADID = fimIPADID == 0 ? realmController.getMaxPrimaryKeyId(schema.FIM_SCHEMA, 'FIMIPADID') : fimIPADID;
        }

        if (preadmissionServerObj.Goal.length > 0) {
            let pre_GoalIPADID = 0;
            if (action == 'update') {
                pre_GoalIPADID = CheckRecordExists(schema.GOALS_PLANS_SCHEMA, preadmissionServerObj.Goal[0].PRE_GoalID, 'PRE_GoalID', 'PRE_GoalIPADID');
            }
            preadmissionServerObj.Goal[0].PRE_GoalIPADID = pre_GoalIPADID == 0 ? realmController.getMaxPrimaryKeyId(schema.GOALS_PLANS_SCHEMA, 'PRE_GoalIPADID') : pre_GoalIPADID;
        }

        if (preadmissionServerObj.ReAssessment.length > 0) {
            let pre_ReAssessmentIPADID = 0;
            if (action == 'update') {
                pre_ReAssessmentIPADID = CheckRecordExists(schema.REASSESSMENT_SCHEMA, preadmissionServerObj.ReAssessment[0].PRE_ReAssessmentID, 'PRE_ReAssessmentID', 'PRE_ReAssessmentIPADID');
            }
            preadmissionServerObj.ReAssessment[0].PRE_ReAssessmentIPADID = pre_ReAssessmentIPADID == 0 ? realmController.getMaxPrimaryKeyId(schema.REASSESSMENT_SCHEMA, 'PRE_ReAssessmentIPADID') : pre_ReAssessmentIPADID;

            // For inserting reassessment date time in to new column in main preadmisison
            if (preadmissionServerObj.ReAssessment[0].ReAssessmentDate) {
                preadmissionServerObj.ReAssessmentDateTime = dateFormats.formatDate(preadmissionServerObj.ReAssessment[0].ReAssessmentDate, config.dbOnlyDateFormat);
                // For inserting assessment date time in to new column
                if (preadmissionServerObj.ReAssessment[0].ReAssessmentTime) {
                    let reassessDateTime = preadmissionServerObj.ReAssessment[0].ReAssessmentTime;
                    let time = reassessDateTime.split(':');
                    if (time && time.length && time.length == 2) {
                        preadmissionServerObj.ReAssessmentDateTime = dateFormats.addTimeToDate(preadmissionServerObj.ReAssessmentDateTime, parseInt(time[0]), parseInt(time[1]));
                    }
                }
            }
        }

        if (preadmissionServerObj.CustomData.length > 0) {
            let customDataIPADID = 0;
            if (action == 'update') {
                customDataIPADID = CheckRecordExists(schema.CUSTOM_SCHEMA, preadmissionServerObj.CustomData[0].CustomDataID, 'CustomDataID', 'CustomDataIPADID');
            }
            preadmissionServerObj.CustomData[0].CustomDataIPADID = customDataIPADID == 0 ? realmController.getMaxPrimaryKeyId(schema.CUSTOM_SCHEMA, 'CustomDataIPADID') : customDataIPADID;
        }

        if (preadmissionServerObj.Note && preadmissionServerObj.Note.length && preadmissionServerObj.Note.length > 0) {
            preadmissionServerObj.Note.forEach((note, index) => {
                let noteIPADID = 0;
                if (action == 'update') {
                    noteIPADID = CheckRecordExists(schema.NOTES_SCHEMA, note.NoteID, 'NoteID', 'NoteIPADID');
                }
                note.NoteIPADID = noteIPADID == 0 ? index + realmController.getMaxPrimaryKeyId(schema.NOTES_SCHEMA, 'NoteIPADID') : noteIPADID;
                note.EntityIPADID = preadmissionServerObj.PreAdmissionIPADID;
            });
        }
        if (preadmissionServerObj.PRESignature && preadmissionServerObj.PRESignature.length && preadmissionServerObj.PRESignature.length > 0) {
            preadmissionServerObj.PRESignature.forEach((signature, index) => {
                let pre_SignatureIPADID = 0;
                if (action == 'update') {
                    pre_SignatureIPADID = CheckRecordExists(schema.SIGNATURE_SCHEMA, signature.PRE_SignatureID, 'PRE_SignatureID', 'PRE_SignatureIPADID')
                }
                signature.PRE_SignatureIPADID = pre_SignatureIPADID == 0 ? index + realmController.getMaxPrimaryKeyId(schema.SIGNATURE_SCHEMA, 'PRE_SignatureIPADID') : pre_SignatureIPADID;
                signature.PreAdmissionIPADID = preadmissionServerObj.PreAdmissionIPADID;
            });
        }

        if (preadmissionServerObj.CurrentFunctionalStatus && preadmissionServerObj.CurrentFunctionalStatus.length && preadmissionServerObj.CurrentFunctionalStatus.length > 0) {
            let functionalStatusIPADID = 0;
            if (action == 'update') {
                functionalStatusIPADID = CheckRecordExists(schema.CurrentFunctionalStatus_SCHEMA, preadmissionServerObj.CurrentFunctionalStatus[0].FunctionalStatusID, 'FunctionalStatusID', 'FunctionalStatusIPADID');
            }
            preadmissionServerObj.CurrentFunctionalStatus[0].FunctionalStatusIPADID = functionalStatusIPADID == 0 ? realmController.getMaxPrimaryKeyId(schema.CurrentFunctionalStatus_SCHEMA, 'FunctionalStatusIPADID') : functionalStatusIPADID;
        }
        return preadmissionServerObj;

    },

    formatCaseListingData: (caseListingServerObj, action) => {
        let patIPADID = CheckRecordExists(schema.CASELISTING_SCHEMA, caseListingServerObj.PatID, 'PatID', 'patIPADID');
        caseListingServerObj.patIPADID = patIPADID == 0 ? realmController.getMaxPrimaryKeyId(schema.CASELISTING_SCHEMA, 'patIPADID') : patIPADID;

        // Insert icd version based on IRFPAI_Version
        if (caseListingServerObj.IRFPAI_Version) {
            let IRFVersionDetails = realmController.getICDVersionBasedOnIRFPAIVersion(caseListingServerObj.IRFPAI_Version);
            if (IRFVersionDetails && IRFVersionDetails.ICDVersion) {
                caseListingServerObj.ICDVersion = IRFVersionDetails.ICDVersion;
            }
        }

        if (caseListingServerObj.Admission && caseListingServerObj.Admission.length && caseListingServerObj.Admission.length > 0) {
            let PatientAdmssionIPADId = 0
            if (action) {
                PatientAdmssionIPADId = CheckRecordExists(schema.ADMISSION_SCHEMA, caseListingServerObj.Admission[0].AdmissionID, 'AdmissionID', 'PatientAdmssionIPADId');
            }
            caseListingServerObj.Admission[0].patIPADID = caseListingServerObj.patIPADID;
            caseListingServerObj.Admission[0].PatientAdmssionIPADId = PatientAdmssionIPADId == 0 ? realmController.getMaxPrimaryKeyId(schema.ADMISSION_SCHEMA, 'PatientAdmssionIPADId') : PatientAdmssionIPADId;
        }

        if (caseListingServerObj.FIM && caseListingServerObj.FIM.length && caseListingServerObj.FIM.length > 0) {
            caseListingServerObj.FIM.forEach((fim, index) => {
                let fimIPADID = 0;
                if (action) {
                    fimIPADID = CheckRecordExists(schema.FIM_SCHEMA, fim.FIMID, 'FIMID', 'FIMIPADID');
                }
                fim.FIMIPADID = fimIPADID == 0 ? index + realmController.getMaxPrimaryKeyId(schema.FIM_SCHEMA, 'FIMIPADID') : fimIPADID;
                fim.EntityIPADID = caseListingServerObj.patIPADID;
            });
        }
        if (caseListingServerObj.ComorbidAdvance && caseListingServerObj.ComorbidAdvance.length && caseListingServerObj.ComorbidAdvance.length > 0) {
            let comorbidAdvanceIPADId = 0
            if (action) {
                comorbidAdvanceIPADId = CheckRecordExists(schema.COMORBID_ADVANCE_SCHEMA, caseListingServerObj.ComorbidAdvance[0].PatID, 'PatID', 'ComorbidAdvanceIPADId');
            }
            caseListingServerObj.ComorbidAdvance[0].PatIPADID = caseListingServerObj.patIPADID;
            caseListingServerObj.ComorbidAdvance[0].ComorbidAdvanceIPADId = comorbidAdvanceIPADId == 0 ? realmController.getMaxPrimaryKeyId(schema.COMORBID_ADVANCE_SCHEMA, 'ComorbidAdvanceIPADId') : comorbidAdvanceIPADId;
        }
        return caseListingServerObj;


    },

    //Preadmission format data to send to server
    formatPreadmissionDataServer: (singlePreadmission) => {
        let preadmissionOfflineObj = { ...singlePreadmission };
        if (preadmissionOfflineObj.DoNotAdmitStatus != null && preadmissionOfflineObj.DoNotAdmitStatus > 0) {
            preadmissionOfflineObj.DoNotAdmitStatus = getCategoryDetailID(preadmissionOfflineObj.DoNotAdmitStatus);
        }
        if (preadmissionOfflineObj.DischargeDestination != null && preadmissionOfflineObj.DischargeDestination > 0) {
            preadmissionOfflineObj.DischargeDestination = getCategoryDetailID(preadmissionOfflineObj.DischargeDestination);
        }
        if (preadmissionOfflineObj.PatientInfo.length && preadmissionOfflineObj.PatientInfo.length > 0) {
            patientInfoTabDetails = { ...preadmissionOfflineObj.PatientInfo[0] };
            if (patientInfoTabDetails.PrimaryInsuranceTypeDetail) {
                //reassign primary insurance value value
                patientInfoTabDetails.PrimaryInsuranceType = patientInfoTabDetails.PrimaryInsuranceTypeDetail;
            }
            if (patientInfoTabDetails.SecondaryInsuranceTypeDetail) {
                //reassign primary insurance value value
                patientInfoTabDetails.SecondaryInsuranceType = patientInfoTabDetails.SecondaryInsuranceTypeDetail;
            }
            if (patientInfoTabDetails.PrimaryCarePhysician) {
                let primarycarePhysician = getCategoryDetailID(patientInfoTabDetails.PrimaryCarePhysician);
                if (primarycarePhysician) {
                    patientInfoTabDetails.PrimaryCarePhysician = primarycarePhysician.toString();
                }
            }
            if (patientInfoTabDetails.AttendingPhysician) {
                let attendingPhysician = getCategoryDetailID(patientInfoTabDetails.AttendingPhysician);
                if (attendingPhysician) {
                    patientInfoTabDetails.AttendingPhysician = attendingPhysician.toString();
                }
            }
            if (patientInfoTabDetails.ConsultingPhysician) {
                let consultingPhysician = getCategoryDetailID(patientInfoTabDetails.ConsultingPhysician);
                if (consultingPhysician) {
                    patientInfoTabDetails.ConsultingPhysician = consultingPhysician.toString();
                }
            }
            if (patientInfoTabDetails.ReferringPhysician) {
                let referringPhysician = getCategoryDetailID(patientInfoTabDetails.ReferringPhysician);
                if (referringPhysician) {
                    patientInfoTabDetails.ReferringPhysician = referringPhysician.toString();
                }
            }
            if (patientInfoTabDetails.ReferringFacility) {
                let referringFacility = getCategoryDetailID(patientInfoTabDetails.ReferringFacility);
                if (referringFacility) {
                    patientInfoTabDetails.ReferringFacility = referringFacility.toString();
                }
            }
            preadmissionOfflineObj.PatientInfo = [{ ...patientInfoTabDetails }];
        }
        if (preadmissionOfflineObj.CurrentMedical.length && preadmissionOfflineObj.CurrentMedical.length > 0) {
            let currentMedicalTabDetails = { ...preadmissionOfflineObj.CurrentMedical[0] };
            if (currentMedicalTabDetails.Lab1_Type) {
                let lab1Type = getCategoryDetailID(currentMedicalTabDetails.Lab1_Type);
                if (lab1Type) {
                    currentMedicalTabDetails.Lab1_Type = lab1Type.toString();
                }
            }
            if (currentMedicalTabDetails.Lab2_Type) {
                let lab2Type = getCategoryDetailID(currentMedicalTabDetails.Lab2_Type);
                if (lab2Type) {
                    currentMedicalTabDetails.Lab2_Type = lab2Type.toString();
                }
            }
            if (currentMedicalTabDetails.Lab3_Type) {
                let lab3Type = getCategoryDetailID(currentMedicalTabDetails.Lab3_Type);
                if (lab3Type) {
                    currentMedicalTabDetails.Lab3_Type = lab3Type.toString();
                }
            }
            if (currentMedicalTabDetails.Lab4_Type) {
                let lab4Type = getCategoryDetailID(currentMedicalTabDetails.Lab4_Type);
                if (lab4Type) {
                    currentMedicalTabDetails.Lab4_Type = lab4Type.toString();
                }
            }
            if (currentMedicalTabDetails.Lab5_Type) {
                let lab5Type = getCategoryDetailID(currentMedicalTabDetails.Lab5_Type);
                if (lab5Type) {
                    currentMedicalTabDetails.Lab5_Type = lab5Type.toString();
                }
            }
            if (currentMedicalTabDetails.Lab6_Type) {
                let lab6Type = getCategoryDetailID(currentMedicalTabDetails.Lab6_Type);
                if (lab6Type) {
                    currentMedicalTabDetails.Lab6_Type = lab6Type.toString();
                }
            }
            if (currentMedicalTabDetails.Lab7_Type) {
                let lab7Type = getCategoryDetailID(currentMedicalTabDetails.Lab7_Type);
                if (lab7Type) {
                    currentMedicalTabDetails.Lab7_Type = lab7Type.toString();
                }
            }
            if (currentMedicalTabDetails.Lab8_Type) {
                let lab8Type = getCategoryDetailID(currentMedicalTabDetails.Lab8_Type);
                if (lab8Type) {
                    currentMedicalTabDetails.Lab8_Type = lab8Type.toString();
                }
            }
            if (currentMedicalTabDetails.Lab9_Type) {
                let lab9Type = getCategoryDetailID(currentMedicalTabDetails.Lab9_Type);
                if (lab9Type) {
                    currentMedicalTabDetails.Lab9_Type = lab9Type.toString();
                }
            }
            if (currentMedicalTabDetails.Lab10_Type) {
                let lab10Type = getCategoryDetailID(currentMedicalTabDetails.Lab10_Type);
                if (lab10Type) {
                    currentMedicalTabDetails.Lab10_Type = lab10Type.toString();
                }
            }
            if (currentMedicalTabDetails.Study1_Type) {
                let study1Type = getCategoryDetailID(currentMedicalTabDetails.Study1_Type);
                if (study1Type) {
                    currentMedicalTabDetails.Study1_Type = study1Type.toString();
                }
            }
            if (currentMedicalTabDetails.Study2_Type) {
                let study2Type = getCategoryDetailID(currentMedicalTabDetails.Study2_Type);
                if (study2Type) {
                    currentMedicalTabDetails.Study2_Type = study2Type.toString();
                }
            }
            if (currentMedicalTabDetails.Study3_Type) {
                let study3Type = getCategoryDetailID(currentMedicalTabDetails.Study3_Type);
                if (study3Type) {
                    currentMedicalTabDetails.Study3_Type = study3Type.toString();
                }
            }
            if (currentMedicalTabDetails.Study4_Type) {
                let study4Type = getCategoryDetailID(currentMedicalTabDetails.Study4_Type);
                if (study4Type) {
                    currentMedicalTabDetails.Study4_Type = study4Type.toString();
                }
            }
            if (currentMedicalTabDetails.Study5_Type) {
                let study5Type = getCategoryDetailID(currentMedicalTabDetails.Study5_Type);
                if (study5Type) {
                    currentMedicalTabDetails.Study5_Type = study5Type.toString();
                }
            }
            preadmissionOfflineObj.CurrentMedical = [{ ...currentMedicalTabDetails }];
        }

        return preadmissionOfflineObj;
    },

    // check the IRF version is greater or equal default config version
    checkIRFVersion(version, fieldVersion, type) {
        if (version && fieldVersion) {
            // allow only numbers
            version = version.replace(/[^\d.]/g, '');
            fieldVersion = fieldVersion.replace(/[^\d.]/g, '');
            let Ver = parseFloat(version.toString());
            let FV = parseFloat(fieldVersion.toString());
            if (type == 'below') {
                if (Ver <= FV) {
                    return true;
                } else {
                    return false;
                }
            } else if (type == 'belowAndNotEqual') {
                if (Ver < FV) {
                    return true;
                } else {
                    return false;
                }
            } else {
                if (Ver >= FV) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    },

    //assign admission values to comorbid conditions
    assignAdmissionValuesToComorbidConditions: (comorbidCoditionLabels, admission) => {
        let comorbidValues = [...comorbidCoditionLabels];
        for (let i = 0; i < comorbidValues.length; i++) {
            let ComorbidCondition = admission['ComorbidConditions' + (i + 1)];
            comorbidValues[i].value = ComorbidCondition;
        }
        return comorbidValues;
    },

    // for adding time to date for assesssmentdatetime and reassessmentdatetime
    addAssOrReassTimeToDate: (date, timeInString) => {
        let time = timeInString.split(':');
        if (time && time.length && time.length == 2) {
            return dateFormats.addTimeToDate(date, parseInt(time[0]), parseInt(time[1]));
        }
    },

    // Compare two objects for equality
    objectsComparisionEquivalent: (a, b) => {
        // Create arrays of property names
        var aProps = Object.getOwnPropertyNames(a);
        var bProps = Object.getOwnPropertyNames(b);

        // If number of properties is different,
        // objects are not equivalent
        if (aProps.length != bProps.length) {
            return false;
        }

        for (var i = 0; i < aProps.length; i++) {
            var propName = aProps[i];

            // If values of same property are not equal,
            // objects are not equivalent
            if (a[propName] !== b[propName]) {
                return false;
            }
        }

        // If we made it this far, objects
        // are considered equivalent
        return true;
    },


    // save login user info into realm
    saveUserInfoInLocalRealm: (userInfo, password) => {
        let userDetails = {};
        userDetails.UserIdFacilityCode = userInfo.UserID.toString() + '-' + userInfo.FacilityCode.toString();
        userDetails.UserID = userInfo.UserID;
        userDetails.Name = userInfo.Name;
        userDetails.Title = userInfo.Title;
        userDetails.LoginName = userInfo.LoginName;
        userDetails.Password = password ? password : null;
        userDetails.FacilityCode = userInfo.FacilityCode;
        userDetails.IsAdmin = userInfo.IsAdmin;
        userDetails.IsActive = userInfo.IsActive;
        userDetails.IsSuperUser = userInfo.IsSuperUser;
        userDetails.RORWAccess = userInfo.RORWAccess;
        userDetails.IsLocked = userInfo.IsLocked;
        userDetails.PreadmissionPrivilege = userInfo.PreadmissionPrivilege;
        userDetails.CaseListingPrivilege = userInfo.CaseListingPrivilege;
        userDetails.PhysicianPrivilege = userInfo.PhysicianPrivilege;
        userDetails.ScreenerPrivilege = userInfo.ScreenerPrivilege;
        userDetails.LastPWDChangeDate = userInfo.LastPWDChangeDate;
        userDetails.ForcePasswordChange = userInfo.ForcePasswordChange;
        userDetails.PasswordExpirationDays = userInfo.PasswordExpirationDays;
        userDetails.IsDebugEnabled = userInfo.IsDebugEnabled;
        realmController.insertOrUpdateRecord(schema.USERS_SCHEMA, userDetails, true);
        return userDetails;
    },

    // insert or update last sync details based on type
    updateSynchronizeDetails: (synctype, facilityCode, userId, syncDetails) => {
        let synDetails = {
            Type: synctype,
            LastSynDate: dateFormats.formatWithTimeZone('todayDate'),
            FacilityCode: facilityCode,
            UserID: userId,
            SynchronizationID: syncDetails && syncDetails[0] ?
                syncDetails[0].SynchronizationID : realmController.getMaxPrimaryKeyId(schema.SYNCHRONIZATION_DETAILS_SCHEMA, 'SynchronizationID'),
        }
        realmController.insertOrUpdateRecord(schema.SYNCHRONIZATION_DETAILS_SCHEMA, synDetails, true);
        return synDetails;
    },

    // Get Last Sync date details
    getLastSyncDate: (synctype, userId, facilityCode) => {
        if (synctype && facilityCode) {
            let filterQuery = 'FacilityCode ==[c] ' + '"' + facilityCode + '"';
            if (synctype && (synctype == 'MasterData' || synctype == 'CategoryMasterData'
                || synctype == 'FacilityCommonData' || synctype == 'IcdData' || synctype == 'ICD10Data' || synctype == 'ICD9Data')) {
                filterQuery = 'Type = ' + '"' + synctype + '"';
            } else if (synctype) {
                filterQuery += ' AND Type = ' + '"' + synctype + '"';
                if (synctype == 'User') {
                    filterQuery += ' AND UserID = ' + userId;
                }
            }
            return realmController.getAllRecordsWithFiltering(schema.SYNCHRONIZATION_DETAILS_SCHEMA, filterQuery);
        } else {
            return null
        }
    },

    //removing Admitted patient data
    removeAdmittedPatient: (caselistingRecord, preAdmissionIPADID) => {
        if (caselistingRecord && caselistingRecord.length) {
            let admissionQuery = 'PatientAdmssionIPADId =' + '"' + caselistingRecord[0].Admission[0].PatientAdmssionIPADId + '"';
            let admissionrecord = realmController.getAllRecordsWithFiltering(schema.ADMISSION_SCHEMA, admissionQuery);

            let fimQuery = 'EntityIPADID =' + '"' + caselistingRecord[0].FIM[0].EntityIPADID + '"';
            let fimRecord = realmController.getAllRecordsWithFiltering(schema.CASE_FIM_SCHEMA, fimQuery);

            realm.write(() => {
                realm.delete(caselistingRecord);
                realm.delete(admissionrecord);
                realm.delete(fimRecord);
            });
            let preadmisisonDetails = {
                PreAdmissionIPADID: preAdmissionIPADID,
                patIPADID: null,
                PatID: null
            };
            realmController.insertOrUpdateRecord(schema.PREADMISSION_SCHEMA, preadmisisonDetails, true);
        }

    },

    // Save current logged in user details in asyncstorage
    saveCurrentLoggedUserInAsyncStorage: (userDetails, type) => {
        // get facility details
        if (userDetails.FacilityCode) {
            let facilityDetails = getFacilityExpDetails(userDetails.FacilityCode);
            if (facilityDetails) {
                userDetails.FacilityDetails = facilityDetails;
            }
        }
        setLoggedInUserDetailsInAsyncStore(userDetails, type);
        return userDetails;
    },

    // for setting logged in user details in async storage
    setLoggedInUserDetailsInAsyncStore: (userDetails) => {
        setLoggedInUserDetailsInAsyncStore(userDetails);
    },

    // Validate icd codes
    validateIcdCodesAndDisplayColor: (ICDVersion, codesInfo, screenName) => {

        if (codesInfo && ICDVersion && screenName) {
            if (codesInfo && codesInfo.length && codesInfo.length > 0) {
                let filterQuery = '';
                let totalCodesCount = 0;
                let schemaName = ICDVersion === 9 ? schema.ICD9Data_SCHEMA : schema.ICD10_SCHEMA;
                let codeField = 'Code';

                codesInfo.forEach((element, index) => {
                    if (element.value) {
                        element.value = element.value.trim();

                        if (index > 0 && filterQuery) {
                            filterQuery += ' OR ';
                        }
                        filterQuery += `(${codeField} == "${element.value}")`;
                        totalCodesCount = totalCodesCount + 1;
                    } else {
                        element.isInvalidCode = false;
                    }
                });

                if (schemaName && filterQuery) {
                    // find codes in corresponding schema
                    let allCodes = [];
                    allCodes = realmController.getAllRecordsWithFiltering(schemaName, filterQuery);
                    // if ((allCodes.length != totalCodesCount) || (allCodes.length == 0)) {
                    // For checking individual code is valid or not
                    for (let i = 0; i < codesInfo.length; i++) {
                        if (codesInfo[i] && codesInfo[i].value) {
                            let sinlgeCode = allCodes.find(code => code[codeField] == codesInfo[i].value);

                            if (!sinlgeCode) {
                                // For enabling validation color
                                codesInfo[i].isInvalidCode = true;
                            } else {
                                // For disabling validation color
                                codesInfo[i].isInvalidCode = false;
                            }
                        }
                    }
                    return codesInfo;
                    // }
                } else {
                    return codesInfo;
                }
            }
        } else {
            return codesInfo;
        }
    },

    // Validate icd codes
    validateICDCodesWithFormat: (ICDVersion, codeType, codesInfo, screenName) => {

        if (codeType && codesInfo && ICDVersion && screenName) {
            if (codesInfo && codesInfo.length && codesInfo.length > 0) {
                if (codesInfo && codesInfo.length && codesInfo.length > 0) {
                    let confirmInfo = {};
                    for (let i = 0; i < codesInfo.length > 0; i++) {
                        let pRestSpaces = false;
                        if (codesInfo[i] && codesInfo[i].value) {
                            codesInfo[i].value = codesInfo[i].value.trim();
                            if (codesInfo[i].value) {
                                codesInfo[i].value = codesInfo[i].value.toUpperCase();
                                confirmInfo = {};
                                // Check for special char format
                                if (checkSpecialCharFormat(codesInfo[i].value, config.icdSpecialCharFormat)) { // true - for invalid code
                                    confirmInfo = prepareValidationMessagesWithInvalidCode(codeType, screenName, i, codesInfo);
                                    break;
                                } else {
                                    if (ICDVersion == 9) {
                                        //Should not allow e,v characters.
                                        let orginalCode = codesInfo[i].value;
                                        if (codesInfo[i].value.substr(0, 1) != "E") {
                                            codesInfo[i].value = " " + codesInfo[i].value;
                                        }
                                        if (codesInfo[i].value.length < 7) {
                                            let codeLength = codesInfo[i].value.length;
                                            for (let k = 0; k < 7 - codeLength; k++) {
                                                codesInfo[i].value = codesInfo[i].value + " "
                                            }
                                        }
                                        if (codesInfo[i].value.indexOf(".") == -1) {
                                            if (orginalCode && orginalCode.length > 5) {
                                                confirmInfo = prepareValidationMessagesWithInvalidCode(codeType, screenName, i, codesInfo);
                                                break;
                                            }
                                            codesInfo[i].value = codesInfo[i].value.substr(0, 4) + "." + codesInfo[i].value.substr(4, 2);
                                        }
                                        //Step1: If the code exists then it should contain 7 digits.
                                        if (codesInfo[i].value.length != 0) {
                                            if (codesInfo[i].value.length != 7) {
                                                confirmInfo = prepareValidationMessagesWithInvalidCode(codeType, screenName, i, codesInfo);
                                                break;
                                            }
                                        }
                                        //Step2: Character 1 must be E or space...
                                        if ((codesInfo[i].value.substr(0, 1) != " ") && (codesInfo[i].value.substr(0, 1) != "E")) {
                                            confirmInfo = prepareValidationMessagesWithInvalidCode(codeType, screenName, i, codesInfo);
                                            break;
                                        }
                                        //Step3: Character 2 can be V if character 1 is a space..Otherwise,character 2 must be numeric (0 through 9).
                                        if (codesInfo[i].value.substr(0, 1) == "E") {
                                            if ((isNaN(codesInfo[i].value.substr(1, 1)) == true) || (codesInfo[i].value.substr(1, 1) == " ")) {
                                                confirmInfo = prepareValidationMessagesWithInvalidCode(codeType, screenName, i, codesInfo);
                                                break;
                                            }
                                        } else if (codesInfo[i].value.substr(0, 1) == " ") {
                                            if (((isNaN(codesInfo[i].value.substr(1, 1)) == true) || (codesInfo[i].value.substr(1, 1) == " ")) && (codesInfo[i].value.substr(1, 1) != "V")) {
                                                confirmInfo = prepareValidationMessagesWithInvalidCode(codeType, screenName, i, codesInfo);
                                                break;
                                            }
                                        }
                                        //Step4: Characters 3 and 4 must be numeric (0 through 9).
                                        if ((isNaN(codesInfo[i].value.substr(2, 1)) == true) || (codesInfo[i].value.substr(2, 1) == " ")) {
                                            confirmInfo = prepareValidationMessagesWithInvalidCode(codeType, screenName, i, codesInfo);
                                            break;
                                        }
                                        if ((isNaN(codesInfo[i].value.substr(3, 1)) == true) || (codesInfo[i].value.substr(3, 1) == " ")) {
                                            confirmInfo = prepareValidationMessagesWithInvalidCode(codeType, screenName, i, codesInfo);
                                            break;
                                        }
                                        //Step5: Character 5 must be a decimal point.
                                        if (codesInfo[i].value.substr(4, 1) != ".") {
                                            confirmInfo = prepareValidationMessagesWithInvalidCode(codeType, screenName, i, codesInfo);
                                            break;
                                        }
                                        //Step6: If character 6 is a space,then character 7 must be a space.
                                        if (codesInfo[i].value.substr(5, 1) != "") {
                                            if ((isNaN(codesInfo[i].value.substr(5, 1)) == true) || (codesInfo[i].value.substr(2, 1) == " ")) {
                                                confirmInfo = prepareValidationMessagesWithInvalidCode(codeType, screenName, i, codesInfo);
                                                break;
                                            }
                                        } else {
                                            if (codesInfo[i].value.substr(6, 1) != "") {
                                                confirmInfo = prepareValidationMessagesWithInvalidCode(codeType, screenName, i, codesInfo);
                                                break;
                                            }
                                        }
                                        //Step7: 7 must be numeric (0 through 9 ) or a space. and also if first char is 'E then 7th char must be empty
                                        if (codesInfo[i].value.substr(6, 1) != " ") {
                                            if (codesInfo[i].value.substr(0, 1) == "E") {
                                                confirmInfo = prepareValidationMessagesWithInvalidCode(codeType, screenName, i, codesInfo);
                                                break;
                                            }
                                            if ((isNaN(codesInfo[i].value.substr(6, 1)) == true) || (codesInfo[i].value.substr(6, 1) == " ")) {
                                                confirmInfo = prepareValidationMessagesWithInvalidCode(codeType, screenName, i, codesInfo);
                                                break;
                                            }
                                        }
                                    } else {

                                        // If no decimal, add it to the 4th local but not if there's only 3 characters, ARM
                                        if (codesInfo[i].value.indexOf(".") == -1) {
                                            if (codesInfo[i].value.length < 3) {
                                                confirmInfo = prepareValidationMessagesWithInvalidCode(codeType, screenName, i, codesInfo);
                                                break;
                                            }
                                            // We want to add the decimal point, even if there is nothing after it
                                            if (codesInfo[i].value.length >= 3) {
                                                codesInfo[i].value = codesInfo[i].value.substr(0, 3) + "." + codesInfo[i].value.substr(3, 5);
                                            }
                                        }

                                        //Force the code to a fixed 8 character length
                                        if (codesInfo[i].value.length != 8) {
                                            for (let j = 0; codesInfo[i].value.length < 8; j++) {
                                                codesInfo[i].value = codesInfo[i].value + " "
                                            }
                                        }

                                        //This shouldn't happen but it was in for the ICD9 check - if length is not 8, exit.
                                        if (codesInfo[i].value.length != 8) {
                                            confirmInfo = prepareValidationMessagesWithInvalidCode(codeType, screenName, i, codesInfo);
                                            break;
                                        }

                                        for (let c = 0; c < 8; c++) {
                                            //if (isNaN(codesInfo[i].value.substr(c, 1)) == true) {
                                            switch (c) {
                                                // 1st character must be an alphabetic [A-Z,a-z].   
                                                case 0:
                                                    if (!(codesInfo[i].value.substr(c, 1).match(/[a-zA-Z]/))) {
                                                        return confirmInfo = prepareValidationMessagesWithInvalidCode(codeType, screenName, i, codesInfo);
                                                    }
                                                    break;

                                                // 2nd character must be numeric [0-9].     
                                                case 1:
                                                    if (!(codesInfo[i].value.substr(c, 1).match(/\d/))) {
                                                        return confirmInfo = prepareValidationMessagesWithInvalidCode(codeType, screenName, i, codesInfo);
                                                    }
                                                    break;

                                                // 3rd character must be numeric [0-9] or alphabetic [A-Z,a-z].      
                                                case 2:
                                                    if (!(codesInfo[i].value.substr(c, 1).match(/[a-zA-Z]|\d/))) {
                                                        return confirmInfo = prepareValidationMessagesWithInvalidCode(codeType, screenName, i, codesInfo);
                                                    }
                                                    break;

                                                // 4th character must be a decimal point. 
                                                case 3:

                                                    if (codesInfo[i].value.substring(3, 8) != "     ") {
                                                        if (codesInfo[i].value.substr(c, 1).match(/[^.]/)) {
                                                            return confirmInfo = prepareValidationMessagesWithInvalidCode(codeType, screenName, i, codesInfo);
                                                        }
                                                        break;
                                                    }

                                                // 5th character must be numeric [0-9], alphabetic [A-Z,a-z], or space.
                                                case 4:
                                                    if (!(codesInfo[i].value.substr(c, 1).match(/[a-zA-Z]|\d| /))) {
                                                        return confirmInfo = prepareValidationMessagesWithInvalidCode(codeType, screenName, i, codesInfo);
                                                    }

                                                    // if this character is a space, all remaining characters must also be a space.
                                                    if (codesInfo[i].value.substr(c, 1).match(/ /)) {
                                                        pRestSpaces = true
                                                    }
                                                    break;

                                                // 6th-8th characters must be numeric [0-9], or alphabetic [A-Z,a-z], unless proceeded  
                                                // by a space, in which case the subsequent character(s) must also be a space.  
                                                case 5:
                                                case 6:
                                                case 7:
                                                    if (pRestSpaces) {
                                                        if (!(codesInfo[i].value.substr(c, 1).match(/ /))) {
                                                            return confirmInfo = prepareValidationMessagesWithInvalidCode(codeType, screenName, i, codesInfo);
                                                        }
                                                    }
                                                    else {
                                                        if (!(codesInfo[i].value.substr(c, 1).match(/[a-zA-Z]|\d| /))) {
                                                            return confirmInfo = prepareValidationMessagesWithInvalidCode(codeType, screenName, i, codesInfo);
                                                        }
                                                        // if this character is a space, all remaining characters must also be a space.
                                                        if (codesInfo[i].value.substr(c, 1).match(/ /)) {
                                                            pRestSpaces = true
                                                        }
                                                        break;
                                                    }
                                            }
                                        }
                                    }
                                }
                                // For removing spaces value
                                codesInfo[i].value = codesInfo[i].value ? codesInfo[i].value.trim() : codesInfo[i].value;
                            }
                        }
                    }

                    if (confirmInfo) {
                        if (!confirmInfo.codesInfo) {
                            confirmInfo.codesInfo = codesInfo
                        }
                        return confirmInfo;
                    } else {
                        return confirmInfo.codesInfo = codesInfo;
                    }
                } else {
                    return confirmInfo.codesInfo = codesInfo;;
                }
            }
        }
    },

    // Validate icd codes
    validateSinlgeICDCodeWithFormat: (codeValue, codeDetails, ICDVersion, codeType, screenName) => {
        if (codeType && codeDetails && codeDetails.fieldName && ICDVersion && screenName) {
            if (codeValue) {
                let confirmInfo = {};
                let pRestSpaces = false;
                if (codeValue) {
                    codeValue = codeValue.trim();
                    if (codeValue) {
                        codeValue = codeValue.toUpperCase();
                        // Check for special char format
                        if (checkSpecialCharFormat(codeValue, config.icdSpecialCharFormat)) { // true - for invalid code
                            return confirmInfo = prepareValidationMessageWithSinlgeInvalidCode(codeDetails, codeType, screenName);
                        } else {
                            if (ICDVersion == 9) {
                                //Should not allow e,v characters.
                                let orginalCode = codeValue;
                                if (codeValue.substr(0, 1) != "E") {
                                    codeValue = " " + codeValue;
                                }
                                if (codeValue.length < 7) {
                                    let codeLength = codeValue.length;
                                    for (let k = 0; k < 7 - codeLength; k++) {
                                        codeValue = codeValue + " "
                                    }
                                }
                                if (codeValue.indexOf(".") == -1) {
                                    if (orginalCode && orginalCode.length > 5) {
                                        return confirmInfo = prepareValidationMessageWithSinlgeInvalidCode(codeDetails, codeType, screenName);
                                    }
                                    codeValue = codeValue.substr(0, 4) + "." + codeValue.substr(4, 2);
                                }
                                //Step1: If the code exists then it should contain 7 digits.
                                if (codeValue.length != 0) {
                                    if (codeValue.length != 7) {
                                        return confirmInfo = prepareValidationMessageWithSinlgeInvalidCode(codeDetails, codeType, screenName);
                                    }
                                }
                                //Step2: Character 1 must be E or space...
                                if ((codeValue.substr(0, 1) != " ") && (codeValue.substr(0, 1) != "E")) {
                                    return confirmInfo = prepareValidationMessageWithSinlgeInvalidCode(codeDetails, codeType, screenName);
                                }
                                //Step3: Character 2 can be V if character 1 is a space..Otherwise,character 2 must be numeric (0 through 9).
                                if (codeValue.substr(0, 1) == "E") {
                                    if ((isNaN(codeValue.substr(1, 1)) == true) || (codeValue.substr(1, 1) == " ")) {
                                        return confirmInfo = prepareValidationMessageWithSinlgeInvalidCode(codeDetails, codeType, screenName);
                                    }
                                } else if (codeValue.substr(0, 1) == " ") {
                                    if (((isNaN(codeValue.substr(1, 1)) == true) || (codeValue.substr(1, 1) == " ")) && (codeValue.substr(1, 1) != "V")) {
                                        return confirmInfo = prepareValidationMessageWithSinlgeInvalidCode(codeDetails, codeType, screenName);
                                    }
                                }
                                //Step4: Characters 3 and 4 must be numeric (0 through 9).
                                if ((isNaN(codeValue.substr(2, 1)) == true) || (codeValue.substr(2, 1) == " ")) {
                                    return confirmInfo = prepareValidationMessageWithSinlgeInvalidCode(codeDetails, codeType, screenName);
                                }
                                if ((isNaN(codeValue.substr(3, 1)) == true) || (codeValue.substr(3, 1) == " ")) {
                                    return confirmInfo = prepareValidationMessageWithSinlgeInvalidCode(codeDetails, codeType, screenName);
                                }
                                //Step5: Character 5 must be a decimal point.
                                if (codeValue.substr(4, 1) != ".") {
                                    return confirmInfo = prepareValidationMessageWithSinlgeInvalidCode(codeDetails, codeType, screenName);
                                }
                                //Step6: If character 6 is a space,then character 7 must be a space.
                                if (codeValue.substr(5, 1) != "") {
                                    if ((isNaN(codeValue.substr(5, 1)) == true) || (codeValue.substr(2, 1) == " ")) {
                                        return confirmInfo = prepareValidationMessageWithSinlgeInvalidCode(codeDetails, codeType, screenName);
                                    }
                                } else {
                                    if (codeValue.substr(6, 1) != "") {
                                        return confirmInfo = prepareValidationMessageWithSinlgeInvalidCode(codeDetails, codeType, screenName);
                                    }
                                }
                                //Step7: 7 must be numeric (0 through 9 ) or a space. and also if first char is 'E then 7th char must be empty
                                if (codeValue.substr(6, 1) != " ") {
                                    if (codeValue.substr(0, 1) == "E") {
                                        return confirmInfo = prepareValidationMessageWithSinlgeInvalidCode(codeDetails, codeType, screenName);
                                    }
                                    if ((isNaN(codeValue.substr(6, 1)) == true) || (codeValue.substr(6, 1) == " ")) {
                                        return confirmInfo = prepareValidationMessageWithSinlgeInvalidCode(codeDetails, codeType, screenName);
                                    }
                                }
                            } else {

                                // If no decimal, add it to the 4th local but not if there's only 3 characters, ARM
                                if (codeValue.indexOf(".") == -1) {
                                    if (codeValue.length < 3) {
                                        return confirmInfo = prepareValidationMessageWithSinlgeInvalidCode(codeDetails, codeType, screenName);
                                    }
                                    // We want to add the decimal point, even if there is nothing after it
                                    if (codeValue.length >= 3) {
                                        codeValue = codeValue.substr(0, 3) + "." + codeValue.substr(3, 5);
                                    }
                                }

                                //Force the code to a fixed 8 character length
                                if (codeValue.length != 8) {
                                    for (let j = 0; codeValue.length < 8; j++) {
                                        codeValue = codeValue + " "
                                    }
                                }

                                //This shouldn't happen but it was in for the ICD9 check - if length is not 8, exit.
                                if (codeValue.length != 8) {
                                    return confirmInfo = prepareValidationMessageWithSinlgeInvalidCode(codeDetails, codeType, screenName);
                                }

                                for (let c = 0; c < 8; c++) {
                                    //if (isNaN(codeValue.substr(c, 1)) == true) {
                                    switch (c) {
                                        // 1st character must be an alphabetic [A-Z,a-z].   
                                        case 0:
                                            if (!(codeValue.substr(c, 1).match(/[a-zA-Z]/))) {
                                                return confirmInfo = prepareValidationMessageWithSinlgeInvalidCode(codeDetails, codeType, screenName);
                                            }
                                            break;

                                        // 2nd character must be numeric [0-9].     
                                        case 1:
                                            if (!(codeValue.substr(c, 1).match(/\d/))) {
                                                return confirmInfo = prepareValidationMessageWithSinlgeInvalidCode(codeDetails, codeType, screenName);
                                            }
                                            break;

                                        // 3rd character must be numeric [0-9] or alphabetic [A-Z,a-z].      
                                        case 2:
                                            if (!(codeValue.substr(c, 1).match(/[a-zA-Z]|\d/))) {
                                                return confirmInfo = prepareValidationMessageWithSinlgeInvalidCode(codeDetails, codeType, screenName);
                                            }
                                            break;

                                        // 4th character must be a decimal point. 
                                        case 3:

                                            if (codeValue.substring(3, 8) != "     ") {
                                                if (codeValue.substr(c, 1).match(/[^.]/)) {
                                                    return confirmInfo = prepareValidationMessageWithSinlgeInvalidCode(codeDetails, codeType, screenName);
                                                }
                                                break;
                                            }

                                        // 5th character must be numeric [0-9], alphabetic [A-Z,a-z], or space.
                                        case 4:
                                            if (!(codeValue.substr(c, 1).match(/[a-zA-Z]|\d| /))) {
                                                return confirmInfo = prepareValidationMessageWithSinlgeInvalidCode(codeDetails, codeType, screenName);
                                            }

                                            // if this character is a space, all remaining characters must also be a space.
                                            if (codeValue.substr(c, 1).match(/ /)) {
                                                pRestSpaces = true
                                            }
                                            break;

                                        // 6th-8th characters must be numeric [0-9], or alphabetic [A-Z,a-z], unless proceeded  
                                        // by a space, in which case the subsequent character(s) must also be a space.  
                                        case 5:
                                        case 6:
                                        case 7:
                                            if (pRestSpaces) {
                                                if (!(codeValue.substr(c, 1).match(/ /))) {
                                                    return confirmInfo = prepareValidationMessageWithSinlgeInvalidCode(codeDetails, codeType, screenName);
                                                }
                                            }
                                            else {
                                                if (!(codeValue.substr(c, 1).match(/[a-zA-Z]|\d| /))) {
                                                    return confirmInfo = prepareValidationMessageWithSinlgeInvalidCode(codeDetails, codeType, screenName);
                                                }
                                                // if this character is a space, all remaining characters must also be a space.
                                                if (codeValue.substr(c, 1).match(/ /)) {
                                                    pRestSpaces = true
                                                }
                                                break;
                                            }
                                    }
                                }
                            }
                        }
                        // For removing spaces value
                        codeValue = codeValue ? codeValue.trim() : codeValue;
                    }
                }

                if (confirmInfo && confirmInfo.validationMsg) {
                    return confirmInfo;
                } else {
                    confirmInfo.isValid = true;
                    confirmInfo.value = codeValue;
                    return confirmInfo;
                }
            } else {
                confirmInfo.isValid = true;
                confirmInfo.value = codeValue;
                return confirmInfo;
            }
        }
    },


    // For checking code in sequence order or not
    checkCodesSequenceOrder: (type, screenName, icdCodeFeilds, ICDVersion) => {

        if (type && screenName) {

            let comorbidFieldNumber = '72'; // for Preadmission
            if (screenName == 'Case') {
                comorbidFieldNumber = '24';
            }

            if (icdCodeFeilds && icdCodeFeilds.length && icdCodeFeilds.length > 0) {
                let lastIndex = '';
                let lastIndexLabel = '';
                let initialIndexLabel = '';
                let lastPreviousLabel = '';

                // For getting last item value index
                if (type == 'Comorbid') {
                    for (let i = icdCodeFeilds.length - 1; i >= 0; i--) {
                        if (icdCodeFeilds[i] && icdCodeFeilds[i].value) {
                            lastIndex = i;
                            lastIndexLabel = icdCodeFeilds[i].label;
                            break;
                        }
                    }
                } else if (type == 'Etiologic') {
                    for (let i = icdCodeFeilds.length - 1; i >= 0; i--) {
                        if (icdCodeFeilds[i] && icdCodeFeilds[i].value) {
                            lastIndex = i;
                            lastIndexLabel = icdCodeFeilds[i].label;
                            break;
                        }
                    }
                } else if (type == 'Additional') {
                    for (let i = icdCodeFeilds.length - 1; i >= 0; i--) {
                        if (icdCodeFeilds[i] && icdCodeFeilds[i].value) {
                            lastIndex = i;
                            lastIndexLabel = icdCodeFeilds[i].label;
                            break;
                        }
                    }
                }

                let missedFeilds = [];
                // check for previous value
                if (lastIndex > 0) {
                    for (let i = 0; i < lastIndex; i++) {
                        if (i == 0 && icdCodeFeilds[i]) {
                            initialIndexLabel = icdCodeFeilds[i].label;
                        }
                        if (icdCodeFeilds[i] && !icdCodeFeilds[i].value) {
                            missedFeilds.push(icdCodeFeilds[i].label);
                        }
                    }

                    // only for etiologic fields find last previous label
                    if (type == 'Etiologic') {
                        for (let i = lastIndex - 1; i >= 0; i--) {
                            if (icdCodeFeilds[i]) {
                                if (icdCodeFeilds[i].value) {
                                    lastIndexLabel = icdCodeFeilds[i].label;
                                } else {
                                    lastPreviousLabel = icdCodeFeilds[i].label;
                                    break;
                                }

                            }
                        }
                    } else if (icdCodeFeilds[lastIndex - 1]) {
                        lastPreviousLabel = icdCodeFeilds[lastIndex - 1].label;
                    }
                }
                let message = '';
                if (missedFeilds.length > 0) {
                    if (initialIndexLabel == lastPreviousLabel) {
                        if (type == 'Comorbid' || type == 'Additional') {
                            message = `'${comorbidFieldNumber}. Comorbid Condition ${lastIndexLabel}' is entered, thus '${comorbidFieldNumber}. Comorbid Condition ${initialIndexLabel}' must be entered.`;
                        } else if (type == 'Etiologic') {
                            message = `'67b. Etiologic Diagnosis Code ${lastIndexLabel}.' is entered, thus '67b. Etiologic Diagnosis Code ${initialIndexLabel}.' must be entered.`;
                        }
                    } else {
                        if (type == 'Comorbid' || type == 'Additional') {
                            message = `'${comorbidFieldNumber}. Comorbid Condition ${lastIndexLabel}' is entered, thus '${comorbidFieldNumber}. Comorbid Condition ${initialIndexLabel} through ${lastPreviousLabel}' must be entered.`;
                        } else if (type == 'Etiologic') {
                            message = `'67b. Etiologic Diagnosis Code ${lastIndexLabel}.' is entered, thus '67b. Etiologic Diagnosis Code ${lastPreviousLabel}.' must be entered.`;
                        }
                    }
                } else {
                    // For etiologic fields check validation with V or Y Validation
                    if (ICDVersion && ICDVersion == 10 && type == 'Etiologic') {
                        let invalidFieldIndex = '';
                        let fieldName = '';
                        for (let i = 0; i < icdCodeFeilds.length; i++) {
                            if (icdCodeFeilds[i] && icdCodeFeilds[i].value) {
                                let icdCodeValue = icdCodeFeilds[i].value.toUpperCase();
                                if (icdCodeValue.indexOf('V') == 0 || icdCodeValue.indexOf('W') == 0 ||
                                    icdCodeValue.indexOf('X') == 0 || icdCodeValue.indexOf('Y') == 0) {
                                    invalidFieldIndex = i;
                                    fieldName = icdCodeFeilds[i].fieldName;
                                    message = `'67b. Etiologic Diagnosis Code (${icdCodeFeilds[i].label}.)' cannot contain the following ICD-10-CM codes: V00.01 through Y99.9`;
                                    break;
                                }
                            }
                        }
                        if (message && (invalidFieldIndex || invalidFieldIndex == 0) && fieldName) {
                            return {
                                validationMessage: message,
                                validationIndex: invalidFieldIndex,
                                fieldName: fieldName
                            }
                        }
                    }
                }

                if (message) {
                    return message;
                }
            }
        }
    },

    //saving race order according to below order in database
    //['Hispanic or Latino', 'American Indian or Alaska Native', 'Asian', 'Black or African American', 'Native Hawaiian or other<br/>Pacific Islander', 'White'],
    changeRaceOrder: (Race) => {
        let newRaceOrder = Race[3] + Race[0] + Race[1] + Race[2] + Race[4] + Race[5];
        return newRaceOrder;
    },

    //changing above array order to original order
    changetoOriginalOrder: (Race) => {
        let newRaceOrder = Race[1] + Race[2] + Race[3] + Race[0] + Race[4] + Race[5];
        return newRaceOrder;
    },

    // changeRaceOrderNames  according to below order 
    //['Hispanic or Latino', 'American Indian or Alaska Native', 'Asian', 'Black or African American', 'Native Hawaiian or other<br/>Pacific Islander', 'White'],
    changeRaceOrderNames: (Race) => {
        let newRace = Race.split(",");
        let index = newRace.findIndex(query => query === ' Hispanic or Latino');
        if (!(index == -1)) {
            newRace.splice(index, 1);
            newRace.unshift('Hispanic or Latino');
            Race = newRace.join(',');
        }
        return Race;
    },

    // To get two letters from name 
    prepareIconTitleFromUsername: (name) => {
        let avatarTitle = '';
        if (name) {
            name = name.toUpperCase();
            let i = name.indexOf(' ');
            if (i != -1) {
                let partOne = name.slice(0, i).trim();
                let partTwo = name.slice(i + 1, name.length).trim();
                avatarTitle = (partOne === 'NULL' ? ' ' : partOne.substring(0, 1)) + '' + (partTwo === 'NULL' ? ' ' : partTwo.substring(0, 1));
            }
        }
        return avatarTitle;
    },

    // For apply pagination to array
    Paginator: (items, page, per_page) => {
        let limitPerPage = '';
        if (!per_page) {
            limitPerPage = getConfigurationDetails(schema.DEFAULT_RECORDS_PERPAGE);
        }
        page = page || 1;
        per_page = per_page ? per_page : limitPerPage ? limitPerPage : config.pageLimit;
        let offset = (page - 1) * per_page;
        let paginatedItems = items.slice(offset).slice(0, per_page);
        let total_pages = Math.ceil(items.length / per_page);

        return {
            page: page,
            per_page: per_page,
            pre_page: page - 1 ? page - 1 : null,
            next_page: (total_pages > page) ? page + 1 : null,
            total: items.length,
            total_pages: total_pages,
            data: paginatedItems
        };

    },

    // get ELOS from casemixgroup table based on PCMG and PCMGVersion
    getELOSFromCaseMixGroup: (PCMG, PCMGVersion) => {
        let filterQuery = 'CMG = ' + '"' + PCMG + '" AND CMGVersion = ' + '"' + PCMGVersion + '"';
        let cmgList = realmController.getAllRecordsWithFiltering(schema.CASEMIXGROUP_SCHEMA, filterQuery);
        if (cmgList && cmgList.length && cmgList.length == 1) {
            if (cmgList[0].ELOS) {
                return cmgList[0].ELOS;
            }
        }
    },

    // Move focus to specific input
    moveFocusToSpecificFeild: (inputRefs, fieldName, feildType) => {
        if (inputRefs && inputRefs[fieldName]) {
            // open date picker
            if (feildType && feildType == 'Date') {
                if (inputRefs[fieldName].onPressDate) {
                    inputRefs[fieldName].onPressDate();
                }
            } else if (feildType && feildType == 'Dropdown') { // // For moving focus to side by side date feilds and dropdowns
                Keyboard.dismiss();
                inputRefs[fieldName].togglePicker();
            } else if (inputRefs[fieldName].refs) {
                if (inputRefs[fieldName].refs[fieldName]
                    && inputRefs[fieldName].refs[fieldName]._root) {
                    inputRefs[fieldName].refs[fieldName]._root.focus();
                }
            }
        }
    },

    // Move focus to feild with blur validations
    moveFocusToSpecificFeildWithBlur: (inputRefs, fieldName) => {
        if (inputRefs && inputRefs[fieldName]) {
            // focus to specific input field
            if (inputRefs[fieldName].refs) {
                if (inputRefs[fieldName].refs[fieldName]
                    && inputRefs[fieldName].refs[fieldName]._root) {
                    setTimeout(() => {
                        inputRefs[fieldName].refs[fieldName]._root.focus();
                    }, config.focusTimeOut);
                }
            }
        }
    },

    // Round number to one digit in medical info tab
    roundNumber: (number, roundTo) => {
        return Math.round(number * Math.pow(10, roundTo)) / Math.pow(10, roundTo)
    },

    // Get user Privillage Type
    getUserPrivillageType: (privillageNo, name) => {
        let text;
        if (name == 'preadmission') {
            config.preadmissionPrivilegesList.forEach((privilege) => {
                if (privilege.type == privillageNo) {
                    text = privilege.name;
                }
            });
        } else if (name == 'caselisting') {
            config.preadmissionPrivilegesList.forEach((privilege) => {
                if (privilege.type == privillageNo) {
                    text = privilege.name;
                }
            });
        }
        return text;
    },

    // check with assessmentDate or ReassessmentDate with in 48hrs and signature to display signature labels
    checkAssessmentDateOrReassessmentDateWithSignatureLabelsForSignatureStatus: (preadmission, PreSignature, signatureType) => {
        let signatureLabelStatus = checkAssessmentDateOrReassessmentDateWithSignatureLabels(preadmission, PreSignature);
        if (signatureLabelStatus) {
            if (signatureType == 'Physician') {
                return signatureLabelStatus.PhysicianSignature;
            }
            if (signatureType == 'Screener') {
                return signatureLabelStatus.PreAdmissionSignature;
            }
        }
    },

    errorLogForStartAndEndTimeOfAPICall: (type, isFatal, dateTime) => {
        if (isFatal) {
            if (type) {
                // Insert exception details into realm
                AsyncStorage.getItem('loggedInUserDetails').then(userData => {
                    userData = JSON.parse(userData);
                    if (userData && userData.UserID) {
                        let exceptionDetails = {};
                        exceptionDetails.ExceptionHandlingID = realmController.getMaxPrimaryKeyId(schema.EXCEPTION_HANDLING_SCHEMA, 'ExceptionHandlingID');
                        if (dateTime) {
                            exceptionDetails.Description = type + ' : ' + dateTime;
                        } else {
                            exceptionDetails.Description = type + '   ' + dateTime;

                        }
                        exceptionDetails.InsertedOn = dateFormats.formatDate('todayDate');
                        exceptionDetails.FacilityCode = userData.FacilityCode;
                        exceptionDetails.InsertedBy = userData.UserID;
                        realmController.insertOrUpdateRecord(schema.EXCEPTION_HANDLING_SCHEMA, exceptionDetails);
                    }
                });
            }
        }
    },
    onClickCompliancePopupLink: (type) => {
        if (type === 'preadmission') {
            Linking.openURL(config.MedicareLinkInNotQualifying);
        } else {
            Linking.openURL(config.reportingCMGLink);
        }
    },


    //Finding Duplicate Values in comorbid fields in Advancedscreen and patient information tab 
    findDuplicateInComorbidAndAdditionalFields: (AdvancedFieldValues) => {
        let duplicateValues = [];
        // let AdvancedFieldValues = [...this.state.comorbidCoditionLabels, ...this.state.additionalICDCodesLabels]
        for (let i = 0; i < AdvancedFieldValues.length; i++) {
            for (let j = i + 1; j < AdvancedFieldValues.length; j++) {
                if (AdvancedFieldValues[j].value && AdvancedFieldValues[j].value) {
                    if (AdvancedFieldValues[i].value === AdvancedFieldValues[j].value) {
                        duplicateValues.push(AdvancedFieldValues[i].value);
                    }
                }
            }
        }
        if (duplicateValues.length) {
            return true;
        }
    },

    // Present time + 20 to get expiry time
    getExpirationtime: (expTime) => {
        if (expTime) {
            let expiryTime = dateFormats.addMinutesToDate(expTime);
            return expiryTime;
        } else {
            return null;
        }
    },

    // check fim fields to display date validation
    checkFimFieldsEmptyOrNot: (fimObject, type) => {
        let isFIMFieldEmpty = false;
        let fimFieldsArray = [];
        if (type == 'caseConflict') {
            fimFieldsArray = config.IsFIMClearFieldsForCaseConflict;
        } else {
            fimFieldsArray = JSON.stringify(config.IsMiniFIMClearFields);
            fimFieldsArray = JSON.parse(fimFieldsArray);
            let fimScoreFields = JSON.stringify(config.IsMiniFIMClearFieldsScores);
            fimScoreFields = JSON.parse(fimScoreFields);
            fimScoreFields.forEach((name) => {
                fimFieldsArray.push(name);
            });
        }
        if (fimFieldsArray && fimFieldsArray.length > 0 && fimObject) {
            fimFieldsArray.forEach((fieldName) => {
                if (fimObject[fieldName]) {
                    isFIMFieldEmpty = true;
                }
            });
        }
        return isFIMFieldEmpty;
    },

    // check empty fields in common fim for all fim tabs
    fimFieldsEmptyInCommonFim: (fimObject) => {
        let isFieldEmpty = false;
        let fimFields = JSON.stringify(config.IsMiniFIMClearFields);
        fimFields = JSON.parse(fimFields);
        if (fimFields && fimFields.length > 0 && fimObject) {
            fimFields.forEach((fieldName) => {
                if (fimObject[fieldName]) {
                    isFieldEmpty = true;
                }
            });
        }
        return isFieldEmpty;
    },

    // // Calculate Help Needed
    calculateHelpNeeded: (range) => {
        let text;
        if (range < 17) {
            text = 'Invalid hours/None'
        } else if (range >= 18 && range <= 20) {
            text = 'Over 8 hours'
        } else if (range >= 21 && range <= 29) {
            text = '7 to 8 hours'
        } else if (range >= 30 && range <= 40) {
            text = '6 to 7 hours'
        } else if (range >= 41 && range <= 50) {
            text = '5 to 6 hours'
        } else if (range >= 51 && range <= 60) {
            text = '4 to 5 hours'
        } else if (range >= 61 && range <= 70) {
            text = '3 to 4 hours'
        } else if (range >= 71 && range <= 80) {
            text = '2 to 3 hours'
        } else if (range >= 81 && range <= 90) {
            text = '1 to 2 hours'
        } else if (range >= 91 && range <= 99) {
            text = 'Under 1 hour'
        } else if (range >= 100) {
            text = 'None'
        }
        return text;
    },

    calculateHelpNeededForVersion3: (range) => {
        let text;
        if (range < 17) {
            text = ''
        } else if (range >= 18 && range <= 23) {
            text = '>8 hours'
        } else if (range >= 24 && range <= 36) {
            text = '7-8 hours'
        } else if (range >= 37 && range <= 54) {
            text = '5-6 hours'
        } else if (range >= 55 && range <= 71) {
            text = '3-4 hours'
        } else if (range >= 72 && range <= 89) {
            text = '2 - <3 hours'
        } else if (range >= 90 && range <= 107) {
            text = '1 - <2 hours'
        } else if (range >= 108 && range <= 119) {
            text = '<1 hours'
        } else if (range >= 120 && range <= 126) {
            text = '0 hours'
        } else {
            text = ''
        }
        return text;
    },
    // FIM-13 Projected Raw Motor Score
    calculateFIM13: (FIM) => {
        let FIM13 = 0;
        config.FIM13Array.forEach(function (element) {
            if (FIM[element.name]) {
                FIM13 += parseInt(FIM[element.name]) * (element.value)
            }
        });
        return Math.round(FIM13 + config.FIM13No);
    },

    // FIM-5 Projected Raw Cognition Score
    calculateFIM5: (FIM) => {
        let FIM5 = 0;
        config.FIM5Array.forEach(function (element) {
            if (FIM[element.name]) {
                FIM5 += parseInt(FIM[element.name]) * (element.value)
            }
        });
        return Math.round(FIM5 + config.FIM5No);
    },

    calculateMotorScoreIndexForV3: (fim, type) => {
        let aEating, aGrooming, aDrsg_Upr,
            aSphnctr_Bwl, aToilt, aDrsg_Lwr,
            aStr, aToiltg, aBed_Chr_Wc, aWlk_Wc, aBathing, aSphnctr_Bladr, ProblemSolving;
        if (fim) {
            aBathing = fim.Bathing || fim.Bathing == 0 ? parseInt(fim.Bathing) : 0;
            aSphnctr_Bladr = fim.Bladder ? parseInt(fim.Bladder) : 0;
            aToiltg = fim.Toileting || fim.Toileting == 0 ? parseInt(fim.Toileting) : 0;
            aWlk_Wc = fim.WalkWheelChair || fim.WalkWheelChair == 0 ? parseInt(fim.WalkWheelChair) : 0;
            ProblemSolving = fim.ProblemSolving ? parseInt(fim.ProblemSolving) : 0;
            aBed_Chr_Wc = fim.BedChair || fim.BedChair == 0 ? parseInt(fim.BedChair) : 0;

            // Projected Eating Score
            aEating = ((aBathing * 0.18) + (aToiltg * 0.08) + (aBed_Chr_Wc * 0.06) + (aSphnctr_Bladr * 0.08) +
                (aWlk_Wc * 0.04) + (ProblemSolving * 0.30) + 2.50);
            if (type == 'Preadmission') {
                aEating = (aEating > 7) ? 7 : aEating;
            }

            // Projected Grooming Score
            aGrooming = ((aBathing * 0.35) + (aToiltg * 0.06) + (aBed_Chr_Wc * 0.09) + (aSphnctr_Bladr * 0.05) +
                (aWlk_Wc * 0.03) + (ProblemSolving * 0.20) + 1.52);

            // Projected Dressing Upper Score
            aDrsg_Upr = ((aBathing * 0.35) + (aToiltg * 0.15) + (aBed_Chr_Wc * 0.14) + (aSphnctr_Bladr * 0.07) +
                (aWlk_Wc * 0.03) + (ProblemSolving * 0.14) + 0.52);

            // Projected Bowel Score
            aSphnctr_Bwl = ((aBathing * 0.09) + (aToiltg * 0.12) + (aBed_Chr_Wc * 0.08) + (aSphnctr_Bladr * 0.39) +
                (aWlk_Wc * 0.01) + (ProblemSolving * 0.06) + 1.24);

            // Projected Toileting Transfer Score
            aToilt = ((aBathing * 0.09) + (aToiltg * 0.25) + (aBed_Chr_Wc * 0.53) + (aSphnctr_Bladr * 0.03) +
                (aWlk_Wc * 0.03) + (ProblemSolving * 0.01) + 0.20);

            // Projected Dressing Lower Score
            aDrsg_Lwr = ((aBathing * 0.25) + (aToiltg * 0.31) + (aBed_Chr_Wc * 0.22) + (aSphnctr_Bladr * 0.05) +
                (aWlk_Wc * 0.08) - 0.04);

            // Projected Stairs Score
            aStr = ((aBathing * 0.06) + (aToiltg * 0.14) + (aBed_Chr_Wc * 0.20) + (aSphnctr_Bladr * 0.02) +
                (aWlk_Wc * 0.30) - 0.50);

            //Round projected scores to 0 decimal places
            aEating = PAIRoundNumber(aEating, 0);
            aGrooming = PAIRoundNumber(aGrooming, 0);
            aDrsg_Upr = PAIRoundNumber(aDrsg_Upr, 0);
            aDrsg_Lwr = PAIRoundNumber(aDrsg_Lwr, 0);
            aSphnctr_Bwl = PAIRoundNumber(aSphnctr_Bwl, 0);
            aToilt = PAIRoundNumber(aToilt, 0);
            aStr = PAIRoundNumber(aStr, 0);

            //Substitute 'bad' values with minimum values (eg if pEating is a 0, it will be 1. If pToilet is a 0, it will be a 2)
            aEating = (aEating === 0) ? 1 : aEating;
            aGrooming = (aGrooming === 0) ? 1 : aGrooming;
            aBathing = (aBathing === 0) ? 1 : aBathing;
            aDrsg_Upr = (aDrsg_Upr === 0) ? 1 : aDrsg_Upr;
            aDrsg_Lwr = (aDrsg_Lwr === 0) ? 1 : aDrsg_Lwr;
            aToiltg = (aToiltg === 0) ? 1 : aToiltg;
            aSphnctr_Bladr = (aSphnctr_Bladr === 0) ? 1 : aSphnctr_Bladr;
            aSphnctr_Bwl = (aSphnctr_Bwl === 0) ? 1 : aSphnctr_Bwl;
            aBed_Chr_Wc = (aBed_Chr_Wc === 0) ? 1 : aBed_Chr_Wc;
            aToilt = (aToilt === 0) ? 2 : aToilt;
            aWlk_Wc = (aWlk_Wc === 0) ? 1 : aWlk_Wc;
            aStr = (aStr === 0) ? 1 : aStr;

            //Weight the values
            aEating = aEating * 0.6
            aGrooming = aGrooming * 0.2
            aBathing = aBathing * 0.9
            aDrsg_Upr = aDrsg_Upr * 0.2
            aDrsg_Lwr = aDrsg_Lwr * 1.4
            aToiltg = aToiltg * 1.2
            aSphnctr_Bladr = aSphnctr_Bladr * 0.5
            aSphnctr_Bwl = aSphnctr_Bwl * 0.2
            aBed_Chr_Wc = aBed_Chr_Wc * 2.2
            aToilt = aToilt * 1.4
            aWlk_Wc = aWlk_Wc * 1.6
            aStr = aStr * 1.6

            let ProjectedMotorScoreIndex = aEating + aGrooming + aBathing + aDrsg_Upr + aDrsg_Lwr + aToiltg +
                aSphnctr_Bladr + aSphnctr_Bwl + aBed_Chr_Wc + aToilt + aWlk_Wc + aStr;

            ProjectedMotorScoreIndex = Math.round(ProjectedMotorScoreIndex * 10) / 10;
            return ProjectedMotorScoreIndex;
        }
    },

    // Change Amount to $ Format
    changeNumberFormat: (value) => {
        var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD'
        })
        return formatter.format(value);
    },

    checkAppVersion: () => {
        AsyncStorage.setItem('availableLatestVersion', JSON.stringify(false));
        let deviceAppversion = DeviceInfo.getVersion();
        let appStoreVersion = getConfigurationDetails('APPVERSION', 'versions');
        if (deviceAppversion && appStoreVersion && appStoreVersion != deviceAppversion) {
            let allowedAppVersionsList = getConfigurationDetails('ALLOWEDAPPVERSIONS', 'versions');
            if (allowedAppVersionsList) {
                allowedAppVersionsList = allowedAppVersionsList.split(',');
                let existingIndex = allowedAppVersionsList.findIndex((allowedVersion) => allowedVersion.trim() == deviceAppversion);
                if (existingIndex == -1) {
                    return 'appVersionChanged';
                } else {
                    AsyncStorage.setItem('availableLatestVersion', JSON.stringify(true));
                    return 'appVersionAllowed';
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    },

    updateDbChangeInUsersTable: (userBody, IsDbChanged) => {
        let UserIdFacilityCode = userBody.UserID.toString() + '-' + userBody.FacilityCode.toString();
        let modifiedUserInfo = {
            UserIdFacilityCode: UserIdFacilityCode,
            IsDbChanged: IsDbChanged
        }
        realmController.insertOrUpdateRecord(schema.USERS_SCHEMA, modifiedUserInfo, true);
    },

    handlePrivacyPolicyDocument: () => {
        let PrivacyPolicyLink = config.PrivacyPolicyLink;
        if (PrivacyPolicyLink) {
            Linking.canOpenURL(PrivacyPolicyLink).then((isSupported) => {
                if (isSupported) {
                    Linking.openURL(PrivacyPolicyLink)
                } 
            })
        }
    }

};