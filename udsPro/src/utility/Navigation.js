/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */
import AsyncStorage from '@react-native-community/async-storage';
import { Navigation } from "react-native-navigation";
import common from './commonFun';
export default {
    LOGIN_SCREEN: 'LoginScreen',
    HOME_SCREEN: 'HomeScreen',
    PREADMISSION_LISTING_SCREEN: 'PreAdmissionListingScreen',
    ADD_PREADMISSION_SCREEN: 'AddPreadmission',
    CASE_LISTING_SCREEN: 'CaseListing',
    ADD_CASE_SCREEN: 'AddCase',
    CASE_CONFLICTS_SCREEN: 'CaseConflictsListScreen',
    PREADMISSION_CONFLICTS_SCREEN: 'PreAdmissionConflictsListScreen',
    ICDCODES_SCREEN: 'ICDCodesScreen',
    ADVANCED_COMORBID_SCREEN: 'AdvancedComorbidScreen',
    MULTIPLE_PREADMIT_SCREEN: 'MultiplePreAdmitDocScreen',
    ERRORLOG_SCREEN: 'ErrorLogsScreen',
    CASECODES_SCREEN: 'CaseCodesScreen',
    COMPARE_PREADMISSION_CONFLICTS: 'ComparePreAdmitConflictsScreen',
    COMPARE_CASELISTING_CONFLICTS: 'CompareCaseListingConflictsScreen',
    FUNCTIONAL_MODIFIERS_FIM_SCREEN: 'FunctionalModifiersFim',


    // Pushes a new screen into the stack
    pushScreenNavigator: async (componentId, screenName, props) => {
        AsyncStorage.setItem('LoadType', JSON.stringify('InitialLoad'));

        let isActiveSessionTime = await common.activateSession();
        if (isActiveSessionTime) {
            Navigation.push(componentId, {
                component: {
                    name: screenName,
                    passProps: props ? props : {},
                }
            });

        }
    },

    // Removes screen from the stack
    popScreenNavigator: async (componentId) => {
        let isActiveSessionTime = await common.activateSession();
        if (isActiveSessionTime) {
            Navigation.pop(componentId);
        }
    },

    // Move user to patricular  screen
    popToSpecificRouteNavigator: async (componentId) => {
        let isActiveSessionTime = await common.activateSession();
        if (isActiveSessionTime) {
            Navigation.popTo(componentId);
        }
    },

    popToTopScreenNavigator: async (componentId) => {
        Navigation.popToRoot(componentId);
    },
}