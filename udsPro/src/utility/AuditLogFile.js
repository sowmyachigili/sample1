/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import DeviceInfo from 'react-native-device-info';
import AsyncStorage from '@react-native-community/async-storage';

import schema from '../Realm/schemaNames';
import realmController from '../Realm/realmController';

import dateFormats from './formatDate';

import navigation from './Navigation';

// Update LastLogin / LastLogout in auditlog detail table
updateLastLoginOrLogoutDateInAuditLogDeatil = async (UserIdFacilityCode, auditLog, type) => {
    let lastLoginOrLogoutInfo, auditLogDetail = {};
    let lastUpdatedLoginInfo = realmController.getSingleRecord(schema.USERS_SCHEMA, UserIdFacilityCode);
    if (lastUpdatedLoginInfo) {
        if (type == 'Login') {
            lastLoginOrLogoutInfo = lastUpdatedLoginInfo.LastLogInDate;
        } else {
            lastLoginOrLogoutInfo = lastUpdatedLoginInfo.LastLogOutDate;
        }
    }
    auditLogDetail.AuditLogDetailIPADID = realmController.getMaxPrimaryKeyId(schema.AuditLogDetail_SCHEMA, 'AuditLogDetailIPADID');
    auditLogDetail.AuditLogIPADID = auditLog.AuditLogIPADID;
    auditLogDetail.FieldName = type == 'Login' ? 'LastLogin' : 'LastLogout';
    auditLogDetail.OldValue = lastLoginOrLogoutInfo
    auditLogDetail.NewValue = auditLog.InsertedOn;
    return auditLogDetail;
}

// Set IsOld is 1 if Session Id is Previous Login ID
updateOldListingAuditLogInfo = (loggedInUserDetails, sessionId) => {
    let filterQuery = 'AuditLevel == 2 AND InsertedBy == ' + loggedInUserDetails.UserID + '';
    let getOldLogInfo = realmController.getAllRecordsWithFiltering(schema.AuditLog_SCHEMA, filterQuery);
    if (getOldLogInfo) {
        for (let oldLog of getOldLogInfo) {
            let oldLogInfo = { ...oldLog };
            // Comapre session id 
            if (sessionId != oldLogInfo.SessionID) {
                let logInfoObj = {
                    AuditLogIPADID: oldLogInfo.AuditLogIPADID,
                    IsOld: 1
                }
                realmController.insertOrUpdateRecord(schema.AuditLog_SCHEMA, logInfoObj, true);
            }
        }
    }
}

export default {
    updateAuditLogInfoOnLogin: async (loginUserDetails) => {
        let auditLog = {};
        let sessionId = await AsyncStorage.getItem('SessionID');

        // insert auditLog table info
        if (loginUserDetails) {
            // First entry for login
            auditLog.AuditLogIPADID = realmController.getMaxPrimaryKeyId(schema.AuditLog_SCHEMA, 'AuditLogIPADID');
            auditLog.EntityID = loginUserDetails.UserID;
            auditLog.FacilityCode = loginUserDetails.FacilityCode;
            auditLog.InsertedOn = dateFormats.formatDate('todayDate');
            auditLog.InsertedBy = loginUserDetails.UserID;
            auditLog.MachineName = DeviceInfo.getUniqueID();;
            auditLog.SessionID = sessionId;
            auditLog.EntityDetail_Key = loginUserDetails.Name;
            auditLog.EntityDetail = loginUserDetails.LoginName;
            auditLog.AuditLevel = 4;
            auditLog.EntityType = 3;
            auditLog.EntityName = 'user';
            auditLog.IsOld = 0;

            // Last login record info
            let UserIdFacilityCode = loginUserDetails.UserID.toString() + '-' + loginUserDetails.FacilityCode.toString();
            let auditLogDetail = await updateLastLoginOrLogoutDateInAuditLogDeatil(UserIdFacilityCode, auditLog, 'Login');
            if (auditLogDetail) {
                let auditLogArr = [];
                auditLogArr.push(auditLogDetail);
                auditLog.AuditLogDetail = [...auditLogArr];
            }
            await realmController.insertOrUpdateRecord(schema.AuditLog_SCHEMA, auditLog, true);

            // Second Entry for login
            auditLog.AuditLogIPADID = realmController.getMaxPrimaryKeyId(schema.AuditLog_SCHEMA, 'AuditLogIPADID');
            auditLog.EntityType = 4;
            auditLog.EntityName = null;
            auditLog.EntityDetail = 'Login';
            auditLog.AuditLevel = 1;
            // Last login record info
            auditLogDetail = await updateLastLoginOrLogoutDateInAuditLogDeatil(UserIdFacilityCode, auditLog, 'Login');
            if (auditLogDetail) {
                let auditLogArr = [];
                auditLogArr.push(auditLogDetail);
                auditLog.AuditLogDetail = auditLogArr;
            }
            await realmController.insertOrUpdateRecord(schema.AuditLog_SCHEMA, auditLog, true);


            // Insert last login date in users table
            let updateLastLoginInUserTable = {
                UserIdFacilityCode: UserIdFacilityCode,
                LastLogInDate: auditLog.InsertedOn
            }
            await realmController.insertOrUpdateRecord(schema.USERS_SCHEMA, updateLastLoginInUserTable, true);
        }
    },

    // Insert auditLog table info logout
    updateAuditLogInfoOnLogout: async (loginUserDetails) => {
        let auditLog = {};
        let sessionId = await AsyncStorage.getItem('SessionID');
        if (loginUserDetails) {
            auditLog.AuditLogIPADID = realmController.getMaxPrimaryKeyId(schema.AuditLog_SCHEMA, 'AuditLogIPADID');
            auditLog.EntityID = loginUserDetails.UserID;
            auditLog.EntityType = 3;
            auditLog.EntityName = 'user';
            auditLog.SessionID = sessionId;
            auditLog.FacilityCode = loginUserDetails.FacilityCode;
            auditLog.EntityDetail = loginUserDetails.LoginName;
            auditLog.AuditLevel = 4;
            auditLog.MachineName = DeviceInfo.getUniqueID();;
            auditLog.InsertedOn = dateFormats.formatDate('todayDate');
            auditLog.InsertedBy = loginUserDetails.UserID;
            auditLog.EntityDetail_Key = loginUserDetails.Name;
            auditLog.IsOld = 0;

            // Last logout record info
            let UserIdFacilityCode = loginUserDetails.UserID.toString() + '-' + loginUserDetails.FacilityCode.toString();
            let auditLogDetail = await updateLastLoginOrLogoutDateInAuditLogDeatil(UserIdFacilityCode, auditLog, 'Logout');
            if (auditLogDetail) {
                let auditLogArr = [];
                auditLogArr.push(auditLogDetail);
                auditLog.AuditLogDetail = auditLogArr;
            }
            await realmController.insertOrUpdateRecord(schema.AuditLog_SCHEMA, auditLog, true);

            // insert last login date in users table
            let updateLastLoginInUserTable = {
                UserIdFacilityCode: UserIdFacilityCode,
                LastLogOutDate: auditLog.InsertedOn
            }
            await realmController.insertOrUpdateRecord(schema.USERS_SCHEMA, updateLastLoginInUserTable, true);
        }
    },

    updateAuditLogInfoForListingCount: async (screenName, loggedInUserDetails) => {
        let selectedScreen = '', sessionId = null;
        sessionId = await AsyncStorage.getItem('SessionID');
        if (screenName) {
            if (screenName == navigation.PREADMISSION_LISTING_SCREEN || screenName == navigation.ADD_PREADMISSION_SCREEN) {
                selectedScreen = 'Pre-admission';
            } else {
                selectedScreen = 'Case Listing';
            }
            let auditLog = {}, screenCount = 0;
            let loginUserDetails = { ...loggedInUserDetails };
            if (loginUserDetails) {
                let filterQuery;
                if (sessionId) {
                    filterQuery = 'AuditLevel == 2 AND EntityDetail == "' + selectedScreen + '" AND SessionID == "' + sessionId + '"';
                } else {
                    filterQuery = 'AuditLevel == 2 AND EntityDetail == "' + selectedScreen + '" AND EntityName >=$0 ';
                }

                // Check auditlog info already existing or not for listing count
                let getExistingLogInfo
                // let getExistingLogInfo = realmController.getAllRecordsWithFiltering(schema.AuditLog_SCHEMA, filterQuery);
                if (getExistingLogInfo && getExistingLogInfo[0] && (sessionId === getExistingLogInfo[0].SessionID)) {
                    if (getExistingLogInfo[0].EntityName) {
                        screenCount = parseInt(getExistingLogInfo[0].EntityName) + 1;
                        let auditLogInfo = {
                            AuditLogIPADID: getExistingLogInfo[0].AuditLogIPADID,
                            SessionID: sessionId,
                            EntityName: screenCount ? screenCount.toString() : null,
                            IsOld: 0
                        }
                        await realmController.insertOrUpdateRecord(schema.AuditLog_SCHEMA, auditLogInfo, true);
                    }
                } else {
                    // check and update old records for userId
                    updateOldListingAuditLogInfo(loggedInUserDetails, sessionId);

                    // Insert new auditLog table info for listing count 
                    screenCount = 1;
                    auditLog.AuditLogIPADID = realmController.getMaxPrimaryKeyId(schema.AuditLog_SCHEMA, 'AuditLogIPADID');
                    auditLog.EntityID = null;
                    auditLog.EntityType = null;
                    auditLog.EntityName = screenCount ? screenCount.toString() : null;
                    auditLog.SessionID = sessionId;
                    auditLog.FacilityCode = loginUserDetails.FacilityCode;
                    auditLog.EntityDetail = selectedScreen;
                    auditLog.AuditLevel = 2;
                    auditLog.MachineName = DeviceInfo.getUniqueID();;
                    auditLog.InsertedOn = dateFormats.formatDate('todayDate');
                    auditLog.InsertedBy = loginUserDetails.UserID;
                    auditLog.EntityDetail_Key = selectedScreen;
                    auditLog.IsOld = 0;
                    await realmController.insertOrUpdateRecord(schema.AuditLog_SCHEMA, auditLog, true);
                }
            }
        }
    }
}