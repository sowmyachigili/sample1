/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import React, { PureComponent } from "react";
import {
	ColorPropType,
	Keyboard,
	Modal,
	Picker,
	Platform,
	StyleSheet,
	Text,
	TextInput,
	TouchableOpacity,
	TouchableWithoutFeedback,
	View
} from "react-native";
import PropTypes from "prop-types";
import isEqual from "lodash.isequal";

export default class RNPickerSelect extends PureComponent {
	static propTypes = {
		onValueChange: PropTypes.func.isRequired,
		items: PropTypes.arrayOf(
			PropTypes.shape({
				label: PropTypes.string.isRequired,
				value: PropTypes.any.isRequired,
				key: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
				color: ColorPropType
			})
		).isRequired,
		value: PropTypes.any, // eslint-disable-line react/forbid-prop-types
		placeholder: PropTypes.shape({
			label: PropTypes.string,
			value: PropTypes.any,
			key: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
			color: ColorPropType
		}),
		disabled: PropTypes.bool,
		itemKey: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
		style: PropTypes.shape({}),
		children: PropTypes.any, // eslint-disable-line react/forbid-prop-types
		hideIcon: PropTypes.bool,
		placeholderTextColor: ColorPropType,
		useNativeAndroidPickerStyle: PropTypes.bool,

		// Custom Modal props (iOS only)
		hideDoneBar: PropTypes.bool,
		doneText: PropTypes.string,
		onDonePress: PropTypes.func,
		onUpArrow: PropTypes.func,
		onDownArrow: PropTypes.func,
		onOpen: PropTypes.func,
		onClose: PropTypes.func,

		// Modal props (iOS only)
		modalProps: PropTypes.shape({}),

		// TextInput props (iOS only)
		textInputProps: PropTypes.shape({}),

		// Picker props
		pickerProps: PropTypes.shape({}),

		// Display name on popover
		displayName: PropTypes.string,
		isNextFieldDisabled: PropTypes.bool,
		isMoveUpwardDisabled: PropTypes.bool
	};

	static defaultProps = {
		value: undefined,
		placeholder: {
			label: "Select an item...",
			value: null,
			color: "#9EA0A4"
		},
		disabled: false,
		itemKey: null,
		style: {},
		children: null,
		hideIcon: false,
		placeholderTextColor: "#C7C7CD",
		useNativeAndroidPickerStyle: true,
		hideDoneBar: false,
		doneText: "Done",
		onDonePress: null,
		onUpArrow: null,
		onDownArrow: null,
		onOpen: null,
		onClose: null,
		modalProps: {},
		textInputProps: {},
		pickerProps: {},
		displayName: null,
		isNextFieldDisabled: false,
		isMoveUpwardDisabled: false
	};

	static handlePlaceholder({ placeholder }) {
		if (isEqual(placeholder, {})) {
			return [];
		}
		return [placeholder];
	}

	static getSelectedItem({ items, key, value }) {
		let idx = items.findIndex(item => {
			if (item.key && key) {
				return isEqual(item.key, key);
			}
			return isEqual(item.value, value);
		});
		if (idx === -1) {
			idx = 0;
		}
		return {
			selectedItem: items[idx],
			idx
		};
	}

	static getDerivedStateFromProps(nextProps, prevState) {
		// update items if items or placeholder prop changes
		const items = RNPickerSelect.handlePlaceholder({
			placeholder: nextProps.placeholder
		}).concat(nextProps.items);
		const itemsChanged = !isEqual(prevState.items, items);

		// update selectedItem if value prop is defined and differs from currently selected item
		const { selectedItem, idx } = RNPickerSelect.getSelectedItem({
			items,
			key: nextProps.itemKey,
			value: nextProps.value
		});
		const selectedItemChanged =
			!isEqual(nextProps.value, undefined) &&
			!isEqual(prevState.selectedItem, selectedItem);

		if (itemsChanged || selectedItemChanged) {
			if (selectedItemChanged) {
				nextProps.onValueChange(selectedItem.value, idx);
			}

			return {
				...(itemsChanged && { items }),
				...(selectedItemChanged && { selectedItem })
			};
		}

		return null;
	}

	constructor(props) {
		super(props);

		const items = RNPickerSelect.handlePlaceholder({
			placeholder: this.props.placeholder
		}).concat(this.props.items);

		const { selectedItem } = RNPickerSelect.getSelectedItem({
			items,
			key: this.props.itemKey,
			value: this.props.value
		});

		this.state = {
			items,
			selectedItem,
			showPicker: false,
			animationType: undefined
		};

		this.onUpArrow = this.onUpArrow.bind(this);
		this.onDownArrow = this.onDownArrow.bind(this);
		this.onValueChange = this.onValueChange.bind(this);
		this.setInputRef = this.setInputRef.bind(this);
		this.togglePicker = this.togglePicker.bind(this);
	}

	// these timeouts were a hacky first pass at ensuring the callback triggered after the modal animation
	// TODO: find a better approach
	onUpArrow() {
		const { onUpArrow } = this.props;

		this.togglePicker();
		setTimeout(onUpArrow);
	}

	onDownArrow() {
		const { onDownArrow } = this.props;

		this.togglePicker();
		setTimeout(onDownArrow);
	}

	onValueChange(value, index) {
		const { onValueChange } = this.props;

		onValueChange(value, index);

		this.setState({
			selectedItem: this.state.items[index]
		});
	}

	setInputRef(ref) {
		this.inputRef = ref;
	}

	getPlaceholderStyle() {
		const { placeholder, placeholderTextColor } = this.props;

		if (
			!isEqual(placeholder, {}) &&
			this.state.selectedItem.label === placeholder.label
		) {
			return {
				color: placeholderTextColor
			};
		}
		return {};
	}

	triggerOpenCloseCallbacks() {
		const { onOpen, onClose } = this.props;

		if (!this.state.showPicker && onOpen) {
			onOpen();
		}

		if (this.state.showPicker && onClose) {
			onClose();
		}
	}

	togglePicker(animate = false) {
		const { modalProps, disabled } = this.props;

		if (disabled) {
			return;
		}

		const animationType =
			modalProps && modalProps.animationType
				? modalProps.animationType
				: "slide";

		this.triggerOpenCloseCallbacks();

		this.setState({
			animationType: animate ? animationType : undefined,
			showPicker: !this.state.showPicker
		});

		// if (!this.state.showPicker && this.inputRef) { // to move dropdown to upward 
		// 	this.inputRef.focus();
		// 	this.inputRef.blur();
		// }

		if (this.inputRef && !this.props.isMoveUpwardDisabled) { // to move dropdown to upward 
			this.inputRef.focus();
			this.inputRef.blur();
		}
	}

	renderPickerItems() {
		return this.state.items.map(item => {
			return (
				<Picker.Item
					label={item.label}
					value={item.value}
					key={item.key || item.label}
					color={item.color}
				/>
			);
		});
	}

	// on done press call donepress function
	onDonePress = () => {
		this.togglePicker(); // this.togglePicker(true);
		const { onDonePress } = this.props;
		if (onDonePress) {
			onDonePress();
		}
	}

	renderDoneBar() {
		const {
			doneText,
			hideDoneBar,
			onUpArrow,
			onDownArrow,
			onDonePress,
			style
		} = this.props;

		if (hideDoneBar) {
			return null;
		}

		return (
			<View
				style={[defaultStyles.modalViewMiddle, style.modalViewMiddle, { width: '100%' }]}
				testID="done_bar"
			>
				<View style={defaultStyles.nextView}>
					<TouchableOpacity
						activeOpacity={onDownArrow ? 0.5 : 1}
						onPress={onDownArrow ? this.onDownArrow : null}>
						<View testID="needed_for_touchable">
							<Text style={[onDownArrow ? (!this.props.isNextFieldDisabled ? defaultStyles.done : defaultStyles.disableNext) :
								defaultStyles.disableNext]}>{'Next'}</Text>
						</View>
					</TouchableOpacity>
				</View>
				{/* For displaying selected dropdown names */}
				<View style={defaultStyles.displayView}>
					<Text style={[defaultStyles.done, defaultStyles.labelText]} numberOfLines={1}>
						{this.props.displayName}
					</Text>
				</View>
				<View style={defaultStyles.doneView}>
					<TouchableWithoutFeedback
						onPress={this.onDonePress}
						hitSlop={{ top: 2, right: 2, bottom: 2, left: 2 }}
						testID="done_button"
					>
						<View testID="needed_for_touchable">
							<Text style={[defaultStyles.done, style.done]}>{doneText}</Text>
						</View>
					</TouchableWithoutFeedback>
				</View>
			</View >
		);
	}

	renderIcon() {
		const { hideIcon, style } = this.props;

		if (hideIcon) {
			return null;
		}

		return <View testID="icon_ios" style={[defaultStyles.icon, style.icon]} />;
	}

	renderTextInputOrChildren() {
		const { children, hideIcon, style, textInputProps } = this.props;
		const containerStyle = style.inputIOSContainer;

		if (children) {
			return (
				<View pointerEvents="box-only" style={containerStyle}>
					{children}
				</View>
			);
		}
		return (
			<View pointerEvents="box-only" style={containerStyle}>
				<TextInput
					style={[
						!hideIcon ? { paddingRight: 30 } : {},
						Platform.OS === 'ios' ? style.inputIOS : style.inputAndroid,
						this.getPlaceholderStyle(),
					]}
					value={this.state.selectedItem.label}
					ref={this.setInputRef}
					{...textInputProps}
				/>
				{this.renderIcon()}
			</View>
		);
	}

	renderIOS() {
		const { style, modalProps, pickerProps } = this.props;

		return (
			<View style={[defaultStyles.viewContainer, style.viewContainer]}>
				<TouchableWithoutFeedback
					onPress={() => {
						Keyboard.dismiss();
						this.togglePicker(true);
					}}
					testID="ios_touchable_wrapper"
				>
					{this.renderTextInputOrChildren()}
				</TouchableWithoutFeedback>
				<Modal
					testID="RNPickerSelectModal"
					visible={this.state.showPicker}
					transparent
					animationType={this.state.animationType}
					supportedOrientations={["portrait", "landscape"]}
					// onOrientationChange={TODO: use this to resize window}
					{...modalProps}
				>
					<TouchableOpacity
						style={[defaultStyles.modalViewTop, style.modalViewTop]}
						onPress={() => {
							this.togglePicker(true);
						}}
					/>
					{this.renderDoneBar()}
					<View style={[defaultStyles.modalViewBottom, style.modalViewBottom]}>
						<Picker
							testID="RNPickerSelectIOS"
							onValueChange={this.onValueChange}
							selectedValue={this.state.selectedItem.value}
							{...pickerProps}
						>
							{this.renderPickerItems()}
						</Picker>
					</View>
				</Modal>
			</View>
		);
	}

	render() {
		return Platform.OS === "ios" ? this.renderIOS() : null;
	}
}

const defaultStyles = StyleSheet.create({
	viewContainer: {
		alignSelf: "stretch"
	},
	chevronContainer: {
		flexDirection: "row",
		marginLeft: 15
	},
	chevron: {
		fontSize: 22,
		color: '#D0D4DB'
	},
	chevronActive: {
		color: "black"
	},
	icon: {
		position: "absolute",
		backgroundColor: "transparent",
		borderTopWidth: 10,
		borderTopColor: "gray",
		borderRightWidth: 10,
		borderRightColor: "transparent",
		borderLeftWidth: 10,
		borderLeftColor: "transparent",
		width: 0,
		height: 0,
		top: 20,
		right: 10
	},
	modalViewTop: {
		flex: 1
	},
	modalViewMiddle: {
		height: 44,
		zIndex: 2,
		flexDirection: "row",
		alignItems: "center",
		backgroundColor: "#EFF1F2",
		borderTopWidth: 0.5,
		borderTopColor: "#919498"
	},
	modalViewBottom: {
		height: 215, // 215
		justifyContent: "center",
		backgroundColor: "#D0D4DB"
	},
	done: {
		color: "black",
		fontWeight: "500",
		padding: 10,
		fontSize: 18
	},
	disableNext: {
		color: "gray",
		padding: 10,
		fontSize: 18
	},
	labelText: {
		fontWeight: 'normal',
		color: '#007AFE'
	},
	underline: {
		borderTopWidth: 1,
		borderTopColor: "#888988",
		marginHorizontal: 4
	},
	nextView: {
		width: '20%',
		alignItems: 'flex-start'
	},
	doneView: {
		width: '20%',
		alignItems: 'flex-start'
	},
	displayView: {
		width: '60%',
		alignItems: 'center'
	}
});
