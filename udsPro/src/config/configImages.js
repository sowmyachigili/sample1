/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

const imagePath = '../assets/';

const serverUrls = {

    // assests path
    tmIcon: require(`${imagePath}icons/tm.png`),

    // Login Screen Icons - Login Folder 
    facilityCodeIcon: require(`${imagePath}Login/FacilityCode.png`),
    userIcon: require(`${imagePath}Login/user.png`),
    passwordIcon: require(`${imagePath}Login/pwd.png`),
    disableLoginButtonIcon: require(`${imagePath}Login/disableLogin.png`),
    loginButtonIcon: require(`${imagePath}Login/login.png`),

    // HomeScreen icons
    homeScreenBackgroundImage: require(`${imagePath}HomeScreen/backgroundnew.png`),
    homeSynchronizeImage: require(`${imagePath}HomeScreen/Synchronize.png`),
    homeAdmissionListingImage: require(`${imagePath}HomeScreen/admissionListing.png`),
    homePathImage: require(`${imagePath}HomeScreen/Path.png`),
    homeAddImage: require(`${imagePath}HomeScreen/add.png`),
    homeCaseListingImage: require(`${imagePath}HomeScreen/CaseListing.png`),
    homePath1Image: require(`${imagePath}HomeScreen/Path1.png`),

    // Add Preadmission folder
    calendarIcon: require(`${imagePath}icons/calender.png`),
    clockIcon: require(`${imagePath}icons/clock.png`),
    selectedCheckboxIcon: require(`${imagePath}icons/Check-Box-Selected.png`),
    emptyCheckboxIcon: require(`${imagePath}icons/Check-box.png`),

    // icons folder
    leftArrowIcon: require(`${imagePath}icons/Left-arrow.png`),
    rightArrowIcon: require(`${imagePath}icons/Right-arrow.png`),
    cancelIcon: require(`${imagePath}icons/cancel.png`),
    questionIcon: require(`${imagePath}icons/question.png`),
    defaultIcon: require(`${imagePath}icons/Icon.png`),
    errorIcon: require(`${imagePath}icons/error.png`),
    tickBigIcon: require(`${imagePath}icons/Tick-icon-BIG.png`),
    warningIcon: require(`${imagePath}icons/warning.png`),
    trashBigIcon: require(`${imagePath}icons/Trash-icon-BIG.png`),
    readOnlyLock: require(`${imagePath}icons/readonly-lock.png`),
    searchIcon: require(`${imagePath}icons/Search-Icon.png`),
    powerButtonOffIcon: require(`${imagePath}icons/power-button-off.png`),
    dropDownIcon: require(`${imagePath}icons/Dropdown.png`),

    // SignatureTab - signature icon
    handIcon: require(`${imagePath}icons/Signature-Icon.png`),

    // NotesTab - Notes icon
    noteIcon: require(`${imagePath}icons/Notes-Icon.png`),

    // buttons folder
    logoIcon: require(`${imagePath}Buttons/Logo.png`),
    homeIcon: require(`${imagePath}Buttons/Home.png`),
    notificationIcon: require(`${imagePath}Buttons/notification.png`),
    backButtonIcon: require(`${imagePath}Buttons/Button-Back.png`),
    printButtonIcon: require(`${imagePath}Buttons/Button-print.png`),
    addButtonIcon: require(`${imagePath}Buttons/Button-Add.png`),
    deleteButtonIcon: require(`${imagePath}Buttons/Button-Delete.png`),
    admitButtonIcon: require(`${imagePath}Buttons/Button-Admit.png`),
    dontAdmitButtonIcon: require(`${imagePath}Buttons/Dont-admit.png`),
    saveButtonIcon: require(`${imagePath}Buttons/Button-Save.png`),
    signButtonIcon: require(`${imagePath}Buttons/Button-Sign.png`),
    // readOnlyLockIcon: require(`${imagePath}Buttons/readonly-lock.png`),
    readOnlyCheckBoxIcon: require(`${imagePath}Buttons/Read-Only-CheckBox.png`),
    moreButtonIcon: require(`${imagePath}Buttons/More.png`),
    lockButtonIcon: require(`${imagePath}Buttons/Lock-Status.png`),
    unlockButtonIcon: require(`${imagePath}Buttons/UnLock-Status.png`),
    outlineButtonIcon: require(`${imagePath}Buttons/Outline-button.png`),
    LogOutIcon: require(`${imagePath}Buttons/LogOut.png`),

    // Interim Tab Images
    firstButtonIcon: require(`${imagePath}Buttons/First.png`),
    previousButtonIcon: require(`${imagePath}Buttons/Previous.png`),
    nextButtonIcon: require(`${imagePath}Buttons/Next.png`),
    lastButtonIcon: require(`${imagePath}Buttons/Last.png`),
    disabledFirstButtonIcon: require(`${imagePath}Buttons/disableFirst.png`),
    disabledPreviousButtonIcon: require(`${imagePath}Buttons/disablePrevious.png`),
    disabledNextButtonIcon: require(`${imagePath}Buttons/disableNext.png`),
    disabledLastButtonIcon: require(`${imagePath}Buttons/disableLast.png`),

}

export default serverUrls;
