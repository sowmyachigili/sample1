/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

const serverUrls = {

    // Error in app if any crash
    serverErrMessage: 'Something went wrong.Try again later...',

    // DB Change Error Message
    databaseChangeErrorMessage: 'The local version of the database for this app is out of sync with the server, and must be updated to continue. Please click on ‘Restore’ button to update the local database. Please note: any unsynchronized data will be removed.',
    appVersionChangeErrorMessage: 'You are currently running an old version of this app. Please update the app to continue. Please note: any unsynchronized data will be removed.',
    appNewVersionAvailableMessage: 'There is a new version of this app available. It is strongly recommended that you update the app to the latest version as soon as possible.',

    // display message when passcode is not enabled in ipad
    passcodeAlertMessage: 'In order to protect access to patient data, you must have a passcode on your device to access this app.  Please set a passcode in Settings before running this app.',

    // guid and invalid token warning messages(401 and 504 errors)
    unableToGenerateGUIDLogin: 'Unable to generate GUID. Please click on login again.',
    invalidTokenMsgLogin: 'Invalid token. Please click on login again.',
    unableToGenarateGUID: 'Unable to generate GUID. Please click on sync again.',
    unableToGenarateGUIDMessageFromConflictScreen: 'Unable to generate GUID. Please click on keep localVersion again.',
    invalidTokenMsg: 'Invalid token. Please click on sync again.',
    invalidTokenMsgCoflictScreen: 'Invalid token. Please click on keep localVersion again.',
    invalidTokenMsgOnCompare: 'Invalid token. Please go back and again click on Compare.',

    // for invalid login credentials
    invalidLoginMsg: 'You have entered invalid login credentials.',
    requiredFieldValidationHeading: 'Please enter the following field(s)',
    facilityExpHeading: 'Facility Expired',
    enterFacilityCode: 'Please enter Facility Code.',
    enterUserName: 'Please enter User Name.',
    loginNoPassword: 'You have either entered an incorrect user name or Facility code or there is not an e-mail address assigned for the user name entered. Please contact technical support for assistance.',

    // check user is in offline or online
    connectOnlineMsg: 'You must be online to login to connect to this app for the first time.',
    coflictsCheckOnlineMsg: 'You must be online to View Conflicts.',
    messageForcheckOnline: 'You must be online for Synchronization.',
    messageForResolveConflicts: 'You must be online to resolve conflicts.',


    // server error
    serverCallErrMsg: 'Could not reach server. Please contact central administrator.',
    userLockedMsg: 'Currently your account is locked, please contact your facility administrator.',

    // activation message
    reactivateYourSession: 'Please click on the link below to reactivate your session.',
    inactiveUserMsg: 'Currently your account is inactive, please contact administrator.',

    // expiration messages
    passwordExpMsg: 'Your password has expired. Please login to PROi to change your password and then try again',
    passwordExpHeading: 'Password Expired',
    sessionExpMsg: 'Your session has been expired. Please login again.',

    // list message
    noRecordsMsg: 'No records to display',

    // version update
    newVersionUpdateHeading: 'New Vesion Update',
    newVersionUpdateMsg: 'New version is available on AppStore. Do you want to update',

    // Home Screen Privilege Message
    noAccessPrivilegeMessage: 'User does not have rights to this screen.',

    // back button message 
    backConfirm: "Are you sure you want to leave the page? Any unsaved changes will be lost.",
    logout: 'Are you sure want to logout?',
    logoutConfirmForModifiedRecord: 'Are you sure want to logout? Any unsaved changes will be lost.',

    // when auto sync is in progress display warning when click on save
    autoSyncInProgressMsg: 'You cannot save new changes while the application is syncing. Please try again once the sync is complete.',
    inAutoSyncCannotAddPatientMsg: 'You cannot add new patient while the application is syncing. Please try again once the sync is complete.',
    syncAlreadyInProgress: 'Synchronization is already in progress. Please wait...',
    conflictsSyncMsg: 'You cannot view the conflicts screen while application is syncing. Please try again once the sync is complete.',

    // preadmission warning popup messages
    preAdmissionViewRightsMsg: 'You only have Pre-admission Listing view rights.',
    preadmissionPrivilegeMsg: 'You only have Pre-admission READ ONLY rights.',
    preadmissionListingReadOnlyPrivilegeMsg: 'You only have Pre-admission Listing READ ONLY rights.',
    preAdmissionAdmitReadonlyMsg: 'You are unable to admit the patient because you do not have Case Listing R/W rights',
    savedSuccessfully: 'Pre-admission information has been \n saved successfully.',
    preAdmissionDeleteSuccessMsg: 'Pre-admission record(s) have been deleted successfully.',
    preAdmissionAlreadyExists: "Pre-admission already exists, please check and re-enter.",
    preadmissionAlreadyAdmitted: 'The selected patient has already been admitted to the facility.',
    preadmissionOneRecordSelection: 'Please select only one patient record for this action.',
    preAdmissionAtleastoneRecord: 'Please select at least one Pre-admission record for this action.',
    dontAdmitImpairmentNotClear: `'Impairment Group' will not be cleared. Do you wish to continue?`,

    // for admit record
    admitPatientReasonAlreadySelected: 'A reason for Not Admitting has already been chosen for this patient.',
    admissionDateNotEarlierAssessment: '\'Admission Date\' may not be earlier than \'3. Assessment Date\'',
    admissionDateEarlierTharCurretDate: 'Admission Date must be earlier than or the same as current date',
    admissionNotSignedByPhysician: 'CMS requires that the rehabilitation physician review and approve the pre-admission screen within the last 48 hours prior to admitting a patient to an IRF. The pre-admission assessment has not been signed by a physician. Do you want to continue? Click ‘Yes’ to continue with the admission process. Click ‘No’ to return to the assessment to review and approve the pre-admission screen.',
    admissionNotSignedByScreener: 'The pre-admission assessment has not been signed by a designated screener within the last 48 hours. Do you want to continue? Click ‘Yes’ to continue with the admission process. Click ‘No’ to return to the assessment to review and approve the pre-admission screen.',
    admissionDateAbove48Hours: 'CMS requires that the patient be admitted within 48 hours of the pre-admission assessment. A reassessment should be performed on this patient prior to admission. Do you want to continue the admission process for this patient?',
    admissionWithSignMsg: "CMS requires the pre-admission assessment to be signed after a re-assessment is completed. Do you want to continue the admission process without signing after re-assessment?",
    admitPrevilegeMsg: 'You are unable to admit the patient because you do not have Case Listing R/W rights.',
    admitSuccessMessage: 'Patient was admitted successfully.',
    // donot admit warning messages
    dontAdmitValidationMsg: '\'Reason\' and \'Impairment Group\' must be selected before the patient is set for non-admission. To make the patient available for admission again, clear the \'Reason\' field.',
    reasonAndOtherFieldReqMsg: 'If \'Reason\' is \'09 - Other\' then \'Reason - Other\' is required before the patient is set for non-admission.',
    dischargeAndOtherFieldReqMsg: 'If \'Discharge Destination\' is \'11 - Other\' then \'Discharge Destination - Other\' is required before the patient is set for non-admission.',
    reasonOtherEarseMsg: '\'Reason - Other\' value will be erased! Would you like to continue?',
    dischargeOtherEarseMsg: '\'Discharge Destination - Other\' value will be erased! Would you like to continue?',

    // messages for cannot delete patient records with Admit/DontAdmit reasons
    multipleDeleteWithAdmitOrDonNotMsg: 'Some of the records you have choosen to delete were not deleted since they have either an Admitted Status or Non-Admitted Status and Reason.',
    nonAdmitPreadmisisonDeleteMsg: 'You cannot delete a patient that has a Non-Admitted status and reason.',
    admitPreadmissionDeletMsg: 'The selected patient has already been admitted to the facility, you cannot delete an admitted patient.',
    addPreadmissionNonAdmitDeleteMsg: 'You cannot delete a patient that has a Non-Admitted status and reason.',
    addPreadmssionAdmitDeleteMsg: 'You cannot delete an admitted patient.',

    // email waring and save popup messages
    emailSentSuccessMsg: 'Email has been sent successfully',
    emailSaveSuccessMsgInOffline: 'Email has been saved successfully.Present you are in offiline. Email will be send once click on synchronize button.',
    emailTemplateSuccessMsg: 'Subject/body has been saved successfully.',
    emailTemplateWithoutSubject: 'Cannot save this subject/body without a subject.',
    emailWithoutToEmail: 'Cannot send this notification without a recipient.',
    emailWithOutSubject: 'Cannot send this notification without a subject.',

    // preadmission delete popup messages
    preadmissionSingleDeleteConfirmation: 'Are you sure you want to delete the selected patient?',
    preadmissionDeleteConfirmation: 'Are you sure you want to delete pre-admission record?',

    // Patient Identification Number,Firstname and Lastname validation message
    invalidInputPatientId: 'Invalid input value in Patient Identification Number.',
    patientFirstNameValidationMsg: 'Patient First Name may not contain certain special characters.',
    patientLastNameValidationMsg: 'Patient Last Name may not contain certain special characters.',

    // PatientInfo tab warning messages 
    assessmentDateAndDOBMsg: 'Assessment Date may not be earlier than Birth Date.',
    assessmentDateAndOnsetImpMsg: "'3. Assessment date' may not be earlier than '68. Date of Onset of Impairment'.",
    assessmentDateAndReassessmentDateMsg: 'Reassessment Date can not be before the Assessment Date.',
    assessmentDateAndProAdmDateMsg: 'Projected Admission Date may not be earlier than Assessment Date.',
    AssessmentFutureTimeMsg: 'Assessment time may not be the future time.',
    ageMsg: "The '7. Birth Date' you have entered indicates this patient is under 7 years of age. If this is incorrect you will need to update the patient's birth date.",
    SSNMsg: "Please enter valid SSN.",
    zipCode: "'17. Zip Code of Patient's Pre-Hospital Residence' is not in current zipcode list. \nCMS will reject the data if zipcode is invalid.",
    MedicaidNumberAcceptEXP: "+0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ",
    MedicaidNumberMsg: "Please enter a valid Patient Medicaid Number.",
    MedicareNumberMsg: "Please enter a valid Patient Medicare Number.",
    secondaryInsuranceTypeMsg: "'27. Secondary Insurance Type' may not be 'Medicare - Fee for Service' or 'Medicare - Medicare Advantage' when '22. Primary Insurance Type' is 'Medicare - Fee for Service' or 'Medicare - Medicare Advantage'.",
    secondaryInsuranceTypeVersionBaseMsg: "'27. Secondary insurance type' may not be 'Medicare non-MCO' or 'Medicare MCO' when '22. Primary insurance type' is 'Medicare non-MCO' or 'Medicare MCO'.",

    // Prior living tab
    AlphanumericMsg: "Please enter alphanumeric characters.",
    // removePriorDeviceUseValues: 'Are you sure you want to remove the Prior Device Use values (55E)?',
    preHospitalVocationalEffortMsg: "'60. Pre-Hospital Vocational Effort' may not be blank if '59. Pre-Hospital Vocational Category' is equal to 'Employed', 'Sheltered', 'Student' or 'Homemaker'.",
    preHospitalLivingWithMsg: "'49. Pre-Hospital Living With' may not be blank if \n '48. Pre-Hospital Living Setting' is equal to 'Home'.",

    // Diagnosis tab
    precautionsConfirmMsg: "This action will clear all information in the precautions section, click 'OK' to continue and clear section, click Cancel to return to precautions",

    // Preadmission FIM tab
    isWalkDiffDataMsg: "The modes of locomotion should be the same at admission and discharge, but the modes you selected are not the same.",
    isMiniFIMConfirmMsg: "All Functional Modifiers and FIM™ Instrument Items will be erased! Would you like to continue?",
    walkWheelchairMsg: "If Pre-admission '116. L. Walk/Wheelchair Type' is C, then Pre-admission '116. L. Walk/Wheelchair' should not be 7.",
    distanceWalkedWithWalk0: "If '112.Distance Walked' is 0, then '113. Walk' should be 0.",
    distanceWalkedWithWalk1: "If '112.Distance Walked' is 1, then '113. Walk' should be 1.",
    distanceWheelchairWithDistanceWheelchair0: "If '114.Distance Wheelchair' is 0, then '115. Wheelchair' should be 0.",
    distanceWheelchairWithDistanceWheelchair1: "If '114.Distance Wheelchair' is 1, then '115. Wheelchair' should be 1.",
    zeroShowValidationMsg: "If the patient does not transfer in/out of a tub or shower during the assessment time period, code Item '33. Tub Transfer' as '0' (Activity does not occur) and leave Item '34. Shower Transfer' blank.",
    BladderFrequency7AndBladderAssistanceNot7: "Level 7 for '102. Pre-admission Bladder Frequency of Accidents' indicates the patient has not had an accident or required equipment, devices or a helper to prevent an accident or incontinent episode during the 7 day look back period. \n Level 7 for '101. Pre-admission Bladder Level of Assistance' is the only appropriate rating based on the above rationale.",
    BowelFrequency7AndBowelAssistanceNot7: "Level 7 for '105. Pre-admission Bowel Frequency of Accidents' indicates the patient has not had an accident or required equipment, devices or a helper to prevent an accident or incontinent episode during the 7 day look back period. \n Level 7 for '104. Pre-admission Bowel Level of Assistance' is the only appropriate rating based on the above rationale.",

    // medical info validation messages for number input fields
    numberValidationFrom0to999: 'Please enter numeric data between 0 and 999',
    heightFeildMaxValidation: 'Please enter numeric data between 0 and 999.9',
    heightZeroValidation: 'Height cannot be zero.',
    weightZeroValidation: 'Weight cannot be zero.',

    // Note Messages
    noteSaveMessage: 'Notes Information has been saved successfully.',
    noteDeleteConfirmationMessage: 'You are about to delete the selected Notes.\n Are you sure you want to continue?',
    noteConfirmMessage: 'The selected Notes information has been deleted successfully.',

    // custom tab messages
    customFieldsSuccessMsg: 'Custom Data has been saved successfully.',
    customFieldsCancelMsg: 'Data may have been changed, you are about to cancel the changes and close the screen. Are you sure you want to continue?',
    customNumberIntegerValidationMsg: 'Please enter a valid Integer value for ',
    customNumberDecimalValidationMsg: 'Please enter a valid Decimal value for ',

    // when click on close button in popup, display warning message, 
    commonCancelMessage: 'You are about to cancel the changes and close the screen. Are you sure you want to continue?',

    // case delete messages
    newCaseDeleteWithoutInfo: 'Patient information is not yet saved.',
    deletemultipleCasesWithLockMsg: 'Some of the selected cases are locked and will not be deleted. Are you sure you want to delete the selected patients?',
    cannotDeleteLockedPatient: 'You cannot delete the patient as the selected record is locked.',
    multiplePatientsDeleteSuccessMsg: 'The selected patient(s) have been deleted successfully.',

    // Patient(Case) warning and validation messges
    savePatientInfoMsg: 'Please save the Patient information.',
    patientAlteastOneRecord: 'Please select at least one patient record for this action.',
    patientDeleteConfirmation: 'Are you sure you want to delete the selected patient(s)?',
    patientViewRightsMsg: 'You only have Case Listing view rights.',
    patientReadonlyMsg: 'You only have Case Listing READ ONLY rights.',
    lockedSaveCaseListing: 'You cannot save the patient as the record is locked.',
    lockedDeleteCaseListing: 'You cannot delete the patient as the record is locked.',

    // Case PatientInfo tab PatientId, Admissiondate and Birthdate warning messages
    caseSavedSuccess: 'Patient Assessment information has \n  been saved successfully.',
    patientIdentificationNo: 'Patient Identification Number',
    admissionDateEarlier: "12. Admission Date' must be earlier than or the same as current date.",
    caseAgeMsg: "The '6. Birth Date' you have entered indicates this patient is under 7 years of age.\nIf this is incorrect you will need to update the patient's birth date.",
    caseAssessmentDateAndDOBMsg: "'12. Admission Date' may not be earlier than '6. Birth Date'.",
    caseAdmissionDateAndDischargeDate: "'12. Admission Date' may not be later than '40. Discharge Date'.",
    caseAdmissionAssementDate: "'P1. Pre-Admission Assessment Date' must be earlier than or the same as '12. Admission Date'.",

    // Fim tab popup validation and success messages
    interimSuccessMessage: 'Interim Assessment information has been saved successfully.',
    followUpSuccessMessage: 'Follow-up Assessment information has been saved successfully.',
    interimRecordExistsValidationMessage: 'Assessment already exists, please check and re-enter.',
    interimDateMessage: 'Please enter Interim Assessment Date.',
    interimAssessmentDateValidation: 'Assessment Date must be between the Admission Date and Discharge Date.',
    interimDateCheckWithCurrentDate: 'Interim assessment must be between admission and discharge dates.',
    interimDateSameAsAdmission: 'Interim assessment must be after admission date.',
    admissionValidationMsgForInterim: 'Admission Date must be earlier than Interim Assessment Reference Date.',
    admissionValidationMsgForFollowup: ' Admission Date must be earlier than Follow-up Assessment Reference Date.',
    followupDateAfterDischarge: 'Follow-up Assessment must be after discharge Date.',
    followupDateAfterAdmissionMsg: 'Follow-up Assessment Date cannot be earlier than the Admission Date',
    FollowUpDateMessage: 'Please enter Follow-up Assessment Date.',
    interimCurrentDateValidation: 'Interim/Follow-up Date Cannot be today Date.',
    dischargeDateAfterInterim: ' Discharge Date must be later than Interim Assessment Date.',
    dischargeDateBeforeFollowup: 'Discharge Date must be earlier than Follow-up Assessment Date.',
    dischargeDateMessage: 'Please enter Discharge Assessment Date.',
    followupSameasAdmissionMsg: 'Follow-up assessment must be after admission date.',

    // Add case fim warning messages
    tubTransferHasZeroAndShowTransferValidation: "The score recorded in Item ' K-Tub/Shower' should be the same as the score recorded for Function Modifier Item '33. Tub Transfer' or Item '34. Shower Transfer'.",
    DistanceWalkedWithZeroWalkWaringMessage: "If '35.Distance Walked' is 0, then  '37. Walk' should be 0.'",
    DistanceWalkedWithOneWalkWaringMessage: "If '35.Distance Walked' is 1, then  '37. Walk' should be 1.'",
    DistanceWheelChairWarningMessage: "If '36.Distance Traveled in Wheelchair' is 0, then '38. Wheelchair' should be 0.'",
    DistanceWheelChairWithOneWarningMessage: "If '36.Distance Traveled in Wheelchair' is 1, then '38. Wheelchair' should be 1.'",
    BladderFrequencyBladderAssistanceMessage: "Level 7 for '30. Bladder Frequency of Accidents' indicates the patient has not had an accident or required equipment, devices or a helper to prevent an accident or incontinent episode during the 7 day look back period. \n Level  for 29. Admission Bladder Level of Assistance' is the only appropriate rating based on the above rationale.",
    BowelFrequencyBowelAssistanceMessage: "Level 7 for '32. Bowel Frequency of Accidents' indicates the patient has not had an accident or required equipment, devices or a helper to prevent an accident or incontinent episode during the 7 day look back period. \n Level 7 for '31. Bowel Level of Assistance' is the only appropriate rating based on the above rationale.",
    FunctionalModifiersSaveMessage: 'Functional Modifiers has been saved successfully.',

    // ICDCodes, CaseCodes and Advanced screen warning messages
    ICDCodesEmptyMsg: 'No records to display',
    mmtCodeDeleteConfirmatiom: 'Are you sure you want to delete the selected mmt code?',
    selectRecord: 'Please select a record.',
    DuplicateICDCodesInAdvanced: 'A duplicate ICD code has been recorded either as an etiologic diagnosis in item 22 or as a comorbid condition in item 24. Each ICD code should be recorded only once. Removing duplicate codes provides more room for representing other conditions.',
    codesLimitExceeded: 'There are not enough available fields for the selected code(s).',
    casePatientComorbidCodesLimitExceed: 'There are no other available fields for this code. To enter additional comorbid codes, please select the Advanced option.',
    selectValidComorbidICd: `Please enter valid Comorbid Conditions `,
    selectValidEtiologicIcd: `Please enter valid Etiologic Diagnosis `,

    // new dropdown options list message in list popup
    duplicateCategoryAvailable: 'This list item code already exists. Please enter another one.',
    newCategoryAdded: 'List Information has been saved successfully.',

    // Password validation messages
    pwdValidation: "Please enter Password.",
    incorrectPwd: "You have entered an incorrect username or password.",

    // BMI Validation message in Case listing based on IRF Version
    BMIWeightValidationMessage: 'Please enter Weight.',
    BMIHeightValidationMessage: 'Height cannot be zero.',
    BMIWeightSpclCharactersValidation: 'Special characters are not allowed in the following field(s).',
    BMIWeigthCannotZeroValidation: 'Weight cannot be zero.',

    BlueLabelCommentsForV3: "Blue highlighted fields indicate CMS required elements as defined in the Medicare Benefit Policy Manual; ensure these entries are detailed and individualized. Complete any other pertinent fields to reflect the comprehensive complexity of the patient requiring IRF level of care.",

    userModifiedMessage: 'User information is modified. Please login again',
    conflictMessage: 'There was a conflict synchronizing this record with UDSPRO Central™.  Do you want to view the conflict now?',


}

export default serverUrls;