/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import config from "./config";
import api from '../config/apiCalls';
import AsyncStorage from '@react-native-community/async-storage';
import commonFun from "../utility/commonFun";
import ErrorLogHandler from '../utility/ErrorLogAlert';

// To get logged in user token
getLoggedInuserGUID = (headers) => {
    return AsyncStorage.getItem('loggedInUserDetails')
        .then((userData) => {
            userData = JSON.parse(userData);
            let apiLoginValidateBody = {
                UserName: userData.LoginName,
                FacilityCode: userData.FacilityCode,
                Password: userData.Password
            }
            if (userData && userData.GUID && config.apiWithGUID) {
                headers.UserID = userData.UserID;
                headers.GUID = userData.GUID;
                let userDetails = {
                    userData: userData,
                    headers: headers,
                    apiLoginValidateBody: apiLoginValidateBody
                }
                return userDetails;
            } else {
                let userDetails = {
                    headers: headers,
                }
                return userDetails;
            }
        });
}

// generate guid and update headers
generateGUID = (method, url, body, headers, userData) => {
    return this.sendRequestToServer(method, url, body, headers).then(async (response) => {
        if (response && response.ResponseCode) {
            if (response.ResponseCode === 'Fail') {
                if (response.ResponseMessage) {
                    return config.generateGuidErrorCode;
                }
            } else if (response.ResponseCode === 'Success') {
                if (response.GUID && config.apiWithGUID) {
                    userData.GUID = response.GUID;
                    // update guid in asyncStore
                    await commonFun.setLoggedInUserDetailsInAsyncStore(userData);

                    headers.UserID = userData.UserID;
                    headers.GUID = response.GUID;
                    return headers;
                }
            }
        } else {
            return null;
        }
    });
}

fetchMethodRequest = (method, url, body) => {
    let headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    };
    if (url == api.loginValidateNew) {
        return this.sendRequestToServer(method, url, body, headers);
    } else {
        return AsyncStorage.getAllKeys()
            .then(keys => {
                if (keys.indexOf("loggedInUserDetails") != -1) {
                    return this.getLoggedInuserGUID(headers);
                } else {
                    let userDetails = {
                        headers: headers,
                    }
                    return userDetails;
                }
            }).then((userDetails) => {
                return this.sendRequestToServer(method, url, body, userDetails.headers).then(response => {
                    if (response) {
                        if (response == config.expiredGUIDCode || response == config.withOutGUIDInHeaderErrorCode) {
                            return this.generateGUID('POST', api.loginValidateNew, userDetails.apiLoginValidateBody, userDetails.headers, userDetails.userData).then(guidData => {
                                if (guidData) {
                                    if (guidData == config.generateGuidErrorCode) {
                                        return config.generateGuidErrorCode;
                                    } else {
                                        return this.sendRequestToServer(method, url, body, guidData);
                                    }
                                }
                            });
                        } else if (response == config.invalidGUIDCode) {
                            return config.invalidGUIDCode;
                        } else {
                            return response;
                        }
                    }
                })
            })
    }
}

sendRequestToServer = (method, url, body, headers) => {
    let request;
    let serverUrl = `${config.serverUrl}${url}`;
    if (url == api.loginValidateNew) {
        headers = {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        };
    }
    // console.log(body);
    // console.log(headers);
    console.log(serverUrl);

    if (method === 'GET' || method === 'DELETE') {
        request = fetch(serverUrl, { method: method, headers: headers })
    } else if (method === 'POST' || method === 'PUT') {
        request = fetch(serverUrl, { method: method, headers: headers, body: JSON.stringify(body) })
    }
    return request.then(res => res.json())
        .then(responseJson => {
            // console.log(responseJson)
            return responseJson;
        }).catch(err => {
            console.log(err)
            ErrorLogHandler(err, true, '', url == api.loginValidateNew ? 'Hide' : '', url + '--' + `serverError in - ${url - JSON.stringify(body)}`);
            return 'serverError';
        })
}

export default fetchMethodRequest;
