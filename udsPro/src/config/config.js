/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

const serverUrls = {

    // cssi url (DEV)
    serverUrl: 'https://udsmruat.udstest.org/UDSMR.UDSCentral.PROiApp.Wrapper.Services/api/',
    // serverUrl: 'http://122.175.12.49:81/UDSMR.UDSCentral.PROiApp.Wrapper.Services/api/',
    // serverUrl: 'https://proiuat.med.buffalo.edu/UDSMR.UDSCentral.PROiApp.Wrapper.Services.Dev/api/',
    MedicareLinkInNotQualifying: 'http://udsuat.solutionsoftware.com/UDSUAT/UDSCentral.Rewrite.UDSUAT/Documents/R938CP.pdf',
    reportingCMGLink: 'http://udsuat.solutionsoftware.com/UDSUAT/UDSCentral.Rewrite.UDSUAT/Documents/R938CP.pdf',


    // Uds Url (TEST)
    // serverUrl: 'https://proiuat.med.buffalo.edu/UDSMR.UDSCentral.PROiApp.Wrapper.Services/api/',
    // MedicareLinkInNotQualifying: 'https://proiuat.med.buffalo.edu/UDSCentral2015/Documents/R938CP.pdf',
    // reportingCMGLink: 'https://proiuat.med.buffalo.edu/UDSCentral2015/documents/RCMGHelp.pdf',

    // UDS New release URL
    // serverUrl: 'https://udsmrweb.udsmr.org/UDSMR.UDSCentral.PROiApp.Wrapper.Services/api/',
    // MedicareLinkInNotQualifying: 'https://proiuat.med.buffalo.edu/UDSCentral2015/Documents/R938CP.pdf',
    // reportingCMGLink: 'https://proiuat.med.buffalo.edu/UDSCentral2015/documents/RCMGHelp.pdf',

    appStoreURL: 'https://www.apple.com/itunes/',

    PrivacyPolicyLink: 'https://www.udsmr.org/Documents/PROiApp/Mobile_App_Privacy_Notice.pdf',

    apiWithGUID: true, // urls with GUID

    // api call error codes
    invalidGUIDCode: 401,
    expiredGUIDCode: 504,
    generateGuidErrorCode: 404,
    withOutGUIDInHeaderErrorCode: 411,

    // IRFVersion names
    IRFVersion2: 'V1.2',
    IRFVersion3: 'V1.3',
    IRFVersion4: 'V1.4',
    IRFVersion10: 'V1.10.1',
    IRFVersion01: 'V1.01A',
    IrfVersionV3: 'V3.0',


    // tab action types
    isPatientTabIndex: 0,
    isAdmissionTabIndex: 1,
    isInterimTabIndex: 2,
    isDischargeTabIndex: 3,
    isFollowupTabIndex: 4,
    isTrendingTabIndex: 5,
    isProfileGraphTabIndex: 6,

    // Preadmission privileges
    preadmissionPrivileges: [-1, 0, 1, 2, 3], // -1 - Super Admin, 0 - No Access, 1 - View, 2 - R/O, 3 - R/W
    preadmissionPrivilegesList: [
        { type: 0, name: 'No Access' },
        { type: 1, name: 'View' },
        { type: 11, name: 'View Locking' },
        { type: 2, name: 'R/O' },
        { type: 12, name: 'Read Only Locking' },
        { type: 3, name: 'R/W' },
        { type: 13, name: 'Read Write Locking' },
        { type: -1, name: 'R/W', usertype: 'Super Admin' }
    ],
    readWrite: 'R/W',
    noAccess: 'No Access',
    viewPermission: 'View',
    readOnly: 'R/O',
    viewLocking: 'View Locking',
    readOnlyLocking: 'Read Only Locking',
    readWriteLocking: 'Read Write Locking',
    superAdminUser: 'Super Admin',

    // Caselisting Privileges
    caselistingPrivileges: [0, 1, 2, 3, 11, 12, 13], //0 - No Access, 1 - View, 2 - R/O, 3 - R/W, 11 -View with Locking, 12 - R/O with Locking, 13 - R/W with Locking
    preadmOrCaseNonSyncPrivileges: [0, 1, 2],

    // Regular Expression for validating names, num input fields
    icdSpecialCharFormat: "`~!@#$%^&*()-_=+[{}];:'<>/? |",
    patientIdSpecialCharFormat: /[!@#$%^&*()_+\-=\[\]{};€£§'-+/₹@!`~':"\\|,<>\/\s?]/g,
    patientNameSpecialCharFormat: /[\<?*^%$>#&;:,()"€£§₹!!`~<>=|%{}[]|]/g,
    patientMedicaidNumberSpecialCharFormat: /[\<?*^%$>#&;:,()"€£§'-+/₹@!!`~<>=|%{}[]|]/g,
    onlyAlphaNumeric: /[^a-zA-Z0-9\s]/,
    onlyIntegerFormat: /^[0-9]+$/,
    onlyDecimalWithSingleDotFormat: /^\d+(\.\d+)?$/,
    weightSpecialCharValidation: /[!@#$%^&*()_+\-=\[\]{};€£§¥'-+/₹@!`~':"\\|,.<>\/\s?a-zA-Z]/g,

    // Preadmission Labels
    firstName: 'Patient First Name',
    lastName: 'Patient Last Name',
    assessmentDate: 'Assessment Date',

    // Preadmission SSN field
    numberInputFields: ['SSN'],

    // MedicalInfo labtype and studyType list 
    medicalInfoDateFeilds: ['Vitals_Date', 'Lab1_Date', 'Lab2_Date', 'Lab3_Date', 'Lab4_Date', 'Lab5_Date',
        'Study1_Date', 'Study2_Date', 'Study3_Date', 'Study4_Date', 'Study5_Date'],
    medicalInfoVitalValidationFields: ['BP_Systolic', 'BP_Diastolic', 'Temperature', 'Pulse', 'Respiration',
        'SP02', 'Height_IN', 'Height_CM', 'Weight_LBS', 'Weight_KGS', 'BMI'],

    // custom tab
    textDataTypeID: 48,
    integerDataTypeID: 49,
    decimalDataTypeID: 50,
    dateDataTypeID: 51,
    checkBoxDataTypeID: 54,
    textBoxDisplayTypeID: 59,
    comboBoxDisplayTypeID: 60,
    checkBoxDisplayTypeID: 61,
    preadmissionCustomFieldsDisplayCount: 10,
    customFieldsDisplayFrom: 159,

    // Email PriorityId
    defaultEmailPriorityId: 2533, // Normal category detail id

    // Addcase labels
    site: 'Site',
    unit: 'Unit',
    admissionDate: 'Admission Date',

    // Icd codes length
    icdCodeInputMaxlength: 10,
    suggestedIcdCodeInputMaxlength: 8,
    icd9codesInputMaxLength: 7,
    icdColorValidationText: 'Validation Color',

    // Case Fim entity types
    caseFimAdmissionEntityType: 1, // admission
    caseFimDischargeEntityType: 3, // discharge
    caseFimGoalsEntityType: 5, // goals


    // Case new Fim entity types 
    FimAdmissionEntityType: 1, // admission
    FimInterimEntityType: 2, // Interim
    FimDischargeEntityType: 3, // discharge
    FimFollowUpEntityType: 4, // Followup
    CaseFimEntityTypesArray: [1, 2, 3, 4, 5, 6],
    checkServerVersionTypes: [1, 3, 5],

    // not in use
    // PeradmissionEntityType: 0,
    FimGoalEntityType: 5,
    CaseListingPreadmitEntityType: 6,

    // Case TrendingReport
    displayLabelsLimit: 9,

    //Case  Fim 6 fields values for calculation
    fimToilet: 2.12,
    fimBladder: 1.66,
    fimBathing: 2.73,
    fimWalkWheelChair: 1.42,
    fimProblemSolving: 4.17,
    fimBedChair: 2.44,

    // for total tub/shower calculation
    tubShowerBathing: 0.36,
    tubShowerBladder: 0.03,
    tubShowerToileting: 0.14,
    tubShowerBedChair: 0.24,
    tubShowerWalkWheelChair: 0.10,

    // FIM Labels Array list
    FIM13No: 2,
    FIM5No: 2.645,
    FIMIntConversion: ['BladderFrequency', 'BowelFrequency', 'DistanceWalked',
        'DistanceWheelchair', 'TotalMotor', 'TotalFIM', 'TotalCognition'],
    FIMCurrentStatusConversion: ['ProjectedRawMotorScore', 'ProjectedRawCognitionScore'],
    CurrentMedicalStringConversion: ['Lab1_Type', 'Lab2_Type', 'Lab3_Type', 'Lab4_Type', 'Lab5_Type',
        'Lab6_Type', 'Lab7_Type', 'Lab8_Type', 'Lab9_Type', 'Lab10_Type',
        'Study1_Type', 'Study2_Type', 'Study3_Type', 'Study4_Type', 'Study5_Type'],
    checkedTotalFIMArray: ['Eating', 'Toileting', 'Bowel', 'BedChair', 'WalkWheelChair', 'Expression', 'Memory'],
    checkedTotalCognitionArray: ['Expression', 'Memory'],

    checkedMotorScoreIndexArray: [
        { name: 'Eating', value: 0.6 },
        { name: 'Bathing', value: 0.9 },
        { name: 'DressingUpper', value: 0.2 },
        { name: 'DressingLower', value: 1.4 },
        { name: 'Toileting', value: 1.2 },
        { name: 'Bladder', value: 0.5 },
        { name: 'Bowel', value: 0.2 },
        { name: 'BedChair', value: 2.2 },
        { name: 'WalkWheelChair', value: 1.6 },
        { name: 'Stairs', value: 1.6 },
    ],
    uncheckedTotalFIMArray: ['Eating', 'Grooming', 'Bathing', 'DressingUpper', 'DressingLower', 'Toileting', 'Bladder',
        'Bowel', 'BedChair', 'Toilet', 'TubShower', 'WalkWheelChair', 'Stairs', 'Comprehension', 'Expression',
        'SocialInteraction', 'ProblemSolving', 'Memory'],
    uncheckedTotalCognitionArray: ['Comprehension', 'Expression', 'SocialInteraction', 'ProblemSolving', 'Memory'],
    uncheckedMotorScoreIndexArray: [
        { name: 'Eating', value: 0.6 },
        { name: 'Grooming', value: 0.2 },
        { name: 'Bathing', value: 0.9 },
        { name: 'DressingUpper', value: 0.2 },
        { name: 'DressingLower', value: 1.4 },
        { name: 'Toileting', value: 1.2 },
        { name: 'Bladder', value: 0.5 },
        { name: 'Bowel', value: 0.2 },
        { name: 'BedChair', value: 2.2 },
        { name: 'Toilet', value: 1.4 },
        { name: 'WalkWheelChair', value: 1.6 },
        { name: 'Stairs', value: 1.6 },
    ],
    FIM13Array: [
        { name: 'Eating', value: 1.39 },
        { name: 'Grooming', value: 2.657 },
        { name: 'Bowel', value: 2.11 },
        { name: 'Toilet', value: 4.801 },
    ],
    FIM5Array: [
        { name: 'Expression', value: 2.105 },
        { name: 'Memory', value: 2.477 },
    ],

    IsMiniFIMClearFields: ['Eating', 'Grooming', 'Bathing', 'DressingUpper', 'DressingLower', 'Toileting',
        'Bladder', 'Bowel', 'BedChair', 'Toilet', 'TubShower', 'WalkWheelChair', 'Stairs', 'Comprehension',
        'Expression', 'SocialInteraction', 'ProblemSolving', 'Memory', 'BedTransfer',
        'BladderAssistance', 'BladderFrequency', 'BowelAssistance', 'BowelFrequency', 'ShowerTransfer', 'DistanceWalked',
        'DistanceWheelchair', 'TubTransfer', 'Walk', 'WheelChair', 'ComprehensionType', 'ExpressionType',
        'LocoMotionType', 'Date'],

    IsMiniFIMClearFieldsScores: ['TotalFIM', 'TotalCognition', 'TotalMotor',
        'HelpNeeded', 'MotorScoreIndex'],

    IsFIMClearFieldsForCaseConflict: ['Eating', 'Grooming', 'Bathing', 'DressingUpper', 'DressingLower', 'Toileting',
        'Bladder', 'Bowel', 'BedChair', 'Toilet', 'TubShower', 'WalkWheelChair', 'Stairs', 'Comprehension',
        'Expression', 'SocialInteraction', 'ProblemSolving', 'Memory', 'Locomotion', 'BedTransfer',
        'TotalFIM', 'TotalCognition', 'TotalMotor', 'BladderAssistance',
        'BladderFrequency', 'BowelAssistance', 'BowelFrequency', 'ShowerTransfer', 'DistanceWalked',
        'DistanceWheelchair', 'TubTransfer', 'Walk', 'WheelChair', 'ComprehensionType', 'ExpressionType',
        , 'LocoMotionType', 'ProjectedMotorScore', 'HelpNeeded', 'MotorScoreIndex'],

    graphLabels: ['Eating', 'Grooming', 'Bathing', 'DressingUpper', 'DressingLower', 'Toileting',
        'Bladder', 'Bowel', 'BedChair', 'Toilet', 'TubShower', 'WalkWheelChair', 'Stairs', 'Comprehension',
        'Expression', 'SocialInteraction', 'ProblemSolving', 'Memory', 'Locomotion', 'BedTransfer'],

    // number feilds array
    numbericInputFields: ['SSN', 'Phone', 'BP_Systolic', 'BP_Diastolic', 'Temperature', 'Pulse',
        'Respiration', 'SP02', 'Height_IN', 'Height_CM', 'Weight_KGS', 'Weight_LBS', 'BMI'],

    // popup modal heading types
    validationSummary: 'Validation Summary',
    informationMsg: 'Information',
    confirmMsg: 'Confirm',

    // set Timeout for state and popup
    pageLimit: 25,
    commonLoaderTimeout: 100,
    caselistLoaderTimeout: 100,
    preadmListLoaderTimeout: 100,
    activeSessionTimeSec: 300, // 300
    sessionExpiryMin: 15,
    focusTimeOut: 500,

    // Date and time display formats
    dateFormat: 'MM/DD/YYYY',
    timeFormat: 'HH:mm',
    syncTimeFormat: 'hh:mm A, MM-DD-YYYY',
    lastModifiedDateFormat: 'MM/DD/YYYY HH:mm',
    dateTimeFormat: 'MM/DD/YYYY hh:mm',
    fullDateFormat: 'YYYY-MM-DD HH:mm:ss',
    dbDateFormat: 'YYYY-MM-DD[T]HH:mm:ss.SSS',
    dbOnlyDateFormat: 'YYYY-MM-DD[T]00:00:00',
    ESTTimezone: "America/New_York",

    // Datepicker minimum date(Starts from)
    datepickerMinDate: '01-02-1900',
    datePlaceholder: '--/--/----',
    timePlaceholder: '--/--',

    // Colors used for text, inputfields, links, images and buttons
    navbarColor: '#323d53',
    borderColor: '#e5e5e5',
    backgroundColor: "#f2f2f2",
    infoColor: '#248dad',
    warnColor: '#ffc107',
    successColor: '#2d922d',
    dangerColor: '#bf1725',
    iconColor: '#757575',
    whiteColor: '#FFF',
    navbarTextColor: '#fff',
    buttonTextColor: '#007AFF',
    buttonBorderColor: '#000000',
    labelFontColor: '#000',
    textFontColor: '#000000',
    blueTextColor: '#027BFF',
    importantText: '#4C74C7',
    tabbarBackgroungdColor: '#F3F3FF',
    tabBarInactiveTextColor: '#36363E',
    tabBarBorderColor: '#E1E1E1',
    addScreenbackgroundColor: '#fbfaff',
    checkboxBorderColor: '#dfdfdf',
    dangerStartColor: '#E64C4C',
    blueButtonColor: '#6781B7',
    greenButtonColor: '#2FB285',

    PAC_CFIToolName: 'Post Acute Care – Common Functional Index (PAC-CFI) Tool',
    RaceOrEthnicityList: [
        { labelName: 'Hispanic or Latino', SequenceNo: 4 },
        { labelName: 'American Indian or Alaska Native', SequenceNo: 1 },
        { labelName: 'Asian', SequenceNo: 2 },
        { labelName: 'Black or African American', SequenceNo: 3 },
        { labelName: 'Native Hawaiian or Other Pacific Islander', SequenceNo: 5 },
        { labelName: 'White', SequenceNo: 6 }
    ]

}

export default serverUrls;