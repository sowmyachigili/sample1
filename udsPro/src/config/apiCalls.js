/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import config from './config';

let apiWithGUID = config.apiWithGUID;
let apiCalls = {};
if (apiWithGUID) {
    apiCalls = {
        // Validate User
        loginValidateNew: 'Login/ValidateUser_New',

        // PROi Settings Call
        getPROiAppSettingsData: 'Common/GetPROiAppSettings',

        // User
        loginMasterData: 'Common/GetUserData',

        // UDSMaster
        getUDSMasterData: 'ICDCodes/GetUDSICDMasterData',

        // ICDData
        getICDData: 'ICDCodes/GetICDData',
        getICD10Data: 'ICDCodes/GetICD10Data',
        getICD10IterationCount: 'ICDCodes/GetICD10IterationCount',
        getICD9Data: 'ICDCodes/GetICD9Data',
        getICD9IterationCount: 'ICDCodes/GetICD9IterationCount',

        // Facility Data
        getFacilityData: 'Common/GetFacilityData',
        getFacilityCommonData: 'Common/GetFacilityCommonData',

        // DropDowns
        categoryData: 'Common/GetCategoryDetails',
        facilityCategoryData: 'Common/GetFacilityCategoryDetails',
        saveCategoryData: 'Common/SaveCategoryDetailsData',

        // Preadmission
        getPreAdmissionData: 'PreAdmission/GetPreAdmissionData',
        savePreadmissionData: 'PreAdmission/SavePreAdmissionData',

        // Email Subject
        saveEmailSubjectBodyData: 'PreAdmission/SaveEmailSubjectAndBodyData',

        // Notification
        getNotificationData: 'PreAdmission/GetNotificationData',
        saveNotificationData: 'PreAdmission/SaveNotificationData',

        // Case
        getCaseListingData: 'Case/GetCaseData',
        saveCaseListingData: 'Case/SaveCaseData',

        //error log
        saveErrorLogDetails: 'Common/ErrorLogDetails',

        // AuditLog
        saveAuditLogData: 'Common/SaveAuditLogDetailsData',

        // Tier and Compliance Codes
        getComorbidAndAdvance: 'ICDCodes/GetComorbidAndAdvance'
    }
} else {
    apiCalls = {
        loginValidateNew: 'Login/ValidateUser_New',
        loginMasterData: 'Login/GetUserData_New',
        getPreAdmissionData: 'Login/GetPreAdmissionData',
        categoryData: 'Login/GetCategoryDetails',
        facilityCategoryData: 'Login/GetFacilityCategoryDetails',
        saveCategoryData: 'Login/SaveCategoryDetailsData',
        getCaseListingData: 'Login/GetCaseData',
        getFacilityCommonData: 'Login/GetFacilityCommonData',
        getUDSMasterData: 'Login/GetUDSICDMasterData',
        getFacilityData: 'Login/GetFacilityData',
        saveCaseListingData: 'Login/SaveCaseData_New',
        savePreadmissionData: 'Login/SavePreAdmissionData_New',
        getNotificationData: 'Login/GetNotificationData',
        saveNotificationData: 'Login/SaveNotificationData',
        saveEmailSubjectBodyData: 'Login/SaveEmailSubjectAndBodyData',
        getICDData: 'Login/GetICDData_New',
        getICD10Data: 'Login/GetICD10Data',
        getICD10IterationCount: 'Login/GetICD10IterationCount',
        getICD9Data: 'Login/GetICD9Data',
        getICD9IterationCount: 'Login/GetICD9IterationCount',
        saveAuditLogData: 'Common/SaveAuditLogDetailsData'
    }
}

// Login/GetUserData_NewGUID
export default apiCalls;