/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

export default {
    'USERS_SCHEMA': 'Users',

    // AuditLog schema
    'AuditLog_SCHEMA': 'AuditLog',
    'AuditLogDetail_SCHEMA': 'AuditLogDetail',

    'SYNCHRONIZATION_DETAILS_SCHEMA': 'SynchronizationDetails',
    'DROPDOWNS_SCHEMA': 'DropDowns',
    'DROPDOWN_OPTIONS_SCHEMA': 'DropDownOptions',
    'CASELISTING_SCHEMA': 'Patient',
    'ADMISSION_SCHEMA': 'Admission',
    'EXCEPTION_HANDLING_SCHEMA': 'ExceptionHandling',

    // login master data
    'PROIAPP_SETTINGS_SCHEMA': 'PROiAppSettings',
    'Facility_SCHEMA': 'Facility',

    // PreAdmission
    'PREADMISSION_SCHEMA': 'Preadmission',
    'PATIENTINFO_SCHEMA': 'Patienttab',
    'PRIOR_LIVING_SCHEMA': 'PREPriorLiving',
    'DIAGNOSIS_SCHEMA': 'PREDiagnosis',
    'MEDICALINFO_SCHEMA': 'CurrentMedical',
    'PREFIMCURRENTFIM_SCHEMA': 'PREFIMCurrentStatus',
    'FIM_SCHEMA': 'FIM',
    'GOALS_PLANS_SCHEMA': 'Goal',
    'REASSESSMENT_SCHEMA': 'ReAssessment',
    'CUSTOM_SCHEMA': 'CustomData',
    'NOTES_SCHEMA': 'Note',
    'SIGNATURE_SCHEMA': 'PRESignature',
    'CurrentFunctionalStatus_SCHEMA': 'CurrentFunctionalStatus',

    // Case Listing
    'CASE_FIM_SCHEMA': 'FIM',
    'COMORBID_ADVANCE_SCHEMA': 'ComorbidAdvance',
    'PREADMIT_SCHEMA': 'Preadmit',
    'DISCHARGE_SCHEMA': 'Discharge',
    'TIER_COMPLIANCE_SCHEMA': 'ComorbidAndAdvanceTierAndPeer',

    // Facility Common Data 
    'IRF_PREADMISSION_SCHEMA': 'IRF CompliancePreAdmission',
    'IRF_CASELISTING_SCHEMA': 'IRF Compliance',
    'IRF_PAI_VERSIONS_SCHEMA': 'IRF_PAI_Versions',
    'ZIP_CODES_SCHEMA': 'ZipCodes',
    'MEDICARE_FORMAT_SCHEMA': 'MedicareFormats',
    'UTC_TIMEZONE_OFFSET': 'UTC_Timezone_Offset',
    'CMGGROUPER_SCHEMA': 'CMGGrouper',
    'CASEMIXGROUP_SCHEMA': 'CaseMixGroup',

    // uds Master data
    'ICD10_MMT_SCHEMA': 'ICD10_MMT',
    'IGICD10LINK_SCHEMA': 'IGICD10Link',
    'IGICD9Link_SCHEMA': 'IGICD9Link',
    'IMPAIRMENTGROUP_SCHEMA': 'ImpairmentGroup',

    // facility
    'EMAIL_ADDRESSES': 'EmailAddresses',
    'EMAIL_SUBJECT_BODY': 'EmailSubjectAndBody',
    'NOTIFICATION_SCHEMA': 'Notifications',
    'FACILITYSITE_SCHEMA': 'FacilitySite',
    'SITE_SCHEMA': 'Site',
    'UNIT_SCHEMA': 'Unit',
    'CUSTOM_FIELD_SCHEMA': 'CustomField',

    // ICD Data
    'ICD10_SCHEMA': 'ICD10Data', // ICD10
    'ICD10Compliance_SCHEMA': 'ICD10Compliance',
    'ICD10Tier_SCHEMA': 'ICD10Tier',
    'ICD9Data_SCHEMA': 'ICD9Data',

    // preadmission don not amdit popup
    'DESPOSITION_CODE': 'Disposition Code',
    'PREADMISSION_DISCHARGE': 'PreAdmission Discharge Destination',

    // Dropdown category list
    'MARITAL_STATUS': 'Marital Status', // 12
    'PAYMENT_SOURCE_1': 'Payment Source - IRFPAI_V2', // 22 and 27 based on version - 1.2 and Above
    'PAYMENT_SOURCE_2': 'Payment Source', // 22 and 27 based on version - 1.10 and 1.01
    'PAYMENT_DETAILS': 'Payment Source Detail - IRFPAI_V2', // 22A and 27A

    'RACE': 'Race_Ethnicity', // 11
    'ADVANCED_DIRE': 'AdvancedDirective', // 19
    'VERIFIED_COVERAGE': 'VerifiedCoverage', // 26

    'PRIMARY_CARE_PHY': 'PrePrimaryPhysician', // 35
    'ATTENDING_PHY': 'PreAttendingPhysician', // 36
    'CONSULTING_PHY': 'PreConsultingPhysician', // 37
    'REFERRING_PHY': 'PreReferringPhysician', // 38
    'REFERRING_FACILITY': 'PreReferringfacility', // 39

    // prior living
    'PRE_HOSPITAL_LIVING_1': 'Pre-Hospital Living Setting', // 48a based on version
    'PRE_HOSPITAL_LIVING_2': 'Living Setting - IRFPAI_V2', // 48a based on version
    'PREHOSPITAL_LIVE_WITH': 'Prehospital Live With',
    'CAREGIVER_AVA': 'Caregiver Availability', // 50
    'HOME_TYPE': 'Home Type', // 51
    'BED_BATH_LOCATION': 'Bed_Bath_Location', // 52
    'BATHROOM_SETUP': 'Bathroom Setup', // 53-1 , 53-2
    'HOME_EQU': 'Home Equipment', // 54
    'INDEPENDENT_WITH': 'Independant With',// 55
    'SELF_CARE': 'SelfCare', // 55A
    'PRIOR_FUN': 'PriorFunctioning', // 55B
    'PRIOR_DEVICE_USE': 'PriorDeviceUse', // 55E
    'ASSISTANCE_WITH': 'Assistance With',// 56
    'ASSISTANCE_REQUIRED': 'Assistance Required',// 56 A
    'PROVIDING_ASSISTANCE': 'Providing Assistance',// 56 B
    'HOME_CARE_SERVICES': 'Home care services', // 57
    'SERVICES_PROVIDED': 'Services provided', // in 57 
    'PRE_VOC_CAT': 'Prehospital VocationalCategory', // 59
    'VOCATIONAL_EFFORT': 'Prehospital Vocational Effort', // 60
    'RETURN_TO_WORK': 'Return To Work',
    'RETURN_TO_SCHOOL': 'Return To School', // 60B nd 60C
    'EDUCATION': 'Education', // 61
    'RESOURCE': 'Resources', // 63

    // Disgnosis Tab
    'ALLERGIES': [{ labelName: 'American Indian or Alaska Native', value: 1 }], //71 
    'ADVANCED_COMORBID_CON': 'Advanced Comorbid Conditions', // 73A
    'ARTHRITIS_CONDITIONS': 'Arthritis Conditions', // 73B
    'IV': 'IV', // 76
    'REQUIRES_MODIFIED_SCHEDULE': 'Requires modified schedule',
    'SPECIAl_EQUIPMENT': 'Special equipment',
    'PRE_DIET': 'PreDietDiagnosis', // 77
    'ISOLATION_PRECAUTIONS': 'Isolation precautions',
    'SAFETY_FALL_PRE': 'Safety/fall precautions',
    'NON_WEIGHT_BEARING': 'Non-weight-bearing',
    'TOE_TOUCH_WEIGHT_BEARING': 'Toe-touch weight-bearing',
    'PARTIAL_WEIGHT_BEARING': 'Partial weight-bearing',
    'WEIGHT_BEARING_TOLERATED': 'Weight-bearing as tolerated',

    // MedicalInfo
    'NO_YES_UNKNOWN': 'NoYesUnknown',
    'LAB_TYPE': 'PrePLRLT1',
    'STUDY_TYPE': 'PrePDSST1',

    //Functional Status Tab
    // 'EXPRESSION_OF_IDEASWANTS': 'ExpressionOfIdeasWants',
    // 'FUNCTIONAL_COMPREHENSION_SCHEMA': 'UnderstandingVerbalContent',
    // 'BLADDERCONTINENCE': 'BladderContinence',
    // 'BOWELCONTINENCE': 'BowelContinence',

    'EXPRESSION_OF_IDEASWANTS': 'PRE QI Expression',
    'FUNCTIONAL_COMPREHENSION_SCHEMA': 'PRE QI Comprehension',
    'BLADDERCONTINENCE': 'PRE QI Bladder',
    'BOWELCONTINENCE': 'PRE QI Bowel',
    'FUNCTIONAL_STATUS_OPTIONS': 'PRE QI Functional',

    // FIM Tab
    'BLADDER_CONTINENCE': 'Bladder Continence', // 102 and 105
    'DISTANCE_WALK': 'Distance Walk', // 112
    'DISTANCE_WHEEL_CHAIR': 'Distance Wheel Chair', // 114
    'LOCOMOTION_TYPE': 'Locomotion Type', // 116
    'COMPREHENSION_TYPE': 'Comprehension Type', // 118
    'EXPRESSION_TYPE': 'Expression Type', // 119
    'FIM_LEVEL7': 'FIMLookUp',
    'FIM_LEVEL8': 'FIMLookUp',
    'BOWEL_CONTINENCE': 'Bowel Continence',

    // goals
    'REQUIRED_TREATMENTS': 'Required Treatments',
    'OUTPATIENT_THERAPY': 'Outpatient therapy',
    'HOME_HEALTH_SERVICES': 'Home Health Services',
    'SUPPORT_GROUP': 'Support Group',
    'Barriers_To_Discharge': 'Barriers to Discharge',
    'CAREGIVER_ORIENTATION': 'Caregiver Orientation',
    'DISCHARGE_SUPPORT': 'Discharge Support',
    'DISCHARGE_DESTINATION': 'Discharge Destination',

    //email priority
    'EMAIL_PRIORITY': 'Email Priority',

    // PROI APP settings names
    DEFAULT_RECORDS_PERPAGE: 'DEFAULGRIDRECORDSPERPAGE',

    // ICD MMT codes 
    'MMT_FRACTURE_TYPES': 'MMT Fracture Types'
}



