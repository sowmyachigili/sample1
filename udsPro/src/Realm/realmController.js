/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import realm from '../Realm/realm';
import schema from '../Realm/schemaNames';
import dateFormats from '../utility/formatDate';

// common function
import commonFun from '../utility/commonFun';

import config from '../config/config';

export default {
    // For inserting new Record
    insertOrUpdateRecord: (schemaName, body, isUpdate) => {
        realm.write(() => {
            if (isUpdate) {
                realm.create(schemaName, body, isUpdate); // third argument for updating same record
            } else {
                realm.create(schemaName, body);
            }

        });
    },

    // For inserting new Record
    insertOrUpdateRecordWithoutTransaction: (schemaName, body, isUpdate) => {
        if (isUpdate) {
            realm.create(schemaName, body, isUpdate); // third argument for updating same record
        } else {
            realm.create(schemaName, body);
        }
    },

    // For getting all records
    getAllRecords: (schemaName) => {
        return realm.objects(schemaName)
    },

    // Get records based on filter query
    getAllRecordsWithFiltering: (schemaName, filterCase) => {
        return realm.objects(schemaName).filtered(filterCase);
    },

    // Get all records filtering with dates
    getAllRecordsWithFilteringWithDate: (schemaName, filterCase, date) => {
        return realm.objects(schemaName).filtered(filterCase, date);
    },

    // To get ma auto increment id
    getMaxPrimaryKeyId: (schemaName, primaryKey, onlyLastKey) => {
        let lastPrimaryKey = realm.objects(schemaName).max(primaryKey);
        if (onlyLastKey) {
            return lastPrimaryKey;
        } else {
            return lastPrimaryKey = lastPrimaryKey ? lastPrimaryKey + 1 : 1;
        }
    },

    // get single record
    getSingleRecord: (schemaName, Id) => {
        return realm.objectForPrimaryKey(schemaName, Id);
    },

    // Fill dropdown data from category table
    getAllcategoriesDataFromRealm: (facilityCode, categoryName, isCheckbox, valueType) => {

        let categoryListOptionsWithLabelValue = [schema.DESPOSITION_CODE, schema.PREADMISSION_DISCHARGE, schema.MARITAL_STATUS, schema.ARTHRITIS_CONDITIONS, schema.BLADDER_CONTINENCE,
        schema.DISTANCE_WALK, schema.DISTANCE_WHEEL_CHAIR, schema.LOCOMOTION_TYPE, schema.COMPREHENSION_TYPE, schema.EXPRESSION_TYPE, schema.BLADDERCONTINENCE, schema.BOWELCONTINENCE,
        schema.EXPRESSION_OF_IDEASWANTS, schema.FUNCTIONAL_COMPREHENSION_SCHEMA, schema.FUNCTIONAL_STATUS_OPTIONS,
        ];

        let categoryListOptions = [schema.DESPOSITION_CODE, schema.PREADMISSION_DISCHARGE, schema.PRIMARY_CARE_PHY, schema.ATTENDING_PHY, schema.CONSULTING_PHY, schema.REFERRING_PHY, schema.REFERRING_FACILITY, schema.LAB_TYPE, schema.STUDY_TYPE];

        let categoryListItemOptions = [schema.HOME_TYPE, schema.PRE_HOSPITAL_LIVING_2, schema.PREHOSPITAL_LIVE_WITH, schema.HOME_TYPE, schema.BATHROOM_SETUP, schema.HOME_EQU, schema.SELF_CARE,
        schema.PRIOR_FUN, schema.HOME_CARE_SERVICES, schema.SERVICES_PROVIDED, schema.PRE_VOC_CAT, schema.VOCATIONAL_EFFORT, schema.RETURN_TO_SCHOOL, schema.RESOURCE, schema.RESOURCE,
        schema.MARITAL_STATUS, schema.PAYMENT_SOURCE_1, schema.PAYMENT_SOURCE_2, schema.PAYMENT_DETAILS, schema.DISCHARGE_DESTINATION, schema.ADVANCED_DIRE, schema.RACE, schema.ADVANCED_DIRE, schema.VERIFIED_COVERAGE,
        schema.CAREGIVER_AVA, schema.BED_BATH_LOCATION, schema.PRIOR_DEVICE_USE, schema.EDUCATION, schema.OUTPATIENT_THERAPY, schema.REQUIRED_TREATMENTS, schema.HOME_HEALTH_SERVICES, schema.SUPPORT_GROUP,
        schema.Barriers_To_Discharge, schema.CAREGIVER_ORIENTATION, schema.DISCHARGE_DESTINATION, schema.PRE_HOSPITAL_LIVING_1, schema.ARTHRITIS_CONDITIONS,
        schema.BLADDER_CONTINENCE, schema.DISTANCE_WALK, schema.DISTANCE_WHEEL_CHAIR, schema.LOCOMOTION_TYPE, schema.COMPREHENSION_TYPE, schema.EXPRESSION_TYPE, schema.NO_YES_UNKNOWN, schema.MMT_FRACTURE_TYPES,
        schema.BLADDERCONTINENCE, schema.BOWELCONTINENCE, schema.EXPRESSION_OF_IDEASWANTS, schema.FUNCTIONAL_COMPREHENSION_SCHEMA, schema.FUNCTIONAL_STATUS_OPTIONS,
        ];

        if (categoryName && facilityCode) {
            let categoryQuery = '(CategoryFacCode ==[c] ' + '"' + facilityCode + '" OR CategoryFacCode = "" OR CategoryFacCode = null) AND (Name = ' + '"' + categoryName + '")';
            let categories = [];
            categories = realm.objects(schema.DROPDOWNS_SCHEMA).filtered(categoryQuery).sorted('Name', false);

            let allCategories = [];
            categories.forEach((category) => {
                if (category.CategoryDetails) {
                    category.CategoryDetails.forEach(categoryDetail => {
                        let categoryOptions = { ...categoryDetail };
                        if (categoryOptions && categoryOptions.DoNotUse == 0 && categoryOptions.DoNotShow == 0) {
                            if (category.CategoryFacCode) {
                                categoryOptions.CategoryFacCode = category.CategoryFacCode;
                            }
                            categoryOptions.label = categoryOptions.Notes;

                            if (categoryListOptionsWithLabelValue.includes(categoryName)) {
                                if (categoryOptions.Item != '-') {
                                    categoryOptions.label = categoryOptions.Item + ' - ' + categoryOptions.Notes;
                                } else {
                                    categoryOptions.label = categoryOptions.Item + ' ' + categoryOptions.Notes;
                                }
                            }


                            if (categoryListOptions.includes(categoryName)) {
                                categoryOptions.value = categoryOptions.CategoryDetailIPADId;  // primary key
                            } else if (categoryListItemOptions.includes(categoryName)) {
                                categoryOptions.value = categoryOptions.Item;  // passing Item
                            } else {
                                categoryOptions.value = categoryOptions.CategoryDetailID;
                            }
                            if ((valueType == 'string') && (typeof categoryOptions.value == 'number')) {
                                categoryOptions.value = categoryOptions.value.toString()
                            } else if ((valueType == 'number') && (typeof categoryOptions.value == 'string')) {
                                categoryOptions.value = parseInt(categoryOptions.value)
                            }

                            // MMT dropdown filter
                            if (categoryName == schema.MMT_FRACTURE_TYPES) {
                                categoryOptions.value = categoryOptions.Notes;
                                categoryOptions.originalDBValue = categoryOptions.Item;
                            }

                            if (isCheckbox) {
                                categoryOptions.disabled = false;
                                categoryOptions.checked = false;
                                categoryOptions.labelName = categoryOptions.Notes.replace('<br/>', ' ');
                            }
                            allCategories.push(categoryOptions);
                        }
                    });
                }
            });
            // For sorting based on category details sequence no
            allCategories.sort(function (a, b) {
                return a.SequenceNo - b.SequenceNo;
            });
            return allCategories;
        }
    },

    // get selected Category details based on CategoryDetailIPADId
    getSelectedCategoryDetailsBasedOnIPADId: (categoryDetailsIpadId) => {
        return realm.objectForPrimaryKey(schema.DROPDOWN_OPTIONS_SCHEMA, categoryDetailsIpadId);
    },

    // get all imapirment group categories
    getAllImpairmentGroupCategories: (filterQuery) => {
        let filterCase = '(MainGroup == 0 OR MainGroup == 2)';
        if (filterQuery) {
            filterCase += ' AND (' + filterQuery + ')';
        }
        let groups = realm.objects(schema.IMPAIRMENTGROUP_SCHEMA).filtered(filterCase).sorted('PrintSeq', false); // true in descending order and then false in ascending
        let imapirmentGroups = [];
        if (groups && groups.length && groups.length > 0) {
            [...groups].forEach(group => {
                let imapirmentGroup = { ...group };
                imapirmentGroup.label = `${group.IGCode} - ${group.IGDescription}`;
                imapirmentGroup.value = `${group.IGCode}`;

                imapirmentGroups.push(imapirmentGroup);
            });
        }
        return imapirmentGroups;
    },

    // for getting IRFPAI_Version based on assessment
    getIRFPAIVersionBasedOnCurrentDate: () => {
        let currentDate = dateFormats.formatDate('todayDate', 'YYYY-MM-DD');
        let filterQuery = '$0 >= StartDate AND $0 <= EndDate';
        let versions = realm.objects(schema.IRF_PAI_VERSIONS_SCHEMA).filtered(filterQuery, currentDate);
        if (versions && versions.length && versions.length === 1) {
            return versions[0];
        } else {
            return null;
        }
    },

    getPCMGVersionBasedOnCurrentDate: () => {
        let currentDate = dateFormats.formatDate('todayDate', 'YYYY-MM-DD');
        let filterQuery = '$0 >= StartDate AND $0 <= EndDate';
        let versions = realm.objects(schema.CMGGROUPER_SCHEMA).filtered(filterQuery, currentDate);
        if (versions && versions.length && versions.length === 1) {
            return versions[0];
        } else {
            return null;
        }
    },

    // for getting IRFPAI_Version based on assessment
    getICDVersionBasedOnIRFPAIVersion: (IRFVeriosn) => {
        if (IRFVeriosn) {
            let filterQuery = 'IRFPAI_Version = ' + '"' + IRFVeriosn + '"';
            let versions = realm.objects(schema.IRF_PAI_VERSIONS_SCHEMA).filtered(filterQuery, IRFVeriosn);
            if (versions && versions.length && versions.length === 1) {
                return versions[0];
            } else {
                return null;
            }
        }
    },

    // Get site dropdown details from facility site
    getSiteDropdownDetailsFromFacilitySite: (facilityCode) => {
        if (facilityCode) {
            let allSitesFilterQuery = 'FacilityCode ==[c] ' + '"' + facilityCode + '"';
            let siteFacilityDetails = realm.objects(schema.FACILITYSITE_SCHEMA).filtered(allSitesFilterQuery);

            if (siteFacilityDetails && siteFacilityDetails.length && siteFacilityDetails.length > 0) {
                let siteInfo = [];
                let filterQuery = '';
                siteFacilityDetails.forEach((site, index) => {
                    if (site && site.SiteID) {
                        if (index > 0) {
                            filterQuery += ' OR ';
                        }
                        if (index === 0) {
                            filterQuery += '(DeletedBy == null) AND ('
                        }
                        filterQuery += 'SiteID = ' + '"' + site.SiteID + '"';
                        if (index === siteFacilityDetails.length - 1) {
                            filterQuery += ")"
                        }
                    }
                });
                if (filterQuery) {
                    siteInfo = realm.objects(schema.SITE_SCHEMA).filtered(filterQuery);
                }
                let dropdownSiteList = [];

                siteInfo.forEach(site => {
                    dropdownSiteList.push({ label: `${site.SiteCode}-${site.SiteDescription}`, value: site.SiteCode, siteId: site.SiteID })
                });
                return dropdownSiteList;
            }
        }
    },

    // get unit dropdown values from site and facility deails
    getUnitDropdownValuesFromSiteAndFacilityDetails: (facilityDetails, siteId) => {
        if (facilityDetails && facilityDetails.HasSite && facilityDetails.HasUnit) {
            if (siteId) {
                let unitFilterQuery = 'DeletedBy == null AND SiteID = ' + '"' + siteId + '"';
                let unitInfo = [];
                unitInfo = realm.objects(schema.UNIT_SCHEMA).filtered(unitFilterQuery);
                let unitList = [];
                unitInfo.forEach(unit => {
                    if (unit && unit.UnitCode) {
                        unitList.push({ label: `${unit.UnitCode}-${unit.UnitDescription}`, value: unit.UnitCode });
                    }
                });
                return unitList;
            }
        }
    },

    getCustomTabDropDownRecords: (categoryId, facilityCode) => {
        if (categoryId && facilityCode) {
            let categoryQuery = '(CategoryFacCode ==[c] ' + '"' + facilityCode + '" OR CategoryFacCode = "" OR CategoryFacCode = null) AND (CategoryID = ' + '"' + categoryId + '")';

            let dropdowns = realm.objects(schema.DROPDOWNS_SCHEMA).filtered(categoryQuery);
            let response = [];
            dropdowns.forEach(data => {
                if (data && data && data.CategoryDetails) {
                    let category = data.CategoryDetails;
                    for (let i = 0; i < category.length; i++) {
                        let total = { ...category[i] };
                        total.label = `${total.Item} - ${total.Notes}`;
                        total.value = total.Item;
                        response.push(total);
                    }
                }
            })
            return response;
        } else {
            return [];
        }

    },

    //get to email dropdown values from email schema
    getEmailDropdownValues: () => {
        let emailInfo = [];
        emailInfo = realm.objects(schema.EMAIL_ADDRESSES);
        let emailList = [];
        emailInfo.forEach(emailAddress => {
            if (emailAddress && emailAddress.UserID) {
                emailList.push({ label: `${emailAddress.RealName}, ${emailAddress.Email}`, value: emailAddress.UserID, email: emailAddress.Email });
            }
        });
        return emailList;
    },


    getcategoryNotesByCategoryNameRealm: (facilityCode, categoryName, isCheckbox, filterField, filterValue, checkboxlist) => {
        if (categoryName && facilityCode) {
            let categoryQuery;
            let categories = [];
            if (categoryName == 'Race') {
                categories = JSON.parse(JSON.stringify(config.RaceOrEthnicityList));
            } else {
                categoryQuery = '(CategoryFacCode ==[c] ' + '"' + facilityCode + '" OR CategoryFacCode = "" OR CategoryFacCode = null) AND (Name = ' + '"' + categoryName + '")';
                categories = realm.objects(schema.DROPDOWNS_SCHEMA).filtered(categoryQuery);
            }

            var CategoryNameData;

            var checkBoxlistNotes = '';
            if (filterField && filterValue || filterValue == "0") {
                categories.forEach((category, index) => {
                    if (categoryName == 'Race' && checkboxlist) {
                        let selectedItems = filterValue;
                        if (category.labelName && Number(selectedItems[index]) == 1) {
                            let NotesOptions = category.labelName.replace(",", ", ");
                            if (checkBoxlistNotes != '') {
                                checkBoxlistNotes = checkBoxlistNotes + ', ';
                            }
                            checkBoxlistNotes = checkBoxlistNotes + NotesOptions;
                        }
                    } else {
                        if (category.CategoryDetails) {
                            if (checkboxlist) {
                                let selectedItems;

                                // for changing race checklist order according to the dropdown list in realm database 
                                if (categoryName == 'Race_Ethnicity') {
                                    selectedItems = commonFun.changetoOriginalOrder(filterValue);
                                } else {
                                    selectedItems = Array.from(filterValue);
                                }
                                category.CategoryDetails.forEach((categoryDetail, i) => {
                                    let categoryOptions = { ...categoryDetail };
                                    if (categoryOptions.Notes && Number(selectedItems[i]) == 1) {
                                        let NotesOptions = categoryOptions.Notes;
                                        NotesOptions = categoryOptions.Notes.replace(",", ", ");
                                        if (checkBoxlistNotes != '') {
                                            checkBoxlistNotes = checkBoxlistNotes + ', ';
                                        }

                                        checkBoxlistNotes = checkBoxlistNotes + NotesOptions;
                                    }
                                });
                            } else {
                                CategoryNameData = category.CategoryDetails.filter(function (categoryDetail) {
                                    let categoryOptions = { ...categoryDetail };
                                    if (categoryOptions[filterField] == filterValue.toString()) {
                                        let Notes = categoryOptions.Notes;
                                        Notes = categoryOptions.Notes.replace(",", ", ");
                                        return Notes;
                                    }
                                });
                            }
                        }
                    }
                });

            }
            if (checkboxlist) {
                return checkBoxlistNotes;
            } else if (CategoryNameData && CategoryNameData[0] && CategoryNameData[0].Notes) {
                return CategoryNameData[0].Notes;
            } else {
                return '';
            }
        }

    },

    // get time details based on timezone
    getTimeDetailsBasedOnSelectedTimezone: (timezone) => {
        if (timezone) {
            // get value between paranthesis
            regEx = /\((.*)\)/;
            let values = timezone.match(regEx);
            if (values && values[1]) {
                let filterQuery = 'DeletedBy == null AND Timezone = ' + '"' + values[1] + '"';
                let timeZoneDetails = [];
                timeZoneDetails = realm.objects(schema.UTC_TIMEZONE_OFFSET).filtered(filterQuery);
                let timeInfo = [...timeZoneDetails];
                if (timeInfo && timeInfo.length && timeInfo.length == 1) {
                    return timeInfo[0].OffsetHours;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    },


    // Get Pre-admission dates for case listing AdmissionDate condition
    getPreAdmissionFiltering: (patIPADID) => {
        let filterCase = 'patIPADID ==[c] "' + patIPADID + '"';
        let data = realm.objects(schema.PREADMISSION_SCHEMA).filtered(filterCase);
        if (data && data.length > 0) {
            let pradmissionDates = {
                AssessmentDate: data[0].AssessmentDate,
            }
            return pradmissionDates;
        } else {
            return null;
        }
    },
}