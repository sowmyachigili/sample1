/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Image, Alert } from 'react-native';
import {
    Container, Header, Body, Title, Button, Left, Right
} from 'native-base';

import { Navigation } from 'react-native-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from '@react-native-community/netinfo';

import config from '../../config/config';
import configMessage from '../../config/configMessages';
import configImage from '../../config/configImages';
import api from '../../config/apiCalls';

import MainHeader from '../../components/mainHeader/mainHeader';

// common fun
import common from '../../utility/commonFun';

// styles
import headerStyles from '../../components/styles/headerStyles';
import modalStyles from '../../components/styles/modalStyles';
import DocStyles from '../../components/styles/PreAdminDocStyles';

// Realm
import realmController from '../../Realm/realmController';
import schema from '../../Realm/schemaNames';

// document
import SingleCaseDocScreen from './SingleCaseDocScreen';

// Loader
import Loader from '../../components/common/loaderNew';

// navigation to home screen
import navigation from '../../utility/Navigation';

// DateFormats
import dateFormats from '../../utility/formatDate';

// commonInfomodal
import CommonInfoModal from '../Common/CommonInfoModal';

//styles
import globalStyles from '../../components/styles/styles';

import SynchronizationFullDataOnSave from '../Synchronization/SynchronizationFullDataOnSave';
import realm from '../../Realm/realm';

class CompareCaseListingConflictsScreen extends Component {
    constructor(props) {
        super(props);
        Navigation.events().bindComponent(this);
        this.state = {
            caseListingConflictRecords: [],
            caseListingConflictRecordsFromApi: {},
            admissionID: '',
            isLoading: false,
            isOpenCommonInfoModal: false,
            infoMessage: '',
            modalAction: '',
            modalBody: '',
            modalHeading: '',
            confirmActionBody: '',
            modalIconType: '',
            isInternetConnected: true,
            autoSyncProgress: this.props.autoSyncProgress
        }
    }

    componentDidAppear = () => {
        AsyncStorage.setItem('screenName', JSON.stringify(navigation.COMPARE_CASELISTING_CONFLICTS));
    }

    // back button handler
    backBtnHandler = () => {
        navigation.popScreenNavigator(this.props.componentId);
    }

    componentDidMount = async () => {
        if (this.props.patID) {
            // this.setState({
            //     isLoading: true
            // });


            // let caseLocal = await this.getSingleConflictRecordByIDFromLocal(this.props.patID);
            let caseServer = {};
            caseServer = await this.getSingleConflictRecordByIDFromServer(this.props.patID);

            // update local record
            // await this.updateLocalRecord(caseLocal, caseServer);
            await this.singleDocServerRef.getPropsDetails(caseServer);
            this.setState({
                // caseListingConflictRecords: caseLocal,
                caseListingConflictRecordsFromApi: caseServer
            });
        }
        // check internet connection
        // NetInfo.addEventListener(state => {
        //     this.handleNetConnection(state);
        // });
        // NetInfo.fetch().then(state => {
        //     this.handleNetConnection(state);
        // });
    }

    // handleNetConnection = (state) => {
    //     if (state && state.type) {
    //         this.setState({
    //             isInternetConnected: state.isConnected
    //         })
    //     }
    // }

    // updateLocalRecord = async (localRecord, serverRecord) => {
    //     if (this.singleDocLocalRef) {
    //         let caseLocal = localRecord;
    //         await this.singleDocLocalRef.getPropsDetails(caseLocal, serverRecord);
    //         await this.updateServerRecord(serverRecord, localRecord);

    //     }
    // }
    // updateServerRecord = async (serverRecord, localRecord) => {
    //     if (this.singleDocServerRef) {
    //         let caseLocal = localRecord;
    //         await this.singleDocServerRef.getPropsDetails(serverRecord, caseLocal);
    //     }
    // }

    getIsLoadingValue = (isLoading) => {
        setTimeout(() => {
            this.setState({
                isLoading: isLoading
            })
        }, 200)
    }

    // get preadmission conflict local record based on preadmission id
    getSingleConflictRecordByIDFromLocal = (patID) => {
        let localRecord;
        if (patID) {
            let filterCriteria = 'PatID == ' + patID;
            localRecord = realmController.getAllRecordsWithFiltering(schema.CASELISTING_SCHEMA, filterCriteria);
        }
        return localRecord;
    }

    // get preadmission conflict server record based on preadmission id
    getSingleConflictRecordByIDFromServer = (patID) => {
        let serverRecord;
        let data = {
            UserID: this.props.loggedInUserDetails.UserID,
            FacilityCode: this.props.loggedInUserDetails.FacilityCode,
            PatID: patID
        }

        return fetchMethodRequest('POST', api.getCaseListingData, data)
            .then(async (response) => {
                if (response == config.invalidGUIDCode) { // 401 response
                    this.displayTokenExpiredOrInvalidGuidMsg('compare');
                } else if (response == config.generateGuidErrorCode) { //guid fail
                    this.displayTokenExpiredOrInvalidGuidMsg('Fail');
                } else {
                    if (response && response.length > 0) {
                        serverRecord = { ...response[0].Patient };
                        return serverRecord;
                    } else {
                        return null;
                    }
                }
            });
    }

    // Open common info modal
    openCommonInfoModal = (modalHeading, iconType, modalAction, confirmActionBody, message) => {
        this.setState({
            isLoading: false,
            isOpenCommonInfoModal: true,
            infoMessage: message ? message : '',
            modalIconType: iconType,
            modalHeading: modalHeading ? modalHeading : '',
            modalAction: modalAction ? modalAction : '',
            confirmActionBody: confirmActionBody ? confirmActionBody : ''
        });
    }

    // Hide common modal
    hideCommonInfoModal = (body) => {
        if (body) {
            this.setState({
                isOpenCommonInfoModal: false,
                isLoading: false
            });
            if (body == 'MoveToLogin' || body == 'appVersionChange' || body == 'dbVersionChange') {
                setTimeout(() => {
                    navigation.popToTopScreenNavigator(this.props.componentId);
                }, 150)

            } else {
                navigation.popScreenNavigator(this.props.componentId);
            }
        } else {
            this.setState({
                isLoading: false,
                isOpenCommonInfoModal: false
            });
        }
    }

    displayTokenExpiredOrInvalidGuidMsg = (messageType) => {
        let message = '';
        if (messageType && messageType == 'Fail') {
            mesage = configMessage.unableToGenarateGUIDMessageFromConflictScreen;
        } else if (messageType == 'compare') {
            message = configMessage.invalidTokenMsgOnCompare;
        } else {
            message = configMessage.invalidTokenMsgCoflictScreen;
        }
        if (message && messageType == 'compare') {
            // if we will get any error from api when click on compare
            this.openCommonInfoModal(config.validationSummary, 'Warning', '', 'compare', message);
        } else {
            this.openCommonInfoModal(config.validationSummary, 'Warning', '', '', message);
        }
    }

    // set isConflicts value to false click on keep local version button
    onClickLocalVersionButton = async (caselistingConflictRecordLocal) => {
        if (caselistingConflictRecordLocal && this.state.isInternetConnected) {
            if (this.state.autoSyncProgress == 100) {
                this.setState({
                    isLoading: true
                }, async () => {
                    let responseMessage = await this.props.caseListingSaveWithMofiedDetails([caselistingConflictRecordLocal], 'Conflict Record');

                    // For display message to user when invalid token or invalid guid
                    if (responseMessage && responseMessage == config.invalidGUIDCode) {
                        this.displayTokenExpiredOrInvalidGuidMsg();
                    } else if (responseMessage == config.generateGuidErrorCode) { //guid fail
                        this.displayTokenExpiredOrInvalidGuidMsg('Fail');
                    } else if (responseMessage != 'serverError') {
                        let responseInfo = await this.synchronizeFullData();
                        if (!responseInfo) {
                            if (this.props.screenType == 'Advanced') {
                                if (this.props.getAllExistingICDCodeValues) {
                                    this.props.getAllExistingICDCodeValues('load');
                                }
                            }
                            navigation.popScreenNavigator(this.props.componentId);
                            this.setState({
                                isLoading: false
                            });
                        }
                    }
                })
            } else {
                this.openCommonInfoModal(configMessage.autoSyncInProgressMsg, 'Warning', '', '', '');
                return;
            }
        } else {
            this.openCommonInfoModal(configMessage.messageForResolveConflicts, 'Warning', '', '', '');
        }
    }

    displayValidationForVersionChange = (responseInfo) => {
        if (responseInfo == 'databaseChanged') {
            this.openCommonInfoModal(configMessage.databaseChangeErrorMessage, 'Warning', '', 'dbVersionChange', '');
        } else if (responseInfo == 'appVersionChanged') {
            this.openCommonInfoModal(configMessage.appVersionChangeErrorMessage, 'Warning', '', 'appVersionChange', '');
        }
    }

    synchronizeFullData = async () => {
        let responseInfo = await SynchronizationFullDataOnSave.synchronizeFullData(this.props.loggedInUserDetails);
        if (responseInfo == 'userModified') {
            this.setState({
                isLoading: false
            })
            this.openCommonInfoModal(configMessage.userModifiedMessage, 'Warning', '', 'MoveToLogin', '');
            return responseInfo;
        } else if (responseInfo == 'serverError') {
            this.setState({
                isLoading: false
            })
            return responseInfo;
        } else if (responseInfo == 'databaseChanged' || responseInfo == 'appVersionChanged') {
            this.displayValidationForVersionChange(responseInfo);
            this.setState({
                isLoading: false
            })
            return responseInfo;
        } else {
            if (this.mainHeaderRef) {
                this.mainHeaderRef.getAppNotificationsWhenSync();
            }
            return null;
        }
    }

    // set isConflicts value to false click on keep server version button
    onClickServerVersionButton = async (caselistingConflictRecordServer) => {
        if (caselistingConflictRecordServer) {

            let caseObject = common.formatCaseListingData(caselistingConflictRecordServer, 'update');
            let caselistingOfflineObj = { ...caseObject };
            caselistingOfflineObj.IsConflictExists = false;
            if (this.state.isInternetConnected) {
                if (this.state.autoSyncProgress == 100) {
                    await this.setState({
                        isLoading: true
                    }, async () => {
                        let responseInfo = await this.synchronizeFullData();
                        if (!responseInfo) {
                            let caselistingConflictRecordLocal = this.state.caseListingConflictRecords && this.state.caseListingConflictRecords[0] ?
                                this.state.caseListingConflictRecords[0] : {};
                            await this.compareLocalAndServerInfoForInterim_Followup(caselistingConflictRecordServer, caselistingConflictRecordLocal);
                            realmController.insertOrUpdateRecord(schema.CASELISTING_SCHEMA, caselistingOfflineObj, true);
                            if (this.props.screenType == 'Advanced') {
                                if (this.props.getAllExistingICDCodeValues) {
                                    this.props.getAllExistingICDCodeValues('load');
                                }
                            }
                            navigation.popScreenNavigator(this.props.componentId);
                            await this.setState({
                                isLoading: false
                            });
                        }
                    });
                } else {
                    this.openCommonInfoModal(configMessage.autoSyncInProgressMsg, 'Warning', '', '', '');
                    return;
                }
            } else {
                realmController.insertOrUpdateRecord(schema.CASELISTING_SCHEMA, caselistingOfflineObj, true);
                navigation.popScreenNavigator(this.props.componentId);
            }
        }
    }

    // Remove Interim/Followup from local if records are not existing in server
    compareLocalAndServerInfoForInterim_Followup = (caselistingConflictRecordServer, caselistingConflictRecordLocal) => {
        if (caselistingConflictRecordServer && caselistingConflictRecordLocal) {
            if (caselistingConflictRecordServer.FIM && caselistingConflictRecordServer.FIM.length > 0 &&
                caselistingConflictRecordLocal.FIM && caselistingConflictRecordLocal.FIM.length > 0) {
                let serverCaseFim = [...caselistingConflictRecordServer.FIM];
                let localCaseFim = [...caselistingConflictRecordLocal.FIM];
                if (localCaseFim && localCaseFim.length > 0) {
                    // Compare Local and serverVesrion records for Interim/Followup 
                    localCaseFim.forEach((singleFim) => {
                        if (singleFim && singleFim.EntityType && (config.checkServerVersionTypes.indexOf(singleFim.EntityType) == -1)) {
                            let indexofFim = serverCaseFim.findIndex((fim) => (fim.EntityType == singleFim.EntityType && fim.Date == singleFim.Date));
                            if (indexofFim == -1) {
                                let getRecord = realmController.getSingleRecord(schema.CASE_FIM_SCHEMA, singleFim.FIMIPADID);
                                realm.write(() => {
                                    realm.delete(getRecord);
                                });
                            }
                        }
                    });
                }
            }
        }
    }

    // check and update autoSync progress status in mainheader
    checkAutoSyncProgress = (type, syncProgressInOffline) => {
        let autoSyncProgress = this.props.checkAutoSyncProgressForStatus(type, syncProgressInOffline);
        if (type) {
            this.setState({
                autoSyncProgress: syncProgressInOffline
            })
            this.updateMainHeaderProgress(autoSyncProgress)
            return autoSyncProgress

        } else {
            if (autoSyncProgress) {
                this.setState({
                    autoSyncProgress: autoSyncProgress
                })
                this.updateMainHeaderProgress(autoSyncProgress)
                return autoSyncProgress;
            }
        }
    }

    updateMainHeaderProgress = (autoSyncProgress) => {
        if (this.mainHeaderRef) {
            this.mainHeaderRef.updateSyncProgress(autoSyncProgress);
        }
    }

    render() {
        return (
            <Container style={{ backgroundColor: config.addScreenbackgroundColor }} 
            onTouchStart={() => common.activateSession()} >
                <Loader loading={this.state.isLoading} />

                <Text style={globalStyles.topStatusBarColor} />
                {/* header1 */}
                <MainHeader
                    homeScreenNavigatorId={this.props.homeScreenNavigatorId}
                    navigatorProps={this.props.componentId}
                    autoSyncProgress={this.state.autoSyncProgress}
                    checkAutoSyncProgressForStatus={this.checkAutoSyncProgress}
                    onlineSyncAutoSuccess={this.state.isInternetConnected}
                    autoSync={true}
                    onRef={ref => (this.mainHeaderRef = ref)}
                    userInfo={this.props.loggedInUserDetails}
                    page={navigation.COMPARE_CASELISTING_CONFLICTS}
                    displayValidationForDBOrAppVersionChange={this.displayValidationForVersionChange} />
                <Header style={headerStyles.subHeader}>
                    <Left>
                        <TouchableOpacity onPress={this.backBtnHandler} >
                            <Image source={configImage.backButtonIcon} style={headerStyles.backBtn} />
                        </TouchableOpacity>
                    </Left>
                    <Right>
                        <Title style={headerStyles.listHeading}>Case Listing Document</Title>
                    </Right>
                </Header>
                {/* <Header style={headerStyles.headSpaceRemove}>
                    <Body>
                    </Body>
                </Header> */}

                {/* case document screen start */}
                <View style={[modalStyles.modal, { flex: 1 }]}>

                    {/* <View style={modalStyles.modalHead}>
                        <View style={{ flexDirection: 'column' }}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={modalStyles.leftContainer}>
                                    <Text style={[{ textAlign: 'left' }]}>
                                        {''}
                                    </Text>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', width: '100%' }}>
                                <View style={DocStyles.halfOneNew}>
                                    <Text style={[modalStyles.modalHeading, { textAlign: 'center' }]}>Local Version</Text>
                                </View>
                                <View style={[DocStyles.halfOneNew]}>
                                    <Text style={[modalStyles.modalHeading, { textAlign: 'center' }]}>Server Version</Text>
                                </View>
                            </View>
                        </View>
                    </View> */}
                    <View style={[modalStyles.modalBody, { flex: 1, flexDirection: 'row' }]}>
                        <ScrollView
                            ref={ref => (this.scrollViewRef = ref)}
                            onScroll={this.handleOnScroll}
                            scrollEventThrottle={16}>

                            <View style={[DocStyles.row, DocStyles.marginBottom10, { flex: 1 }]}>
                                {/* <View style={DocStyles.halfOneNew}>
                                    <View style={[DocStyles.row, DocStyles.marginBottom10]}>
                                        <Text style={[DocStyles.halfOne, DocStyles.lableClass]}>Last Modified</Text>
                                        <Text style={[DocStyles.halfTwo, DocStyles.textStyle]}>
                                            {this.state.caseListingConflictRecords &&
                                                this.state.caseListingConflictRecords[0] && this.state.caseListingConflictRecords[0].UpdatedOn ?
                                                dateFormats.formatDate(this.state.caseListingConflictRecords[0].UpdatedOn, config.lastModifiedDateFormat) + ' EST'
                                                : this.state.caseListingConflictRecords && this.state.caseListingConflictRecords[0] &&
                                                    this.state.caseListingConflictRecords[0].InsertedOn ?
                                                    dateFormats.formatDate(this.state.caseListingConflictRecords[0].InsertedOn, config.lastModifiedDateFormat) + ' EST' : null}
                                        </Text>
                                    </View>

                                    <SingleCaseDocScreen loggedInUserDetails={this.props.loggedInUserDetails}
                                        caseDetails={this.state.caseListingConflictRecords}
                                        caseDetailsOther={this.state.caseListingConflictRecordsFromApi}
                                        server={false}
                                        onRef={ref => (this.singleDocLocalRef = ref)}
                                        getIsLoadingValue={this.getIsLoadingValue}
                                    />

                                </View> */}
                                <View style={[DocStyles.halfOneNew]}>
                                    {/* DocStyles.borderLeft */}
                                    <View style={[DocStyles.row, DocStyles.marginBottom10]}>
                                        <Text style={[DocStyles.halfOne, DocStyles.lableClass]}>Last Modified</Text>
                                        <Text style={[DocStyles.halfTwo, DocStyles.textStyle]}>
                                            {this.state.caseListingConflictRecordsFromApi && this.state.caseListingConflictRecordsFromApi.UpdatedOn ?
                                                dateFormats.formatDate(this.state.caseListingConflictRecordsFromApi.UpdatedOn, config.lastModifiedDateFormat) + ' EST'
                                                : this.state.caseListingConflictRecordsFromApi && this.state.caseListingConflictRecordsFromApi.InsertedOn ?
                                                    dateFormats.formatDate(this.state.caseListingConflictRecordsFromApi.InsertedOn, config.lastModifiedDateFormat) + ' EST' : null}
                                        </Text>
                                    </View>
                                    <SingleCaseDocScreen 
                                        loggedInUserDetails={this.props.loggedInUserDetails}
                                        caseDetails={this.state.caseListingConflictRecordsFromApi}
                                        // caseDetailsOther={this.state.caseListingConflictRecords}
                                        server={true}
                                        onRef={ref => (this.singleDocServerRef = ref)}
                                    />

                                </View>
                            </View>

                        </ScrollView>
                    </View>
                    {/* <View style={[styles.footer]}>
                        <Button style={[modalStyles.btnDefault, modalStyles.buttonLight]}
                            onPress={() => this.onClickLocalVersionButton(this.state.caseListingConflictRecords[0])}>
                            <Text style={[modalStyles.btnDefaultText, modalStyles.buttonLightText]}>Keep Local Version</Text>
                        </Button>
                        <Button style={[modalStyles.btnDefault, modalStyles.buttonLight]}
                            onPress={() => this.onClickServerVersionButton(this.state.caseListingConflictRecordsFromApi)}>
                            <Text style={[modalStyles.btnDefaultText, modalStyles.buttonLightText]}>Push Server Version</Text>
                        </Button>
                    </View> */}
                </View>
                {/* case document screen end */}

                {/* Common info modal start */}
                <CommonInfoModal
                    iconType={this.state.modalIconType}
                    isOpenCommonInfoModal={this.state.isOpenCommonInfoModal}
                    displayMessage={this.state.infoMessage}
                    hideCommonInfoModal={this.hideCommonInfoModal}
                    modalAction={this.state.modalAction}
                    modalHeading={this.state.modalHeading}
                    confirmActionBody={this.state.confirmActionBody} />
                {/* common info modal end */}

            </Container >
        );
    }
}



const styles = StyleSheet.create({
    lists: {
        borderColor: '#70707026',  // 0.15 opacity applied
        borderBottomWidth: 0.5,
        marginLeft: 0,
    },
    button: {
        height: 35,
        backgroundColor: config.blueButtonColor,
        borderRadius: 5,
    },
    btnText: {
        color: '#ffffff',
        fontSize: 16,
        fontFamily: 'Roboto-Medium',
        paddingLeft: 12,
        paddingRight: 12,
    },
    footer: {
        marginBottom: 30,
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },

});

export default CompareCaseListingConflictsScreen;