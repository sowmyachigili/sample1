/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import React, { Component } from 'react';
import { View, FlatList, Alert } from 'react-native';
import { Text, ListItem } from 'native-base';
import { Table, Row, TableWrapper, Cell } from 'react-native-table-component';

import config from '../../config/config';

// Realm
import realmController from '../../Realm/realmController';
import schema from '../../Realm/schemaNames'

// Components
import dateFormats from '../../utility/formatDate';
import commonFun from '../../utility/commonFun';

// Styles
import DocStyles from '../../components/styles/PreAdminDocStyles';

class SingleCaseDocScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: [],
            tableHead: ['FIM® Instrument (FIM® Screen)'],
            tableSubHead: ['Admission'],
            caseListDetails: {},
            caseDetailsOther: {},
            loggedInUserDetails: this.props.loggedInUserDetails,
            newCaseList: [],
            newAdmissionList: [],
            newComorbidList: [],
        };
        this.inputRefs = {};

    }

    componentDidMount = () => {
        this.props.onRef(this);
    }

    componentWillUnmount = () => {
        this.props.onRef(null)
    }

    getPropsDetails = (localRecord, serverRecord) => {
        let Case, CaseOther = [];
        if (localRecord) {
            setTimeout(async () => {
                if (this.props.server) {
                    Case = localRecord;
                    // if (serverRecord && serverRecord[0]) {
                    //     CaseOther = serverRecord[0];
                    // }
                } else {
                    Case = localRecord && localRecord[0] ? localRecord[0] : null;

                    // if (serverRecord) {
                    //     CaseOther = serverRecord;
                    // }

                }
                // All selct boxes
                let Site = realmController.getSiteDropdownDetailsFromFacilitySite(this.state.loggedInUserDetails.FacilityCode);
                this.setState({
                    Site: Site,
                });

                let newCaseArray = [], patientInfoObject = [], newObj;
                if (Case) {
                    newCaseArray = Object.keys(Case);

                    for (let i = 0; i < newCaseArray.length; i++) {
                        if (newCaseArray[i] == 'FirstName' || newCaseArray[i] == 'DOB' || newCaseArray[i] == 'AdmissionDate' || newCaseArray[i] == 'Site' || newCaseArray[i] == 'Unit') {
                            newObj = {
                                displayField: newCaseArray[i],
                                value: Case[newCaseArray[i]],
                                color: Case[newCaseArray[i]] == (CaseOther && CaseOther[newCaseArray[i]] ? CaseOther[newCaseArray[i]] : '') ? 'black' : 'green'
                            }
                            if (newCaseArray[i] == 'DOB' || newCaseArray[i] == 'AdmissionDate') {
                                let OtherDate = (CaseOther && CaseOther[newCaseArray[i]] ? CaseOther[newCaseArray[i]] : '');
                                let dateComparision = dateFormats.datesComparisionSame(Case[newCaseArray[i]], OtherDate);
                                newObj.color = dateComparision ? 'black' : 'green';
                            }
                            if (newObj.value) {
                                if (newObj.displayField == 'FirstName') {
                                    newObj.displayField = 'Name';
                                    if (Case['LastName']) {
                                        newObj.value = Case['FirstName'] + ',' + Case['LastName'];
                                        if (newObj.color == 'black') {
                                            newObj.color = Case['LastName'] == (CaseOther && CaseOther['LastName'] ? CaseOther['LastName'] : '') ? 'black' : 'green';
                                        }

                                    } else {
                                        newObj.value = Case['FirstName'] + ',' + Case['LastName'];
                                    }

                                }
                                if (newObj.displayField == 'AdmissionDate') {
                                    newObj.displayField = 'Admission Date';
                                    newObj.value = dateFormats.formatDate(Case[newCaseArray[i]], config.dateFormat)
                                }
                                if (newObj.displayField == 'DOB') {
                                    newObj.displayField = 'DOB';
                                    newObj.value = dateFormats.formatDate(Case[newCaseArray[i]], config.dateFormat)
                                }

                                if (newObj.displayField == 'Site') {
                                    let siteId = this.getSiteIdOfSite(Case[newCaseArray[i]]);
                                    newObj.value = siteId ? commonFun.getSiteItemByValue(siteId) : '';
                                }
                                if (newObj.displayField == 'Unit') {
                                    let siteIdForUnit = this.getSiteIdOfSite(Case['Site']);
                                    newObj.value = siteIdForUnit ? commonFun.getUnitItemByValue(this.state.loggedInUserDetails.FacilityDetails, siteIdForUnit, Case[newCaseArray[i]]) : '';
                                }
                                patientInfoObject.push(newObj);
                            }
                        }
                    }
                }


                // admission data in case
                let newCaseAdmissionArray = [], admissionObject = [];
                let comorbidconditionsObject = [], newComborbid = [];
                let ComorbidConditions = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
                    'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y'];
                if (Case && Case.Admission && Case.Admission[0]) {
                    newCaseAdmissionArray = Object.keys(Case.Admission[0]);

                    for (let i = 0; i <= newCaseAdmissionArray.length; i++) {
                        if (newCaseAdmissionArray[i] == 'ImpairmentGroup' && Case.Admission[0][newCaseAdmissionArray[i]]) {
                            newObj = {
                                displayField: newCaseAdmissionArray[i],
                                value: Case.Admission[0][newCaseAdmissionArray[i]],
                                // color: Case.Admission[0][newCaseAdmissionArray[i]] == (CaseOther && CaseOther.Admission && CaseOther.Admission[0] && CaseOther.Admission[0][newCaseAdmissionArray[i]]) ? CaseOther.Admission[0][newCaseAdmissionArray[i]] : '' ? 'black' : 'green'
                            }
                            if (newObj.value) {
                                if (newObj.displayField == 'ImpairmentGroup') {
                                    newObj.displayField = 'Impairment Group';
                                    newObj.value = commonFun.getImpairMentGroupItemByValue(Case.Admission[0][newCaseAdmissionArray[i]]);
                                }
                                admissionObject.push(newObj);
                            }
                        }
                    }

                    for (let i = 0; i < newCaseAdmissionArray.length; i++) {
                        if (newCaseAdmissionArray[i] && Case.Admission[0]['ComorbidConditions' + (i + 1)]) {

                            newComborbid = {
                                displayField: ComorbidConditions[i],
                                value: Case.Admission[0]['ComorbidConditions' + (i + 1)],
                                // color: Case.Admission[0]['ComorbidConditions' + (i + 1)] == ((CaseOther && CaseOther.Admission && CaseOther.Admission[0]) ? CaseOther.Admission[0]['ComorbidConditions' + (i + 1)] : '') ? 'black' : 'green'
                            }
                            if (newComborbid.value) {
                                comorbidconditionsObject.push(newComborbid);
                            }
                        }
                    }
                }

                let caseLocal = await this.formatData(Case);
                // let caseOther = await this.formatData(CaseOther);
                this.setState({
                    caseListDetails: Case,
                    // caseDetailsOther: CaseOther,
                    newCaseList: patientInfoObject,
                    newAdmissionList: admissionObject,
                    newComorbidList: comorbidconditionsObject,
                    // tableDataOther: caseOther,
                    tableData: caseLocal
                })

            }, 0);
        }
    }

    // get unit data based on site id
    getSiteIdOfSite = (Site) => {
        let sideDetails = {};
        if (this.state.Site && this.state.Site.length && this.state.Site.length > 0) {
            sideDetails = [...this.state.Site].find(site => site.value == Site);
        }
        if (sideDetails && sideDetails.siteId) {
            return sideDetails.siteId;
        }
    }

    // format data in document
    formatData = async (caseListDetails) => {
        let FIM;
        if (caseListDetails && caseListDetails.FIM && caseListDetails.FIM.length) {
            FIM = [...caseListDetails.FIM];

            // sort based on date
            FIM.sort(function (a, b) {
                return new Date(a.Date) - new Date(b.Date)
            })

            // sort based on entity type
            FIM.sort(function (a, b) {
                return a.EntityType - b.EntityType
            });

        }
        let fimObj = {}, modifiedFimTableData = [];
        if (FIM && FIM.length) {
            let interimCount = 1, followupCount = 1;

            FIM.forEach(async (single) => {
                let isFieldsNotEmpty = commonFun.checkFimFieldsEmptyOrNot({ ...single }, 'caseConflict');
                if (single && single.EntityType &&
                    (single.EntityType != config.FimGoalEntityType &&
                        single.EntityType != config.CaseListingPreadmitEntityType)
                    && single.DeletedBy == null && isFieldsNotEmpty) {
                    fimObj = { ...single };
                    let tableSubHead = '', fimId = '';

                    if (fimObj.EntityType == config.FimAdmissionEntityType) {
                        tableSubHead = 'Admission';
                    } else if (fimObj.EntityType == config.FimDischargeEntityType) {
                        tableSubHead = 'Discharge';
                    } else if (fimObj.EntityType == config.FimInterimEntityType) {
                        tableSubHead = 'Interim - ' + interimCount;
                        interimCount = interimCount + 1;
                    } else if (fimObj.EntityType == config.FimFollowUpEntityType) {
                        tableSubHead = 'Follow-up - ' + followupCount;
                        followupCount = followupCount + 1;
                    }
                    fimId = fimObj.FIMID;
                    let tableData = [['Bladder Level of Assistance', '', 'BladderAssistance', 'FIM_LEVEL7'],
                    ['Bladder Frequency of Accidents', '', 'BladderFrequency', 'BLADDER_CONTINENCE'],
                    ['Bowel Level of Assistance', '', 'BowelAssistance', 'FIM_LEVEL7'],
                    ['Bowel Frequency of Accidents', '', 'BowelFrequency', 'BOWEL_CONTINENCE'],
                    ['Tub Transfer', '', 'TubTransfer', 'FIM_LEVEL7'],
                    ['Shower Transfer', '', 'ShowerTransfer', 'FIM_LEVEL7'],
                    ['Distance Walked', '', 'DistanceWalked', 'DISTANCE_WALK'],
                    ['Distance Traveled in Wheelchair', '', 'DistanceWheelchair', 'DISTANCE_WHEEL_CHAIR'],
                    ['Walk', '', 'Walk', 'FIM_LEVEL7'],
                    ['Wheelchair', '', 'WheelChair', 'FIM_LEVEL7'],
                    ['Eating', '', 'Eating', 'FIM_LEVEL7'],
                    ['Grooming', '', 'Grooming', 'FIM_LEVEL7'],
                    ['Bathing', '', 'Bathing', 'FIM_LEVEL7'],
                    ['Dressing-Upper', '', 'DressingUpper', 'FIM_LEVEL7'],
                    ['Dressing-Lower', '', 'DressingLower', 'FIM_LEVEL7'],
                    ['Toileting', '', 'Toileting', 'FIM_LEVEL7'],
                    ['Bladder', '', 'Bladder', 'FIM_LEVEL7'],
                    ['Bowel', '', 'Bowel', 'FIM_LEVEL7'],
                    ['Bed, Chair, Wheelchair', '', 'BedChair', 'FIM_LEVEL7'],
                    ['Toilet', '', 'Toilet', 'FIM_LEVEL7'],
                    ['Tub, Shower', '', 'TubShower', 'FIM_LEVEL7'],
                    ['Walk/Wheelchair', '', 'WalkWheelChair', 'FIM_LEVEL7'],
                    ['Stairs', '', 'Stairs', 'FIM_LEVEL7'],
                    ['Comprehension', '', 'Comprehension', 'FIM_LEVEL7'],
                    ['Expression', '', 'Expression', 'FIM_LEVEL7'],
                    ['Social Interaction', '', 'SocialInteraction', 'FIM_LEVEL7'],
                    ['Problem Solving', '', 'ProblemSolving', 'FIM_LEVEL7'],
                    ['Memory', '', 'Memory', 'FIM_LEVEL7'],
                    ['Total Motor Score', '', 'TotalMotor',],
                    ['Total Cognition Score', '', 'TotalCognition',],
                    ['Total FIM® Score', '', 'TotalFIM',]];
                    for (var i = 0; i < tableData.length; i++) {
                        // format data from Fim

                        let label1 = tableData[i][2];
                        if (label1) {
                            // for admit 
                            if (fimObj && fimObj[label1] || fimObj[label1] == '0') {

                                if (tableData[i] && tableData[i][3]) {
                                    fimLabelAdmit = realmController.getcategoryNotesByCategoryNameRealm(this.state.loggedInUserDetails.FacilityCode, schema[tableData[i][3]], '', 'Item', fimObj[label1]);
                                } else {
                                    fimLabelAdmit = '';
                                }
                                if (fimLabelAdmit && fimLabelAdmit != '') {

                                    if (label1 == "DistanceWalked" || label1 == 'DistanceWheelchair'
                                        || label1 == 'BladderFrequency' || label1 == 'BowelFrequency') {
                                        tableData[i][1] = fimObj[label1] + '-' + fimLabelAdmit;
                                    } else {
                                        tableData[i][1] = fimLabelAdmit;
                                    }
                                } else {

                                    // to fix digits in motorScoreIndex
                                    if (label1 == 'MotorScoreIndex' && fimObj[label1]) {
                                        if (parseInt(fimObj[label1]) === fimObj[label1]) {
                                            tableData[i][1] = Number(fimObj[label1]);
                                        } else {
                                            tableData[i][1] = Number(fimObj[label1]).toFixed(1)
                                        }
                                    } else {
                                        tableData[i][1] = fimObj[label1];
                                    }
                                }

                            } else {
                                tableData[i][1] = '';
                            }
                            if (label1 == 'WalkWheelChair' && fimObj['LocoMotionType']) {
                                locoData = realmController.getcategoryNotesByCategoryNameRealm(this.props.loggedInUserDetails.FacilityCode, schema.LOCOMOTION_TYPE, '', 'Item', fimObj['LocoMotionType']);
                                tableData[i][1] = tableData[i][1] + ' (' + fimObj['LocoMotionType'] + ' - ' + locoData + ')';
                            }
                            if (label1 == 'Comprehension' && fimObj['ComprehensionType']) {
                                locoData = realmController.getcategoryNotesByCategoryNameRealm(this.props.loggedInUserDetails.FacilityCode, schema.COMPREHENSION_TYPE, '', 'Item', fimObj['ComprehensionType']);
                                tableData[i][1] = tableData[i][1] + ' (' + fimObj['ComprehensionType'] + ' - ' + locoData + ')';
                            }
                            if (label1 == 'Expression' && fimObj['ExpressionType']) {
                                locoData = realmController.getcategoryNotesByCategoryNameRealm(this.props.loggedInUserDetails.FacilityCode, schema.EXPRESSION_TYPE, '', 'Item', fimObj['ExpressionType']);
                                tableData[i][1] = tableData[i][1] + ' (' + fimObj['ExpressionType'] + ' - ' + locoData + ')';
                            }
                            // to add mode when there is only data
                            if (tableData[i][0] == 'Walk/Wheelchair' && fimObj && fimObj['LocoMotionType']) {
                                tableData[i][0] = tableData[i][0] + ' (Mode)';
                            }
                            // to add mode when there is only data
                            if (tableData[i][0] == 'Comprehension' && fimObj && fimObj['ComprehensionType']) {
                                tableData[i][0] = tableData[i][0] + ' (Mode)';
                            }
                            // to add mode when there is only data
                            if (tableData[i][0] == 'Expression' && fimObj && fimObj['ExpressionType']) {
                                tableData[i][0] = tableData[i][0] + ' (Mode)';
                            }
                        }
                        if (tableData[i][2]) {
                            tableData[i].splice(2, 2);
                        }
                    }
                    modifiedFimTableData.push({ 'tableSubHead': [tableSubHead], fimId: fimId, 'tableInfo': tableData })
                }
            });
        }
        if (!this.props.server) {
            await this.props.getIsLoadingValue(false);
        }
        return modifiedFimTableData;
    }

    // apply margin bottom space for flat list item
    renderSeparator = () => {
        return (<View style={[{ marginBottom: 7 }]} />)
    }

    checkRecordData = (tableDataLocal, tableDataOther, rowIndex, cellData, cellIndex, index) => {
        if (cellIndex == 1 && (cellData || cellData == 0) && cellData != '') {
            if (tableDataOther && tableDataLocal && tableDataLocal['tableSubHead'] && tableDataLocal['tableSubHead'][0]) {
                let existingIndex = tableDataOther.findIndex(singleObj =>
                    singleObj['tableSubHead'] && singleObj['tableSubHead'][0] ? singleObj['tableSubHead'][0] == tableDataLocal['tableSubHead'][0] : null &&
                        singleObj['fimId'] ? singleObj['fimId'] == tableDataLocal['fimId'] : null);
                if (existingIndex != -1) {
                    if (cellData != (tableDataOther && tableDataOther[existingIndex] &&
                        tableDataOther[existingIndex]['tableInfo'] &&
                        tableDataOther[existingIndex]['tableInfo'][index] &&
                        (tableDataOther[existingIndex]['tableInfo'][index][cellIndex] ||
                            tableDataOther[existingIndex]['tableInfo'][index][cellIndex] == 0))) {
                        return 'diff';
                    } else {
                        return null;
                    }
                } else {
                    return 'diff';
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    render() {
        return (

            <View style={{ marginBottom: 10 }}>
                {this.state.caseListDetails || this.state.caseDetailsOther ?
                    <View style={[DocStyles.row, DocStyles.marginBottom10, {
                        flexDirection: 'row',
                        justifyContent: 'flex-end',
                        alignItems: 'center'
                    }]}>
                        <View style={{ borderWidth: 1, padding: 10, flexDirection: "column", width: '100%' }}>
                            <View style={{ justifyContent: "space-between", flexDirection: 'row' }}>
                                <View style={[{ width: '48%' }]}>
                                    {this.state.caseListDetails && this.state.caseDetailsOther &&
                                        this.state.caseListDetails.LastName &&
                                        this.state.caseListDetails.FirstName && this.state.caseDetailsOther.LastName &&
                                        this.state.caseDetailsOther.FirstName &&
                                        (this.state.caseListDetails.LastName == this.state.caseDetailsOther.LastName)
                                        && (this.state.caseListDetails.FirstName == this.state.caseDetailsOther.FirstName) ?
                                        <Text style={[DocStyles.labelStyle]}>
                                            {this.state.caseListDetails &&
                                                this.state.caseListDetails.LastName && this.state.caseListDetails.FirstName ?
                                                this.state.caseListDetails.LastName + ', ' + this.state.caseListDetails.FirstName : null}</Text> :
                                        this.state.caseListDetails && this.state.caseListDetails.LastName && this.state.caseListDetails.FirstName ?
                                            <Text style={[DocStyles.labelStyleDiff]}>
                                                {this.state.caseListDetails &&
                                                    this.state.caseListDetails.LastName && this.state.caseListDetails.FirstName ?
                                                    this.state.caseListDetails.LastName + ', ' + this.state.caseListDetails.FirstName : null}
                                            </Text> :
                                            <Text> </Text>}
                                </View>
                                <View style={[{ width: '48%' }]}>
                                    {this.state.caseDetailsOther &&
                                        this.state.caseListDetails &&
                                        this.state.caseListDetails.PatientID &&
                                        this.state.caseListDetails.PatientID == this.state.caseDetailsOther.PatientID ?
                                        <Text style={[DocStyles.labelStyle]}>{this.state.caseListDetails.PatientID}</Text> :
                                        <Text style={[DocStyles.labelStyleDiff]}>{this.state.caseListDetails &&
                                            this.state.caseListDetails.PatientID ? this.state.caseListDetails.PatientID : null}</Text>}
                                </View>
                            </View>
                            {this.state.loggedInUserDetails && this.state.loggedInUserDetails.FacilityDetails ?
                                <Text style={[DocStyles.lableClass, { marginTop: 5 }]}>
                                    {this.state.loggedInUserDetails.FacilityDetails.Description ?
                                        this.state.loggedInUserDetails.FacilityDetails.Description : null}
                                    {this.state.loggedInUserDetails.FacilityDetails.MedicareNumber ?
                                        ', ' + this.state.loggedInUserDetails.FacilityDetails.MedicareNumber : null}</Text> : null}
                        </View>
                    </View> : null}
                <View>
                    {this.state.newCaseList.length ?
                        <View style={[DocStyles.row, DocStyles.marginBottom10]}>
                            <Text style={[DocStyles.halfOne, DocStyles.subHeading, DocStyles.HeadingColor]}>Patient Information</Text>
                        </View> : null}
                    {this.state.newCaseList ?
                        <View style={[DocStyles.row, DocStyles.marginBottom10]}>
                            <FlatList
                                data={this.state.newCaseList}
                                renderItem={({ item, index }) =>
                                    <ListItem avatar >
                                        {/* {item.color == 'green' ?
                                            <View style={[DocStyles.rowDiff, DocStyles.marginBottom13]}>
                                                <Text style={[DocStyles.halfTwo, DocStyles.labelStyleDiff]}>{item.displayField}</Text>
                                                <Text style={[DocStyles.halfOne, DocStyles.textStyleDiff]}>{item.value}</Text>
                                            </View> : */}
                                            <View style={[DocStyles.row, DocStyles.marginBottom13]}>
                                                <Text style={[DocStyles.halfTwo, DocStyles.labelStyle]}>{item.displayField}</Text>
                                                <Text style={[DocStyles.halfOne, DocStyles.textStyle]}>{item.value}</Text>
                                            </View>
                                            {/* } */}
                                    </ListItem>
                                }
                                keyExtractor={(item, index) => index.toString()}
                                extraData={this.state.newCaseList}
                                ItemSeparatorComponent={this.renderSeparator}
                            />
                        </View> : null}
                    {this.state.newAdmissionList ?
                        <View style={[DocStyles.row, DocStyles.marginBottom10]}>
                            <FlatList
                                data={this.state.newAdmissionList}
                                renderItem={({ item, index }) =>
                                    <ListItem avatar >
                                        {/* {item.color == 'green' ?
                                            <View style={[DocStyles.rowDiff, DocStyles.marginBottom13]}>
                                                <Text style={[DocStyles.halfTwo, DocStyles.labelStyleDiff]}>{item.displayField}</Text>
                                                <Text style={[DocStyles.halfOne, DocStyles.textStyleDiff]}>{item.value}</Text>
                                            </View> : */}
                                            <View style={[DocStyles.row, DocStyles.marginBottom13]}>
                                                <Text style={[DocStyles.halfTwo, DocStyles.labelStyle]}>{item.displayField}</Text>
                                                <Text style={[DocStyles.halfOne, DocStyles.textStyle]}>{item.value}</Text>
                                            </View>
                                            {/* } */}
                                    </ListItem>
                                }
                                keyExtractor={(item, index) => index.toString()}
                                extraData={this.state.newAdmissionList}
                                ItemSeparatorComponent={this.renderSeparator}
                            />
                        </View> : null}

                    {this.state.newComorbidList.length ?
                        <View style={[DocStyles.row, DocStyles.marginBottom10]}>
                            <Text style={[DocStyles.halfOne, DocStyles.subHeading, DocStyles.HeadingColor]}>Comorbid Conditions</Text>
                        </View> : null}
                    {this.state.newComorbidList ?
                        <View style={[DocStyles.row, DocStyles.marginBottom10]}>
                            <FlatList
                                data={this.state.newComorbidList}
                                renderItem={({ item, index }) =>
                                    <ListItem avatar >
                                        {/* {item.color == 'green' ?
                                            <View style={[DocStyles.rowDiff, DocStyles.marginBottom13]}>
                                                <Text style={[DocStyles.halfTwo, DocStyles.labelStyleDiff]}>{item.displayField}</Text>
                                                <Text style={[DocStyles.halfOne, DocStyles.textStyleDiff]}>{item.value}</Text>
                                            </View> : */}
                                            <View style={[DocStyles.row, DocStyles.marginBottom13]}>
                                                <Text style={[DocStyles.halfTwo, DocStyles.labelStyle]}>{item.displayField}</Text>
                                                <Text style={[DocStyles.halfOne, DocStyles.textStyle]}>{item.value}</Text>
                                            </View>
                                            {/* } */}
                                    </ListItem>
                                }
                                keyExtractor={(item, index) => index.toString()}
                                extraData={this.state.newComorbidList}
                                ItemSeparatorComponent={this.renderSeparator}
                            />
                        </View> : null}

                    {this.state.tableData && this.state.tableData.length ?

                        this.state.tableData.map((tableData, rowIndex) => (

                            tableData['tableInfo'] && tableData['tableInfo'].length ?

                                <View style={[DocStyles.row, DocStyles.marginBottom10]} key={rowIndex}>
                                    <Table borderStyle={{ borderWidth: 2, borderColor: '#000000' }}>
                                        <Row data={this.state.tableHead} style={DocStyles.head} textStyle={[DocStyles.bold, DocStyles.tableHeadingAlign]} />
                                        <Row data={tableData['tableSubHead']} textStyle={[DocStyles.bold, DocStyles.text, { textAlign: 'center' }]} />

                                        {tableData['tableInfo'].map((rowData, index) => (
                                            <TableWrapper key={index} style={DocStyles.row}>

                                                {rowData.map((cellData, cellIndex) => (
                                                    <Cell key={cellIndex} data={cellData}
                                                        textStyle={[DocStyles.text, ((cellIndex == 1) ? DocStyles.tabletextStyle : null),
                                                        (cellIndex == 0) ? ((cellData == 'Eating' || cellData == 'Dressing – Upper Body' || cellData == 'Dressing – Lower Body' || cellData == 'Toileting' ||
                                                            cellData == 'Bowel' || cellData == 'Walk/Wheelchair' || cellData == 'Walk/Wheelchair' || cellData == 'Expression' || cellData == 'Expression'
                                                            || cellData == 'Memory' || cellData == 'Total FIM® Score' || cellData == 'Total Motor Score' || cellData == 'Motor Score Index' || cellData == 'Total Cognition Score'
                                                            || cellData == 'Total FIM® Score')
                                                            ? DocStyles.boldNew : DocStyles.tablelabelStyle, DocStyles.bold600) : null,
                                                        (cellIndex == 1) ? DocStyles.bold300 : null
                                                        ]}
                                                        style={[DocStyles.caseTableCellWidth, (index > 27 && (cellIndex == 0 || cellIndex == 1)) && { backgroundColor: '#d9d9d9' },
                                                        ((cellIndex == 1) && (cellData || cellData == 0))
                                                            ? this.checkRecordData(tableData, this.state.tableDataOther, rowIndex, cellData, cellIndex, index) == 'diff' ?
                                                                DocStyles.tableTextDiff
                                                                : ''
                                                            : '']} />
                                                ))
                                                }
                                            </TableWrapper>
                                        ))
                                        }
                                    </Table>
                                </View>
                                // ))
                                : null

                        ))
                        : null}

                </View>
            </View>
        );
    }
}

export default SingleCaseDocScreen;