/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import Sychronization from './Synchronization';
import config from '../../config/config';

export default {
    synchronizeFullData: async (loggedInUserDetails) => {
        let responseFromApi, conflictPatIdsArray;
        let apiUserBody = {};
        if (loggedInUserDetails && loggedInUserDetails.UserID) {
            apiUserBody = {
                FacilityCode: loggedInUserDetails.FacilityCode,
                UserID: loggedInUserDetails.UserID
            }
            // sync uds User Mata
            responseFromApi = await Sychronization.syncUDSUserData(apiUserBody, loggedInUserDetails, 'autoSync');
            if (responseFromApi == 'databaseChanged') {
                return 'databaseChanged';
            }
            if (responseFromApi == 'appVersionChanged') {
                return 'appVersionChanged';
            }
            if (responseFromApi && (responseFromApi != 'FailGUID' || responseFromApi != 'invalidGUIDCode'
                || responseFromApi != 'serverError') &&
                responseFromApi.isUserModify != 'userModified') {
                loggedInUserDetails = responseFromApi.loggedInUserDetails;
            } else if (responseFromApi.isUserModify == 'userModified') {
                return 'userModified';
            } else {
                return;
            }

            // sync UDS Master Mata
            if (responseFromApi != 'serverError') {
                responseFromApi = await Sychronization.syncUDSMasterData(apiUserBody);
            } else {
                return 'serverError';
            }

            // sync ICD Data
            if (responseFromApi != 'serverError') {
                responseFromApi = await Sychronization.syncICDData(apiUserBody);
            } else {
                return 'serverError';
            }

            // sync ICD10 Full Data
            if (responseFromApi != 'serverError') {
                responseFromApi = await Sychronization.syncICD10FullData(apiUserBody);
            } else {
                return 'serverError';
            }

            // sync ICD9 Full Data
            if (responseFromApi != 'serverError') {
                responseFromApi = await Sychronization.syncICD9FullData(apiUserBody);
            } else {
                return 'serverError';
            }

            // sync Facility Common Data
            if (responseFromApi != 'serverError') {
                responseFromApi = await Sychronization.syncFacilityCommonData(apiUserBody);
            } else {
                return 'serverError';
            }

            // sync Facility Data
            if (responseFromApi != 'serverError') {
                responseFromApi = await Sychronization.syncFacilityData(apiUserBody);
            } else {
                return 'serverError';
            }

            // sync Category Data
            if (responseFromApi != 'serverError') {
                responseFromApi = await Sychronization.syncCategoryData(apiUserBody);
            } else {
                return 'serverError';
            }

            // sync Facility Category Data
            if (responseFromApi != 'serverError') {
                responseFromApi = await Sychronization.syncFacilityCategoryData(apiUserBody);
            } else {
                return 'serverError';
            }



            //only if user has write permissions for Preadmission sync the data
            if (!config.preadmOrCaseNonSyncPrivileges.includes(loggedInUserDetails.PreadmissionPrivilege)
                && responseFromApi != 'serverError') {
                // sync Preadmission Data
                responseFromApi = await Sychronization.syncPreadmissionData(apiUserBody);
                if (responseFromApi != 'serverError') {
                    conflictPatIdsArray = responseFromApi.conflictPatIdsArray;
                    // sync Notification Data
                    responseFromApi = await Sychronization.syncNotificationData(apiUserBody);
                } else if (responseFromApi == 'serverError') {
                    return 'serverError';
                }

                // sync CaseListing Data only if user has write permissions for CaseListing sync the data
                if (!config.preadmOrCaseNonSyncPrivileges.includes(loggedInUserDetails.CaseListingPrivilege) &&
                    responseFromApi != 'serverError') {
                    responseFromApi = await Sychronization.syncCaseListingData(apiUserBody, conflictPatIdsArray);

                    if (responseFromApi == 'serverError') {
                        return 'serverError';
                    }
                    if (responseFromApi != 'serverError') {
                        // Insert Compliance Data 
                        responseFromApi = await Sychronization.syncTierAndComplianceData(apiUserBody);
                        if (responseFromApi != 'serverError') {
                            responseFromApi = await Sychronization.syncAuditLogData();
                        }

                        if (responseFromApi != 'serverError') {
                            await Sychronization.syncErrorLogData();
                            return;
                        } else {
                            return;
                        }
                    }
                } else if (responseFromApi != 'serverError') {
                    responseFromApi = await Sychronization.syncAuditLogData();
                    if (responseFromApi != 'serverError') {
                        await Sychronization.syncErrorLogData();
                        return;
                    } else {
                        return;
                    }
                } else {
                    return 'serverError';
                }
            } else if (!config.preadmOrCaseNonSyncPrivileges.includes(loggedInUserDetails.CaseListingPrivilege) &&
                responseFromApi != 'serverError') {
                //only if user has write permissions for CaseListing sync the data
                // sync CaseListing Data
                responseFromApi = await Sychronization.syncCaseListingData(apiUserBody, conflictPatIdsArray);
                if (responseFromApi != 'serverError') {
                    // Insert Compliance Data 
                    responseFromApi = await Sychronization.syncTierAndComplianceData(apiUserBody);

                    if (responseFromApi != 'serverError') {
                        responseFromApi = await Sychronization.syncAuditLogData();
                    }
                    if (responseFromApi != 'serverError') {
                        await Sychronization.syncErrorLogData();
                        return;
                    } else {
                        return;
                    }
                }
                if (responseFromApi == 'serverError') {
                    return 'serverError';
                }
            } else if (responseFromApi == 'serverError') {
                return 'serverError';
            } else {
                responseFromApi = await Sychronization.syncAuditLogData();
                if (responseFromApi != 'serverError') {
                    await Sychronization.syncErrorLogData();
                    return;
                } else {
                    return;
                }
            }
        }
    }

}