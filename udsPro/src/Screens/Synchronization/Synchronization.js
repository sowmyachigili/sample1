/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import { Alert } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import fetchMethodRequest from '../../config/service';
import config from '../../config/config';
import api from '../../config/apiCalls';
import realm from '../../Realm/realm';
import realmController from '../../Realm/realmController';
import schema from '../../Realm/schemaNames';
import ErrorLogHandler from '../../utility/ErrorLogAlert';
import common from '../../utility/commonFun';
import dateFormats from '../../utility/formatDate';
import dataJson from '../../Realm/icd10Data1.json';
import dataJson1 from '../../Realm/icd10Data2.json';
import dataJson9 from '../../Realm/data9.json';
import IRFVersionsJson from '../../Realm/IRFVersions.json';

import DeviceInfo from 'react-native-device-info';

// When click on synchronize button insert lastsync date into synchronization details
updateLastSynchronizeDetails = async (synctype, userBody) => {
    if (userBody) {
        let syncDetails = this.getLastSyncDate(synctype, userBody);
        let synDetails = await common.updateSynchronizeDetails(synctype, userBody.FacilityCode, userBody.UserID, syncDetails);
        return synDetails;
    }
}

// Get Last Sync date details
getLastSyncDate = (synctype, userBody) => {
    if (userBody) {
        return common.getLastSyncDate(synctype, userBody.UserID, userBody.FacilityCode);
    }
}


//Fetch records that are modified after the last synchornization
getRecordsModifiedAfterLastSync = (schemaname, lastSyncDate) => {
    let filterQuery;
    if (schemaname == schema.DROPDOWN_OPTIONS_SCHEMA) {
        filterQuery = ' CategoryDetailID == null';
        return realmController.getAllRecordsWithFilteringWithDate(schemaname, filterQuery);
    } else if (schemaname == schema.NOTIFICATION_SCHEMA) {
        filterQuery = 'InsertedOn > $0 or UpdatedOn > $0 or DeletedOn > $0 or ActionCompletedOn > $0';
    } else {
        filterQuery = 'InsertedOn > $0 or UpdatedOn > $0';
    }
    if (lastSyncDate) {
        let date = dateFormats.formatDate(lastSyncDate);
        return realmController.getAllRecordsWithFilteringWithDate(schemaname, filterQuery, date);
    } else {
        return null
    }
}

// Check if category exists if yes return IPADID
CheckRecordExists = (schemaname, recordId, fieldname, fieldnameIPAD) => {
    filterQuery = fieldname + ' = ' + recordId;
    let obj = realmController.getAllRecordsWithFiltering(schemaname, filterQuery);
    return obj && obj.length > 0 ? obj[0][fieldnameIPAD] : 0;
}

//synchronise ICD10Data based on Iterations
syncICD10Data = async (index, userBody) => {
    let data = {
        ...userBody,
        Iterations: index
    }
    return await fetchMethodRequest('POST', api.getICD10Data, data)
        .then((response) => {
            if (response == config.invalidGUIDCode) { // 401 response
                return 'invalidGUIDCode';
            } else if (response == config.generateGuidErrorCode) { //guid fail
                return 'FailGUID';
            } else if (response == 'serverError') {
                return 'serverError';
            } else {
                if (response) {
                    // insert icd10
                    realm.write(() => {
                        if (response.ICD10Data && response.ICD10Data.length > 0) {
                            response.ICD10Data.forEach(element => {
                                realmController.insertOrUpdateRecordWithoutTransaction(schema.ICD10_SCHEMA, element, true);
                            });
                        }
                    });
                }
            }
        }).catch(err => {
            common.errorLogForStartAndEndTimeOfAPICall('getICD10Data', true, data);
            ErrorLogHandler(err, true, data);
            return;
        });
}

// sync ICD9 Data based on Iterations
syncICD9Data = async (index) => {
    let data = {
        ...userBody,
        Iterations: index
    }
    return await fetchMethodRequest('POST', api.getICD9Data, data)
        .then((response) => {
            if (response == config.invalidGUIDCode) { // 401 response
                return 'invalidGUIDCode';
            } else if (response == config.generateGuidErrorCode) { //guid fail
                return 'FailGUID';
            } else if (response == 'serverError') {
                return 'serverError';
            } else {
                if (response) {
                    // insert icd9
                    realm.write(() => {
                        if (response.ICD9Data && response.ICD9Data.length > 0) {
                            response.ICD9Data.forEach(element => {
                                realmController.insertOrUpdateRecordWithoutTransaction(schema.ICD9Data_SCHEMA, element, true);
                            });
                        }
                    });
                }
            }
        }).catch(err => {
            common.errorLogForStartAndEndTimeOfAPICall('getICD9Data', true, data);
            ErrorLogHandler(err, true, data);
            return;
        });
}

// email subject and body modifieddata save to api
emailBodySaveWithModifiedDetails = async (modifiedEmailSubjectDataOffline) => {
    let emailDetails = [];
    for (let i = 0; i < modifiedEmailSubjectDataOffline.length; i++) {
        let modifiedServerObj = {};
        if (modifiedEmailSubjectDataOffline[i]) {
            modifiedServerObj = { ...modifiedEmailSubjectDataOffline[i] };
        }
        emailDetails.push(modifiedServerObj);
    }
    if (emailDetails) {
        return await fetchMethodRequest('POST', api.saveEmailSubjectBodyData, emailDetails)
            .then((response) => {
                if (response == config.invalidGUIDCode) { // 401 response
                    return 'invalidGUIDCode';
                } else if (response == config.generateGuidErrorCode) { //guid fail
                    return 'FailGUID';
                } else if (response == 'serverError') {
                    return 'serverError';
                } else {
                    if (response && response.length && response.length > 0) {
                        response.forEach(element => {
                            let newEmailSubjectBodyObj = { ...element };
                            realmController.insertOrUpdateRecord(schema.EMAIL_SUBJECT_BODY, newEmailSubjectBodyObj, true);
                        });
                    }
                }
            }).catch(err => {
                ErrorLogHandler(err, true);
                return;
            });
    }
}

// categories list save api with modified records
categorySaveWithMofiedDetails = async (modifiedCategoryDataOffline, UserID) => {
    let categoryServerObj = {}, categoryDetails = [];
    for (let i = 0; i < modifiedCategoryDataOffline.length; i++) {
        let modifiedServerObj = {};
        if (modifiedCategoryDataOffline[i]) {
            modifiedServerObj = { ...modifiedCategoryDataOffline[i] };
        }
        categoryDetails.push(modifiedServerObj);
    }
    categoryServerObj = {
        UserID: UserID,
        CategoryDetails: [...categoryDetails]
    };
    return await fetchMethodRequest('POST', api.saveCategoryData, categoryServerObj)
        .then(async (response) => {
            if (response == config.invalidGUIDCode) { // 401 response
                return 'invalidGUIDCode';
            } else if (response == config.generateGuidErrorCode) { //guid fail
                return 'FailGUID';
            } else if (response == 'serverError') {
                return 'serverError';
            } else {
                let modifiedResponse = [];
                modifiedResponse.push(response);
                if (modifiedResponse && modifiedResponse.length && modifiedResponse.length > 0) {
                    modifiedResponse.forEach(element => {
                        let newCategoryOptionsList = { ...element };
                        if (newCategoryOptionsList && newCategoryOptionsList.CategoryDetails && newCategoryOptionsList.CategoryDetails.length &&
                            newCategoryOptionsList.CategoryDetails.length > 0) {
                            newCategoryOptionsList.CategoryDetails.forEach(categoryOptionsList => {
                                let newCategoryListObj = {
                                    CategoryDetailIPADId: categoryOptionsList.CategoryDetailIPADId,
                                    CategoryDetailID: categoryOptionsList.CategoryDetailID,
                                    CategoryID: categoryOptionsList.CategoryID,
                                    CategoryIPADID: categoryOptionsList.CategoryIPADID,
                                };
                                realmController.insertOrUpdateRecord(schema.DROPDOWN_OPTIONS_SCHEMA, newCategoryListObj, true);

                                let filterQuery = 'CategoryID == ' + categoryOptionsList.CategoryID;
                                let categoryOptions = realmController.getAllRecordsWithFiltering(schema.DROPDOWN_OPTIONS_SCHEMA, filterQuery);
                                categoryOptions = [...categoryOptions];
                                let newCategory = {
                                    CategoryIPADID: categoryOptionsList.CategoryIPADID,
                                    CategoryDetails: categoryOptions
                                }
                                realmController.insertOrUpdateRecord(schema.DROPDOWNS_SCHEMA, newCategory, true);

                            });
                        }
                    });
                }
                return 'Success';
            }
        }).catch(err => {
            ErrorLogHandler(err, true);
            return;
        });
}

// Preadmission save api with modified records
preadmsisionSaveWithModifiedDetails = async (modifiedPreadmissionDataOffline, data) => {
    let modifiedPreadmissionDataServer = [];
    for (let i = 0; i < modifiedPreadmissionDataOffline.length; i++) {
        let serverObj = {};
        if (!modifiedPreadmissionDataOffline[i].IsConflictExists) {
            serverObj = common.formatPreadmissionDataServer(modifiedPreadmissionDataOffline[i]);
            let modifiedServerObj = { ...serverObj };
            let preadmissionServerObj = {};
            preadmissionTabDetails = {};
            preadmissionTabDetails.PatientInfo = [...modifiedServerObj.PatientInfo];
            preadmissionTabDetails.PREPriorLiving = [...modifiedServerObj.PREPriorLiving];
            preadmissionTabDetails.PREDiagnosis = [...modifiedServerObj.PREDiagnosis];
            preadmissionTabDetails.FIM = [...modifiedServerObj.FIM];
            preadmissionTabDetails.PREFIMCurrentStatus = [...modifiedServerObj.PREFIMCurrentStatus];
            preadmissionTabDetails.Goal = [...modifiedServerObj.Goal];
            preadmissionTabDetails.CurrentMedical = [...modifiedServerObj.CurrentMedical];
            preadmissionTabDetails.CustomData = [...modifiedServerObj.CustomData];
            preadmissionTabDetails.ReAssessment = [...modifiedServerObj.ReAssessment];
            preadmissionTabDetails.Note = [...modifiedServerObj.Note];
            preadmissionTabDetails.PRESignature = [...modifiedServerObj.PRESignature];
            preadmissionTabDetails.CurrentFunctionalStatus = [...modifiedServerObj.CurrentFunctionalStatus];

            // delete keys from main obj
            delete modifiedServerObj.PatientInfo;
            delete modifiedServerObj.PREPriorLiving;
            delete modifiedServerObj.PREDiagnosis;
            delete modifiedServerObj.FIM;
            delete modifiedServerObj.PREFIMCurrentStatus;
            delete modifiedServerObj.Goal;
            delete modifiedServerObj.CurrentMedical;
            delete modifiedServerObj.CustomData;
            delete modifiedServerObj.ReAssessment;
            delete modifiedServerObj.Note;
            delete modifiedServerObj.PRESignature;
            delete modifiedServerObj.CurrentFunctionalStatus;
            preadmissionServerObj = { ...modifiedServerObj, ...preadmissionTabDetails };
            modifiedPreadmissionDataServer.push({ PreAdmission: preadmissionServerObj });
        }
    }
    if (modifiedPreadmissionDataServer && modifiedPreadmissionDataServer.length > 0) {
        return await fetchMethodRequest('POST', api.savePreadmissionData, modifiedPreadmissionDataServer)
            .then(async (response) => {
                if (response == config.invalidGUIDCode) { // 401 response
                    return 'invalidGUIDCode';
                } else if (response == config.generateGuidErrorCode) { //guid fail
                    return 'FailGUID';
                } else if (response == 'serverError') {
                    return 'serverError';
                } else {
                    if (response && response.length && response.length > 0) {
                        for (let element of response) {
                            let newPreadmissionObj = {};
                            newPreadmissionObj = { ...element.PreAdmission };
                            newPreadmissionObj.IsConflictExists = false;
                            let totalData = realmController.getSingleRecord(schema.PREADMISSION_SCHEMA, newPreadmissionObj.PreAdmissionIPADID);
                            if (newPreadmissionObj.PreAdmissionID) {
                                if (totalData.PatientInfo && totalData.PatientInfo[0] && !totalData.PatientInfo[0].PreAdmissionID) {
                                    newPreadmissionObj.PatientInfo = [];
                                    let PatientInfo = { PreAdmissionID: newPreadmissionObj.PreAdmissionID, PRE_PatientInfoIPADID: totalData.PatientInfo[0].PRE_PatientInfoIPADID }
                                    newPreadmissionObj.PatientInfo.push(PatientInfo);
                                }
                                if (totalData.PREPriorLiving && totalData.PREPriorLiving[0] && !totalData.PREPriorLiving[0].PreAdmissionID) {
                                    newPreadmissionObj.PREPriorLiving = [];
                                    let PREPriorLiving = { PreAdmissionID: newPreadmissionObj.PreAdmissionID, PRE_PriorLivingIPADID: totalData.PREPriorLiving[0].PRE_PriorLivingIPADID }
                                    newPreadmissionObj.PREPriorLiving.push(PREPriorLiving);
                                }
                                if (totalData.PREDiagnosis && totalData.PREDiagnosis[0] && !totalData.PREDiagnosis[0].PreAdmissionID) {
                                    newPreadmissionObj.PREDiagnosis = [];
                                    let PREDiagnosis = { PreAdmissionID: newPreadmissionObj.PreAdmissionID, PRE_DiagnosisIPADID: totalData.PREDiagnosis[0].PRE_DiagnosisIPADID }
                                    newPreadmissionObj.PREDiagnosis.push(PREDiagnosis);
                                }
                                if (totalData.CurrentMedical && totalData.CurrentMedical[0] && !totalData.CurrentMedical[0].PreAdmissionID) {
                                    newPreadmissionObj.CurrentMedical = [];
                                    let CurrentMedical = { PreAdmissionID: newPreadmissionObj.PreAdmissionID, PRE_CurrentMedicalIPADID: totalData.CurrentMedical[0].PRE_CurrentMedicalIPADID }
                                    newPreadmissionObj.CurrentMedical.push(CurrentMedical);
                                }
                                if (totalData.PREFIMCurrentStatus && totalData.PREFIMCurrentStatus[0] && !totalData.PREFIMCurrentStatus[0].PreAdmissionID) {
                                    newPreadmissionObj.PREFIMCurrentStatus = [];
                                    let PREFIMCurrentStatus = { PreAdmissionID: newPreadmissionObj.PreAdmissionID, PRE_FIMCurrent_StatusIPADID: totalData.PREFIMCurrentStatus[0].PRE_FIMCurrent_StatusIPADID }
                                    newPreadmissionObj.PREFIMCurrentStatus.push(PREFIMCurrentStatus);
                                }
                                if (totalData.FIM && totalData.FIM[0] && !totalData.FIM[0].PreAdmissionID) {
                                    newPreadmissionObj.FIM = [];
                                    let FIM = { PreAdmissionID: newPreadmissionObj.PreAdmissionID, FIMIPADID: totalData.FIM[0].FIMIPADID }
                                    newPreadmissionObj.FIM.push(FIM);
                                }
                                if (totalData.ReAssessment && totalData.ReAssessment[0] && !totalData.ReAssessment[0].PreAdmissionID) {
                                    newPreadmissionObj.ReAssessment = [];
                                    let ReAssessment = { PreAdmissionID: newPreadmissionObj.PreAdmissionID, PRE_ReAssessmentIPADID: totalData.ReAssessment[0].PRE_ReAssessmentIPADID }
                                    newPreadmissionObj.ReAssessment.push(ReAssessment);
                                }
                                if (totalData.Goal && totalData.Goal[0] && !totalData.Goal[0].PreAdmissionID) {
                                    newPreadmissionObj.Goal = [];
                                    let Goal = { PreAdmissionID: newPreadmissionObj.PreAdmissionID, PRE_GoalIPADID: totalData.Goal[0].PRE_GoalIPADID }
                                    newPreadmissionObj.Goal.push(Goal);
                                }
                            }
                            await realmController.insertOrUpdateRecord(schema.PREADMISSION_SCHEMA, newPreadmissionObj, true);
                        }
                        await updatePreadmssionIdInCaseListingForAdmittedRecords(data.FacilityCode);
                    } else if (response && response.length == 0) {

                    }
                    return 'Success';
                }
            }).catch(err => {
                ErrorLogHandler(err, true, modifiedPreadmissionDataServer);
                return;
            });
    }
}

// update preadmsision id in cased listing for admitted records
updatePreadmssionIdInCaseListingForAdmittedRecords = (FacilityCode) => {
    let pendingAdmitCaseQuery = 'IsAdmitted == true AND PreAdmissionID == null AND FacilityCode ==[c] "' + FacilityCode + '" AND IsConflictExists == false';
    let caseRecords = [];
    caseRecords = realmController.getAllRecordsWithFiltering(schema.CASELISTING_SCHEMA, pendingAdmitCaseQuery);

    [...caseRecords].forEach(caseRecord => {
        if (caseRecord && caseRecord.patIPADID) {
            // get preadmsision admitted record based on patid
            let preadmsisionQuery = 'patIPADID ==  "' + caseRecord.patIPADID + '" AND FacilityCode ==[c] "' + FacilityCode + '" AND IsConflictExists == false';
            let preadmisisonRecords = [];
            preadmisisonRecords = realmController.getAllRecordsWithFiltering(schema.PREADMISSION_SCHEMA, preadmsisionQuery);
            let preadmisisonIPADRecords = [...preadmisisonRecords];

            if (preadmisisonIPADRecords && preadmisisonIPADRecords.length && preadmisisonIPADRecords.length == 1) {
                if (preadmisisonIPADRecords[0] && preadmisisonIPADRecords[0].PreAdmissionID) {
                    let caseDetails = {};
                    caseDetails.patIPADID = caseRecord.patIPADID;
                    caseDetails.PreAdmissionID = preadmisisonIPADRecords[0].PreAdmissionID;
                    realmController.insertOrUpdateRecord(schema.CASELISTING_SCHEMA, caseDetails, true);
                }
            }
        }
    });
}

// notification save api call
notificationSaveWithModifiedDetails = async (modifiedNotificationDataOffline) => {
    let notificationDetails = [];
    for (let i = 0; i < modifiedNotificationDataOffline.length; i++) {
        let modifiedServerObj = {};
        if (modifiedNotificationDataOffline[i]) {
            modifiedServerObj = { ...modifiedNotificationDataOffline[i] };
        }
        notificationDetails.push(modifiedServerObj);
    }
    return await fetchMethodRequest('POST', api.saveNotificationData, notificationDetails)
        .then(async (response) => {
            if (response == config.invalidGUIDCode) { // 401 response
                return 'invalidGUIDCode';
            } else if (response == config.generateGuidErrorCode) { //guid fail
                return 'FailGUID';
            } else if (response == 'serverError') {
                return 'serverError';
            } else {
                if (response && response.length && response.length > 0) {
                    response.forEach(element => {
                        let newNotificationObj = { ...element }
                        realmController.insertOrUpdateRecord(schema.NOTIFICATION_SCHEMA, newNotificationObj, true);
                    });
                }
            }
        }).catch(err => {
            ErrorLogHandler(err, true);
            return;
        });
}

// case listing save api with modified records
caseListingSaveWithMofiedDetails = async (modifiedCaseListingDataOffline) => {
    let modifiedCaselistingDataServer = [];
    for (let i = 0; i < modifiedCaseListingDataOffline.length; i++) {
        let serverObj = {};
        if (!modifiedCaseListingDataOffline[i].IsConflictExists) {
            serverObj = { ...modifiedCaseListingDataOffline[i] };
            let modifiedServerObj = { ...serverObj };
            let caseServerObj = {}, casePatIpadId, caseTabDetails = {};
            caseTabDetails.Admission = [...modifiedServerObj.Admission];
            caseTabDetails.FIM = [...modifiedServerObj.FIM];
            caseTabDetails.ComorbidAdvance = [...modifiedServerObj.ComorbidAdvance];

            // delete keys from main obj
            delete modifiedServerObj.Admission;
            delete modifiedServerObj.FIM;
            delete modifiedServerObj.ComorbidAdvance;

            caseServerObj = { ...modifiedServerObj, ...caseTabDetails };

            // check patient admit status and send data to server or not when preadmission record is in conflicts
            casePatIpadId = modifiedServerObj.patIPADID;
            let filterQuery = 'IsConflictExists == true AND patIPADID == ' + casePatIpadId;
            let preadmssionRecord = realmController.getAllRecordsWithFiltering(schema.PREADMISSION_SCHEMA, filterQuery);
            preadmssionRecord = [...preadmssionRecord];
            if (preadmssionRecord.length == 0) {
                modifiedCaselistingDataServer.push({ Patient: caseServerObj });
            }
        }
    }
    if (modifiedCaselistingDataServer) {
        return await fetchMethodRequest('POST', api.saveCaseListingData, modifiedCaselistingDataServer)
            .then(async (response) => {
                if (response == config.invalidGUIDCode) { // 401 response
                    return 'invalidGUIDCode';
                } else if (response == config.generateGuidErrorCode) { //guid fail
                    return 'FailGUID';
                } else if (response == 'serverError') {
                    return 'serverError';
                } else {
                    if (response && response.length && response.length > 0) {
                        let caseResponse = [];
                        caseResponse = response;
                        for (let caseObj of caseResponse) {
                            let newCaseObj = {};
                            newCaseObj = { ...caseObj.Patient };
                            if (newCaseObj && newCaseObj.patIPADID) {
                                newCaseObj.IsConflictExists = false;
                                await realmController.insertOrUpdateRecord(schema.CASELISTING_SCHEMA, newCaseObj, true);

                                // Insert PatId in Preadmission Record(Which is Admitted)
                                let filterQuery = 'patIPADID = ' + newCaseObj.patIPADID;
                                let preadmission = realmController.getAllRecordsWithFiltering(schema.PREADMISSION_SCHEMA, filterQuery);
                                if (preadmission && preadmission.length && preadmission.length > 0) {
                                    let preAdmissionObject = {
                                        PreAdmissionIPADID: preadmission[0].PreAdmissionIPADID,
                                        PatID: newCaseObj.PatID
                                    }
                                    await realmController.insertOrUpdateRecord(schema.PREADMISSION_SCHEMA, preAdmissionObject, true);
                                }
                            }
                        }
                    } else if (response && response.length == 0) {

                    }
                    return 'Success';
                }
            }).catch(err => {
                ErrorLogHandler(err, true, JSON.stringify(modifiedCaselistingDataServer));
                return;
            });
    }
}

// update patIPADID in pre admission schema
updatePatIPADID = async (FacilityCode) => {
    let preadmissionQuery = 'PatID != null AND FacilityCode ==[c] "' + FacilityCode + '" AND IsConflictExists == false';
    let preAdmissionRecords = realmController.getAllRecordsWithFilteringWithDate(schema.PREADMISSION_SCHEMA, preadmissionQuery);
    let newpreadmissions = [...preAdmissionRecords]
    if (newpreadmissions.length > 0) {
        realm.write(() => {
            for (let i = 0; i < newpreadmissions.length; i++) {
                let PreAdmission = { ...newpreadmissions[i] };
                let caseQuery = 'PatID == "' + PreAdmission.PatID + '"';
                let caseListings = realmController.getAllRecordsWithFilteringWithDate(schema.CASELISTING_SCHEMA, caseQuery);
                if (caseListings.length > 0) {
                    let caseListing = caseListings[0]
                    if (caseListing.patIPADID && PreAdmission.patIPADID === null) {
                        let Preadm = {};
                        Preadm.PreAdmissionIPADID = PreAdmission.PreAdmissionIPADID;
                        Preadm.patIPADID = caseListing.patIPADID;
                        realmController.insertOrUpdateRecordWithoutTransaction(schema.PREADMISSION_SCHEMA, Preadm, true);
                    }
                    if (caseListing.PreAdmissionID != null) {
                        caseListing.PreAdmissionID = PreAdmission.PreAdmissionID;
                        realmController.insertOrUpdateRecordWithoutTransaction(schema.CASELISTING_SCHEMA, caseListing, true);
                    }
                }
            }
        });
    }
    await this.updateAdmittedRecordsStatusInCase(FacilityCode); // for updating isadmitted to false
}

// Update all admitted records status in case listing
updateAdmittedRecordsStatusInCase = (FacilityCode) => {
    let pendingAdmitCaseQuery = 'IsAdmitted == true AND FacilityCode ==[c] "' + FacilityCode + '"';
    let caseRecords = [];
    caseRecords = realmController.getAllRecordsWithFiltering(schema.CASELISTING_SCHEMA, pendingAdmitCaseQuery);
    [...caseRecords].forEach(caseRecord => {
        if (caseRecord && caseRecord.patIPADID) {
            let caseDetails = {};
            caseDetails.patIPADID = caseRecord.patIPADID;
            caseDetails.IsAdmitted = false;
            realmController.insertOrUpdateRecord(schema.CASELISTING_SCHEMA, caseDetails, true);
        }
    });
}

removeAuditLog_InfoForLoginOrLogoutAfterSync = () => {
    let filterQuery = 'EntityType == 3 OR EntityType == 4';
    let getRecordsInfoFromAuditLog = realmController.getAllRecordsWithFiltering(schema.AuditLog_SCHEMA, filterQuery);
    let getRecordsFromAuditLogDetail = realm.objects(schema.AuditLogDetail_SCHEMA);

    let filterCriteria = 'AuditLevel == 2 AND IsOld == 1';
    let getOldRecordsInfo = realmController.getAllRecordsWithFiltering(schema.AuditLog_SCHEMA, filterCriteria);
    realm.write(() => {
        realm.delete(getRecordsInfoFromAuditLog);
        realm.delete(getRecordsFromAuditLogDetail);
        realm.delete(getOldRecordsInfo);
    })
}

syncPROiAppSettingsDataWhenSync = async (userBody) => {
    let data = {
        FacilityCode: userBody && userBody.FacilityCode ? userBody.FacilityCode : null
    }
    return await fetchMethodRequest('POST', api.getPROiAppSettingsData, data)
        .then(async (response) => {
            if (response == config.invalidGUIDCode) { // 401 response
                return 'invalidGUIDCode';
            } else if (response == config.generateGuidErrorCode) { //guid fail
                return 'FailGUID';
            } else if (response == 'serverError') {
                return 'serverError';
            } else {
                let dataBaseThubPrintFromPROiApp = common.getConfigurationDetails('DATABASETHUMBPRINT', 'DBUniqueID');
                if (response && response.PROiAppSettings && response.PROiAppSettings.length && response.PROiAppSettings.length > 0) {
                    let isDatabaseChanged = false;
                    common.updateDbChangeInUsersTable(userBody, false);
                    response.PROiAppSettings.forEach(settings => {
                        if (settings && settings.Property == 'DATABASETHUMBPRINT' && dataBaseThubPrintFromPROiApp) {
                            if (settings.Value != dataBaseThubPrintFromPROiApp) {
                                common.updateDbChangeInUsersTable(userBody, true);
                                isDatabaseChanged = 'databaseChanged';
                                return;
                            }
                        }
                        realmController.insertOrUpdateRecord(schema.PROIAPP_SETTINGS_SCHEMA, settings, true);
                    });
                    return isDatabaseChanged;
                }
            }
        })
}


export default {

    // Check server DB Version with Local DB Version Before sync start (From login only)
    syncPROiAppSettingsData: async (userBody, type) => {
        let data = {
            FacilityCode: userBody && userBody.FacilityCode ? userBody.FacilityCode : null
        }
        return await fetchMethodRequest('POST', api.getPROiAppSettingsData, data)
            .then(async (response) => {
                if (response == config.invalidGUIDCode) { // 401 response
                    return 'invalidGUIDCode';
                } else if (response == config.generateGuidErrorCode) { //guid fail
                    return 'FailGUID';
                } else if (response == 'serverError') {
                    return 'serverError';
                } else {
                    let dataBaseThubPrintFromPROiApp = common.getConfigurationDetails('DATABASETHUMBPRINT', 'DBUniqueID');

                    if (response && response.PROiAppSettings && response.PROiAppSettings.length && response.PROiAppSettings.length > 0) {
                        let isDatabaseChanged = false;
                        common.updateDbChangeInUsersTable(userBody, false);
                        response.PROiAppSettings.forEach(settings => {
                            if (settings && settings.Property == 'DATABASETHUMBPRINT' && dataBaseThubPrintFromPROiApp) {
                                if (settings.Value != dataBaseThubPrintFromPROiApp) {
                                    common.updateDbChangeInUsersTable(userBody, true);
                                    isDatabaseChanged = 'databaseChanged';
                                    return;
                                }
                            }
                            realmController.insertOrUpdateRecord(schema.PROIAPP_SETTINGS_SCHEMA, settings, true);
                        });
                        return isDatabaseChanged;
                    }
                }
            })
    },

    // sync UDS User Data
    syncUDSUserData: async (userBody, loggedInUserDetails, type) => {
        if (type == 'autoSync') {
            let dbInfo = await syncPROiAppSettingsDataWhenSync(userBody);
            if (dbInfo == 'databaseChanged') {
                return 'databaseChanged';
            }
            let appVersionInfo;
            let deviceAppversion = DeviceInfo.getVersion();
            let appStoreVersion = common.getConfigurationDetails('APPVERSION', 'versions');
            if (deviceAppversion && appStoreVersion && appStoreVersion != deviceAppversion) {
                let allowedAppVersionsList = common.getConfigurationDetails('ALLOWEDAPPVERSIONS', 'versions');
                if (allowedAppVersionsList) {
                    allowedAppVersionsList = allowedAppVersionsList.split(',');
                    let existingIndex = allowedAppVersionsList.findIndex((allowedVersion) => allowedVersion.trim() == deviceAppversion);
                    if (existingIndex == -1) {
                        appVersionInfo = 'appVersionChanged';
                    } else {
                        appVersionInfo = 'appVersionAllowed';
                        AsyncStorage.setItem('availableLatestVersion', JSON.stringify(true));
                    }
                }
            }

            if (appVersionInfo == 'appVersionChanged') {
                return 'appVersionChanged';
            } else {
                AsyncStorage.setItem('availableLatestVersion', JSON.stringify(false));
            }
        }

        let userSyncDetails = await getLastSyncDate('User', userBody);
        let userSyncDate = null;
        if (userSyncDetails && userSyncDetails.length > 0) {
            userSyncDate = userSyncDetails[0].LastSynDate;
        }
        let data = {
            ...userBody,
            LastSyncDate: dateFormats.formatWithTimeZone(userSyncDate, config.dbDateFormat)
        }
        // check login master data
        return await fetchMethodRequest('POST', api.loginMasterData, data)
            .then(async (response) => {
                if (response == config.invalidGUIDCode) { // 401 response
                    return 'invalidGUIDCode';
                } else if (response == config.generateGuidErrorCode) { //guid fail
                    return 'FailGUID';
                } else if (response == 'serverError') {
                    return 'serverError';
                } else {
                    if (response && response.Users) {
                        // Insert facility
                        if (response.Facility && response.Facility.length && response.Facility.length > 0) {
                            response.Facility.forEach(facility => {
                                if (facility && facility.FacilityCode) {
                                    realmController.insertOrUpdateRecord(schema.Facility_SCHEMA, facility, true);
                                }
                            });
                        }
                        if (response.Users.length && response.Users.length > 0) {
                            let userWithFacilityDetails, isUserModify = null;
                            response.Users.forEach(async serverUserObj => {
                                let originalServerObj = { ...serverUserObj };
                                if (serverUserObj && serverUserObj.UserID === userBody.UserID) {

                                    // get offline user and compare if changes are made
                                    let localUserDetails = realmController.getSingleRecord(schema.USERS_SCHEMA, `${userBody.UserID}-${userBody.FacilityCode}`);
                                    let localUserObj = { ...localUserDetails };

                                    // comparing local object with server object
                                    if (localUserObj) {
                                        delete localUserObj.InsertedBy;
                                        delete localUserObj.InsertedOn;
                                        delete localUserObj.UpdatedBy;
                                        delete localUserObj.UpdatedOn;
                                        delete localUserObj.Password;
                                        delete localUserObj.UserIdFacilityCode;
                                        delete localUserObj.ShowEST; // need to remove once feild added on server side
                                        delete localUserObj.IsDebugEnabled;
                                        delete localUserObj.LastLogInDate;
                                        delete localUserObj.LastLogOutDate;
                                        delete localUserObj.IsPreadmissionChecked;
                                        delete localUserObj.IsDbChanged
                                    }

                                    // Comparing dates like string
                                    serverUserObj.LastPWDChangeDate = dateFormats.formatDate(serverUserObj.LastPWDChangeDate, config.fullDateFormat);
                                    localUserObj.LastPWDChangeDate = localUserObj.LastPWDChangeDate ?
                                        dateFormats.formatDate(localUserObj.LastPWDChangeDate, config.fullDateFormat) : null;

                                    // remove z from local date
                                    localUserObj.LastPWDChangeDate = localUserObj.LastPWDChangeDate ?
                                        localUserObj.LastPWDChangeDate.replace('Z', '') : null;
                                    let isEqualObjects = common.objectsComparisionEquivalent(serverUserObj, localUserObj);
                                    // Update server user info in local realm
                                    let userDetails;
                                    if (loggedInUserDetails && loggedInUserDetails.Password) {
                                        userDetails = common.saveUserInfoInLocalRealm(originalServerObj, loggedInUserDetails.Password);
                                    }

                                    if (loggedInUserDetails && loggedInUserDetails.GUID) {
                                        userDetails.GUID = loggedInUserDetails.GUID;
                                    }
                                    userWithFacilityDetails = await common.saveCurrentLoggedUserInAsyncStorage(userDetails, 'online');

                                    if (!isEqualObjects) {
                                        // user userBody modified
                                        isUserModify = 'userModified'
                                    }
                                }
                            });
                            let syncDetails = await updateLastSynchronizeDetails('User', userBody);
                            return {
                                loggedInUserDetails: userWithFacilityDetails,
                                isUserModify: isUserModify,
                                syncDetails: syncDetails
                            }
                        }
                    }
                }
            }).catch(err => {
                ErrorLogHandler(err, true);
                return;
            });
    },

    // sync UDS MasterData
    syncUDSMasterData: async (userBody) => {
        let syncObject = await getLastSyncDate('MasterData', userBody);
        let masterSyncDate = null;
        if (syncObject.length > 0) {
            masterSyncDate = syncObject[0].LastSynDate;
        }
        let data = {
            ...userBody,
            LastSyncDate: dateFormats.formatWithTimeZone(masterSyncDate, config.dbDateFormat)
        }
        return await fetchMethodRequest('POST', api.getUDSMasterData, data)
            .then(async (response) => {
                if (response == config.invalidGUIDCode) { // 401 response
                    return 'invalidGUIDCode';
                } else if (response == config.generateGuidErrorCode) { //guid fail
                    return 'FailGUID';
                } else if (response == 'serverError') {
                    return 'serverError';
                } else {
                    let endTime = dateFormats.formatWithTimeZone('todayDate');
                    await common.errorLogForStartAndEndTimeOfAPICall(`UDSMasterEnd LastSyncDate - ${data.LastSyncDate} endDate-`, true, endTime);
                    if (response) {
                        realm.write(() => {
                            if (data && data.LastSyncDate) {
                                if (response.ICD10_MMT && response.ICD10_MMT.length > 0) {
                                    let ICD10_MMTDataFromRealm = realm.objects(schema.ICD10_MMT_SCHEMA);
                                    realm.delete(ICD10_MMTDataFromRealm);
                                }
                                if (response.IGICD10Link && response.IGICD10Link.length > 0) {
                                    let IGICD10LinkDataFromRealm = realm.objects(schema.IGICD10LINK_SCHEMA);
                                    realm.delete(IGICD10LinkDataFromRealm);
                                }

                                if (response.IGICD9Link && response.IGICD9Link.length > 0) {
                                    let IGICD9LinkTempDataFromRealm = realm.objects(schema.IGICD9Link_SCHEMA);
                                    realm.delete(IGICD9LinkTempDataFromRealm);
                                }

                                if (response.ImpairmentGroup && response.ImpairmentGroup.length > 0) {
                                    let ImpairmentGroupDataFromRealm = realm.objects(schema.IMPAIRMENTGROUP_SCHEMA);
                                    realm.delete(ImpairmentGroupDataFromRealm);
                                }
                            }

                            if (response.ICD10_MMT && response.ICD10_MMT.length > 0) {
                                response.ICD10_MMT.forEach(element => {
                                    realmController.insertOrUpdateRecordWithoutTransaction(schema.ICD10_MMT_SCHEMA, element, true);
                                });
                            }
                            if (response.IGICD10Link && response.IGICD10Link.length > 0) {
                                response.IGICD10Link.forEach(element => {
                                    element.Excl_RICS = element['Excl.RICS'];
                                    realmController.insertOrUpdateRecordWithoutTransaction(schema.IGICD10LINK_SCHEMA, element, true);
                                });
                            }
                            if (response.IGICD9Link && response.IGICD9Link.length > 0) {
                                response.IGICD9Link.forEach(element => {
                                    element.Excl_RICS = element['Excl.RICS'];
                                    realmController.insertOrUpdateRecordWithoutTransaction(schema.IGICD9Link_SCHEMA, element, true);
                                });
                            }
                            if (response.ImpairmentGroup && response.ImpairmentGroup.length > 0) {
                                response.ImpairmentGroup.forEach(element => {
                                    realmController.insertOrUpdateRecordWithoutTransaction(schema.IMPAIRMENTGROUP_SCHEMA, element, true);
                                });
                            }
                        });
                        let syncDetails = await updateLastSynchronizeDetails('MasterData', userBody);
                        return syncDetails;
                    }
                }
            }).catch(err => {
                ErrorLogHandler(err, true);
                return;
            });
    },

    // sync ICDData
    syncICDData: async (userBody) => {
        let syncObject = await getLastSyncDate('IcdData', userBody);
        let icdSyncDate = null;
        if (syncObject.length > 0) {
            icdSyncDate = syncObject[0].LastSynDate;
        }
        let data = {
            ...userBody,
            LastSyncDate: dateFormats.formatWithTimeZone(icdSyncDate, config.dbDateFormat)
        }
        return await fetchMethodRequest('POST', api.getICDData, data)
            .then(async (response) => {
                if (response == config.invalidGUIDCode) { // 401 response
                    return 'invalidGUIDCode';
                } else if (response == config.generateGuidErrorCode) { //guid fail
                    return 'FailGUID';
                } else if (response == 'serverError') {
                    return 'serverError';
                } else {
                    let endTime = dateFormats.formatWithTimeZone('todayDate');
                    await common.errorLogForStartAndEndTimeOfAPICall(`ICDDataEnd LastSyncDate - ${data.LastSyncDate} endDate-`, true, endTime);

                    if (response) {
                        realm.write(() => {

                            if (data && data.LastSyncDate) {
                                // delete current Icd10Compliace data in realm if any modified data in server
                                if (response.ICD10Compliance && response.ICD10Compliance.length > 0) {
                                    let ICD10ComplianceDataFromRealm = realm.objects(schema.ICD10Compliance_SCHEMA);
                                    realm.delete(ICD10ComplianceDataFromRealm);
                                }

                                // delete current Icd10Tier data in realm if any modified data in server
                                if (response.ICD10Tier && response.ICD10Tier.length > 0) {
                                    let ICD10TierDataFromRealm = realm.objects(schema.ICD10Tier_SCHEMA);
                                    realm.delete(ICD10TierDataFromRealm);
                                }
                            }

                            // insert icd10 compliance
                            if (response.ICD10Compliance && response.ICD10Compliance.length > 0) {
                                response.ICD10Compliance.forEach(element => {
                                    element.Excl_RICS = element['Excl.RICS'];
                                    realmController.insertOrUpdateRecordWithoutTransaction(schema.ICD10Compliance_SCHEMA, element, true);
                                });
                            }
                            // insert icd10Tier
                            if (response.ICD10Tier && response.ICD10Tier.length > 0) {
                                response.ICD10Tier.forEach(element => {
                                    element.Excl_RICS = element['Excl.RICS'];
                                    realmController.insertOrUpdateRecordWithoutTransaction(schema.ICD10Tier_SCHEMA, element, true);
                                });
                            }
                        });
                        let syncDetails = await updateLastSynchronizeDetails('IcdData', userBody);
                        return syncDetails;
                    }
                }
            }).catch(err => {
                ErrorLogHandler(err, true);
                return;
            });
    },

    // sync ICD10 Data
    syncICD10FullData: async (userBody) => {

        let syncObject = await getLastSyncDate('ICD10Data', userBody);
        let icdSyncDate = null;
        if (syncObject.length > 0) {
            icdSyncDate = syncObject[0].LastSynDate;
        }
        let data = {
            ...userBody,
            LastSyncDate: dateFormats.formatWithTimeZone(icdSyncDate, config.dbDateFormat),
        }
        if (data && data.LastSyncDate) {
            return await fetchMethodRequest('POST', api.getICD10IterationCount, data)
                .then(async (response) => {
                    if (response == config.invalidGUIDCode) { // 401 response
                        return 'invalidGUIDCode';
                    } else if (response == config.generateGuidErrorCode) { //guid fail
                        return 'FailGUID';
                    } else if (response == 'serverError') {
                        return 'serverError';
                    } else {
                        let endTime = dateFormats.formatWithTimeZone('todayDate');
                        await common.errorLogForStartAndEndTimeOfAPICall(`getICD10IterationCountEnd LastSyncDate - ${data.LastSyncDate} endDate-`, true, endTime);
                        let isInvalidGUID, FailGUID;
                        if (response) {
                            realm.write(() => {
                                if (data && data.LastSyncDate && response.Iterations > 0) {
                                    // delete current Icd10 data in realm if any modified data in server
                                    if (response.ICD10Data && response.ICD10Data.length > 0) {
                                        let ICD10DataFromRealm = realm.objects(schema.ICD10_SCHEMA);
                                        realm.delete(ICD10DataFromRealm);
                                    }
                                }
                            });
                            if (response.Iterations) {
                                let Iterations = response.Iterations;
                                let userBody = {
                                    UserID: data.UserID,
                                    FacilityCode: data.FacilityCode
                                }
                                for (let i = 1; i <= Iterations; i++) {
                                    let iterationsResponse = await syncICD10Data(i, userBody);
                                    if (iterationsResponse) {
                                        if (icd10DataResponse == 'invalidGUIDCode') {
                                            isInvalidGUID = 'invalidGUIDCode';
                                            return;
                                        } else if (icd10DataResponse == 'FailGUID') {
                                            FailGUID = 'FailGUID';
                                            return;
                                        }
                                    }
                                    let endTime = dateFormats.formatWithTimeZone('todayDate');
                                    await common.errorLogForStartAndEndTimeOfAPICall('End------ICD10----', true, endTime);
                                }
                            }
                            if (isInvalidGUID) {
                                return isInvalidGUID;
                            } else if (FailGUID) {
                                return FailGUID;
                            } else {
                                let syncDetails = await updateLastSynchronizeDetails('ICD10Data', userBody);
                                return syncDetails;
                            }

                        }
                    }
                }).catch(err => {
                    ErrorLogHandler(err, true);
                    return;
                });
        } else if (data && data.LastSyncDate == null) {
            let StartTime = dateFormats.formatWithTimeZone('todayDate');
            await common.errorLogForStartAndEndTimeOfAPICall('Start------ICD10----', true, StartTime);
            if (dataJson) {
                // insert icd10
                realm.write(() => {
                    if (dataJson.ICD10Data && dataJson.ICD10Data.length > 0) {
                        dataJson.ICD10Data.forEach(element => {
                            realmController.insertOrUpdateRecordWithoutTransaction(schema.ICD10_SCHEMA, element, true);
                        });
                    }
                });
            }
            if (dataJson1) {
                // insert icd10
                realm.write(() => {
                    if (dataJson1.ICD10Data && dataJson1.ICD10Data.length > 0) {
                        dataJson1.ICD10Data.forEach(element => {
                            realmController.insertOrUpdateRecordWithoutTransaction(schema.ICD10_SCHEMA, element, true);
                        });
                    }
                });
            }
            let endTime = dateFormats.formatWithTimeZone('todayDate');
            await common.errorLogForStartAndEndTimeOfAPICall('End------ICD10----', true, endTime);
            let syncDetails = await updateLastSynchronizeDetails('ICD10Data', userBody);
            return syncDetails;
        }
    },

    // sync ICD9 Full Data
    syncICD9FullData: async (userBody) => {
        let syncObject = await getLastSyncDate('ICD9Data', userBody);
        let icdSyncDate = null;
        if (syncObject.length > 0) {
            icdSyncDate = syncObject[0].LastSynDate;
        }
        let data = {
            ...userBody,
            LastSyncDate: dateFormats.formatWithTimeZone(icdSyncDate, config.dbDateFormat),
        }
        if (data && data.LastSyncDate) {
            return await fetchMethodRequest('POST', api.getICD9IterationCount, data)
                .then(async (response) => {
                    if (response == config.invalidGUIDCode) { // 401 response
                        return 'invalidGUIDCode';
                    } else if (response == config.generateGuidErrorCode) { //guid fail
                        return 'FailGUID';
                    } else if (response == 'serverError') {
                        return 'serverError';
                    } else {
                        let endTime = dateFormats.formatWithTimeZone('todayDate');
                        await common.errorLogForStartAndEndTimeOfAPICall(`getICD9IterationCountEnd LastSyncdate - ${data.LastSyncDate} endDate-`, true, endTime);
                        let isInvalidGUID, FailGUID;

                        if (response) {
                            realm.write(() => {
                                if (data && data.LastSyncDate && response.Iterations > 0) {
                                    // delete current Icd10 data in realm if any modified data in server
                                    if (response.ICD9Data && response.ICD9Data.length > 0) {
                                        let ICD9DataFromRealm = realm.objects(schema.ICD9Data_SCHEMA);
                                        realm.delete(ICD9DataFromRealm);
                                    }
                                }
                            });
                            if (response.Iterations) {
                                let Iterations = response.Iterations;
                                let StartTime = dateFormats.formatWithTimeZone('todayDate');
                                await common.errorLogForStartAndEndTimeOfAPICall('Start------ICD9----', true, StartTime);
                                let userBodyData = {
                                    UserID: data.UserID,
                                    FacilityCode: data.FacilityCode
                                }
                                for (let i = 1; i <= Iterations; i++) {
                                    let iterationsResponse = await syncICD9Data(i, userBodyData);
                                    if (iterationsResponse) {
                                        if (icd10DataResponse == 'invalidGUIDCode') {
                                            isInvalidGUID = 'invalidGUIDCode';
                                            return;
                                        } else if (icd10DataResponse == 'FailGUID') {
                                            FailGUID = 'FailGUID';
                                            return;
                                        }
                                    }
                                }
                                let endTime = dateFormats.formatWithTimeZone('todayDate');
                                await common.errorLogForStartAndEndTimeOfAPICall('End------ICD9----', true, endTime);
                            }
                        }
                        if (isInvalidGUID) {
                            return isInvalidGUID;
                        } else if (FailGUID) {
                            return FailGUID;
                        } else {
                            let syncDetails = await updateLastSynchronizeDetails('ICD9Data', userBody);
                            return syncDetails;
                        }
                    }
                }).catch(err => {
                    ErrorLogHandler(err, true);
                    return;
                });
        } else if (data && data.LastSyncDate == null) {
            let StartTime = dateFormats.formatWithTimeZone('todayDate');
            await common.errorLogForStartAndEndTimeOfAPICall('Start------ICD9----', true, StartTime);
            if (dataJson9) {
                // insert icd10
                realm.write(() => {
                    if (dataJson9.ICD9Data && dataJson9.ICD9Data.length > 0) {
                        dataJson9.ICD9Data.forEach(element => {
                            realmController.insertOrUpdateRecordWithoutTransaction(schema.ICD9Data_SCHEMA, element, true);
                        });
                    }
                });
            }
            let endTime = dateFormats.formatWithTimeZone('todayDate');
            await common.errorLogForStartAndEndTimeOfAPICall('End------ICD9----', true, endTime);
            let syncDetails = await updateLastSynchronizeDetails('ICD9Data', userBody);
            return syncDetails;
        }
    },

    // sync Facility Common Data
    syncFacilityCommonData: async (userBody) => {
        let syncObject = await getLastSyncDate('FacilityCommonData', userBody);
        let facilityCommonSyncDate = null;
        if (syncObject.length > 0) {
            facilityCommonSyncDate = syncObject[0].LastSynDate;
        }
        let data = {
            ...userBody,
            LastSyncDate: dateFormats.formatWithTimeZone(facilityCommonSyncDate, config.dbDateFormat)
        }
        return await fetchMethodRequest('POST', api.getFacilityCommonData, data)
            .then(async (response) => {
                if (response == config.invalidGUIDCode) { // 401 response
                    return 'invalidGUIDCode';
                } else if (response == config.generateGuidErrorCode) { //guid fail
                    return 'FailGUID';
                } else if (response == 'serverError') {
                    return 'serverError';
                } else {
                    let endTime = dateFormats.formatWithTimeZone('todayDate');
                    await common.errorLogForStartAndEndTimeOfAPICall(`FacilityCommonEnd LastSyncDate - ${data.LastSyncDate} endDate-`, true, endTime);

                    if (response) {
                        realm.write(() => {
                            // if (response.IRF_PAI_Versions && response.IRF_PAI_Versions.length > 0) {
                            //     response.IRF_PAI_Versions.forEach(element => {
                            //         realmController.insertOrUpdateRecordWithoutTransaction(schema.IRF_PAI_VERSIONS_SCHEMA, element, true);
                            //     });
                            // }
                            if (IRFVersionsJson) {
                                let IRFVersions = realm.objects(schema.IRF_PAI_VERSIONS_SCHEMA);
                                realm.delete(IRFVersions);
                                // insert icd10
                                if (IRFVersionsJson.IRFVersions && IRFVersionsJson.IRFVersions.length > 0) {
                                    IRFVersionsJson.IRFVersions.forEach(element => {
                                        realmController.insertOrUpdateRecordWithoutTransaction(schema.IRF_PAI_VERSIONS_SCHEMA, element, true);
                                    });
                                }
                            }
                            if (response.CMGGrouper && response.CMGGrouper.length > 0) {
                                response.CMGGrouper.forEach(element => {
                                    realmController.insertOrUpdateRecordWithoutTransaction(schema.CMGGROUPER_SCHEMA, element, true);
                                });
                            }
                            if (response.CaseMixGroup && response.CaseMixGroup.length > 0) {
                                response.CaseMixGroup.forEach(element => {
                                    realmController.insertOrUpdateRecordWithoutTransaction(schema.CASEMIXGROUP_SCHEMA, element, true);
                                });
                            }

                            if (response.MedicareFormats && response.MedicareFormats.length > 0) {
                                response.MedicareFormats.forEach(element => {
                                    realmController.insertOrUpdateRecordWithoutTransaction(schema.MEDICARE_FORMAT_SCHEMA, element, true);
                                });
                            }
                            if (response.ZipCodes && response.ZipCodes.length > 0) {
                                response.ZipCodes.forEach(element => {
                                    realmController.insertOrUpdateRecordWithoutTransaction(schema.ZIP_CODES_SCHEMA, element, true);
                                });
                            }
                            if (response.UTC_Timezone_Offset && response.UTC_Timezone_Offset.length > 0) {
                                response.UTC_Timezone_Offset.forEach(element => {
                                    realmController.insertOrUpdateRecordWithoutTransaction(schema.UTC_TIMEZONE_OFFSET, element, true);
                                });
                            }
                        });
                        let syncDetails = await updateLastSynchronizeDetails('FacilityCommonData', userBody);
                        return syncDetails;
                    }
                }
            }).catch(err => {
                ErrorLogHandler(err, true);
                return;
            });
    },

    // sync Facility Data
    syncFacilityData: async (userBody) => {
        let syncObject = await getLastSyncDate('Facility', userBody);
        let facilitySyncDate = null;
        if (syncObject.length > 0) {
            facilitySyncDate = syncObject[0].LastSynDate;
        }
        let data = {
            ...userBody,
            LastSyncDate: dateFormats.formatWithTimeZone(facilitySyncDate, config.dbDateFormat)
        }
        return await fetchMethodRequest('POST', api.getFacilityData, data)
            .then(async (response) => {
                if (response == config.invalidGUIDCode) { // 401 response
                    return 'invalidGUIDCode';
                } else if (response == config.generateGuidErrorCode) { //guid fail
                    return 'FailGUID';
                } else if (response == 'serverError') {
                    return 'serverError';
                } else {
                    let endTime = dateFormats.formatWithTimeZone('todayDate');
                    await common.errorLogForStartAndEndTimeOfAPICall(`FacilityEnd LastSyncDate - ${data.LastSyncDate} endDate-`, true, endTime);
                    if (response) {
                        let isInvalidGUID, FailGUID;

                        let emailData = [];
                        if (data && data.LastSyncDate) {
                            let modifiedList = getRecordsModifiedAfterLastSync(schema.EMAIL_SUBJECT_BODY, data.LastSyncDate);
                            emailData = [...modifiedList];
                        }
                        realm.write(async () => {
                            if (response.FacilitySite && response.FacilitySite.length > 0) {
                                response.FacilitySite.forEach(element => {
                                    realmController.insertOrUpdateRecordWithoutTransaction(schema.FACILITYSITE_SCHEMA, element, true);
                                });
                            }
                            if (response.Site && response.Site.length > 0) {
                                response.Site.forEach(element => {
                                    realmController.insertOrUpdateRecordWithoutTransaction(schema.SITE_SCHEMA, element, true);
                                });
                            }
                            if (response.Unit && response.Unit.length > 0) {
                                response.Unit.forEach(element => {
                                    realmController.insertOrUpdateRecordWithoutTransaction(schema.UNIT_SCHEMA, element, true);
                                });
                            }
                            if (response.EmailAddresses && response.EmailAddresses.length > 0) {
                                response.EmailAddresses.forEach(element => {
                                    realmController.insertOrUpdateRecordWithoutTransaction(schema.EMAIL_ADDRESSES, element, true);
                                });
                            }
                            if (response.CustomField && response.CustomField.length > 0) {
                                response.CustomField.forEach(element => {
                                    realmController.insertOrUpdateRecordWithoutTransaction(schema.CUSTOM_FIELD_SCHEMA, element, true);
                                });
                            }
                        });

                        if (data && data.LastSyncDate) {
                            if (response.EmailSubjectAndBody && response.EmailSubjectAndBody.length > 0) {


                                let index = 0;
                                for (let emailBodyInfo of response.EmailSubjectAndBody) {
                                    let emailBody = { ...emailBodyInfo };
                                    if (emailBody && emailBody.ID) {
                                        EmailSubjectIPADID = CheckRecordExists(schema.EMAIL_SUBJECT_BODY, emailBody.ID, 'ID', 'EmailSubjectIPADID');
                                        if (EmailSubjectIPADID == 0) {
                                            // Comparing Subject already exist or not
                                            if (emailData && emailData.length && emailData.length > 0) {
                                                let existingIndex = 0;
                                                for (let emailObj of emailData) {
                                                    if (emailObj && emailObj.EmailSubject) {
                                                        if (emailObj.EmailSubject == emailBody.EmailSubject) {
                                                            let existingEmailBody = { ...emailBody };
                                                            existingEmailBody.EmailSubjectIPADID = emailObj.EmailSubjectIPADID;
                                                            existingEmailBody.EntityType = 0;
                                                            existingEmailBody.UpdatedOn = dateFormats.formatWithTimeZone('todayDate');
                                                            existingEmailBody.UpdatedBy = data.UserID;
                                                            await realmController.insertOrUpdateRecord(schema.EMAIL_SUBJECT_BODY, existingEmailBody, true);
                                                            emailData.splice(existingIndex, 1);
                                                        } else {
                                                            emailBody.EmailSubjectIPADID = index + realmController.getMaxPrimaryKeyId(schema.EMAIL_SUBJECT_BODY, 'EmailSubjectIPADID');
                                                            emailBody.InsertedOn = dateFormats.formatWithTimeZone('todayDate');
                                                            emailBody.InsertedBy = data.UserID;
                                                            emailBody.EntityType = 0;
                                                            await realmController.insertOrUpdateRecord(schema.EMAIL_SUBJECT_BODY, emailBody, true);
                                                        }
                                                        existingIndex++;
                                                    }
                                                }
                                            } else {
                                                emailBody.EmailSubjectIPADID = index + realmController.getMaxPrimaryKeyId(schema.EMAIL_SUBJECT_BODY, 'EmailSubjectIPADID');
                                                emailBody.InsertedOn = dateFormats.formatWithTimeZone('todayDate');
                                                emailBody.InsertedBy = data.UserID;
                                                emailBody.EntityType = 0;
                                                await realmController.insertOrUpdateRecord(schema.EMAIL_SUBJECT_BODY, emailBody, true);
                                            }
                                        } else if (EmailSubjectIPADID > 0) {
                                            emailBody.EmailSubjectIPADID = EmailSubjectIPADID;
                                            emailBody.EntityType = 0;
                                            emailBody.UpdatedOn = dateFormats.formatWithTimeZone('todayDate');
                                            emailBody.UpdatedBy = data.UserID;
                                            await realmController.insertOrUpdateRecord(schema.EMAIL_SUBJECT_BODY, emailBody, true);

                                        }
                                    }
                                }

                                // save to api email subject and body
                                if (emailData && emailData.length && emailData.length > 0) {
                                    let emailBodyResponse = await emailBodySaveWithModifiedDetails(emailData);
                                    if (emailBodyResponse) {
                                        if (emailBodyResponse == 'invalidGUIDCode') {
                                            isInvalidGUID = 'invalidGUIDCode';
                                        } else if (emailBodyResponse == 'FailGUID') {
                                            FailGUID = 'FailGUID';
                                        }
                                    }
                                }



                            } else {
                                // save to api email subject and body
                                if (emailData && emailData.length && emailData.length > 0) {
                                    let emailBodyResponse = await emailBodySaveWithModifiedDetails(emailData);
                                    if (emailBodyResponse) {
                                        if (emailBodyResponse == 'invalidGUIDCode') {
                                            isInvalidGUID = 'invalidGUIDCode';
                                        } else if (emailBodyResponse == 'FailGUID') {
                                            FailGUID = 'FailGUID';
                                        }
                                    }
                                }
                            }
                        } else {
                            realm.write(() => {
                                if (response.EmailSubjectAndBody && response.EmailSubjectAndBody.length > 0) {
                                    emailData = getRecordsModifiedAfterLastSync(schema.EMAIL_SUBJECT_BODY, data.LastSyncDate);
                                    response.EmailSubjectAndBody.forEach((emailBody, index) => {
                                        emailBody.EmailSubjectIPADID = realmController.getMaxPrimaryKeyId(schema.EMAIL_SUBJECT_BODY, 'EmailSubjectIPADID');
                                        realmController.insertOrUpdateRecordWithoutTransaction(schema.EMAIL_SUBJECT_BODY, emailBody, true);
                                    });
                                }
                            });
                        }

                        if (isInvalidGUID) {
                            return isInvalidGUID;
                        } else if (FailGUID) {
                            return FailGUID;
                        } else {
                            let syncDetails = await updateLastSynchronizeDetails('Facility', userBody);
                            return syncDetails;
                        }
                    }
                }
            }).catch(err => {
                ErrorLogHandler(err, true);
                return;
            });
    },

    // sync Facility Category Master Data
    syncCategoryData: async (userBody) => {
        let syncObject = await getLastSyncDate('CategoryMasterData', userBody);
        let categorySyncDate = null;
        if (syncObject.length > 0) {
            categorySyncDate = syncObject[0].LastSynDate;
        }
        let data = {
            ...userBody,
            LastSyncDate: dateFormats.formatWithTimeZone(categorySyncDate, config.dbDateFormat)
        }
        return await fetchMethodRequest('POST', api.categoryData, data)
            .then(async (response) => {
                if (response == config.invalidGUIDCode) { // 401 response
                    return 'invalidGUIDCode';
                } else if (response == config.generateGuidErrorCode) { //guid fail
                    return 'FailGUID';
                } else if (response == 'serverError') {
                    return 'serverError';
                } else {
                    let endTime = dateFormats.formatWithTimeZone('todayDate');
                    await common.errorLogForStartAndEndTimeOfAPICall(`CategoryMasterEnd LastSyncDate - ${data.LastSyncDate} endDate-`, true, endTime);
                    let syncDetails;
                    if (response && response.length && response.length > 0) {
                        if (data && data.LastSyncDate) {

                        } else {
                            realm.write(() => {
                                response.forEach(category => {
                                    category.CategoryIPADID = realmController.getMaxPrimaryKeyId(schema.DROPDOWNS_SCHEMA, 'CategoryIPADID');
                                    if (category && category.CategoryID) {
                                        if (category.CategoryDetails && category.CategoryDetails.length && category.CategoryDetails.length > 0) {
                                            category.CategoryDetails.forEach(async (categoryDetail, index) => {
                                                categoryDetail.CategoryDetailIPADId = index + realmController.getMaxPrimaryKeyId(schema.DROPDOWN_OPTIONS_SCHEMA, 'CategoryDetailIPADId');
                                                categoryDetail.InsertedOn = dateFormats.formatWithTimeZone('todayDate');
                                                categoryDetail.InsertedBy = data.UserID;
                                            });
                                        }
                                        realmController.insertOrUpdateRecordWithoutTransaction(schema.DROPDOWNS_SCHEMA, category, true);
                                    }
                                });
                            });
                        }
                        syncDetails = await updateLastSynchronizeDetails('CategoryMasterData', userBody);
                        return syncDetails;
                    } else {
                        syncDetails = await updateLastSynchronizeDetails('CategoryMasterData', userBody);
                        return syncDetails;
                    }
                }
            }).catch(err => {
                ErrorLogHandler(err, true);
                return;
            });
    },

    // sync Facility Category Data
    syncFacilityCategoryData: async (userBody) => {
        let categoryDetailIPADId = 0, CategoryData;
        let syncObject = await getLastSyncDate('CategoryFacilityData', userBody);
        let facilityCategorySyncDate = null;
        if (syncObject.length > 0) {
            facilityCategorySyncDate = syncObject[0].LastSynDate;
        }
        let data = {
            ...userBody,
            LastSyncDate: dateFormats.formatWithTimeZone(facilityCategorySyncDate, config.dbDateFormat)
        }
        return await fetchMethodRequest('POST', api.facilityCategoryData, data)
            .then(async (response) => {
                if (response == config.invalidGUIDCode) { // 401 response
                    return 'invalidGUIDCode';
                } else if (response == config.generateGuidErrorCode) { //guid fail
                    return 'FailGUID';
                } else if (response == 'serverError') {
                    return 'serverError';
                } else {
                    let endTime = dateFormats.formatWithTimeZone('todayDate');
                    await common.errorLogForStartAndEndTimeOfAPICall(`CategoryFacilityEnd LastSyncDate - ${data.LastSyncDate} endDate-`, true, endTime);

                    //get the data modified after last synchronization from realm
                    CategoryData = getRecordsModifiedAfterLastSync(schema.DROPDOWN_OPTIONS_SCHEMA, data.LastSyncDate);
                    let isInvalidGUID, FailGUID;
                    if (response && response.length && response.length > 0) {
                        if (data && data.LastSyncDate) {
                            response.forEach(async (category) => {
                                if (category && category.CategoryID) {

                                    categoryIpadId = CheckRecordExists(schema.DROPDOWNS_SCHEMA, category.CategoryID, 'CategoryID', 'CategoryIPADID');
                                    category.CategoryIPADID = categoryIpadId == 0 ? realmController.getMaxPrimaryKeyId(schema.DROPDOWNS_SCHEMA, 'CategoryIPADID') : categoryIpadId;
                                    if (category.CategoryDetails && category.CategoryDetails.length > 0) {
                                        let index = 0;
                                        for (let categoryDetail of category.CategoryDetails) {
                                            categoryDetailIPADId = await CheckRecordExists(schema.DROPDOWN_OPTIONS_SCHEMA, categoryDetail.CategoryDetailID, 'CategoryDetailID', 'CategoryDetailIPADId');
                                            if (categoryDetailIPADId == 0) {
                                                // check for duplicate record exists or not
                                                let duplicateCategoryQuery = 'CategoryID == "' + category.CategoryID + '" AND Item ==[c] "' + categoryDetail.Item
                                                    + '" AND Notes ==[c] "' + categoryDetail.Item + '"';
                                                let dupCategoryDetails = [];
                                                dupCategoryDetails = realmController.getAllRecordsWithFiltering(schema.DROPDOWN_OPTIONS_SCHEMA, duplicateCategoryQuery);
                                                let isDupCategoryExists = [...dupCategoryDetails];

                                                if (isDupCategoryExists && isDupCategoryExists[0]) {
                                                    categoryDetail.CategoryDetailIPADId = isDupCategoryExists[0].CategoryDetailIPADId;
                                                } else {
                                                    categoryDetail.CategoryDetailIPADId = index + realmController.getMaxPrimaryKeyId(schema.DROPDOWN_OPTIONS_SCHEMA, 'CategoryDetailIPADId');
                                                    categoryDetail.InsertedOn = dateFormats.formatWithTimeZone('todayDate');
                                                    categoryDetail.InsertedBy = data.UserID;
                                                }
                                            } else if (categoryDetailIPADId > 0) {
                                                categoryDetail.CategoryDetailIPADId = categoryDetailIPADId;
                                                categoryDetail.UpdatedOn = dateFormats.formatWithTimeZone('todayDate');
                                                categoryDetail.UpdatedBy = data.UserID;
                                            }
                                            index++;
                                        }
                                    }
                                    realmController.insertOrUpdateRecord(schema.DROPDOWNS_SCHEMA, category, true);
                                }
                            });

                            //Call the API Method to post data to server and in the response set the categorydetailid in realm db
                            if (CategoryData && CategoryData.length > 0) {
                                let categoryResponse = await categorySaveWithMofiedDetails(CategoryData, data.UserID);
                                if (categoryResponse) {
                                    if (categoryResponse == 'invalidGUIDCode') {
                                        isInvalidGUID = 'invalidGUIDCode';
                                    } else if (categoryResponse == 'FailGUID') {
                                        FailGUID = 'FailGUID';
                                    }
                                }
                            }
                        } else {
                            realm.write(() => {
                                response.forEach(category => {
                                    category.CategoryIPADID = realmController.getMaxPrimaryKeyId(schema.DROPDOWNS_SCHEMA, 'CategoryIPADID');
                                    if (category && category.CategoryID) {
                                        if (category.CategoryDetails && category.CategoryDetails.length && category.CategoryDetails.length > 0) {
                                            category.CategoryDetails.forEach(async (categoryDetail, index) => {
                                                categoryDetail.CategoryDetailIPADId = index + realmController.getMaxPrimaryKeyId(schema.DROPDOWN_OPTIONS_SCHEMA, 'CategoryDetailIPADId');
                                                categoryDetail.InsertedOn = dateFormats.formatWithTimeZone('todayDate');
                                                categoryDetail.InsertedBy = data.UserID;
                                            });
                                        }
                                        realmController.insertOrUpdateRecordWithoutTransaction(schema.DROPDOWNS_SCHEMA, category, true);
                                    }
                                });
                            });
                        }

                        if (isInvalidGUID) {
                            return isInvalidGUID;
                        } else if (FailGUID) {
                            return FailGUID;
                        } else {
                            let syncDetails = await updateLastSynchronizeDetails('CategoryFacilityData', userBody);
                            return syncDetails;
                        }
                    } else if (response && response.length == 0) {

                        //Call the API Method to post data to server and in the response set the categorydetailid in realm db
                        if (CategoryData && CategoryData.length > 0) {
                            let categoryResponse = await categorySaveWithMofiedDetails(CategoryData, data.UserID);
                            if (categoryResponse) {
                                if (categoryResponse == 'invalidGUIDCode') {
                                    isInvalidGUID = 'invalidGUIDCode';
                                } else if (categoryResponse == 'FailGUID') {
                                    FailGUID = 'FailGUID';
                                }
                            }
                        }
                        if (isInvalidGUID) {
                            return isInvalidGUID;
                        } else if (FailGUID) {
                            return FailGUID;
                        } else {
                            let syncDetails = await updateLastSynchronizeDetails('CategoryFacilityData', userBody);
                            return syncDetails;
                        }
                    }
                }
            }).catch(err => {
                ErrorLogHandler(err, true);
                return;
            });
    },

    // Synchronize Preadmission Data
    syncPreadmissionData: async (userBody) => {
        let conflictPatIdsArray = [];
        let syncObject = await getLastSyncDate('Preadmission', userBody);
        let preAdmissionSyncDate = null;
        if (syncObject.length > 0) {
            preAdmissionSyncDate = syncObject[0].LastSynDate;
        }
        let data = {
            ...userBody,
            LastSyncDate: dateFormats.formatWithTimeZone(preAdmissionSyncDate, config.dbDateFormat)
        }

        return await fetchMethodRequest('POST', api.getPreAdmissionData, data)
            .then(async (response) => {
                if (response == config.invalidGUIDCode) { // 401 response
                    return 'invalidGUIDCode';
                } else if (response == config.generateGuidErrorCode) { //guid fail
                    return 'FailGUID';
                } else if (response == 'serverError') {
                    return 'serverError';
                } else {
                    let endTime = dateFormats.formatWithTimeZone('todayDate');
                    await common.errorLogForStartAndEndTimeOfAPICall(`PreadmissionEnd LastSyncDate - ${data.LastSyncDate} endDate-`, true, endTime);

                    //get the data modified after last synchronization from realm
                    let modifiedPreadmissionDataOffline = [], isInvalidGUID, FailGUID;
                    if (data.LastSyncDate) {
                        let modifiedList = getRecordsModifiedAfterLastSync(schema.PREADMISSION_SCHEMA, data.LastSyncDate);
                        modifiedPreadmissionDataOffline = [...modifiedList];
                    }
                    if (response && response.length > 0) {
                        if (data.LastSyncDate) {
                            let conflictPreadmissionData = [], conflictExists = false, conflictCount = 0, conflictsPreadmisisonArray = [];
                            for (let i = 0; i < response.length; i++) {
                                let object = { ...response[i].PreAdmission };
                                if (object && object.PreAdmissionID) {
                                    let conflictRecordIndex = modifiedPreadmissionDataOffline.findIndex(offlineRecord => offlineRecord.PreAdmissionID == object.PreAdmissionID);


                                    if (conflictRecordIndex != -1) {
                                        //check if add object or conflictrecord
                                        conflictPreadmissionData.push(modifiedPreadmissionDataOffline[conflictRecordIndex]);
                                        modifiedPreadmissionDataOffline.splice(conflictRecordIndex, 1);
                                        if (object && object.PatID) {
                                            conflictPatIdsArray.push(object.PatID);
                                        }
                                        if (!conflictExists) {
                                            conflictExists = true;
                                        }
                                        conflictCount++;
                                    } else {
                                        // for checking duplicate records with patient name and assessment date
                                        let query = 'FacilityCode ==[c] "' + object.FacilityCode +
                                            '" AND FirstName ==[c] "' + object.FirstName +
                                            '" AND LastName ==[c] "' + object.LastName +
                                            '" AND AssessmentDate = $0 AND DeletedBy == null and PreAdmissionID != ' + object.PreAdmissionID;

                                        let serverassDate = dateFormats.formatDate(object.AssessmentDate, 'YYYY-MM-DD');
                                        let isRecordExists = realmController.getAllRecordsWithFilteringWithDate(schema.PREADMISSION_SCHEMA, query, serverassDate);
                                        let existingRecord = [...isRecordExists];

                                        if (existingRecord && existingRecord[0]) {
                                            let preadmssionDetails = { ...existingRecord[0] };

                                            // check same record exists in modified records
                                            let conflictRecordIndex = modifiedPreadmissionDataOffline.findIndex(offlineRecord =>
                                                offlineRecord.PreAdmissionID == preadmssionDetails.PreAdmissionID);

                                            if (conflictRecordIndex != -1) {
                                                modifiedPreadmissionDataOffline.splice(conflictRecordIndex, 1);
                                            }
                                            preadmssionDetails.PreAdmissionID = object.PreAdmissionID;
                                            conflictPreadmissionData.push(preadmssionDetails);
                                            conflictCount++;
                                        } else {
                                            // get all conclict records in local for checking update senario
                                            let filterQuery = 'IsConflictExists == true AND FacilityCode ==[c]"' + object.FacilityCode + '"';
                                            conflictsPreadmisisonArray = realmController.getAllRecordsWithFiltering(schema.PREADMISSION_SCHEMA, filterQuery);

                                            // find index if it is matching with conflict record
                                            let conflictRecordIndex = conflictsPreadmisisonArray ? conflictsPreadmisisonArray.findIndex(offlineRecord => offlineRecord.PreAdmissionID == object.PreAdmissionID) : null;
                                            if (conflictRecordIndex == -1) {
                                                let preadmissionServerObject = common.formatPreadmissionData(object, 'update');
                                                preadmissionServerObject.IsConflictExists = false;
                                                realmController.insertOrUpdateRecord(schema.PREADMISSION_SCHEMA, preadmissionServerObject, true);
                                            } else {
                                                // If conflict already exists , check admit status in server and get case record based on patId
                                                if (object && object.PatID) {
                                                    conflictPatIdsArray.push(object.PatID);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            //save confilct data to realm
                            if (conflictCount > 0) {
                                conflictPreadmissionData.forEach(async (conflictPreadmission) => {
                                    let singlePreadmission = {};
                                    singlePreadmission.PreAdmissionID = conflictPreadmission.PreAdmissionID;  // for updating conflict record
                                    singlePreadmission.PreAdmissionIPADID = conflictPreadmission.PreAdmissionIPADID;
                                    singlePreadmission.IsConflictExists = true;
                                    await realmController.insertOrUpdateRecord(schema.PREADMISSION_SCHEMA, singlePreadmission, true);
                                });
                            }
                            //Call the API Method to post data to server and in the response set the preadmission id in realm db
                            if (modifiedPreadmissionDataOffline && modifiedPreadmissionDataOffline.length > 0) {
                                let preadmissionSaveResponse = await preadmsisionSaveWithModifiedDetails(modifiedPreadmissionDataOffline, data);
                                if (preadmissionSaveResponse) {
                                    if (preadmissionSaveResponse == 'invalidGUIDCode') {
                                        isInvalidGUID = 'invalidGUIDCode';
                                    } else if (preadmissionSaveResponse == 'FailGUID') {
                                        FailGUID = 'FailGUID';
                                    }
                                }
                            }
                        } else {
                            // first time sync
                            realm.write(() => {
                                for (let i = 0; i < response.length; i++) {
                                    let object = { ...response[i].PreAdmission };
                                    preAdmissionObject = common.formatPreadmissionData(object);
                                    realmController.insertOrUpdateRecordWithoutTransaction(schema.PREADMISSION_SCHEMA, preAdmissionObject, true);
                                }
                            });

                        }
                    } else {
                        //Call the API Method to post data to server and in the response set the preadmission id in realm db
                        if (modifiedPreadmissionDataOffline && modifiedPreadmissionDataOffline.length > 0) {
                            let preadmissionSaveResponse = await preadmsisionSaveWithModifiedDetails(modifiedPreadmissionDataOffline, data);
                            if (preadmissionSaveResponse) {
                                if (preadmissionSaveResponse == 'invalidGUIDCode') {
                                    isInvalidGUID = 'invalidGUIDCode';
                                } else if (preadmissionSaveResponse == 'FailGUID') {
                                    FailGUID = 'FailGUID';
                                }
                            }
                        }
                    }
                    if (isInvalidGUID) {
                        return isInvalidGUID;
                    } else if (FailGUID) {
                        return FailGUID;
                    } else {
                        let syncDetails = await updateLastSynchronizeDetails('Preadmission', userBody);
                        return {
                            conflictPatIdsArray: conflictPatIdsArray,
                            syncDetails: syncDetails
                        }
                    }
                }
            }).catch(err => {
                ErrorLogHandler(err, true);
                return;
            });
    },

    syncNotificationData: async (userBody) => {
        let syncObject = await getLastSyncDate('Notification', userBody);

        let preAdmissionSyncDate = null;
        if (syncObject.length > 0) {
            preAdmissionSyncDate = syncObject[0].LastSynDate;
        }
        let data = {
            ...userBody,
            LastSyncDate: dateFormats.formatWithTimeZone(preAdmissionSyncDate, config.dbDateFormat)
        }
        return await fetchMethodRequest('POST', api.getNotificationData, data)
            .then(async (response) => {
                if (response == config.invalidGUIDCode) { // 401 response
                    return 'invalidGUIDCode';
                } else if (response == config.generateGuidErrorCode) { //guid fail
                    return 'FailGUID';
                } else if (response == 'serverError') {
                    return 'serverError';
                } else {
                    let endTime = dateFormats.formatWithTimeZone('todayDate');
                    await common.errorLogForStartAndEndTimeOfAPICall(`NotificationEnd LastSyncDate - ${data.LastSyncDate} endDate-`, true, endTime);

                    let modifiedNotificationDataOffline = [], isInvalidGUID, FailGUID;
                    if (data.LastSyncDate) {
                        let modifiedList = getRecordsModifiedAfterLastSync(schema.NOTIFICATION_SCHEMA, data.LastSyncDate);
                        modifiedNotificationDataOffline = [...modifiedList];
                    }

                    if (response) {
                        if (data.LastSyncDate) {
                            if (modifiedNotificationDataOffline && modifiedNotificationDataOffline.length > 0) {
                                let NotifiationSaveResponse = await notificationSaveWithModifiedDetails(modifiedNotificationDataOffline);
                                if (NotifiationSaveResponse) {
                                    if (NotifiationSaveResponse == 'invalidGUIDCode') {
                                        isInvalidGUID = 'invalidGUIDCode';
                                    } else if (NotifiationSaveResponse == 'FailGUID') {
                                        FailGUID = 'FailGUID';
                                    }
                                }
                            }
                        }
                        if (response.Notifications && response.Notifications.length > 0) {
                            realm.write(() => {
                                response.Notifications.forEach((notifications) => {
                                    let notificationIpadId = CheckRecordExists(schema.NOTIFICATION_SCHEMA, notifications.NotificationID, 'NotificationID', 'NotificationIPADID');
                                    notifications.NotificationIPADID = notificationIpadId == 0 ? realmController.getMaxPrimaryKeyId(schema.NOTIFICATION_SCHEMA, 'NotificationIPADID') : notificationIpadId;
                                    notifications.EntityIPADID = CheckRecordExists(schema.PREADMISSION_SCHEMA, notifications.EntityID, 'PreAdmissionID', 'PreAdmissionIPADID');
                                    realmController.insertOrUpdateRecordWithoutTransaction(schema.NOTIFICATION_SCHEMA, notifications, true);
                                });
                            });
                        }
                    } else {
                        //send modifiedNotificationDataOffline to save api
                        if (modifiedNotificationDataOffline && modifiedNotificationDataOffline.length > 0) {
                            let NotifiationSaveResponse = await notificationSaveWithModifiedDetails(modifiedNotificationDataOffline);
                            if (NotifiationSaveResponse) {
                                if (NotifiationSaveResponse == 'invalidGUIDCode') {
                                    isInvalidGUID = 'invalidGUIDCode';
                                } else if (NotifiationSaveResponse == 'FailGUID') {
                                    FailGUID = 'FailGUID';
                                }
                            }
                        }
                    }
                    if (isInvalidGUID) {
                        return isInvalidGUID;
                    } else if (FailGUID) {
                        return FailGUID;
                    } else {
                        let syncDetails = await updateLastSynchronizeDetails('Notification', userBody);
                        return syncDetails;
                    }
                }

            }).catch(err => {
                ErrorLogHandler(err, true);
                return;
            });
    },

    // sync case listing data
    syncCaseListingData: async (userBody, conflictPatIdsArray) => {
        let syncObject = await getLastSyncDate('CaseListing', userBody);
        let caselistingSyncDate = null;
        if (syncObject.length > 0) {
            caselistingSyncDate = syncObject[0].LastSynDate;
        }
        let data = {
            ...userBody,
            LastSyncDate: dateFormats.formatWithTimeZone(caselistingSyncDate, config.dbDateFormat)
        }
        return await fetchMethodRequest('POST', api.getCaseListingData, data)
            .then(async (response) => {
                if (response == config.invalidGUIDCode) { // 401 response
                    return 'invalidGUIDCode';
                } else if (response == config.generateGuidErrorCode) { //guid fail
                    return 'FailGUID';
                } else if (response == 'serverError') {
                    return 'serverError';
                } else {


                    let endTime = dateFormats.formatWithTimeZone('todayDate');
                    await common.errorLogForStartAndEndTimeOfAPICall(`CaseListingEnd LastSyncDate - ${data.LastSyncDate} endDate-`, true, endTime);

                    let modifiedCaseListingDataOffline = [], isInvalidGUID, FailGUID;
                    if (data.LastSyncDate) {
                        let modifiedList = getRecordsModifiedAfterLastSync(schema.CASELISTING_SCHEMA, data.LastSyncDate);
                        modifiedCaseListingDataOffline = [...modifiedList];
                    }
                    if (response && response.length && response.length > 0) {
                        if (data && data.LastSyncDate) {
                            let conflictCaseListingData = [], conflictExists = false, conflictCount = 0, conflictsCaseArray = [];
                            for (let i = 0; i < response.length; i++) {
                                let object = { ...response[i].Patient };
                                if (object && object.PatID) {
                                    let conflictRecordIndex = modifiedCaseListingDataOffline.findIndex(x => x.PatID == object.PatID);
                                    if (conflictRecordIndex != -1) {
                                        //chk if add object or conflictrecord
                                        conflictCaseListingData.push(modifiedCaseListingDataOffline[conflictRecordIndex]);
                                        modifiedCaseListingDataOffline.splice(conflictRecordIndex, 1);
                                        if (!conflictExists) {
                                            conflictExists = true;
                                        }
                                        conflictCount++;
                                    } else {
                                        let caseRecordIndex = -1;
                                        if (conflictPatIdsArray) {
                                            caseRecordIndex = conflictPatIdsArray.indexOf(object.PatID);
                                        }
                                        if (caseRecordIndex == -1) {
                                            // for checking duplicate records with facility and patient id and admission date
                                            let query = 'FacilityCode ==[c] "' + object.FacilityCode + '" AND PatientID ==[c] "' + object.PatientID
                                                + '" AND AdmissionDate != null AND AdmissionDate = $0 AND DeletedBy == null AND PatID != ' + object.PatID;

                                            let serverAdmDate = dateFormats.formatDate(object.AdmissionDate, 'YYYY-MM-DD');

                                            let isRecordExists = realmController.getAllRecordsWithFilteringWithDate(schema.CASELISTING_SCHEMA, query, serverAdmDate);
                                            let existingRecord = [...isRecordExists];
                                            if (existingRecord && existingRecord[0]) {
                                                let caseDetails = { ...existingRecord[0] };
                                                // check same record exists in modified records
                                                let conflictRecordIndex = modifiedCaseListingDataOffline.findIndex(offlineRecord => offlineRecord.PatID == caseDetails.PatID);

                                                if (conflictRecordIndex != -1) {
                                                    modifiedCaseListingDataOffline.splice(conflictRecordIndex, 1);
                                                }

                                                caseDetails.PatID = object.PatID;
                                                conflictCaseListingData.push(caseDetails);
                                                conflictCount++;
                                            } else {
                                                // get all conclict records in local for checking update senario
                                                let filterQuery = 'IsConflictExists == true AND FacilityCode ==[c]"' + object.FacilityCode + '"';
                                                conflictsCaseArray = realmController.getAllRecordsWithFiltering(schema.CASELISTING_SCHEMA, filterQuery);

                                                // find index if it is matching with conflict record
                                                let conflictRecordIndex = conflictsCaseArray ? conflictsCaseArray.findIndex(offlineRecord => offlineRecord.PatID == object.PatID) : null;
                                                if (conflictRecordIndex == -1) {
                                                    let caseObject = common.formatCaseListingData(object, 'update')
                                                    caseObject.IsConflictExists = false;
                                                    realmController.insertOrUpdateRecord(schema.CASELISTING_SCHEMA, caseObject, true);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            //save confilct data to realm
                            if (conflictCount > 0) {
                                conflictCaseListingData.forEach((conflictCaselisting) => {
                                    let singleCaseListing = {};
                                    singleCaseListing.PatID = conflictCaselisting.PatID;
                                    singleCaseListing.patIPADID = conflictCaselisting.patIPADID;
                                    singleCaseListing.IsConflictExists = true;
                                    realmController.insertOrUpdateRecord(schema.CASELISTING_SCHEMA, singleCaseListing, true);
                                });

                            }


                            //Call the API Method to post data to server and in the response set the preadmission id in realm db
                            if (modifiedCaseListingDataOffline && modifiedCaseListingDataOffline.length && modifiedCaseListingDataOffline.length > 0) {
                                let CaseSaveResponse = await caseListingSaveWithMofiedDetails(modifiedCaseListingDataOffline);
                                if (CaseSaveResponse) {
                                    if (CaseSaveResponse == 'invalidGUIDCode') {
                                        isInvalidGUID = 'invalidGUIDCode';
                                    } else if (CaseSaveResponse == 'FailGUID') {
                                        FailGUID = 'FailGUID';
                                    }
                                }
                            }
                        } else {
                            realm.write(() => {
                                for (let i = 0; i < response.length; i++) {
                                    let object = { ...response[i].Patient };
                                    let caseObject = common.formatCaseListingData(object)
                                    realmController.insertOrUpdateRecordWithoutTransaction(schema.CASELISTING_SCHEMA, caseObject, true);
                                }
                            });
                        }
                    } else {

                        //Call the API Method to post data to server and in the response set the preadmission id in realm db
                        if (modifiedCaseListingDataOffline && modifiedCaseListingDataOffline.length > 0) {
                            let CaseSaveResponse = await caseListingSaveWithMofiedDetails(modifiedCaseListingDataOffline);
                            if (CaseSaveResponse) {
                                if (CaseSaveResponse == 'invalidGUIDCode') {
                                    isInvalidGUID = 'invalidGUIDCode';
                                } else if (CaseSaveResponse == 'FailGUID') {
                                    FailGUID = 'FailGUID';
                                }
                            }
                        }
                    }

                    if (isInvalidGUID) {
                        return isInvalidGUID;
                    } else if (FailGUID) {
                        return FailGUID;
                    } else {
                        await updatePatIPADID(data.FacilityCode); // update patIPADID in pre admission schema
                        let syncDetails = await updateLastSynchronizeDetails('CaseListing', userBody);
                        return syncDetails;
                    }
                }
            }).catch(err => {
                ErrorLogHandler(err, true);
                return;
            });
    },

    // check login master data
    syncAuditLogData: async () => {
        let auditLogArray = [];
        let getAllAuditLogRecords = realm.objects(schema.AuditLog_SCHEMA);
        if (getAllAuditLogRecords) {
            for (let singleAuditLog of getAllAuditLogRecords) {
                if (singleAuditLog) {
                    let auditLogInfo = {
                        AuditLog: singleAuditLog
                    }
                    let auditLog = { ...auditLogInfo.AuditLog };
                    if (singleAuditLog.AuditLogDetail && singleAuditLog.AuditLogDetail[0]) {
                        auditLog.AuditLogDetail = [singleAuditLog.AuditLogDetail[0]];
                    } else {
                        auditLog.AuditLogDetail = [];
                    }
                    auditLogInfo.AuditLog = auditLog;
                    auditLogArray.push(auditLogInfo);
                }
            }
        }
        return await fetchMethodRequest('POST', api.saveAuditLogData, auditLogArray)
            .then(async (response) => {
                if (response == config.invalidGUIDCode) { // 401 response
                    return 'invalidGUIDCode';
                } else if (response == config.generateGuidErrorCode) { //guid fail
                    return 'FailGUID';
                } else if (response == 'serverError') {
                    return 'serverError';
                } else {
                    if (response && response.ResponseCode == "Success") {
                        let endTime = dateFormats.formatWithTimeZone('todayDate');
                        await common.errorLogForStartAndEndTimeOfAPICall(`AuditLogSaveEnd`, true, endTime);
                        await removeAuditLog_InfoForLoginOrLogoutAfterSync();
                        return 'success';
                    } else {
                        return 'fail';
                    }
                }
            }).catch(err => {
                ErrorLogHandler(err, true);
                return;
            });
    },

    // Insert Error Log in server and Delete log after sync
    syncErrorLogData: async () => {
        let errorLogArray = [];
        let getAllErrorLogRecords = realm.objects(schema.EXCEPTION_HANDLING_SCHEMA);
        if (getAllErrorLogRecords) {
            let allErrorLogRecords = [...getAllErrorLogRecords]
            for (let singleErrorLog of allErrorLogRecords) {
                if (singleErrorLog) {
                    errorLogArray.push({ ...singleErrorLog });
                }
            }
        }
        return await fetchMethodRequest('POST', api.saveErrorLogDetails, errorLogArray)
            .then(async (response) => {
                if (response == config.invalidGUIDCode) { // 401 response
                    return 'invalidGUIDCode';
                } else if (response == config.generateGuidErrorCode) { //guid fail
                    return 'FailGUID';
                } else if (response == 'serverError') {
                    return 'serverError';
                } else {
                    if (response && response.ResponseCode == "Success") {
                        realm.write(() => {
                            realm.delete(getAllErrorLogRecords);
                        });
                        return 'success';
                    } else {
                        return 'fail';
                    }
                }
            }).catch(err => {
                ErrorLogHandler(err, true);
                return;
            });
    },

    // Insert Tier and Compliance for Advanced and Comorbid codes
    syncTierAndComplianceData: async (userBody) => {
        let syncObject = await getLastSyncDate('TierCompliance', userBody);
        let caselistingSyncDate = null;
        if (syncObject.length > 0) {
            caselistingSyncDate = syncObject[0].LastSynDate;
        }
        let data = {
            ...userBody,
            LastSyncDate: dateFormats.formatWithTimeZone(caselistingSyncDate, config.dbDateFormat)
        }
        return await fetchMethodRequest('POST', api.getComorbidAndAdvance, data)
            .then(async (response) => {
                if (response == config.invalidGUIDCode) { // 401 response
                    return 'invalidGUIDCode';
                } else if (response == config.generateGuidErrorCode) { //guid fail
                    return 'FailGUID';
                } else if (response == 'serverError') {
                    return 'serverError';
                } else {
                    if (response) {
                        let ComorbidAndAdvanceTierAndPeer = [];
                        if (response.ComorbidAndAdvanceTierAndPeer && response.ComorbidAndAdvanceTierAndPeer.length > 0) {
                            ComorbidAndAdvanceTierAndPeer = response.ComorbidAndAdvanceTierAndPeer;
                            if (data && data.LastSyncDate) {
                                let uniquePatId = null;
                                for (let singleCodeInfo of ComorbidAndAdvanceTierAndPeer) {
                                    if (singleCodeInfo && singleCodeInfo.PatID && uniquePatId != singleCodeInfo.PatID) {
                                        uniquePatId = singleCodeInfo.PatID;
                                        let filterCriteria = "PatID == " + uniquePatId;
                                        let existingCodesInfoFromRelam = realmController.getAllRecordsWithFiltering(schema.TIER_COMPLIANCE_SCHEMA, filterCriteria);
                                        if (existingCodesInfoFromRelam) {
                                            realm.write(() => {
                                                realm.delete(existingCodesInfoFromRelam);
                                            });
                                        }
                                    }
                                }

                                realm.write(() => {
                                    for (let singleCodeInfo of ComorbidAndAdvanceTierAndPeer) {
                                        if (singleCodeInfo && singleCodeInfo.PatID) {
                                            let singleCode = { ...singleCodeInfo };
                                            singleCode.ComorbidAndAdvanceTierAndPeerIPADID = realmController.getMaxPrimaryKeyId(schema.TIER_COMPLIANCE_SCHEMA, 'ComorbidAndAdvanceTierAndPeerIPADID');
                                            realmController.insertOrUpdateRecordWithoutTransaction(schema.TIER_COMPLIANCE_SCHEMA, singleCode, true);
                                        }
                                    }
                                });
                            } else {
                                realm.write(() => {
                                    for (let singleCodeInfo of ComorbidAndAdvanceTierAndPeer) {
                                        singleCodeInfo.ComorbidAndAdvanceTierAndPeerIPADID = realmController.getMaxPrimaryKeyId(schema.TIER_COMPLIANCE_SCHEMA, 'ComorbidAndAdvanceTierAndPeerIPADID');
                                        realmController.insertOrUpdateRecordWithoutTransaction(schema.TIER_COMPLIANCE_SCHEMA, singleCodeInfo, true);
                                    }
                                });
                            }
                        }
                        let syncDetails = await updateLastSynchronizeDetails('TierCompliance', userBody);
                        return syncDetails;
                    }
                }
            }).catch(err => {
                ErrorLogHandler(err, true);
                return;
            });
    },
}