/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import React, { Component } from 'react';
import {
    Text, View, FlatList, Alert, StatusBar
} from 'react-native';
import { Body, Title, List, ListItem, Header, Container, Right, Left } from 'native-base';

import config from '../../config/config';

// Realm 
import realm from '../../Realm/realm';
import schema from '../../Realm/schemaNames';
import realmController from '../../Realm/realmController';

// Components
import MainHeader from "../../components/mainHeader/mainHeader";
import ErrorMessage from '../../components/common/ErrorMessage';
import LoaderNew from '../../components/common/loaderNew';
import dateFormats from '../../utility/formatDate';

// Styles
import headerStyles from '../../components/styles/headerStyles';
import liststyles from '../../components/styles/liststyles';
import globalStyles from '../../components/styles/styles';


class ErrorLogsScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            errorLogs: [],
        }
    }
    componentDidMount() {
        this.updateDataInState('isLoading', false);
        this.getAllErrorLogs();
        this.props.getIsLoading(false);

    }

    // update data in state
    updateDataInState = (fieldName, value) => {
        this.setState({ [fieldName]: value });
    }

    // Get all error logs
    getAllErrorLogs = () => {
        if (this.props.loggedInUserDetails && this.props.loggedInUserDetails.FacilityCode && this.props.loggedInUserDetails.UserID) {
            this.updateDataInState('isLoading', true);
            let filterQuery = 'FacilityCode ==[c] ' + '"' + this.props.loggedInUserDetails.FacilityCode + '" AND InsertedBy = ' + '"' + this.props.loggedInUserDetails.UserID + '"';
            let allErrorLogs = [];
            allErrorLogs = realm.objects(schema.EXCEPTION_HANDLING_SCHEMA).filtered(filterQuery).sorted([['InsertedOn', true]]);
            this.setState({ errorLogs: allErrorLogs, isLoading: false });
        }
        // else {
        //     if (this.props.screenType == 'login') {
        //         let allErrorLogs = [];
        //         allErrorLogs = realm.objects(schema.EXCEPTION_HANDLING_SCHEMA);
        //         this.setState({ errorLogs: allErrorLogs, isLoading: false });
        //     }
        // }
    }

    clearErrorLogData = () => {
        this.setState({
            isLoading: true
        }, () => {
            let currentErrorLogs = [...this.state.errorLogs];
            currentErrorLogs.forEach((log) => {
                let errorLog = { ...log }
                let deleteErrorLog = dateFormats.datesComparisionSameOrBefore(errorLog.InsertedOn, dateFormats.formatDate('todayDate'));
                if (deleteErrorLog) {
                    let filterQuery = 'ExceptionHandlingID== ' + errorLog.ExceptionHandlingID;
                    let errorLogRecord = realmController.getAllRecordsWithFiltering(schema.EXCEPTION_HANDLING_SCHEMA, filterQuery);
                    realm.write(() => {
                        realm.delete(errorLogRecord);
                    });
                }
            });
            this.setState({
                errorLogs: [],
                isLoading: false
            })
        });
    }

    render() {
        StatusBar.setBarStyle('light-content', true);

        return (
            <Container style={{ backgroundColor: config.whiteColor }}>
                <Text style={globalStyles.topStatusBarColor} />
                <LoaderNew loading={this.state.isLoading} />
                {/* header1 */}
                <MainHeader homeScreenNavigatorId={this.props.homeScreenNavigatorId}
                    navigatorProps={this.props.componentId}
                    logScreen={true} />
                <Header style={headerStyles.headSpaceRemove}>
                    <Left></Left>
                    <Body>
                        <Title style={headerStyles.listHeading}>Error Logs</Title>
                    </Body>
                    <Right>
                        <Text style={headerStyles.privilegesViewLabel} onPress={() => this.clearErrorLogData()}>
                            ClearLog
                        </Text>
                    </Right>
                </Header>

                <View style={{ flex: 1 }}>
                    {/*List Display section start  */}
                    <List style={[liststyles.listData, { flex: 1 }]}>
                        <ListItem avatar style={liststyles.lists} >
                            <Body style={[liststyles.list, liststyles.width50]}>
                                <Text style={liststyles.headingList}>#</Text>
                            </Body>
                            <Body style={[liststyles.list]}>
                                <Text style={liststyles.headingList}>Description</Text>
                            </Body>
                            <Body style={[liststyles.list, liststyles.width120]}>
                                <Text style={liststyles.headingList}>Inserted On</Text>
                            </Body>
                        </ListItem>
                        <FlatList
                            data={this.state.errorLogs}
                            renderItem={({ item, index }) =>
                                <ListItem avatar style={liststyles.lists} >
                                    <Body style={[liststyles.list, liststyles.width50]}>
                                        <View style={liststyles.borderRight}>
                                            <Text style={liststyles.assDate}>
                                                {index + 1}
                                            </Text>
                                        </View>
                                    </Body>

                                    <Body style={[liststyles.list]}>
                                        <View style={liststyles.borderRight}>
                                            <Text style={liststyles.assDate}>
                                                {item.Description}
                                            </Text>
                                        </View>
                                    </Body>
                                    <Body style={[liststyles.list, liststyles.width120]}>
                                        <Text style={liststyles.assDate}>
                                            {dateFormats.formatDate(item.InsertedOn, 'DD MMM YYYY, HH:mm')}
                                        </Text>
                                    </Body>
                                </ListItem>
                            }
                            keyExtractor={(item, index) => index.toString()}
                            extraData={this.state.errorLogs}
                            ItemSeparatorComponent={this.renderSeparator}
                            ListEmptyComponent={<ErrorMessage />}
                        />
                    </List>
                    {/*List Display section End  */}
                </View>
            </Container >
        );
    }
}


export default ErrorLogsScreen;