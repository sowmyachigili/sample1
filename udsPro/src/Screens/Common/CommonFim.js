/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import React, { Component } from 'react';
import { Text, View, Alert } from 'react-native';
import { Left, Right } from 'native-base';
import { Navigation } from "react-native-navigation";

// input fields
import DefaultInput from '../../components/input/defaultInput';

// styles
import styles from '../../components/styles/styles';
import pickerSelectStyles from '../../components/styles/pickerSelectStyles';
import briefInfoStyles from '../../components/styles/briefInfoStyles';

// button group
import ButtonGroup from '../../components/buttonGroup/defaultBtnGroup';

//RnpickerSelect
import RNPickerSelect from '../../utility/RNPickerSelect';

// Realm
import realmController from '../../Realm/realmController';
import schema from '../../Realm/schemaNames';

// Fim calculation
import FIMCalculation from '../../utility/CaseFimChildTabsCalculation';

// config files
import config from '../../config/config';
import configMessage from '../../config/configMessages';

// commonFUn
import common from '../../utility/commonFun';

// navigation
import navigation from '../../utility/Navigation';

export default class CommonFim extends Component {
    constructor(props) {
        super(props);
        Navigation.events().bindComponent(this);
        this.inputRefs = {};
        this.oneToSeven = [1, 2, 3, 4, 5, 6, 7];
        this.zeroToSeven = [0, 1, 2, 3, 4, 5, 6, 7];
        this.userModifyThePage = false;
        // this.IsMiniFIMClearFields = ['Eating', 'Grooming', 'Bathing', 'DressingUpper', 'DressingLower', 'Toileting',
        //     'Bladder', 'Bowel', 'BedChair', 'Toilet', 'TubShower', 'WalkWheelChair', 'Stairs', 'Comprehension',
        //     'Expression', 'SocialInteraction', 'ProblemSolving', 'Memory', 'BedTransfer',
        //     'BladderAssistance', 'BladderFrequency', 'BowelAssistance', 'BowelFrequency', 'ShowerTransfer', 'DistanceWalked',
        //     'DistanceWheelchair', 'TubTransfer', 'Walk', 'WheelChair', 'ComprehensionType', 'ExpressionType',
        //     'LocoMotionType', 'Date'];

        this.IsMiniFIMClearFieldsScores = ['TotalFIM', 'TotalCognition', 'TotalMotor', 'FIMIPADID', 'IsMiniFIM',
            'HelpNeeded', 'FIMID', 'MotorScoreIndex'];

        this.clickEventCount = 0;
        this.state = {
            loggedInUserDetails: this.props.loggedInUserDetails,
            FIM: {},
            isMiniFim: null,
            checkFimIpadid: null,
            isAddNew: false,
            CFITool: false,
            isLoading: true,

            // select dropdowns
            BladderFrequency: [], // 30
            BowelFrequency: [], // 32
            DistanceWalked: [], // 35
            DistanceWheelchair: [], // 36
            LocoMotionType: [], //L
            ComprehensionType: [], // N
            ExpressionType: [], // O
        }
    }

    componentDidMount = async () => {
        this.props.onRef(this);
        let singleFim = {};
        // update admissionFim fields data
        if (this.props.admissionFim && this.props.admissionFim.EntityType == config.FimAdmissionEntityType) {
            singleFim = this.props.admissionFim;
        } else if (this.props.dischargeFim && this.props.dischargeFim.EntityType == config.FimDischargeEntityType) {
            // update dischargeFim fields data
            singleFim = this.props.dischargeFim;
        } else if (this.props.interimFim && this.props.interimFim.EntityType == config.FimInterimEntityType) {
            // update interimFim fields data
            singleFim = this.props.interimFim;
        } else if (this.props.followupFim && this.props.followupFim.EntityType == config.FimFollowUpEntityType) {
            // update followup fim fields data
            singleFim = this.props.followupFim;
        }

        await this.setState({
            FIM: singleFim
        })
        // get dropdowns data
        await this.getAllDropDownsData();

        // set loader 
        await this.sendloaderDataToParent(false);
    }

    componentWillUnmount() {
        this.props.onRef(null);
    }

    componentDidAppear = () => {
        this.clickEventsCount();
        this.sendloaderDataToParent(false)
    }

    // get all dropdowns Data
    getAllDropDownsData = () => {
        // All selct boxes
        let BladderAndBowelFrequency = realmController.getAllcategoriesDataFromRealm(this.state.loggedInUserDetails.FacilityCode, schema.BLADDER_CONTINENCE);
        let DistanceWalked = realmController.getAllcategoriesDataFromRealm(this.state.loggedInUserDetails.FacilityCode, schema.DISTANCE_WALK);
        let DistanceWheelchair = realmController.getAllcategoriesDataFromRealm(this.state.loggedInUserDetails.FacilityCode, schema.DISTANCE_WHEEL_CHAIR);
        let LocoMotionType = realmController.getAllcategoriesDataFromRealm(this.state.loggedInUserDetails.FacilityCode, schema.LOCOMOTION_TYPE);
        let ComprehensionType = realmController.getAllcategoriesDataFromRealm(this.state.loggedInUserDetails.FacilityCode, schema.COMPREHENSION_TYPE);
        let ExpressionType = realmController.getAllcategoriesDataFromRealm(this.state.loggedInUserDetails.FacilityCode, schema.EXPRESSION_TYPE);

        this.setState({
            BladderFrequency: [...BladderAndBowelFrequency], // 30
            BowelFrequency: [...BladderAndBowelFrequency], // 32
            DistanceWalked: DistanceWalked, // 35
            DistanceWheelchair: DistanceWheelchair, // 36
            LocoMotionType: LocoMotionType, // L
            ComprehensionType: ComprehensionType, // N
            ExpressionType: ExpressionType, // O
        });
    }

    // check it is mini fim or not
    checkMiniFimOrNot = () => {
        let isMiniFim = 0;
        if (this.props.FieldsDisplay == 'PAC-CFI') {
            isMiniFim = 1;
        } else {
            isMiniFim = 0;
        }
        return isMiniFim;
    }

    // handle value in button group
    handleFIMInstrument = async (value, name, tabName, fieldType, tabTypeOnSave) => {
        if ((!this.state.isLoading && !this.state.isAddNew && name) || tabName == 'Admission') {
            let fim = { ...this.state.FIM };
            if (tabName === 'Admission') {
                fim.Date = value ? value : null;
            }
            if (fim.EntityType == null) {
                fim.EntityType = this.props.EntityType;
            }
            if (fim.EntityIPADID == null) {
                fim.EntityIPADID = this.props.patIPADID;
            }
            if (fim.EntityID == null) {
                fim.EntityID = this.props.PatID;
            }
            fim.IsMiniFIM = this.checkMiniFimOrNot();
            if (fim.FIMIPADID == null) {
                fim.FIMIPADID = realmController.getMaxPrimaryKeyId(schema.CASE_FIM_SCHEMA, 'FIMIPADID');
            }
            if (name == 'Date') {
                fim[name] = value ? value : null;
            } else {
                fim[name] = this.getUpdatedValue(fieldType, tabName, name, value);
            }

            // convert string to integer
            if (name == 'BladderFrequency' || name == 'BowelFrequency' || name == 'DistanceWalked' || name == 'DistanceWheelchair') {
                fim[name] = value ? parseInt(value) : null;
            }

            // When 34. Field is '0'. show Validation message
            // TubShower filed data add
            if (name == 'TubTransfer') {

                if (fim.TubTransfer || fim.TubTransfer == 0 || fim.TubTransfer == null) {
                    if (fim.TubTransfer == 0 && (fim.EntityType == config.FimInterimEntityType || fim.EntityType == config.FimFollowUpEntityType
                        || fim.EntityType == config.FimDischargeEntityType)) {
                        fim.TubShower = 1;
                    } else if (fim.TubTransfer == 0 && fim.EntityType == config.FimAdmissionEntityType) {
                        fim.TubShower = 0;
                    } else {
                        fim.TubShower = fim.TubTransfer ? parseInt(fim.TubTransfer) : null;
                    }
                }
            } else if (name == 'ShowerTransfer') {
                if (fim.ShowerTransfer || fim.ShowerTransfer == null) {
                    if (fim.ShowerTransfer) {
                        fim.TubShower = parseInt(fim.ShowerTransfer)
                    } else {
                        fim.TubShower = null;
                    }
                }
            }
            if (name == 'ShowerTransfer' && value === 0) {
                this.props.openShowTransferModal(config.validationSummary, configMessage.zeroShowValidationMsg, 'Validation', '', '', true);
                fim[name] = null;
                fim.TubShower = null
            }

            // the least value in both the values is displayed
            if (name == 'BladderAssistance' || name == 'BladderFrequency') {
                if (fim.BladderAssistance && fim.BladderFrequency) {
                    fim.Bladder = this.leastValueAssign(fim, 'BladderAssistance', 'BladderFrequency')
                } else {
                    fim.Bladder = null;
                }
            }


            if (name == 'BowelAssistance' || name == 'BowelFrequency') {
                if (fim.BowelAssistance && fim.BowelFrequency) {
                    fim.Bowel = this.leastValueAssign(fim, 'BowelAssistance', 'BowelFrequency')
                } else {
                    fim.Bowel = null;
                }
            }

            // Fim fields calculation
            if (fim) {
                if (fim.IsMiniFIM == 1) {
                    if (fim.Toileting || fim.Bathing || fim.Bladder || fim.WalkWheelChair || fim.BedChair || fim.ProblemSolving) {
                        let data = FIMCalculation.checkedFIMStatusCalculation(fim);
                        fim = { ...fim, ...data };
                        let range = fim.TotalFIM.toString();
                        fim.HelpNeeded = common.calculateHelpNeededForVersion3(range);
                    } else {
                        fim.TotalFIM = null;
                        fim.TotalMotor = null;
                        fim.TotalCognition = null;
                    }

                    // calculate mini fim Projected motor score
                    if (fim.Toileting || fim.Bathing || fim.Bladder || fim.WalkWheelChair || fim.BedChair || fim.ProblemSolving) {
                        // let ProjectedMotorScore = FIMCalculation.calculateProjectedMotorScore(fim);
                        // fim.ProjectedMotorScore = ProjectedMotorScore;
                        fim.ProjectedMotorScore = null;
                    } else {
                        fim.ProjectedMotorScore = null;
                    }
                } else {
                    let data = FIMCalculation.uncheckedFIMStatusCalculation(fim);
                    fim = { ...fim, ...data };
                    fim.ProjectedMotorScore = null;
                }
            }

            if (fim.IsMiniFIM == 1 && (!fim.Bathing && !fim.Toileting && !fim.ProblemSolving &&
                !fim.WalkWheelChair && !fim.BedChair && !fim.Bladder && !fim.Date && !fim.LocoMotionType)) {
                // clear modifiers data also when deselect buttons group in 6-fim item 
                config.IsMiniFIMClearFields.forEach((i) => {
                    let name = i;
                    fim[name] = null;
                });
                config.IsMiniFIMClearFieldsScores.forEach((i) => {
                    let name = i;
                    fim[name] = null;
                });
                this.userModifyThePage = true;
                let existInRealm = realmController.getSingleRecord(schema.CASE_FIM_SCHEMA, fim.FIMIPADID);
                if (fim.EntityType && (fim.EntityType == config.FimInterimEntityType || fim.EntityType == config.FimFollowUpEntityType)) {
                    if (existInRealm && existInRealm.FIMIPADID) {
                        this.updateFimDataInParent(fim);
                    } else {
                        this.updateFimDataInParent(fim, 'deleteChanges');
                    }
                } else {
                    this.updateFimDataInParent(fim);
                }
            } else {
                this.userModifyThePage = true;
                // send data to corresponding tabs
                let isEmptyFields = common.fimFieldsEmptyInCommonFim(fim);
                if (!isEmptyFields && !fim.Date) {
                    fim.MotorScoreIndex = null;
                    fim.TotalFIM = null;
                    fim.TotalCognition = null;
                    fim.TotalMotor = null;
                    fim.HelpNeeded = null;
                }
                if (!tabTypeOnSave) {
                    this.updateFimDataInParent(fim);
                }
            }
            this.setState({
                FIM: fim,
                checkFimIpadid: fim.FIMIPADID
            });
        }
    }

    getUserModifiedValue = (value, type) => {
        if (type) {
            this.userModifyThePage = value;
        }
        return this.userModifyThePage;
    }

    // send data to corresponding tabs
    updateFimDataInParent = async (fim, type) => {
        if (fim.EntityType == config.FimAdmissionEntityType) {
            await this.props.getAdmissionFimChildData(fim, type);
        }
        if (fim.EntityType == config.FimDischargeEntityType) {
            await this.props.getDischargeFIMChildData(fim, type);
        }
        if (fim.EntityType == config.FimInterimEntityType) {
            await this.props.getInterimFIMChildData(fim, type);
        }
        if (fim.EntityType == config.FimFollowUpEntityType) {
            await this.props.getFollowupFIMChildData(fim, type);
        }
    }

    // FIM Tab - Update  admission, discharge and goals object into state
    getUpdatedValue = (selectedFieldType, tabName, fieldName, value) => {
        // update edit field
        let fieldVal = value;
        if (selectedFieldType == 'btnGroup') {
            if (this.state.FIM[fieldName] == value) {
                fieldVal = null;
            }
        }
        return fieldVal;
    }

    // the least value in both the values is displayed
    leastValueAssign = (FIM, value1, value2) => {
        let tovalue = null
        if (FIM[value1] < parseInt(FIM[value2])) {
            tovalue = FIM[value1]
        } else if (FIM[value1] > parseInt(FIM[value2])) {
            tovalue = parseInt(FIM[value2]);
        } else if (FIM[value1] == parseInt(FIM[value2])) {
            tovalue = FIM[value1]
        }
        return tovalue;
    }

    // update interim data in current state
    updateDataInFim = (fim) => {
        this.setState({
            isAddNew: false
        })
        if (fim) {
            this.setState({
                FIM: fim
            });
            return fim;
        } else {
            this.setState({
                FIM: {}
            });
            return null;
        }
    }

    // get interimRecord
    getSingleFimData = () => {
        return this.state.FIM;
    }

    // send loader true or false to parent
    sendloaderDataToParent = (isLoading) => {
        setTimeout(() => {
            this.setState({
                isLoading: isLoading
            })
            this.props.getChildIsLoading(isLoading);
        }, 0)
    }


    // clear fim fields data
    onPressYesClearFimData = (actionType) => {
        if (actionType) {
            this.clearFIMData();
        } else {
            return;
        }
    }

    // clear fim fim fields when click on tool PAC to chanage fim fields
    clearFIMData = async () => {
        let fim = { ...this.state.FIM };
        if (fim && fim.FIMIPADID) {
            // Clear only fim Fields
            await config.IsMiniFIMClearFields.forEach(async (i) => {
                let name = i;
                if (fim[name] || fim[name] == 0) {
                    if (name != 'Date') {
                        fim[name] = null;
                    }

                }
            });
            // Clear only Calculation Fields
            await this.IsMiniFIMClearFieldsScores.forEach(async (i) => {
                let name = i;
                if (fim[name] || fim[name] == 0) {
                    if (name == 'IsMiniFIM') {
                        fim[name] = fim[name] == 0 ? 1 : 0;
                    } else {
                        if (name != 'FIMIPADID' && name != 'FIMID') {
                            fim[name] = null;
                        }
                    }
                }
            });
            let currentFimExistInrealm = realmController.getSingleRecord(schema.CASE_FIM_SCHEMA, fim.FIMIPADID);

            if (fim && fim.Date) {
                if (currentFimExistInrealm) {
                    await realmController.insertOrUpdateRecord(schema.CASE_FIM_SCHEMA, fim, true);

                }
                // send data to corresponding tabs
                await this.updateFimDataInParent(fim, 'clear');
            } else if (!currentFimExistInrealm) {
                // Delete data when change PAC tool and assessment date empty 
                if (fim.EntityType == config.FimDischargeEntityType) {
                    await this.props.getDischargeFIMChildData(fim, 'deleteChanges');
                }
                if (fim.EntityType == config.FimInterimEntityType) {
                    await this.props.getInterimFIMChildData(fim, 'deleteChanges');
                }
                if (fim.EntityType == config.FimFollowUpEntityType) {
                    await this.props.getFollowupFIMChildData(fim, 'deleteChanges');
                }
            } else {
                await this.updateFimDataInParent(fim);
            }
            this.setState({
                FIM: fim
            });

        }
    }


    // when click on add in interim/Followup tab empty existing data in current fields
    clearData = async () => {
        let fim = { ...this.state.FIM };
        let isAssessmentDateExist = false;
        if (fim.EntityType && fim.Date) {
            isAssessmentDateExist = true;
            if (fim.EntityType == config.FimInterimEntityType) {
                await this.props.getInterimFIMChildData(fim);
            } else if (fim.EntityType == config.FimFollowUpEntityType) {
                await this.props.getFollowupFIMChildData(fim);

            }
        }
        // Clear only Fim Fields
        config.IsMiniFIMClearFields.forEach((i) => {
            let name = i;
            fim[name] = null;
        });
        // Clear only Calculation Fields
        this.IsMiniFIMClearFieldsScores.forEach(async (i) => {
            let name = i;
            fim[name] = null;
        });

        if (fim.Date == null && this.state.checkFimIpadid && !isAssessmentDateExist) {
            fim.FIMIPADID = this.state.checkFimIpadid;
            if (fim.EntityType == config.FimInterimEntityType) {
                await this.props.getInterimFIMChildData(fim, 'deleteChanges');
            } else if (fim.EntityType == config.FimFollowUpEntityType) {
                await this.props.getFollowupFIMChildData(fim, 'deleteChanges');
            }
            this.setState({
                checkFimIpadid: null
            })
        }
        await this.setState({
            FIM: fim,
            isAddNew: true
        }, () => {
            this.setState({
                isAddNew: false

            })
        });
    }

    // when click on cancel Button , cancel changes since last save
    cancelAllChangesSinceLastSave = async (type) => {
        if (type == 'CancelChanges') {
            let fim = { ...this.state.FIM };
            let currentInterimFim = realmController.getSingleRecord(schema.CASE_FIM_SCHEMA, fim.FIMIPADID);
            let FimObject = {};
            if (currentInterimFim && currentInterimFim.FIMIPADID && currentInterimFim.EntityType == this.props.EntityType) {
                FimObject = currentInterimFim;
                await this.updateFimDataInParent(FimObject, 'CancelChanges');
            } else {
                FimObject = {};
                if (fim.EntityType == config.FimInterimEntityType && fim.FIMIPADID) {
                    await this.props.getInterimFIMChildData(fim, 'deleteChanges');
                }
                if (fim.EntityType == config.FimFollowUpEntityType && fim.FIMIPADID) {
                    await this.props.getFollowupFIMChildData(fim, 'deleteChanges');
                }
            }

            this.setState({
                FIM: FimObject
            });
            return FimObject;
        } else {
            return;
        }
    }

    // when click on functional modifiers link 
    onClickFunctionalModifiers = () => {
        this.sendloaderDataToParent(true);
        if (this.clickEventCount == 0) {
            this.clickEventCount++;
            let props = {
                loggedInUserDetails: this.props.loggedInUserDetails,
                componentId: this.props.componentId,
                homeScreenNavigatorId: this.props.homeScreenNavigatorId,
                tabName: this.props.tabName,
                EntityType: this.props.EntityType,
                patIPADID: this.props.patIPADID,
                PatID: this.props.PatID,
                fimObject: this.state.FIM,
                saveModifiersFimFieldsData: this.saveModifiersFimFieldsData,
                cancelChangesInTab: this.updateModifierFimChangesInTab
            };
            navigation.pushScreenNavigator(this.props.componentId, navigation.FUNCTIONAL_MODIFIERS_FIM_SCREEN, props);
        }
    }

    clickEventsCount = () => {
        this.clickEventCount = 0;
    }

    // when click on save in modifiers screen update data in parent 
    saveModifiersFimFieldsData = (screenName, fim) => {
        if (screenName == 'modifiersScreen') {
            this.updateFimDataInParent(fim);
            this.updateModifierFimChangesInTab(fim);
        }
    }

    // cancel changes in individual tab 
    updateModifierFimChangesInTab = (fim) => {
        let fimObj = fim;
        this.setState({
            FIM: fimObj
        });
    }

    render() {
        return (
            <View>
                {this.props.CFITool ? null :
                    <View>
                        <View style={styles.row}>
                            <Left>
                                <Text style={styles.fimHeading}>Function Modifiers</Text>
                            </Left>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.halfWithMargin40}>
                                <Text numberOfLines={1} style={styles.textStrong}>29. Bladder Level of Assistance</Text>
                                <Text numberOfLines={1} style={styles.textSmall}>(Score using FIM® Levels 1-7)</Text>
                            </View>
                            <View style={[styles.half2WithMargin, styles.center]}>
                                <ButtonGroup
                                    onPress={this.handleFIMInstrument}
                                    selectedIndex={this.state.FIM.BladderAssistance}
                                    buttons={this.oneToSeven}
                                    name="BladderAssistance"
                                />
                            </View>
                        </View>

                        <View style={styles.row}>
                            <View style={styles.halfWithMargin40}>
                                <Text numberOfLines={1} style={styles.textStrong}>30. Bladder Frequency of Accidents</Text>
                                <Text numberOfLines={2} style={styles.textSmall}>(Enter in Item 39G (Bladder) the lower (more dependent) scores from 29 and 30 above.)</Text>
                            </View>
                            <View style={[styles.half2WithMargin, styles.center]}>
                                <View style={[styles.width100]}>
                                    <RNPickerSelect
                                        displayName={'30. Bladder Frequency of Accidents'}
                                        hideDoneBar={false}
                                        placeholder={{
                                            label: '',
                                            value: null,
                                        }}
                                        items={this.state.BladderFrequency}
                                        onValueChange={(value) => {
                                            this.handleFIMInstrument(value, 'BladderFrequency')
                                        }}
                                        style={{ ...pickerSelectStyles }}
                                        value={this.state.FIM.BladderFrequency ? this.state.FIM.BladderFrequency.toString() : null}
                                        ref={(el) => {
                                            this.inputRefs.BladderFrequency = el;
                                        }}
                                    />
                                </View>
                            </View>
                        </View>

                        <View style={styles.row}>
                            <View style={styles.halfWithMargin40}>
                                <Text numberOfLines={1} style={styles.textStrong}>31. Bowel Level of Assistance</Text>
                                <Text numberOfLines={1} style={styles.textSmall}>(Score using FIM® Levels 1-7)</Text>
                            </View>
                            <View style={[styles.half2WithMargin, styles.center]}>
                                <ButtonGroup
                                    onPress={this.handleFIMInstrument}
                                    selectedIndex={this.state.FIM.BowelAssistance}
                                    buttons={this.oneToSeven}
                                    name="BowelAssistance"
                                />
                            </View>
                        </View>

                        <View style={styles.row}>
                            <View style={styles.halfWithMargin40}>
                                <Text numberOfLines={1} style={styles.textStrong}>32. Bowel Frequency of Accidents</Text>
                                <Text numberOfLines={2} style={styles.textSmall}>(Enter in Item 39H (Bowel) the lower (more dependent) scores from 31 and 32 above.)</Text>
                            </View>
                            <View style={[styles.half2WithMargin, styles.center]}>
                                <View style={[styles.width100]}>
                                    <RNPickerSelect
                                        displayName={'32. Bowel Frequency of Accidents'}
                                        hideDoneBar={false}
                                        placeholder={{
                                            label: '',
                                            value: null,
                                        }}
                                        items={this.state.BowelFrequency}
                                        onValueChange={(value) => {
                                            this.handleFIMInstrument(value, 'BowelFrequency')
                                        }}
                                        style={{ ...pickerSelectStyles }}
                                        value={this.state.FIM.BowelFrequency ? this.state.FIM.BowelFrequency.toString() : null}
                                        ref={(el) => {
                                            this.inputRefs.BowelFrequency = el;
                                        }}
                                    />

                                </View>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.halfWithMargin40}>
                                <Text numberOfLines={1} style={styles.textStrong}>33. Tub Transfer</Text>
                            </View>
                            <View style={[styles.half2WithMargin, styles.center]}>

                                <ButtonGroup
                                    disableSelected={this.state.FIM.ShowerTransfer || this.state.FIM.ShowerTransfer === 0 ? true : false}
                                    onPress={this.handleFIMInstrument}
                                    selectedIndex={this.state.FIM.TubTransfer}
                                    buttons={this.zeroToSeven}
                                    name="TubTransfer"
                                />
                            </View>
                        </View>

                        <View style={styles.row}>
                            <View style={styles.halfWithMargin40}>
                                <Text numberOfLines={1} style={styles.textStrong}>34. Shower Transfer</Text>
                                <Text numberOfLines={3} style={styles.textSmall}>(Score Items 33 and 34 using FIM® Levels 1-7; Use 0 if activity does not occur). See training manual for scoring of Item 39K (Tub/Shower Transfer)</Text>
                            </View>
                            <View style={[styles.half2WithMargin, styles.center]}>
                                <ButtonGroup
                                    disableSelected={this.state.FIM.TubTransfer || this.state.FIM.TubTransfer === 0 ? true : false}
                                    onPress={this.handleFIMInstrument}
                                    selectedIndex={this.state.FIM.ShowerTransfer}
                                    buttons={this.zeroToSeven}
                                    name="ShowerTransfer"
                                />
                            </View>
                        </View>



                        <View style={styles.row}>
                            <View style={styles.halfWithMargin40}>
                                <Text numberOfLines={1} style={styles.textStrong}>35. Distance Walked</Text>
                            </View>
                            <View style={[styles.half2WithMargin, styles.center]}>
                                <View style={[styles.width100]}>
                                    <RNPickerSelect
                                        displayName={'35. Distance Walked'}
                                        hideDoneBar={false}
                                        placeholder={{
                                            label: '',
                                            value: null,
                                        }}
                                        items={this.state.DistanceWalked}
                                        onValueChange={(value) => {
                                            this.handleFIMInstrument(value, 'DistanceWalked')
                                        }}
                                        style={{ ...pickerSelectStyles }}
                                        value={(this.state.FIM.DistanceWalked || this.state.FIM.DistanceWalked == 0) ?
                                            this.state.FIM.DistanceWalked.toString() : null}
                                        ref={(el) => {
                                            this.inputRefs.DistanceWalked = el;
                                        }}
                                        onDownArrow={() => common.moveFocusToSpecificFeild(this.inputRefs, 'admissionDistanceWheelchair', 'Dropdown')}
                                    />
                                </View>
                            </View>
                        </View>

                        <View style={styles.row}>
                            <View style={styles.halfWithMargin40}>
                                <Text numberOfLines={1} style={styles.textStrong}>36. Distance Traveled in Wheelchair</Text>
                            </View>
                            <View style={[styles.half2WithMargin, styles.center]}>
                                <View style={[styles.width100]}>
                                    <RNPickerSelect
                                        displayName={'36. Distance Traveled in Wheelchair'}
                                        hideDoneBar={false}
                                        placeholder={{
                                            label: '',
                                            value: null,
                                        }}
                                        items={this.state.DistanceWheelchair}
                                        onValueChange={(value) => {
                                            this.handleFIMInstrument(value, 'DistanceWheelchair')
                                        }}
                                        style={{ ...pickerSelectStyles }}
                                        value={((this.state.FIM.DistanceWheelchair || this.state.FIM.DistanceWheelchair == 0) ||
                                            this.state.FIM.DistanceWheelchair == 0) ?
                                            this.state.FIM.DistanceWheelchair.toString() : ''}
                                        ref={(el) => {
                                            this.inputRefs.admissionDistanceWheelchair = el;
                                        }}
                                    />
                                </View>
                            </View>
                        </View>

                        <View style={styles.row}>
                            <View style={styles.halfWithMargin40}>
                                <Text numberOfLines={1} style={styles.textStrong}>37. Walk</Text>
                            </View>
                            <View style={[styles.half2WithMargin, styles.center]}>
                                <ButtonGroup
                                    onPress={this.handleFIMInstrument}
                                    selectedIndex={this.state.FIM.Walk}
                                    buttons={this.zeroToSeven}
                                    name="Walk"
                                />
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.halfWithMargin40}>
                                <Text numberOfLines={1} style={styles.textStrong}>38. Wheelchair</Text>
                                <Text numberOfLines={3} style={styles.textSmall}>(Score Items 37 and 38 using FIM® Levels 1-7; Use 0 if activity does not occur). See training manual for scoring of Item 39L (Walk/Wheelchair)</Text>
                            </View>
                            <View style={[styles.half2WithMargin, styles.center]}>
                                <ButtonGroup
                                    onPress={this.handleFIMInstrument}
                                    selectedIndex={this.state.FIM.WheelChair}
                                    buttons={this.zeroToSeven}
                                    name="WheelChair"
                                />
                            </View>

                        </View>
                    </View>
                }
                {this.props.CFITool ?
                    <View style={styles.row}>
                        <Right>
                            <Text
                                style={[briefInfoStyles.fimModifiersLinkText]}
                                onRef={ref => (this.modifiersRef = ref)}
                                onPress={() => this.onClickFunctionalModifiers()}>
                                Click here for a functional modifier worksheet for the PAC-CFI
                        </Text>
                        </Right>
                    </View>
                    : null}
                {/* FIM® Instrument start */}
                <Text style={styles.heading}>FIM® Instrument</Text>

                <View >
                    {this.props.CFITool ?
                        <View>

                            <View style={styles.row}>
                                <View style={styles.width3ItemsPadding}>
                                    <View style={styles.row}>
                                        <Text numberOfLines={1} style={styles.formLabel}>Toileting</Text>
                                    </View>
                                    <View style={styles.width100}>
                                        <ButtonGroup
                                            onPress={this.handleFIMInstrument}
                                            selectedIndex={this.state.FIM.Toileting}
                                            buttons={this.oneToSeven}
                                            name="Toileting"
                                        />
                                    </View>
                                </View>
                                <View style={styles.width3ItemsPadding}>
                                    <View style={styles.row}>
                                        <Text numberOfLines={1} style={styles.formLabel}>Locomotion</Text>
                                    </View>
                                    <View style={styles.width100}>
                                        <ButtonGroup
                                            onPress={this.handleFIMInstrument}
                                            selectedIndex={this.state.FIM.WalkWheelChair}
                                            buttons={this.oneToSeven}
                                            name="WalkWheelChair"
                                        />
                                    </View>
                                </View>
                                <View style={styles.width3ItemsPadding}>
                                    <View style={styles.row}>
                                        <Text numberOfLines={1} style={styles.formLabel}>Bathing</Text>
                                    </View>
                                    <View style={styles.width100}>
                                        <ButtonGroup
                                            onPress={this.handleFIMInstrument}
                                            selectedIndex={this.state.FIM.Bathing}
                                            buttons={this.oneToSeven}
                                            name="Bathing"
                                        />
                                    </View>
                                </View>

                            </View>
                            <View style={styles.row}>
                                <View style={styles.width3ItemsPadding}>
                                </View>
                                <View style={styles.width3ItemsPadding}>
                                    <View style={styles.row}>
                                        <Text numberOfLines={1} style={styles.formLabel}>Locomotion Mode</Text>
                                    </View>
                                    <View style={styles.width100}>
                                        <RNPickerSelect
                                            displayName={'Locomotion Type'}
                                            hideDoneBar={false}
                                            placeholder={{
                                                label: '',
                                                value: null,
                                            }}
                                            items={this.state.LocoMotionType}
                                            onValueChange={(value) => {
                                                this.handleFIMInstrument(value, 'LocoMotionType')
                                            }}
                                            style={{
                                                ...pickerSelectStyles,
                                                inputIOS: {
                                                    ...pickerSelectStyles.inputIOS,
                                                    height: 38
                                                },
                                            }}
                                            value={this.state.FIM.LocoMotionType}
                                            ref={(el) => {
                                                this.inputRefs.picker2 = el;
                                            }}
                                        />
                                    </View>
                                </View>
                            </View>
                            <View style={styles.row}>
                                <View style={styles.width3ItemsPadding}>
                                    <View style={styles.row}>
                                        <Text numberOfLines={1} style={styles.formLabel}>Bed Transfer</Text>
                                    </View>
                                    <View style={styles.width100}>
                                        <ButtonGroup
                                            onPress={this.handleFIMInstrument}
                                            selectedIndex={this.state.FIM.BedChair}
                                            buttons={this.oneToSeven}
                                            name="BedChair"
                                        />
                                    </View>
                                </View>
                                <View style={styles.width3ItemsPadding}>
                                    <View style={styles.row}>
                                        <Text numberOfLines={1} style={styles.formLabel}>Bladder</Text>
                                    </View>
                                    <View style={styles.width100}>
                                        <ButtonGroup
                                            onPress={this.handleFIMInstrument}
                                            selectedIndex={this.state.FIM.Bladder}
                                            buttons={this.oneToSeven}
                                            name="Bladder"
                                        />
                                    </View>
                                </View>
                                <View style={styles.width3ItemsPadding}>
                                    <View style={styles.row}>
                                        <Text numberOfLines={1} style={styles.formLabel}>Problem Solving</Text>
                                    </View>
                                    <View style={styles.width100}>
                                        <ButtonGroup
                                            onPress={this.handleFIMInstrument}
                                            selectedIndex={this.state.FIM.ProblemSolving}
                                            buttons={this.oneToSeven}
                                            name="ProblemSolving"
                                        />
                                    </View>
                                </View>

                            </View>

                            <View style={[styles.row, { marginTop: 20 }]}>
                                <View style={styles.FIMCalHeading}>
                                    <Text numberOfLines={1} style={styles.totalHeading}>Projected Total Motor</Text>
                                </View>
                                <View style={[styles.FIMCalBox, styles.center]}>
                                    <View style={[styles.input100]}>
                                        <DefaultInput
                                            name={'TotalMotor'}
                                            disabled={true}
                                            image={'lock'}
                                            value={this.state.FIM.TotalMotor ? this.state.FIM.TotalMotor.toString() : ''} />
                                    </View>
                                </View>
                            </View>
                            <View style={styles.row}>
                                <View style={styles.FIMCalHeading}>
                                    <Text numberOfLines={1} style={styles.totalHeading}>Projected Total Cognition</Text>
                                </View>
                                <View style={[styles.FIMCalBox, styles.center]}>
                                    <View style={[styles.input100]}>
                                        <DefaultInput
                                            name={'TotalCognition'}
                                            disabled={true}
                                            image={'lock'}
                                            value={this.state.FIM.TotalCognition ? this.state.FIM.TotalCognition.toString() : ''} />
                                    </View>
                                </View>
                            </View>
                            <View style={styles.row}>
                                <View style={styles.FIMCalHeading}>
                                    <Text numberOfLines={1} style={styles.totalHeading}>Projected Total FIM</Text>
                                </View>
                                <View style={[styles.FIMCalBox, styles.center]}>
                                    <View style={[styles.input100]}>
                                        <DefaultInput
                                            name={'TotalFIM'}
                                            disabled={true}
                                            image={'lock'}
                                            value={this.state.FIM.TotalFIM ? this.state.FIM.TotalFIM.toString() : ''} />
                                    </View>
                                </View>
                            </View>
                            <View style={styles.row}>
                                <View style={styles.FIMCalHeading}>
                                    <Text numberOfLines={1} style={styles.totalHeading}>Help Needed</Text>
                                </View>
                                <View style={[styles.FIMCalBoxWidth30, styles.center]}>
                                    <View style={[styles.width72]}>
                                        <DefaultInput
                                            name={'HelpNeeded'}
                                            disabled={true}
                                            image={'lock'}
                                            value={this.state.FIM.HelpNeeded ? this.state.FIM.HelpNeeded.toString() : ''} />
                                    </View>
                                </View>
                            </View>
                        </View>
                        :
                        <View>
                            <View style={styles.row}>
                                <View style={styles.width3ItemsPadding}>
                                    <View style={styles.row}>
                                        <Text numberOfLines={1} style={styles.formLabel}>A. Eating</Text>
                                    </View>
                                    <View style={styles.width100}>
                                        <ButtonGroup
                                            onPress={this.handleFIMInstrument}
                                            selectedIndex={this.state.FIM.Eating}
                                            buttons={this.props.EntityType == config.FimAdmissionEntityType ?
                                                this.zeroToSeven : this.oneToSeven}
                                            name="Eating"
                                        />
                                    </View>
                                </View>
                                <View style={styles.width3ItemsPadding}>
                                    <View style={styles.row}>
                                        <Text numberOfLines={1} style={styles.formLabel}>B. Grooming</Text>
                                    </View>
                                    <View style={styles.width100}>
                                        <ButtonGroup
                                            onPress={this.handleFIMInstrument}
                                            selectedIndex={this.state.FIM.Grooming}
                                            buttons={this.props.EntityType == config.FimAdmissionEntityType ?
                                                this.zeroToSeven : this.oneToSeven}
                                            name="Grooming"
                                        />
                                    </View>
                                </View>
                                <View style={styles.width3ItemsPadding}>
                                    <View style={styles.row}>
                                        <Text numberOfLines={1} style={styles.formLabel}>C. Bathing</Text>
                                    </View>
                                    <View style={styles.width100}>
                                        <ButtonGroup
                                            onPress={this.handleFIMInstrument}
                                            selectedIndex={this.state.FIM.Bathing}
                                            buttons={this.props.EntityType == config.FimAdmissionEntityType ?
                                                this.zeroToSeven : this.oneToSeven}
                                            name="Bathing"
                                        />
                                    </View>
                                </View>
                            </View>
                            <View style={styles.row}>
                                <View style={styles.width3ItemsPadding}>
                                    <View style={styles.row}>
                                        <Text numberOfLines={1} style={styles.formLabel}>D. Dressing - Upper</Text>
                                    </View>
                                    <View style={styles.width100}>
                                        <ButtonGroup
                                            onPress={this.handleFIMInstrument}
                                            selectedIndex={this.state.FIM.DressingUpper}
                                            buttons={this.props.EntityType == config.FimAdmissionEntityType ?
                                                this.zeroToSeven : this.oneToSeven}
                                            name="DressingUpper"
                                        />
                                    </View>
                                </View>
                                <View style={styles.width3ItemsPadding}>
                                    <View style={styles.row}>
                                        <Text numberOfLines={1} style={styles.formLabel}>E. Dressing - Lower</Text>
                                    </View>
                                    <View style={styles.width100}>
                                        <ButtonGroup
                                            onPress={this.handleFIMInstrument}
                                            selectedIndex={this.state.FIM.DressingLower}
                                            buttons={this.props.EntityType == config.FimAdmissionEntityType ?
                                                this.zeroToSeven : this.oneToSeven}
                                            name="DressingLower"
                                        />
                                    </View>
                                </View>
                                <View style={styles.width3ItemsPadding}>
                                    <View style={styles.row}>
                                        <Text numberOfLines={1} style={styles.formLabel}>F. Toileting</Text>
                                    </View>
                                    <View style={styles.width100}>
                                        <ButtonGroup
                                            onPress={this.handleFIMInstrument}
                                            selectedIndex={this.state.FIM.Toileting}
                                            buttons={this.props.EntityType == config.FimAdmissionEntityType ?
                                                this.zeroToSeven : this.oneToSeven}
                                            name="Toileting"
                                        />
                                    </View>
                                </View>
                            </View>
                            <View style={styles.row}>
                                <View style={styles.width3ItemsPadding}>
                                    <View style={styles.row}>
                                        <Text numberOfLines={1} style={styles.formLabel}>G. Bladder</Text>
                                    </View>
                                    <View style={styles.width100}>
                                        <ButtonGroup
                                            disableSelected={true}
                                            onPress={this.handleFIMInstrument}
                                            selectedIndex={this.state.FIM.Bladder}
                                            buttons={this.oneToSeven}
                                            name="Bladder"
                                        />
                                    </View>
                                </View>
                                <View style={styles.width3ItemsPadding}>
                                    <View style={styles.row}>
                                        <Text numberOfLines={1} style={styles.formLabel}>H. Bowel</Text>
                                    </View>
                                    <View style={styles.width100}>
                                        <ButtonGroup
                                            disableSelected={true}
                                            onPress={this.handleFIMInstrument}
                                            selectedIndex={this.state.FIM.Bowel}
                                            buttons={this.oneToSeven}
                                            name="Bowel"
                                        />
                                    </View>
                                </View>
                                <View style={styles.width3ItemsPadding}>
                                    <View style={styles.row}>
                                        <Text numberOfLines={1} style={styles.formLabel}>I. Bed, Chair, WheelChair</Text>
                                    </View>
                                    <View style={styles.width100}>
                                        <ButtonGroup
                                            onPress={this.handleFIMInstrument}
                                            selectedIndex={this.state.FIM.BedChair}
                                            buttons={this.props.EntityType == config.FimAdmissionEntityType ?
                                                this.zeroToSeven : this.oneToSeven}
                                            name="BedChair"
                                        />
                                    </View>
                                </View>
                            </View>
                            <View style={styles.row}>
                                <View style={styles.width3ItemsPadding}>
                                    <View style={styles.row}>
                                        <Text numberOfLines={1} style={styles.formLabel}>J. Toilet</Text>
                                    </View>
                                    <View style={styles.width100}>
                                        <ButtonGroup
                                            onPress={this.handleFIMInstrument}
                                            selectedIndex={this.state.FIM.Toilet}
                                            buttons={this.props.EntityType == config.FimAdmissionEntityType ?
                                                this.zeroToSeven : this.oneToSeven}
                                            name="Toilet"
                                        />
                                    </View>
                                </View>
                                <View style={styles.width3ItemsPadding}>
                                    <View style={styles.row}>
                                        <Text numberOfLines={1} style={styles.formLabel}>K. Tub, Shower</Text>
                                    </View>
                                    <View style={styles.width100}>
                                        <ButtonGroup
                                            disableSelected={true}
                                            onPress={this.handleFIMInstrument}
                                            selectedIndex={this.state.FIM.TubShower}
                                            buttons={this.props.EntityType == config.FimAdmissionEntityType ?
                                                this.zeroToSeven : this.oneToSeven}
                                            name="TubShower"
                                        />
                                    </View>
                                </View>
                            </View>
                            <View style={styles.row}>
                                <View style={styles.width3ItemsPadding}>
                                    <View style={styles.row}>
                                        <Text numberOfLines={1} style={styles.formLabel}>L. Walk/WheelChair</Text>
                                    </View>
                                    <View style={styles.width100}>
                                        <ButtonGroup
                                            onPress={this.handleFIMInstrument}
                                            selectedIndex={this.state.FIM.WalkWheelChair}
                                            buttons={this.props.EntityType == config.FimAdmissionEntityType ?
                                                this.zeroToSeven : this.oneToSeven}
                                            name="WalkWheelChair"
                                        />
                                    </View>
                                </View>
                                <View style={styles.width3ItemsPadding}>
                                    <View style={styles.row}>
                                        <Text numberOfLines={1} style={styles.formLabel}>L. Locomotion Mode</Text>
                                    </View>
                                    <View style={styles.width100}>
                                        <RNPickerSelect
                                            displayName={'L. Locomotion Mode'}
                                            hideDoneBar={false}
                                            placeholder={{
                                                label: '',
                                                value: null,
                                            }}
                                            items={this.state.LocoMotionType}
                                            onValueChange={(value) => {
                                                this.handleFIMInstrument(value, 'LocoMotionType')
                                            }}
                                            style={{
                                                ...pickerSelectStyles,
                                                inputIOS: {
                                                    ...pickerSelectStyles.inputIOS,
                                                    height: 37
                                                },
                                            }}
                                            value={this.state.FIM.LocoMotionType}
                                            ref={(el) => {
                                                this.inputRefs.picker2 = el;
                                            }}
                                        />
                                    </View>
                                </View>
                                <View style={styles.width3ItemsPadding}>
                                    <View style={styles.row}>
                                        <Text numberOfLines={1} style={styles.formLabel}>M. Stairs</Text>
                                    </View>
                                    <View style={styles.width100}>
                                        <ButtonGroup
                                            onPress={this.handleFIMInstrument}
                                            selectedIndex={this.state.FIM.Stairs}
                                            buttons={this.props.EntityType == config.FimAdmissionEntityType ?
                                                this.zeroToSeven : this.oneToSeven}
                                            name="Stairs"
                                        />
                                    </View>
                                </View>
                            </View>
                            <View style={styles.row}>
                                <View style={styles.width3ItemsPadding}>
                                    <View style={styles.row}>
                                        <Text numberOfLines={1} style={styles.formLabel}>N. Comprehension</Text>
                                    </View>
                                    <View style={styles.width100}>
                                        <ButtonGroup
                                            onPress={this.handleFIMInstrument}
                                            selectedIndex={this.state.FIM.Comprehension}
                                            buttons={this.oneToSeven}
                                            name="Comprehension"
                                        />
                                    </View>
                                </View>
                                <View style={styles.width3ItemsPadding}>
                                    <View style={styles.row}>
                                        <Text numberOfLines={1} style={styles.formLabel}>N. Comprehension Mode</Text>
                                    </View>
                                    <View style={styles.width100}>
                                        <RNPickerSelect
                                            displayName={'Comprehension Type'}
                                            hideDoneBar={false}
                                            placeholder={{
                                                label: '',
                                                value: null,
                                            }}
                                            items={this.state.ComprehensionType}
                                            onValueChange={(value) => {
                                                this.handleFIMInstrument(value, 'ComprehensionType');
                                            }}
                                            style={{
                                                ...pickerSelectStyles,
                                                inputIOS: {
                                                    ...pickerSelectStyles.inputIOS,
                                                    height: 36
                                                },
                                            }}
                                            value={this.state.FIM.ComprehensionType}
                                            ref={(el) => {
                                                this.inputRefs.picker2 = el;
                                            }}
                                        />
                                    </View>
                                </View>
                            </View>
                            <View style={styles.row}>
                                <View style={styles.width3ItemsPadding}>
                                    <View style={styles.row}>
                                        <Text numberOfLines={1} style={styles.formLabel}>O. Expression</Text>
                                    </View>
                                    <View style={styles.width100}>
                                        <ButtonGroup
                                            onPress={this.handleFIMInstrument}
                                            selectedIndex={this.state.FIM.Expression}
                                            buttons={this.oneToSeven}
                                            name="Expression"
                                        />
                                    </View>
                                </View>
                                <View style={styles.width3ItemsPadding}>
                                    <View style={styles.row}>
                                        <Text numberOfLines={1} style={styles.formLabel}>O. Expression Mode</Text>
                                    </View>
                                    <View style={styles.width100}>
                                        <RNPickerSelect
                                            displayName={'Expression Type'}
                                            hideDoneBar={false}
                                            placeholder={{
                                                label: '',
                                                value: null,
                                            }}
                                            items={this.state.ExpressionType}
                                            onValueChange={(value) => {
                                                this.handleFIMInstrument(value, 'ExpressionType')
                                            }}
                                            style={{
                                                ...pickerSelectStyles,
                                                inputIOS: {
                                                    ...pickerSelectStyles.inputIOS,
                                                    height: 36
                                                },
                                            }}
                                            value={this.state.FIM.ExpressionType}
                                            ref={(el) => {
                                                this.inputRefs.picker2 = el;
                                            }}
                                        />
                                    </View>
                                </View>

                            </View>
                            <View style={styles.row}>
                                <View style={styles.width3ItemsPadding}>
                                    <View style={styles.row}>
                                        <Text numberOfLines={1} style={styles.formLabel}>P. Social Interaction</Text>
                                    </View>
                                    <View style={styles.width100}>
                                        <ButtonGroup
                                            onPress={this.handleFIMInstrument}
                                            selectedIndex={this.state.FIM.SocialInteraction}
                                            buttons={this.oneToSeven}
                                            name="SocialInteraction"
                                        />
                                    </View>
                                </View>
                                <View style={styles.width3ItemsPadding}>
                                    <View style={styles.row}>
                                        <Text numberOfLines={1} style={styles.formLabel}>Q. Problem Solving</Text>
                                    </View>
                                    <View style={styles.width100}>
                                        <ButtonGroup
                                            onPress={this.handleFIMInstrument}
                                            selectedIndex={this.state.FIM.ProblemSolving}
                                            buttons={this.oneToSeven}
                                            name="ProblemSolving"
                                        />
                                    </View>
                                </View>
                                <View style={styles.width3ItemsPadding}>
                                    <View style={styles.row}>
                                        <Text>{this.state.TotalMotor}</Text>
                                        <Text numberOfLines={1} style={styles.formLabel}>R. Memory</Text>
                                    </View>
                                    <View style={styles.width100}>
                                        <ButtonGroup
                                            onPress={this.handleFIMInstrument}
                                            selectedIndex={this.state.FIM.Memory}
                                            buttons={this.oneToSeven}
                                            name="Memory"
                                        />
                                    </View>
                                </View>
                            </View>
                            <View style={[styles.row, { marginTop: 25 }]}>
                                <View style={[styles.FIMCalHeading]}>
                                    <Text numberOfLines={1} style={styles.totalHeading}>Total Motor</Text>
                                </View>
                                <View style={[styles.FIMCalBox, styles.center]}>
                                    <View style={[styles.input100]}>
                                        <DefaultInput
                                            name={'TotalMotor'}
                                            disabled={true}
                                            image={'lock'}
                                            value={this.state.FIM.TotalMotor ? this.state.FIM.TotalMotor.toString() : ''} />
                                    </View>
                                </View>
                            </View>
                            <View style={styles.row}>
                                <View style={styles.FIMCalHeading}>
                                    <Text numberOfLines={1} style={styles.totalHeading}>Total Cognition</Text>
                                </View>
                                <View style={[styles.FIMCalBox, styles.center]}>
                                    <View style={[styles.input100]}>
                                        <DefaultInput
                                            name={'TotalCognition'}
                                            disabled={true}
                                            image={'lock'}
                                            value={this.state.FIM.TotalCognition ? this.state.FIM.TotalCognition.toString() : ''} />
                                    </View>
                                </View>
                            </View>
                            <View style={styles.row}>
                                <View style={styles.FIMCalHeading}>
                                    <Text numberOfLines={1} style={styles.totalHeading}>Total FIM</Text>
                                </View>
                                <View style={[styles.FIMCalBox, styles.center]}>
                                    <View style={[styles.input100]}>
                                        <DefaultInput
                                            name={'TotalFIM'}
                                            disabled={true}
                                            image={'lock'}
                                            value={this.state.FIM.TotalFIM ? this.state.FIM.TotalFIM.toString() : ''} />
                                    </View>
                                </View>
                            </View>
                        </View>
                    }
                </View>
            </View>
        );
    }
}

