/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Alert } from 'react-native';
import { Button, Icon, Left } from 'native-base';
import RNPickerSelect from '../../utility/RNPickerSelect';
import Modal from "react-native-modal";
import { PreviousNextView } from 'react-native-keyboard-manager';

// config files
import config from '../../config/config';

// Components
import DefaultInput from "../../components/input/defaultInput";
import common from "../../utility/commonFun";

// styles
import styles from '../../components/styles/styles';
import pickerSelectStyles from '../../components/styles/pickerSelectStyles';
import briefInfoStyles from '../../components/styles/briefInfoStyles';
import modalStyles from '../../components/styles/modalStyles';
import headerStyles from '../../components/styles/headerStyles';

import CommonInfoModal from '../Common/CommonInfoModal';

// config 
import configMessage from '../../config/configMessages';

export default class BMICalculator extends Component {
    constructor(props) {
        super(props);
        this.inputRefs = {};
        this.state = {
            // common info modal
            isOpenBMIValidationModal: false,
            infoMessage: '',
            modalIconType: '',
            modalAction: '',
            confirmActionBody: '',
            modalHeading: '',
            isBMIModal: false,
            removeAsterisk: true,
            HeightInInch: 0,
            HeightInFeet: 0,
            BMI: null,
            Weight: null,
            heightListInFeet: [
                { label: '1', value: 1 },
                { label: '2', value: 2 },
                { label: '3', value: 3 },
                { label: '4', value: 4 },
                { label: '5', value: 5 },
                { label: '6', value: 6 },
                { label: '7', value: 7 },
                { label: '8', value: 8 },
            ],
            heightListInInch: [
                { label: '1', value: 1 },
                { label: '2', value: 2 },
                { label: '3', value: 3 },
                { label: '4', value: 4 },
                { label: '5', value: 5 },
                { label: '6', value: 6 },
                { label: '7', value: 7 },
                { label: '8', value: 8 },
                { label: '9', value: 9 },
                { label: '10', value: 10 },
                { label: '11', value: 11 }
            ],

        }
    }

    // calculate BMI 
    calculateBMI = async () => {
        let messages = [], isWeightSpclChar = false;
        var weightSpclCharFormat = new RegExp(config.weightSpecialCharValidation);

        if (this.state.HeightInFeet == 0) {
            messages.push(configMessage.BMIHeightValidationMessage);
        } else if (!this.state.Weight) {
            messages.push(configMessage.BMIWeightValidationMessage);
        } else if (this.state.Weight == 0) {
            messages.push(configMessage.BMIWeigthCannotZeroValidation);
        } else {
            if (this.state.Weight && (weightSpclCharFormat.test(this.state.Weight))) {
                isWeightSpclChar = true;
                messages.push('Weight');
            }
        }
        if (messages && messages.length && messages.length > 0) {
            if (isWeightSpclChar) {
                this.openCommonInfoModal(messages, 'Validation', '', '', configMessage.BMIWeightSpclCharactersValidation, false);
            } else {
                this.openCommonInfoModal(messages, 'Validation', '', '', config.validationSummary, true);
            }
        } else {
            if (this.state.HeightInFeet) {
                height = (this.state.HeightInFeet * 12) + this.state.HeightInInch;
                let BMIValue = ((this.state.Weight * 703) / (height * height));
                let BMI = common.roundNumber(BMIValue, 1);
                if (BMI) {
                    await this.setState({
                        BMI: BMI.toString(),
                    });
                }
            }
        }
    }

    // Open common info modal
    openCommonInfoModal = async (message, iconType, modalAction, confirmActionBody, modalHeading, removeAsterisk) => {
        await this.setState({
            isOpenBMIValidationModal: true,
            infoMessage: message,
            modalIconType: iconType,
            modalHeading: modalHeading,
            modalAction: modalAction ? modalAction : '',
            confirmActionBody: confirmActionBody ? confirmActionBody : '',
            removeAsterisk: removeAsterisk
        });
    }

    // Hide succes modal
    hideBMIModal = () => {
        this.setState({
            Weight: '',
            BMI: '',
            HeightInFeet: 0,
            HeightInInch: 0,
        });
        this.props.hideBMIModal();
    }

    // close BMI validation modal
    hideBMIValidationModal = (body) => {
        this.setState({
            isOpenBMIValidationModal: false
        });
    }

    render() {
        return (
            <Modal
                isVisible={this.props.isOpenBMIModal}
                backdropOpacity={0.5}
                onBackButtonPress={() => this.hideBMIModal()}
            >
                <View style={modalStyles.modal}>

                    {/* <Loader loading={this.state.isLoading} /> */}

                    <View style={modalStyles.modalHead}>
                        <View style={modalStyles.leftContainer}>
                            <Text style={[{ textAlign: 'left' }]}>
                                {''}
                            </Text>
                        </View>
                        <Text style={[modalStyles.modalHeading, headerStyles.listHeading]}>BMI Calculator</Text>
                        <View style={modalStyles.rightContainer}>
                            <Button transparent onPress={() => this.hideBMIModal()}>
                                <Icon style={modalStyles.closeIcon} name="close-circle" />
                            </Button>
                        </View>
                    </View>
                    <View style={[modalStyles.modalBody]}>
                        <PreviousNextView>
                            <View style={[styles.row]}>
                                <View style={[styles.bmiCalculatorPopupFieldsWidth1, styles.formLabel]}>
                                    <Text numberOfLines={1}>Height: </Text>
                                </View>
                                <View style={[styles.bmiCalculatorPopupFieldsWidth2]}>
                                    <RNPickerSelect
                                        displayName='Height(feet)'
                                        hideDoneBar={false}
                                        placeholder={{
                                            label: '0',
                                            value: 0,
                                        }}
                                        placeholderTextColor={'black'}
                                        items={this.state.heightListInFeet}
                                        onValueChange={(value) => { this.setState({ HeightInFeet: value }) }}
                                        style={{ ...pickerSelectStyles }}
                                        value={this.state.HeightInFeet}
                                        ref={(el) => {
                                            this.inputRefs['HeightInFeet'] = el;
                                        }}
                                        onDownArrow={() => common.moveFocusToSpecificFeild(this.inputRefs, 'HeightInInch')}
                                    />
                                    <Text numberOfLines={1} style={[styles.formLabel, { paddingTop: 5 }]}> feet</Text>
                                </View>
                                <View style={[styles.bmiCalculatorPopupFieldsWidth3]}>
                                    <RNPickerSelect
                                        displayName='Height(inch)'
                                        hideDoneBar={false}
                                        placeholder={{
                                            label: '0',
                                            value: 0,
                                        }}
                                        placeholderTextColor={'black'}
                                        items={this.state.heightListInInch}
                                        onValueChange={(value) => { this.setState({ HeightInInch: value }) }}
                                        style={{ ...pickerSelectStyles }}
                                        value={this.state.HeightInInch}
                                        ref={(el) => {
                                            this.inputRefs['HeightInInch'] = el;
                                        }}
                                        onDownArrow={() => common.moveFocusToSpecificFeild(this.inputRefs, 'Weight')}
                                    />
                                    <Text numberOfLines={1} style={[styles.formLabel, { paddingTop: 5 }]}> inch(es)</Text>
                                </View>
                            </View>
                            <View style={[styles.row, { paddingTop: 15 }]}>
                                <View style={[styles.bmiCalculatorPopupFieldsWidth1, styles.formLabel]}>
                                    <Text numberOfLines={1}>Weight: </Text>
                                </View>
                                <View style={[styles.bmiCalculatorPopupFieldsWidth2]}>
                                    <DefaultInput
                                        name={'Weight'}
                                        maxLength={4} value={this.state.Weight}
                                        handlerFromParant={(value) => { this.setState({ Weight: value }) }}
                                        ref={(el) => {
                                            this.inputRefs['Weight'] = el;
                                        }} />
                                    <TouchableOpacity onPress={() => this.calculateBMI()}>
                                        <Text numberOfLines={1} style={[styles.formLabel, briefInfoStyles.textcssLink, { paddingTop: 10 }]}>Calculate</Text>
                                    </TouchableOpacity>
                                </View>
                                <Text numberOfLines={1}> pounds</Text>
                            </View>
                            <View style={[styles.row, styles.margin20]}>
                                <View style={[styles.bmiCalculatorPopupFieldsWidth1, styles.formLabel]}>
                                    <Text numberOfLines={1}>BMI: </Text>
                                </View>
                                <View style={[styles.bmiCalculatorPopupFieldsWidth2]}>
                                    <DefaultInput
                                        name={'BMI'}
                                        maxLength={30} value={this.state.BMI ? this.state.BMI : null}
                                        disabled={true}
                                        ref={(el) => {
                                            this.inputRefs['BMI'] = el;
                                        }}
                                    />
                                </View>
                            </View>
                            <View style={[styles.row, styles.marginBottom20]}>
                                <Text style={[styles.formLabel, { lineHeight: 20 }]}>
                                    Body mass index (BMI) is measure of body fat based on height and weight that applies to both adult men and women.
                                     BMI is a reliable indicator of total body fat, which is related to the risk of disease and death.
                                     The score is valid for both men and women but it does have some limits. The limits are:
                                    </Text>
                            </View>
                            <Text style={[styles.formLabel, { paddingLeft: 50 }]}>
                                <Text style={[styles.bmiCalculatorTextRadius]}>
                                    {'\u2022' + " "}</Text>
                                It may overestimate body fat in athletes and others who have a muscular build.
                                </Text>
                            <Text style={[styles.formLabel, { paddingLeft: 50 }]}>
                                <Text style={[styles.bmiCalculatorTextRadius]}>
                                    {'\u2022' + " "}</Text>It may underestimate body fat in older persons and others who have lost muscle mass.
                                </Text>
                        </PreviousNextView>
                    </View>
                    <View style={[modalStyles.modalFooter]}>
                        <Button style={[modalStyles.btnDefault, modalStyles.buttonLight]} onPress={() => this.hideBMIModal()}>
                            <Text style={[modalStyles.btnDefaultText, modalStyles.buttonLightText]}>Close</Text>
                        </Button>
                    </View>
                </View>
                {/* Confirm modal start */}
                <CommonInfoModal
                    modalHeading={this.state.modalHeading}
                    iconType={this.state.modalIconType}
                    isOpenCommonInfoModal={this.state.isOpenBMIValidationModal}
                    displayMessage={this.state.infoMessage}
                    hideCommonInfoModal={this.hideBMIValidationModal}
                    modalAction={this.state.modalAction}
                    confirmYesAction={this.state.modalAction && this.state.modalAction == 'Confirm' ?
                        this.clickCommonConfirmAction : ''}
                    confirmActionBody={this.state.confirmActionBody}
                    removeAsterisk={this.state.removeAsterisk}
                />
            </Modal >

        )
    }
}