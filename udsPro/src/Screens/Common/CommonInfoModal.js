/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import React, { Component } from 'react';
import { View, Image, Text, ScrollView, Keyboard, Linking } from 'react-native';
import { Button } from 'native-base';
import Modal from "react-native-modal";

// styles
import modalStyles from '../../components/styles/modalStyles';
import configImage from '../../config/configImages';
import config from '../../config/config';

import commonFun from '../../utility/commonFun';

let appStoreURLLink = '';
class CommonInfoModal extends Component {
    constructor(props) {
        super(props);
        this.clickEventCount = 0;
        this.state = {
            isLoading: false,
        }
    }

    // when opening new info modal clear count
    componentDidUpdate(props) {
        if (props.isOpenCommonInfoModal) {
            this.clearEventsCount();
            Keyboard.dismiss();
        }
    }

    // For hiding modal
    hideInfoModal = () => {
        if (this.clickEventCount == 0) {
            this.clickEventCount++;
            this.props.hideCommonInfoModal(this.props.confirmActionBody);
            if (this.props.confirmActionBody == 'appVersionChange') {
                if (appStoreURLLink) {
                    Linking.openURL(appStoreURLLink);
                }
            }
        }
    }
    // clear click events count to zero
    clearEventsCount = () => {
        this.clickEventCount = 0;

    }


    // For confirm buttons when click yes in modal
    confirmYesAction = () => {
        if (this.clickEventCount == 0) {
            this.clickEventCount++;
            if (this.props.confirmYesAction) {
                if (this.props.confirmActionBody != 'donNotCallHide') {
                    this.props.hideCommonInfoModal();
                }
                setTimeout(() => {
                    this.props.confirmYesAction(this.props.confirmActionBody);
                }, 10);
            }
        }
    }

    render() {
        let validationMessages;
        if (typeof this.props.displayMessage === 'object') {
            validationMessages = this.props.displayMessage.map((data, i) => {
                return (
                    data ?
                        <View key={i}>
                            {this.props.removeAsterisk ?
                                <Text style={modalStyles.validationText} >
                                    <Text>{data}</Text>
                                </Text>
                                :
                                <Text style={modalStyles.validationText} >
                                    <Text style={modalStyles.bigStar}>* </Text>
                                    <Text>{data}</Text>
                                </Text>
                            }
                        </View> : null
                )
            });
        }
        if (this.props.confirmActionBody == 'appVersionChange') {
            appStoreURLLink = commonFun.getConfigurationDetails('APPUPDATEIOSURL', 'versions');
        }

        return (
            <Modal
                isVisible={this.props.isOpenCommonInfoModal}
                backdropOpacity={0.5}
                onBackButtonPress={this.hideInfoModal}
                style={[modalStyles.modalSmall]} >
                <View style={[modalStyles.modalContent]}>
                    {/* <Loader loading={this.state.isLoading} /> */}
                    <ScrollView style={[modalStyles.modalSmallBody]}>
                        <View style={{ alignItems: 'center' }}>
                            {this.props.iconType === 'Success' ?
                                <Image style={[modalStyles.tickIcon]} source={configImage.tickBigIcon} />
                                : this.props.iconType === 'Validation' ?
                                    <Image style={[modalStyles.warning]} source={configImage.errorIcon} />
                                    : this.props.iconType === 'Warning' ?
                                        <Image style={[modalStyles.warning]} source={configImage.warningIcon} />
                                        : this.props.iconType === 'Delete' ?
                                            <Image style={[modalStyles.deleteIcon]} source={configImage.trashBigIcon} />
                                            : this.props.iconType === 'Confirm' ?
                                                <Image style={[modalStyles.logoutIcon]} source={configImage.questionIcon} />
                                                : null
                            }

                            {/* modal heading */}
                            {this.props.modalHeading ?
                                <Text style={modalStyles.informationText}>{this.props.modalHeading}</Text>
                                : null}
                            {
                                typeof this.props.displayMessage === 'string' && this.props.displayMessage ?
                                    <Text style={[modalStyles.validationText, { marginTop: 20 }]} >
                                        {this.props.displayMessage}
                                    </Text>
                                    : this.props.displayMessage && this.props.displayMessage.length
                                        && this.props.displayMessage.length > 0 ?
                                        <View style={{ marginTop: 20 }}>
                                            {validationMessages}
                                        </View>
                                        : null
                            }
                        </View>
                    </ScrollView>
                    <View style={[modalStyles.modalFooter]}>
                        {this.props.modalAction && this.props.modalAction === 'Confirm' ?
                            <View style={{ flexDirection: 'row', alignItems: "center" }}>
                                <Button style={[modalStyles.btnDefault, modalStyles.buttonDark, { marginRight: 20 }]} onPress={this.confirmYesAction}>
                                    <Text style={[modalStyles.btnDefaultText, modalStyles.buttonDarkText]}>{this.props.confirmBtnType ? 'OK' : 'Yes'}</Text>
                                </Button>
                                <Button style={[modalStyles.btnDefault, modalStyles.buttonLight]} onPress={this.hideInfoModal}>
                                    <Text style={[modalStyles.btnDefaultText, modalStyles.buttonLightText]}>{this.props.confirmBtnType ? 'Cancel' : 'No'}</Text>
                                </Button>
                            </View> :
                            <Button style={[modalStyles.btnDefault, modalStyles.buttonDark]} onPress={this.hideInfoModal}>
                                <Text style={[modalStyles.btnDefaultText, modalStyles.buttonDarkText]}>
                                    {this.props.confirmActionBody == 'dbVersionChange' ? 'Restore' :
                                        this.props.confirmActionBody == 'appVersionChange' ? 'Go To AppStore' : 'Ok'}</Text>
                            </Button>}
                    </View>
                </View>
            </Modal>
        )
    }
}

export default CommonInfoModal;