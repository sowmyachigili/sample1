/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import React, { Component } from 'react';
import { Text, View, TouchableOpacity, ScrollView, Image, Alert } from 'react-native';
import { Left, Header, Right, Button, Body, Title, Container } from 'native-base';
import { Navigation } from "react-native-navigation";
import AsyncStorage from '@react-native-community/async-storage';

// input fields
import DefaultInput from '../../components/input/defaultInput';

// styles
import styles from '../../components/styles/styles';
import pickerSelectStyles from '../../components/styles/pickerSelectStyles';
import headerStyles from '../../components/styles/headerStyles';
import modalStyles from '../../components/styles/modalStyles';

// button group
import ButtonGroup from '../../components/buttonGroup/defaultBtnGroup';

//RnpickerSelect
import RNPickerSelect from '../../utility/RNPickerSelect';

// Realm
import realmController from '../../Realm/realmController';
import schema from '../../Realm/schemaNames';

// Fim calculation
import FIMCalculation from '../../utility/CaseFimChildTabsCalculation';

// config files
import config from '../../config/config';
import configMessage from '../../config/configMessages';
import configImage from '../../config/configImages';

// commonFUn
import common from '../../utility/commonFun';

// navigation
import navigation from '../../utility/Navigation';

// MainHeader
import MainHeader from '../../components/mainHeader/mainHeader';

// modal 
import CommonInfoModal from '../Common/CommonInfoModal';

// interim/followup fim common warnings 
import InterimFollowupFim from '../../Screens/Case/Add/InterimFollowupFim';

export default class FunctionalModifiersFim extends Component {
    constructor(props) {
        super(props);
        Navigation.events().bindComponent(this);
        this.inputRefs = {};
        this.oneToSeven = [1, 2, 3, 4, 5, 6, 7];
        this.zeroToSeven = [0, 1, 2, 3, 4, 5, 6, 7];
        this.userModifyThePage = false;
        this.buttonActionType = undefined;
        this.IsMiniFIMClearFields = ['Eating', 'Grooming', 'Bathing', 'DressingUpper', 'DressingLower', 'Toileting',
            'Bladder', 'Bowel', 'BedChair', 'Toilet', 'TubShower', 'WalkWheelChair', 'Stairs', 'Comprehension',
            'Expression', 'SocialInteraction', 'ProblemSolving', 'Memory', 'Locomotion', 'BedTransfer',
            'TotalFIM', 'TotalCognition', 'TotalMotor', 'FIMIPADID', 'IsMiniFIM', 'BladderAssistance',
            'BladderFrequency', 'BowelAssistance', 'BowelFrequency', 'ShowerTransfer', 'DistanceWalked',
            'DistanceWheelchair', 'TubTransfer', 'Walk', 'WheelChair', 'ComprehensionType', 'ExpressionType',
            'LocoMotionType', 'Date', 'HelpNeeded', 'MotorScoreIndex'];
        this.state = {
            loggedInUserDetails: this.props.loggedInUserDetails,
            FIM: {
                FIMIPADID: this.props.FIMIPADID,
                FIMID: null,
                EntityType: this.props.EntityType,
                EntityID: null,
                EntityIPADID: null,
                Date: null,
                IsMiniFIM: null,
                Eating: null,
                Grooming: null,
                Bathing: null,
                DressingUpper: null,
                DressingLower: null,
                Toileting: null,
                Bladder: null,
                Bowel: null,
                BedChair: null,
                Toilet: null,
                TubShower: null,
                WalkWheelChair: null,
                Stairs: null,
                Comprehension: null,
                Expression: null,
                SocialInteraction: null,
                ProblemSolving: null,
                Memory: null,
                Locomotion: null,
                BedTransfer: null,
                Date: null,
                TotalMotor: null,
                TotalCognition: null,
                TotalFIM: null,
                BladderAssistance: null,
                BladderFrequency: null,
                BowelAssistance: null,
                BowelFrequency: null,
                ShowerTransfer: null,
                DistanceWalked: null,
                DistanceWheelchair: null,
                TubTransfer: null,
                Walk: null,
                WheelChair: null,
                ComprehensionType: null,
                ExpressionType: null,
                LocoMotionType: null,
                InsertedOn: null,
                InsertedBy: null,
                UpdatedOn: null,
                UpdatedBy: null,
                DeletedOn: null,
                DeletedBy: null,
                ProjectedMotorScore: null
            },
            isMiniFim: null,
            checkFimIpadid: null,
            isAddNew: false,
            CFITool: false,
            // select dropdowns
            BladderFrequency: [], // 30
            BowelFrequency: [], // 32
            DistanceWalked: [], // 35
            DistanceWheelchair: [], // 36
            LocoMotionType: [], //L
            ComprehensionType: [], // N
            ExpressionType: [], // O
            isOpenCommonInfoModal: false,
            infoMessage: '',
            modalIconType: '',
            modalHeading: '',
            modalAction: '',
            confirmActionBody: '',
            isRenderLoading: true,
            removeAsterisk: true,
            Case: {}
        }
    }

    componentDidAppear = () => {
        this.getNotifications();
    }

    componentDidMount = async () => {
        let fim = {};
        if (this.props.fimObject) {
            fim = { ...this.props.fimObject };
        }

        await this.getAllDropDownsData();
        await this.setState({
            FIM: fim,
            isRenderLoading: false
        });
        this.userModifyThePage = false;

    }



    // get all dropdowns Data
    getAllDropDownsData = () => {
        // All selct boxes
        let BladderAndBowelFrequency = realmController.getAllcategoriesDataFromRealm(this.state.loggedInUserDetails.FacilityCode, schema.BLADDER_CONTINENCE);
        let DistanceWalked = realmController.getAllcategoriesDataFromRealm(this.state.loggedInUserDetails.FacilityCode, schema.DISTANCE_WALK);
        let DistanceWheelchair = realmController.getAllcategoriesDataFromRealm(this.state.loggedInUserDetails.FacilityCode, schema.DISTANCE_WHEEL_CHAIR);
        let LocoMotionType = realmController.getAllcategoriesDataFromRealm(this.state.loggedInUserDetails.FacilityCode, schema.LOCOMOTION_TYPE);
        let ComprehensionType = realmController.getAllcategoriesDataFromRealm(this.state.loggedInUserDetails.FacilityCode, schema.COMPREHENSION_TYPE);
        let ExpressionType = realmController.getAllcategoriesDataFromRealm(this.state.loggedInUserDetails.FacilityCode, schema.EXPRESSION_TYPE);

        this.setState({
            BladderFrequency: [...BladderAndBowelFrequency], // 30
            BowelFrequency: [...BladderAndBowelFrequency], // 32
            DistanceWalked: DistanceWalked, // 35
            DistanceWheelchair: DistanceWheelchair, // 36
            LocoMotionType: LocoMotionType, // L
            ComprehensionType: ComprehensionType, // N
            ExpressionType: ExpressionType, // O
        });
    }

    // check user modified or not and go back to corresponding screens 
    checkUserModifiedOrNot = async (actionType) => {
        this.buttonActionType = actionType;
        if (actionType == 'Back' || actionType == 'Home' || actionType == 'Logout') {
            if (this.userModifyThePage) {
                this.openCommonInfoModal(configMessage.backConfirm, '', 'Confirm', 'Confirm');
            } else if (actionType == 'Logout') {
                // Call logout child ref function
                this.logoutClick();
            } else {
                this.backBtnHandler(actionType)
            }
        }
    }

    // Call logout child ref function
    logoutClick = () => {
        let onClickLogout = this.mainHeaderRef.onClickLogout();
    }

    // back button handler
    backBtnHandler = (type) => {
        if (type == 'Home') {
            navigation.popToSpecificRouteNavigator(this.props.homeScreenNavigatorId);
        } else {
            navigation.popScreenNavigator(this.props.componentId);
        }
    }

    // Open common info modal
    openCommonInfoModal = (modalHeading, messgae, iconType, modalAction, confirmActionBody, removeAsterisk) => {
        setTimeout(() => {
            this.setState({
                isOpenCommonInfoModal: true,
                infoMessage: messgae,
                modalIconType: iconType,
                modalHeading: modalHeading,
                modalAction: modalAction ? modalAction : '',
                confirmActionBody: confirmActionBody ? confirmActionBody : '',
                removeAsterisk: removeAsterisk
            });
        }, 10);
    }

    // Hide succes modal
    hideCommonInfoModal = (body) => {
        this.setState({
            isOpenCommonInfoModal: false
        });
    }

    // on click delete confirm action
    clickConfirmYesAction = async (body) => {
        if (body == 'Cancel') {
            let fim = { ...this.state.FIM };
            let fimObjectFromRealm = realmController.getSingleRecord(schema.CASE_FIM_SCHEMA, fim.FIMIPADID);
            if (fimObjectFromRealm && fimObjectFromRealm.FIMIPADID && fimObjectFromRealm.EntityType == this.props.EntityType) {
                this.setState({
                    FIM: fimObjectFromRealm
                });
                fimObjectFromRealm = { ...fimObjectFromRealm, ...this.props.fimObject };
            } else {
                fimObjectFromRealm = this.props.fimObject ? this.props.fimObject : {};
            }
            await this.props.cancelChangesInTab(fimObjectFromRealm);
            await this.backBtnHandler();
        } else {
            if (this.buttonActionType == 'Back' || this.buttonActionType == 'Home') {
                this.backBtnHandler();
            } else if (this.buttonActionType == 'Logout') {
                setTimeout(() => {
                    // Call logout child ref function
                    this.logoutClick();
                }, 1000);
            }
        }
    }

    // send data to individual tab when click on save 
    onClickSaveAction = async () => {
        let fim = { ...this.state.FIM };
        AsyncStorage.setItem('LoadType', JSON.stringify('modifiersSave'));
        this.props.saveModifiersFimFieldsData('modifiersScreen', fim);

        this.backBtnHandler();
        this.userModifyThePage = false;
    }

    // cancel all changes since last save
    cancelChangesSinceLastSave = () => {
        this.openCommonInfoModal(configMessage.commonCancelMessage, '', 'Confirm', 'Confirm', 'Cancel', true);
    }

    // handle modifier fim field data and send to common fim
    handleFimModifiersData = (value, name, tabName, fieldType) => {
        if (!this.state.isRenderLoading) {
            let fim = { ...this.state.FIM };
            if (fim.EntityType == null) {
                fim.EntityType = this.props.EntityType;
            }
            if (fim.EntityIPADID == null) {
                fim.EntityIPADID = this.props.patIPADID;
            }
            fim.IsMiniFIM = 1;
            if (fim.EntityID == null) {
                fim.EntityID = this.props.PatID;
            }
            if (fim.FIMIPADID == null) {
                fim.FIMIPADID = realmController.getMaxPrimaryKeyId(schema.CASE_FIM_SCHEMA, 'FIMIPADID');
            }
            if (name == 'Date') {
                fim[name] = value ? value : null;
            } else {
                fim[name] = this.getUpdatedValue(fieldType, tabName, name, value);
            }

            // convert string to integer
            if (name == 'BladderFrequency' || name == 'DistanceWalked' ||
                name == 'DistanceWheelchair') {
                fim[name] = value ? parseInt(value) : null;
            }

            // the least value in both the values is displayed
            if (name == 'BladderAssistance' || name == 'BladderFrequency') {
                if (fim.BladderAssistance && fim.BladderFrequency) {
                    fim.Bladder = this.leastValueAssign(fim, 'BladderAssistance', 'BladderFrequency')
                } else {
                    fim.Bladder = null;
                }
            }

            if (name == 'BowelAssistance' || name == 'BowelFrequency') {
                if (fim.BowelAssistance && fim.BowelFrequency) {
                    fim.Bowel = this.leastValueAssign(fim, 'BowelAssistance', 'BowelFrequency')
                } else {
                    fim.Bowel = null;
                }
            }

            // Fim fields calculation
            if (fim) {
                if (fim.IsMiniFIM == 1) {
                    if (fim.Toileting || fim.Bathing || fim.Bladder || fim.WalkWheelChair || fim.BedChair || fim.ProblemSolving) {
                        let data = FIMCalculation.checkedFIMStatusCalculation(fim);
                        fim = { ...fim, ...data };
                        let range = fim.TotalFIM.toString();
                        fim.HelpNeeded = common.calculateHelpNeededForVersion3(range);
                    } else {
                        fim.TotalFIM = null;
                        fim.TotalMotor = null;
                        fim.TotalCognition = null;
                    }

                    // calculate mini fim Projected motor score
                    if (fim.Toileting || fim.Bathing || fim.Bladder || fim.WalkWheelChair || fim.BedChair || fim.ProblemSolving) {
                        // let ProjectedMotorScore = FIMCalculation.calculateProjectedMotorScore(fim);
                        // fim.ProjectedMotorScore = ProjectedMotorScore;
                        fim.ProjectedMotorScore = null;
                    } else {
                        fim.ProjectedMotorScore = null;
                    }
                } else {
                    let data = FIMCalculation.uncheckedFIMStatusCalculation(fim);
                    fim = { ...fim, ...data };
                    fim.ProjectedMotorScore = null;
                }
            }

            this.setState({
                FIM: fim,
            });
            this.userModifyThePage = true;
        }
    }
    // FIM Tab - Update  admission, discharge and goals object into state
    getUpdatedValue = (selectedFieldType, tabName, fieldName, value) => {
        // update edit field
        let fieldVal = value;
        if (selectedFieldType == 'btnGroup') {
            if (this.state.FIM[fieldName] == value) {
                fieldVal = null;
            }
        }
        return fieldVal;
    }

    // the least value in both the values is displayed
    leastValueAssign = (FIM, value1, value2) => {
        let tovalue = null
        if (FIM[value1] < parseInt(FIM[value2])) {
            tovalue = FIM[value1]
        } else if (FIM[value1] > parseInt(FIM[value2])) {
            tovalue = parseInt(FIM[value2]);
        } else if (FIM[value1] == parseInt(FIM[value2])) {
            tovalue = FIM[value1]
        }
        return tovalue;
    }

    getNotifications = () => {
        if (this.mainHeaderRef) {
            this.mainHeaderRef.getAppNotificationsWhenSync();
        }
    }

    render() {
        return (
            <Container>
                <Text style={styles.topStatusBarColor} />
                {/* header1 */}
                <MainHeader
                    homeScreenNavigatorId={this.props.homeScreenNavigatorId}
                    navigatorProps={this.props.componentId}
                    page={'FimModifiers'}
                    clickHomeButton={this.checkUserModifiedOrNot}
                    onRef={ref => (this.mainHeaderRef = ref)}
                />

                <Header style={headerStyles.subHeader}>
                    <Left style={[{ width: '30%' }]}>
                        <TouchableOpacity onPress={() => this.checkUserModifiedOrNot('Back')} >
                            <Image source={configImage.backButtonIcon} style={headerStyles.backBtn} />
                        </TouchableOpacity>
                    </Left>
                    <Right style={headerStyles.buttonTopBottomSpace}>
                        <TouchableOpacity>
                            <Button style={[modalStyles.btnDefault, { marginRight: 20 }]} bordered onPress={() => this.onClickSaveAction()}>
                                <Text style={{ color: config.blueTextColor }}> Save </Text>
                            </Button>
                        </TouchableOpacity>
                        <TouchableOpacity >
                            <Button style={[modalStyles.btnDefault]} bordered onPress={() => this.cancelChangesSinceLastSave()} >
                                <Text style={{ color: config.blueTextColor }}> Cancel </Text>
                            </Button>
                        </TouchableOpacity>
                    </Right>
                </Header>
                <Header style={headerStyles.headSpaceRemove}>
                    <Body>
                        <Title style={headerStyles.listHeading}>{this.props.tabName}</Title>
                    </Body>
                </Header>
                <View style={[styles.tabContent, { flex: 1 }]}>
                    <ScrollView
                        ref={ref => (this.scrollViewRef = ref)}
                        onScroll={this.handleOnScroll}
                        scrollEventThrottle={16}>
                        <View style={styles.row}>
                            <Left>
                                <Text style={styles.fimHeading}>Function Modifiers</Text>
                            </Left>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.halfWithMargin40}>
                                <Text numberOfLines={1} style={styles.textStrong}>29. Bladder Level of Assistance</Text>
                                <Text numberOfLines={1} style={styles.textSmall}>(Score using FIM® Levels 1-7)</Text>
                            </View>
                            <View style={[styles.half2WithMargin, styles.center]}>
                                <ButtonGroup
                                    onPress={this.handleFimModifiersData}
                                    selectedIndex={this.state.FIM.BladderAssistance}
                                    buttons={this.oneToSeven}
                                    name="BladderAssistance"
                                />
                            </View>
                        </View>

                        <View style={styles.row}>
                            <View style={styles.halfWithMargin40}>
                                <Text numberOfLines={1} style={styles.textStrong}>30. Bladder Frequency of Accidents</Text>
                                <Text numberOfLines={2} style={styles.textSmall}>(Enter in Item 39G (Bladder) the lower (more dependent) scores from 29 and 30 above.)</Text>
                            </View>
                            <View style={[styles.half2WithMargin, styles.center]}>
                                <View style={[styles.width100]}>
                                    <RNPickerSelect
                                        displayName={'30. Bladder Frequency of Accidents'}
                                        hideDoneBar={false}
                                        placeholder={{
                                            label: '',
                                            value: null,
                                        }}
                                        items={this.state.BladderFrequency}
                                        onValueChange={(value) => {
                                            this.handleFimModifiersData(value, 'BladderFrequency')
                                        }}
                                        style={{ ...pickerSelectStyles }}
                                        value={this.state.FIM.BladderFrequency ? this.state.FIM.BladderFrequency.toString() : null}
                                        ref={(el) => {
                                            this.inputRefs.BladderFrequency = el;
                                        }}
                                    />
                                </View>
                            </View>
                        </View>

                        <View style={styles.row}>
                            <View style={styles.halfWithMargin40}>
                                <Text numberOfLines={1} style={styles.textStrong}>35. Distance Walked</Text>
                            </View>
                            <View style={[styles.half2WithMargin, styles.center]}>
                                <View style={[styles.width100]}>
                                    <RNPickerSelect
                                        displayName={'35. Distance Walked'}
                                        hideDoneBar={false}
                                        placeholder={{
                                            label: '',
                                            value: null,
                                        }}
                                        items={this.state.DistanceWalked}
                                        onValueChange={(value) => {
                                            this.handleFimModifiersData(value, 'DistanceWalked')
                                        }}
                                        style={{ ...pickerSelectStyles }}
                                        value={(this.state.FIM.DistanceWalked || this.state.FIM.DistanceWalked == 0) ?
                                            this.state.FIM.DistanceWalked.toString() : null}
                                        ref={(el) => {
                                            this.inputRefs.DistanceWalked = el;
                                        }}
                                        onDownArrow={() => common.moveFocusToSpecificFeild(this.inputRefs, 'admissionDistanceWheelchair', 'Dropdown')}
                                    />
                                </View>
                            </View>
                        </View>

                        <View style={styles.row}>
                            <View style={styles.halfWithMargin40}>
                                <Text numberOfLines={1} style={styles.textStrong}>36. Distance Traveled in Wheelchair</Text>
                            </View>
                            <View style={[styles.half2WithMargin, styles.center]}>
                                <View style={[styles.width100]}>
                                    <RNPickerSelect
                                        displayName={'36. Distance Traveled in Wheelchair'}
                                        hideDoneBar={false}
                                        placeholder={{
                                            label: '',
                                            value: null,
                                        }}
                                        items={this.state.DistanceWheelchair}
                                        onValueChange={(value) => {
                                            this.handleFimModifiersData(value, 'DistanceWheelchair')
                                        }}
                                        style={{ ...pickerSelectStyles }}
                                        value={((this.state.FIM.DistanceWheelchair || this.state.FIM.DistanceWheelchair == 0) ||
                                            this.state.FIM.DistanceWheelchair == 0) ?
                                            this.state.FIM.DistanceWheelchair.toString() : ''}
                                        ref={(el) => {
                                            this.inputRefs.admissionDistanceWheelchair = el;
                                        }}
                                    />
                                </View>
                            </View>
                        </View>

                        <View style={styles.row}>
                            <View style={styles.halfWithMargin40}>
                                <Text numberOfLines={1} style={styles.textStrong}>37. Walk</Text>
                            </View>
                            <View style={[styles.half2WithMargin, styles.center]}>
                                <ButtonGroup
                                    onPress={this.handleFimModifiersData}
                                    selectedIndex={this.state.FIM.Walk}
                                    buttons={this.zeroToSeven}
                                    name="Walk"
                                />
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.halfWithMargin40}>
                                <Text numberOfLines={1} style={styles.textStrong}>38. Wheelchair</Text>
                                <Text numberOfLines={3} style={styles.textSmall}>(Score Items 37 and 38 using FIM® Levels 1-7; Use 0 if activity does not occur). See training manual for scoring of Item 39L (Walk/Wheelchair)</Text>
                            </View>
                            <View style={[styles.half2WithMargin, styles.center]}>
                                <ButtonGroup
                                    onPress={this.handleFimModifiersData}
                                    selectedIndex={this.state.FIM.WheelChair}
                                    buttons={this.zeroToSeven}
                                    name="WheelChair"
                                />
                            </View>
                        </View>
                        {/* FIM® Instrument start */}
                        <Text style={styles.heading}>FIM® Instrument</Text>

                        <View >

                            <View>
                                <View style={styles.row}>
                                    <View style={styles.width3ItemsPadding}>
                                        <View style={styles.row}>
                                            <Text numberOfLines={1} style={styles.formLabel}>Toileting</Text>
                                        </View>
                                        <View style={styles.width100}>
                                            <ButtonGroup
                                                onPress={this.handleFimModifiersData}
                                                selectedIndex={this.state.FIM.Toileting}
                                                buttons={this.oneToSeven}
                                                name="Toileting"
                                            />
                                        </View>
                                    </View>
                                    <View style={styles.width3ItemsPadding}>
                                        <View style={styles.row}>
                                            <Text numberOfLines={1} style={styles.formLabel}>Locomotion</Text>
                                        </View>
                                        <View style={styles.width100}>
                                            <ButtonGroup
                                                onPress={this.handleFimModifiersData}
                                                selectedIndex={this.state.FIM.WalkWheelChair}
                                                buttons={this.oneToSeven}
                                                name="WalkWheelChair"
                                            />
                                        </View>
                                    </View>
                                    <View style={styles.width3ItemsPadding}>
                                        <View style={styles.row}>
                                            <Text numberOfLines={1} style={styles.formLabel}>Bathing</Text>
                                        </View>
                                        <View style={styles.width100}>
                                            <ButtonGroup
                                                onPress={this.handleFimModifiersData}
                                                selectedIndex={this.state.FIM.Bathing}
                                                buttons={this.oneToSeven}
                                                name="Bathing"
                                            />
                                        </View>
                                    </View>

                                </View>
                                <View style={styles.row}>
                                    <View style={styles.width3ItemsPadding}>
                                    </View>
                                    <View style={styles.width3ItemsPadding}>
                                        <View style={styles.row}>
                                            <Text numberOfLines={1} style={styles.formLabel}>Locomotion Mode</Text>
                                        </View>
                                        <View style={styles.width100}>
                                            <RNPickerSelect
                                                displayName={'Locomotion Type'}
                                                hideDoneBar={false}
                                                placeholder={{
                                                    label: '',
                                                    value: null,
                                                }}
                                                items={this.state.LocoMotionType}
                                                onValueChange={(value) => {
                                                    this.handleFimModifiersData(value, 'LocoMotionType')
                                                }}
                                                style={{
                                                    ...pickerSelectStyles,
                                                    inputIOS: {
                                                        ...pickerSelectStyles.inputIOS,
                                                        height: 38
                                                    },
                                                }}
                                                value={this.state.FIM.LocoMotionType}
                                                ref={(el) => {
                                                    this.inputRefs.picker2 = el;
                                                }}
                                            />
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.row}>
                                    <View style={styles.width3ItemsPadding}>
                                        <View style={styles.row}>
                                            <Text numberOfLines={1} style={styles.formLabel}>Bed Transfer</Text>
                                        </View>
                                        <View style={styles.width100}>
                                            <ButtonGroup
                                                onPress={this.handleFimModifiersData}
                                                selectedIndex={this.state.FIM.BedChair}
                                                buttons={this.oneToSeven}
                                                name="BedChair"
                                            />
                                        </View>
                                    </View>
                                    <View style={styles.width3ItemsPadding}>
                                        <View style={styles.row}>
                                            <Text numberOfLines={1} style={styles.formLabel}>Bladder</Text>
                                        </View>
                                        <View style={styles.width100}>
                                            <ButtonGroup
                                                disableSelected={true}
                                                onPress={this.handleFimModifiersData}
                                                selectedIndex={this.state.FIM.Bladder}
                                                buttons={this.oneToSeven}
                                                name="Bladder"
                                            />
                                        </View>
                                    </View>
                                    <View style={styles.width3ItemsPadding}>
                                        <View style={styles.row}>
                                            <Text numberOfLines={1} style={styles.formLabel}>Problem Solving</Text>
                                        </View>
                                        <View style={styles.width100}>
                                            <ButtonGroup
                                                onPress={this.handleFimModifiersData}
                                                selectedIndex={this.state.FIM.ProblemSolving}
                                                buttons={this.oneToSeven}
                                                name="ProblemSolving"
                                            />
                                        </View>
                                    </View>
                                </View>

                                <View style={[styles.row, { marginTop: 20 }]}>
                                    <View style={styles.FIMCalHeading}>
                                        <Text numberOfLines={1} style={styles.totalHeading}>Projected Total Motor</Text>
                                    </View>
                                    <View style={[styles.FIMCalBox, styles.center]}>
                                        <View style={[styles.input100]}>
                                            <DefaultInput
                                                name={'TotalMotor'}
                                                disabled={true}
                                                image={'lock'}
                                                value={this.state.FIM.TotalMotor ? this.state.FIM.TotalMotor.toString() : ''} />
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.row}>
                                    <View style={styles.FIMCalHeading}>
                                        <Text numberOfLines={1} style={styles.totalHeading}>Projected Total Cognition</Text>
                                    </View>
                                    <View style={[styles.FIMCalBox, styles.center]}>
                                        <View style={[styles.input100]}>
                                            <DefaultInput
                                                name={'TotalCognition'}
                                                disabled={true}
                                                image={'lock'}
                                                value={this.state.FIM.TotalCognition ? this.state.FIM.TotalCognition.toString() : ''} />
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.row}>
                                    <View style={styles.FIMCalHeading}>
                                        <Text numberOfLines={1} style={styles.totalHeading}>Projected Total FIM</Text>
                                    </View>
                                    <View style={[styles.FIMCalBox, styles.center]}>
                                        <View style={[styles.input100]}>
                                            <DefaultInput
                                                name={'TotalFIM'}
                                                disabled={true}
                                                image={'lock'}
                                                value={this.state.FIM.TotalFIM ? this.state.FIM.TotalFIM.toString() : ''} />
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.row}>
                                    <View style={styles.FIMCalHeading}>
                                        <Text numberOfLines={1} style={styles.totalHeading}>Help Needed</Text>
                                    </View>
                                    <View style={[styles.FIMCalBoxWidth30, styles.center]}>
                                        <View style={[styles.width72]}>
                                            <DefaultInput
                                                name={'HelpNeeded'}
                                                disabled={true}
                                                image={'lock'}
                                                value={this.state.FIM.HelpNeeded ? this.state.FIM.HelpNeeded.toString() : ''} />
                                        </View>
                                    </View>
                                </View>
                            </View>

                        </View>
                    </ScrollView>
                </View>
                {/* Common info modal start */}
                <CommonInfoModal
                    iconType={this.state.modalIconType}
                    isOpenCommonInfoModal={this.state.isOpenCommonInfoModal}
                    displayMessage={this.state.infoMessage}
                    hideCommonInfoModal={this.hideCommonInfoModal}
                    modalAction={this.state.modalAction}
                    modalHeading={this.state.modalHeading}
                    confirmYesAction={this.state.modalAction && this.state.modalAction == 'Confirm' ?
                        this.clickConfirmYesAction : ''}
                    confirmActionBody={this.state.confirmActionBody}
                    removeAsterisk={this.state.removeAsterisk} />
                {/* common info modal end */}


            </Container >
        )
    }
}

