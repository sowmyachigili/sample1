/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import React, { Component } from 'react';
import { View, TouchableOpacity, FlatList, Image, StyleSheet, ScrollView, Alert } from 'react-native';
import { Button, Icon, Text } from 'native-base';
import RNPickerSelect from '../../utility/RNPickerSelect';
import DatePicker from '../../utility/Datepiker/datepicker';
import * as Animatable from 'react-native-animatable';
import Popover from 'react-native-popover-view';

import config from '../../config/config';
import configImage from '../../config/configImages';
import DefaultInput from "../../components/input/defaultInput";
import ErrorLogHandler from '../../utility/ErrorLogAlert';

// styles
import formStyles from '../../components/styles/styles';
import modalStyles from '../../components/styles/modalStyles';
import pickerSelectStyles from '../../components/styles/pickerSelectStyles';
import dateStyles from '../../components/styles/dateStyles';


class CommonSearchBarPopover extends Component {

    constructor(props) {
        super(props);
        this.clickEventCount = 0;
        this.state = {
            searchTooltipVisible: false,
            searchCriteria: [],
            searchTypeFilterList: this.props.searchFilterList,
            selectedFieldForSearch: this.props.searchType == 'listing' ? 'Patient Name' : '',
            selectedFieldType: '',
            searchDateValue: '',
            valueForSearch: '',
            selectedFieldInputMaxlength: ''
        }
    }

    // on click search send selected search criteria to the parent
    prepareSelectedDataForSearch = () => {
        this.props.prepareQueryForSearch(this.state.searchCriteria);
    }

    // For displaying search popover
    showPopover = async () => {
        if (!this.state.searchTooltipVisible && this.clickEventCount == 0) {
            this.clickEventCount++;
            try {
                await this.setState({
                    searchDateValue: '',
                    selectedFieldForSearch: this.props.searchType == 'listing' ? 'Patient Name' : '',
                    valueForSearch: '',
                    searchTooltipVisible: true,
                    dropdownOptions: [],
                    selectedFieldType: ''
                });
            } catch (error) { // to log an exception
                if (error) {
                    if (error.message) {
                        error.message = 'From showPopover fun - ' + JSON.stringify(error.message);
                    } else {
                        error.message = 'From showPopover fun ';
                    }
                }
                ErrorLogHandler(error, true);
            }
        }
    }

    // Close search popover
    closePopover = async () => {
        try {
            if (this.clickEventCount && this.state.searchTooltipVisible) {
                await this.setState({ searchTooltipVisible: false }, () => {
                    this.clickEventCount = 0;
                });
            }
        } catch (error) {
            if (error) {
                if (error.message) {
                    error.message = 'From closePopover fun - ' + JSON.stringify(error.message);
                } else {
                    error.message = 'From closePopover fun ';
                }
            }
            ErrorLogHandler(error, true);
        }
    }




    componentWillUnmount() {
        this.props.onRef(null);
    }
    componentDidMount() {
        this.props.onRef(this);
    }

    // For removing items in searchbar
    removeItemsInSearchBar = (index) => {
        let searchFields = [...this.state.searchCriteria];
        searchFields.splice(index, 1);
        this.setState({ searchCriteria: searchFields })
    }

    // For adding search info 
    addSelectedSearchInfo = async () => {
        try {
            let searchFields = [...this.state.searchCriteria];
            if (this.state.selectedFieldForSearch) {
                if ((this.state.selectedFieldType === 'Date' && this.state.searchDateValue)
                    || this.state.valueForSearch) {

                    let seachObj = {
                        label: this.state.selectedFieldForSearch,
                        value: this.state.selectedFieldType === 'Date'
                            ? this.state.searchDateValue : this.state.valueForSearch
                    }

                    let duplicateInsertIndex = searchFields.findIndex(query => query.label === this.state.selectedFieldForSearch);
                    if (duplicateInsertIndex != -1) {
                        searchFields[duplicateInsertIndex] = seachObj;
                    } else {
                        searchFields.push(seachObj);
                    }
                }
            }
            await this.setState({
                searchCriteria: searchFields,
                searchTooltipVisible: false
            }, () => {
                this.clickEventCount = 0;
            });
        } catch (error) {
            if (error) {
                if (error.message) {
                    error.message = 'From addSelectedSearchInfo fun - ' + JSON.stringify(error.message);
                } else {
                    error.message = 'From addSelectedSearchInfo fun ';
                }

            }
            ErrorLogHandler(error, true, this.state.searchCriteria);
        }
    }

    // update data in state
    updateDataInState = async (fieldName, value) => {
        await this.setState({ [fieldName]: value });
    }

    // set type of selected field - input or date 
    setTypeOfSelecteFieldInSearch = (value) => {
        this.setState({
            selectedFieldForSearch: value,
            searchDateValue: '',
            valueForSearch: '',
            dropdownOptions: [],
            selectedFieldType: '',
            selectedFieldInputMaxlength: ''
        });

        let selectedFieldDetails = [...this.state.searchTypeFilterList].find(field => value && field.value === value);

        if (selectedFieldDetails && selectedFieldDetails.type && selectedFieldDetails.type == 'Date') {
            this.updateDataInState('selectedFieldType', 'Date');
        } else if (selectedFieldDetails && selectedFieldDetails.type && selectedFieldDetails.type == 'Dropdown') {
            this.updateDataInState('selectedFieldType', 'Dropdown');
            this.updateDataInState('dropdownOptions', selectedFieldDetails.dropdownOptions);
        } else {
            this.updateDataInState('selectedFieldType', 'Input');
            if (selectedFieldDetails && selectedFieldDetails.maxLength) {
                this.updateDataInState('selectedFieldInputMaxlength', selectedFieldDetails.maxLength);
            }
        }
    }

    render() {
        return (
            <View style={localStyles.searchBar} >
                <View style={localStyles.searchRowAlign} >
                    {/* Search bar */}

                    <TouchableOpacity style={{ alignItems: 'flex-start', flex: 0.8 }}
                        onPress={this.showPopover}>

                        <ScrollView horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            ref={ref => this.searchScrollView = ref}
                            onContentSizeChange={() => this.searchScrollView.scrollToEnd({ animated: true })}
                            onLayout={() => this.searchScrollView.scrollToEnd({ animated: true })}>
                            <TouchableOpacity
                                ref={ref => this.searchButtonRef = ref} onPress={this.showPopover}>
                                <View style={localStyles.searchViewPadding} >
                                    <View style={{ flexDirection: 'row' }}>
                                        <FlatList
                                            showsHorizontalScrollIndicator={false}
                                            data={this.state.searchCriteria}
                                            renderItem={({ item, index }) =>
                                                <Animatable.View duration={100} animation="zoomIn" style={localStyles.searchTextBacground}>
                                                    <View style={localStyles.centerAlignInRow}>
                                                        <Text style={localStyles.textInSearchBar}>{item.label}:
                                                            <Text style={{ fontWeight: 'bold', color: '#fff' }}> {item.value}</Text> </Text>

                                                        <TouchableOpacity style={localStyles.cancelIconInSearchBar} onPress={() => this.removeItemsInSearchBar(index)}>
                                                            <Icon style={{ color: '#FFFFFF' }} fontSize={14} name="close" />
                                                        </TouchableOpacity>
                                                    </View>
                                                </Animatable.View>
                                            }
                                            horizontal={true}
                                            keyExtractor={(item, index) => index.toString()}
                                            extraData={this.state.searchCriteria}
                                        />
                                    </View>
                                </View>
                            </TouchableOpacity>

                        </ScrollView>
                    </TouchableOpacity>
                    {/* Start of search popover */}
                    <Popover
                        isVisible={this.state.searchTooltipVisible}
                        fromView={this.searchScrollView}
                        showBackground={false}
                        placement='bottom'
                        // onClose={this.closePopover}
                        popoverStyle={localStyles.searchPopoverStyle}>
                        <View>
                            <View>
                                <TouchableOpacity onPress={() => this.closePopover()} style={{ backgroundColor: 'transparent' }}>
                                    <Icon style={modalStyles.smallCloseIcon} name="close-circle" />
                                </TouchableOpacity>
                            </View>
                            <View style={localStyles.labelWidthInPopover}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text numberOfLines={1}
                                        style={localStyles.formLabelInPopover}>Type</Text>
                                </View>

                                <RNPickerSelect
                                    hideDoneBar={false}
                                    placeholder={{
                                        label: '',
                                        value: '',
                                    }}
                                    placeholderTextColor={this.props.type == 'listing' ? 'black' : null}
                                    items={this.state.searchTypeFilterList}
                                    onValueChange={(value) => this.setTypeOfSelecteFieldInSearch(value)}
                                    style={{ ...pickerSelectStyles }}
                                    value={this.state.selectedFieldForSearch}
                                    isMoveUpwardDisabled={true}
                                />
                            </View>
                            <View style={localStyles.labelWidthInPopover}>
                                <Text numberOfLines={1} style={localStyles.formLabelInPopover}>Search Description</Text>
                                {this.state.selectedFieldForSearch && this.state.selectedFieldType &&
                                    this.state.selectedFieldType === 'Date' ?
                                    <View style={formStyles.row}>
                                        <DatePicker
                                            style={dateStyles.dateInputWidth}
                                            date={this.state.searchDateValue}
                                            mode="date"
                                            placeholder={config.datePlaceholder}
                                            format={config.dateFormat}
                                            confirmBtnText="Done"
                                            cancelBtnText="Cancel"
                                            customStyles={dateStyles}
                                            onDateChange={(date) => { this.setState({ searchDateValue: date }) }}
                                            iconSource={configImage.calendarIcon}
                                            onClickCancelIcon={() => { this.setState({ searchDateValue: undefined }) }}
                                        />
                                    </View>
                                    : this.state.selectedFieldForSearch && this.state.selectedFieldType && this.state.selectedFieldType === 'Dropdown' ?
                                        // Dropdown options
                                        <RNPickerSelect
                                            hideDoneBar={false}
                                            placeholder={{
                                                label: '',
                                                value: null,
                                            }}
                                            items={this.state.dropdownOptions}
                                            onValueChange={(value) => this.updateDataInState('valueForSearch', value)}
                                            style={{ ...pickerSelectStyles }}
                                            value={this.state.valueForSearch}
                                        />
                                        : <DefaultInput value={this.state.valueForSearch}
                                            handlerFromParant={(value) => this.updateDataInState('valueForSearch', value)}
                                            maxLength={this.state.selectedFieldInputMaxlength ? this.state.selectedFieldInputMaxlength : 100} returnKeyType={'done'} />}
                            </View>

                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                                <Button style={[modalStyles.btnDefault, modalStyles.buttonLight, { marginRight: 20 }]} onPress={this.closePopover}>
                                    <Text style={[modalStyles.btnDefaultText, modalStyles.buttonLightText]}>Remove</Text>
                                </Button>
                                <Button style={[modalStyles.btnDefault, modalStyles.buttonDark]} onPress={this.addSelectedSearchInfo}>
                                    <Text style={[modalStyles.btnDefaultText, modalStyles.buttonDarkText]}>Add</Text>
                                </Button>
                            </View>
                        </View>
                    </Popover>
                    {/* End of search popover */}

                    <View style={{ alignItems: 'flex-end', flex: 0.2 }}>
                        <View style={{ padding: 3 }}>

                            {this.props.displaySearchButton ?
                                <Button style={[modalStyles.buttonGreen, { height: 34 }]} onPress={() => this.prepareSelectedDataForSearch()}>
                                    <Icon type="FontAwesome" name="search" style={{ fontSize: 20, fontFamily: 'Roboto-Regular', }} />
                                </Button>
                                :
                                <Button style={[modalStyles.btnDefault, modalStyles.buttonGreen, { height: 34 }]} onPress={() => this.prepareSelectedDataForSearch()}>
                                    <Text style={[modalStyles.btnDefaultText, modalStyles.buttonDarkText]}>Search</Text>
                                </Button>
                            }

                        </View>
                    </View>

                </View>
            </View >
        );
    }

}
const localStyles = StyleSheet.create({

    searchBar: {
        margin: 10,
        height: 40,
        backgroundColor: 'white',
        borderRadius: 5
    },
    searchRowAlign: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center'
    },
    searchViewPadding: {
        paddingHorizontal: 10,
        paddingVertical: 4
    },
    searchTextBacground: {
        borderRadius: 50,
        backgroundColor: '#007AFF',
        marginRight: 5
    },
    centerAlignInRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    textInSearchBar: {
        fontSize: 16,
        color: '#FFFFFF',
        paddingVertical: 6,
        paddingHorizontal: 15,
        fontFamily: 'Roboto-Medium'
    },
    cancelIconInSearchBar: {
        paddingRight: 10,
        paddingTop: 2
    },
    searchPopoverStyle: {
        backgroundColor: '#fbfaff',
        borderColor: '#E1E1E1',
        borderBottomColor: '#E1E1E1',
        borderWidth: 1,
        borderRadius: 15,
        paddingHorizontal: 30,
        paddingVertical: 20
    },
    formLabelInPopover: {
        ...formStyles.formLabel,
        color: '#717171',
        paddingBottom: 7
    },
    labelWidthInPopover: {
        paddingBottom: 25,
        width: 250
    }
})
export default CommonSearchBarPopover;