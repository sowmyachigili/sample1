/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import React, { Component } from 'react';
import { Text, View, Alert } from 'react-native';
import { Switch } from 'native-base';

// styles
import styles from '../../components/styles/styles';

// modal popup
import CommonInfoModal from './CommonInfoModal';

// Config files
import config from '../../config/config';
import configMessage from '../../config/configMessages';

export default class CommonSwitchAction extends Component {
    constructor(props) {
        super(props);
        this.inputRefs = {};
        this.state = {
            CFITool: true,
            modalIconType: '',
            isOpenCommonInfoModal: false,
            modalHeading: '',
            infoMessage: '',
            confirmActionBody: '',
            modalAction: ''
        }
    }

    componentDidMount = () => {
        this.props.onRef(this);
        if (!this.props.isNotMiniFim) {
            this.setState({
                CFITool: false
            });
        }
    }

    componentWillUnmount() {
        this.props.onRef(null);
    }

    // move to 6 or 18 fim fields data confirmation popup
    checkOrUncheckCFITool = async () => {
        await this.openCommonInfoModal(configMessage.commonCancelMessage, '', 'Confirm', 'Confirm', 'Cancel');
    }

    // update the Switch tool value
    updateSwitchTool = (cfiTool) => {
        this.setState({
            CFITool: cfiTool
        });
    }
    // Open common info modal
    openCommonInfoModal = (modalHeading, infoMessage, iconType, modalAction, confirmActionBody) => {
        setTimeout(() => {
            this.setState({
                isOpenCommonInfoModal: true,
                modalIconType: iconType,
                modalAction: modalAction ? modalAction : '',
                confirmActionBody: confirmActionBody ? confirmActionBody : '',
                modalHeading: modalHeading ? modalHeading : '',
                infoMessage: infoMessage ? infoMessage : ''
            });
        }, 10);
    }

    // Hide succes modal
    hideCommonInfoModal = () => {
        this.setState({
            isOpenCommonInfoModal: false
        });
    }

    // onclick yes action in popup
    onClickConfirmYesAction = (body) => {
        if (body == 'Cancel') {
            if (this.state.CFITool) {
                this.setState({
                    CFITool: false
                })
            } else {
                this.setState({
                    CFITool: true
                })
            }
        }
        this.props.onPressYesFimAction(this.state.CFITool, 'Clear');
    }

    render() {
        return (
            <View >
                <View style={[styles.row]}>
                    <Text style={styles.tabHeading}>Use PAC-CFI Tool  </Text>
                    <Switch value={this.state.CFITool}
                        onChange={() => {
                            this.checkOrUncheckCFITool()
                        }}
                    />
                </View>

                <CommonInfoModal
                    iconType={this.state.modalIconType}
                    isOpenCommonInfoModal={this.state.isOpenCommonInfoModal}
                    modalHeading={this.state.modalHeading}
                    displayMessage={this.state.infoMessage}
                    modalAction={this.state.modalAction}
                    confirmActionBody={this.state.confirmActionBody}
                    confirmYesAction={this.state.modalAction && this.state.modalAction == 'Confirm' ?
                        this.onClickConfirmYesAction : ''}
                    hideCommonInfoModal={this.hideCommonInfoModal} />
            </View>
        );
    }
}
