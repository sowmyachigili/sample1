/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import React, { Component } from 'react';
import { View, Text, ScrollView, FlatList } from 'react-native';
import { Button, Left, Body, Icon, List, ListItem } from 'native-base';
import Modal from "react-native-modal";

import configMessage from '../../config/configMessages';

// Realm
import realmController from '../../Realm/realmController';
import schema from '../../Realm/schemaNames';

// Components
import DefaultInput from '../../components/input/defaultInput';
import ErrorMessage from '../../components/common/ErrorMessage';
import CommonInfoModal from './CommonInfoModal';
import dateFormats from '../../utility/formatDate';
import common from "../../utility/commonFun";

// styles
import modalStyles from '../../components/styles/modalStyles';
import styles from '../../components/styles/styles';

class CommonListPopupModal extends Component {
    constructor(props) {
        super(props);
        this.inputRefs = {};
        this.state = {
            listItemCode: '',
            listItemDesc: '',
            modalIconType: '',
            modalHeading: '',
            isOpenCommonInfoModal: false,
            infoMessage: '',
            saveActionType: ''
        }
    }


    // For hiding modal
    hideInfoModal = () => {
        this.setState({
            listItemCode: '',
            listItemDesc: ''
        });
        setTimeout(() => {
            this.props.hideCommonListInfoModal();
        }, 100);
    }

    // update data in state
    updateDataInState = (fieldName, value) => {
        this.setState({ [fieldName]: value });
    }

    // For saving new list details 
    saveNewListData = (type) => {
        if (this.state.listItemCode && this.state.listItemDesc && type) {
            this.checkAndSaveNewListValuesInCategoryDetails(type);
        } else {
            let messages = [];
            if (!this.state.listItemCode) {
                messages.push('List Item Code');
            }
            if (!this.state.listItemDesc) {
                messages.push('List Item');
            }
            this.openCommonInfoModal(messages, 'Validation', configMessage.requiredFieldValidationHeading);
        }
    }

    // Close list popup modal
    onClickCloseCheckCodeInInput = () => {
        if (this.state.listItemCode || this.state.listItemDesc) {
            this.saveNewListData('Close');
        } else {
            this.hideInfoModal();
        }
    }

    // On click cancel button close model based on confirmation
    onClickCancelButton = () => {
        if (this.state.listItemCode || this.state.listItemDesc) {
            this.openCommonInfoModal(configMessage.commonCancelMessage, 'Confirm', '', 'Confirm', 'Cancel Changes');
        } else {
            this.hideInfoModal();
        }
    }


    // Open common info modal
    openCommonInfoModal = (messgae, iconType, modalHeading, modalAction, confirmActionBody) => {
        setTimeout(() => {
            this.setState({
                isOpenCommonInfoModal: true,
                infoMessage: messgae,
                modalIconType: iconType,
                modalHeading: modalHeading ? modalHeading : '',
                modalAction: modalAction ? modalAction : '',
                confirmActionBody: confirmActionBody ? confirmActionBody : ''
            });
        }, 10);
    }


    onClickConfirmYesAction = (body) => {
        if (body) {
            if (body === 'Cancel Changes') {
                this.updateDataInState('saveActionType', 'Close');
                this.hideCommonInfoModal();
            }
        }
    }


    // Hide common info modal
    hideCommonInfoModal = () => {
        // Close for auto save and close
        if (this.state.saveActionType && this.state.saveActionType == 'Close') {
            this.hideInfoModal();
        }
        this.setState({
            isOpenCommonInfoModal: false,
            saveActionType: ''
        });
    }



    // check new list value  in category details
    checkAndSaveNewListValuesInCategoryDetails = (type) => {
        if (this.props.loggedInUserFacilityId) {
            let Item = this.state.listItemCode;
            let Notes = this.state.listItemDesc;

            let dropdownOptions = [...this.props.dropdownsList];

            let isDuplicateAvailable = dropdownOptions.findIndex(option => option.Item && option.Item === Item || (option.Item === Item && option.Notes && option.Notes === Notes));

            if (isDuplicateAvailable != -1) {
                // duplicate record available
                this.openCommonInfoModal(configMessage.duplicateCategoryAvailable, 'Warning');
                return
            } else {
                let filterQuery = 'CategoryFacCode ==[c] ' + '"' + this.props.loggedInUserFacilityId + '" AND Name = ' + '"' + this.props.categoryName + '"';
                let facilityCategoryData = realmController.getAllRecordsWithFiltering(schema.DROPDOWNS_SCHEMA, filterQuery);

                if (facilityCategoryData && facilityCategoryData.length && facilityCategoryData.length == 1) {
                    let categoryInfo = { ...facilityCategoryData[0] };

                    if (type === 'Close') {
                        this.setState({ saveActionType: 'Close' });
                    } else {
                        this.setState({ saveActionType: 'Save' });
                    }

                    this.insertrecordIntoCategories(categoryInfo, dropdownOptions, type);
                }
            }
        } else {
            this.setState({
                isOpenCommonInfoModal: true,
                modalIconType: 'Warning',
                infoMessage: '',
                modalHeading: 'Facility code not Found',
            });
        }
    }

    // Insert record into dropdown table
    insertrecordIntoCategories = (categoryInfo, dropdownOptions, type) => {
        let categorydetails = {};
        categorydetails.CategoryIPADID = categoryInfo.CategoryIPADID;
        categorydetails.CategoryDetailIPADId = realmController.getMaxPrimaryKeyId(schema.DROPDOWN_OPTIONS_SCHEMA, 'CategoryDetailIPADId');
        categorydetails.CategoryDetailID = null;
        categorydetails.DBName = categoryInfo.DBName;
        categorydetails.CategoryID = categoryInfo.CategoryID;
        categorydetails.SequenceNo = dropdownOptions.length + 1;
        categorydetails.Item = this.state.listItemCode;
        categorydetails.Notes = this.state.listItemDesc;
        categorydetails.DoNotUse = 0;
        categorydetails.DoNotShow = 0;
        categorydetails.DoNotDelete = 0;
        categorydetails.DoNotEdit = 0;
        categorydetails.InsertedOn = dateFormats.formatDate('todayDate');
        // categorydetails.InsertedBy=this.props.loggedInUserDetails.UserID;

        let newCategoryDetails = {};
        newCategoryDetails.CategoryIPADID = categoryInfo.CategoryIPADID;
        newCategoryDetails.CategoryDetails = [];

        if (categoryInfo.CategoryDetails && categoryInfo.CategoryDetails.length
            && categoryInfo.CategoryDetails.length > 0) {
            categoryInfo.CategoryDetails.forEach(category => {
                newCategoryDetails.CategoryDetails.push(category);
            });
        }
        newCategoryDetails.CategoryDetails.push(categorydetails)
        realmController.insertOrUpdateRecord(schema.DROPDOWNS_SCHEMA, newCategoryDetails, true);

        this.props.updateCategoryDetailsInDropdown(this.props.categoryName);

        // After saving record display success messages and update dropdown values
        if (type != 'Close') {
            this.setState({
                listItemCode: '',
                listItemDesc: ''
            });
            this.openCommonInfoModal('', 'Success', configMessage.newCategoryAdded);
        } else if (type == 'Close') {
            this.hideInfoModal();
        }
    }


    render() {
        return (
            <Modal
                isVisible={this.props.isOpenListPopupModal}
                backdropOpacity={0.5}
                onBackButtonPress={() => this.hideInfoModal()}
            >
                <View style={[modalStyles.modal, { maxHeight: '90%' }]}>
                    <View style={modalStyles.modalHead}>
                        <View style={modalStyles.leftContainer}>
                            <Text style={[{ textAlign: 'left' }]}>
                                {''}
                            </Text>
                        </View>
                        <Text style={modalStyles.modalHeading}>
                            {this.props.dropdownListModalHeading} - Add/Edit List
                            </Text>
                        <View style={modalStyles.rightContainer}>
                            <Button transparent onPress={() => this.hideInfoModal()}>
                                <Icon style={modalStyles.closeIcon} name="close-circle" />
                            </Button>
                        </View>
                    </View>

                    <ScrollView style={modalStyles.modalBody}>
                        <View style={styles.row}>
                            <View style={styles.halfCol}>
                                <Text numberOfLines={1} style={styles.formLabel}>List Item Code</Text>
                                <View style={styles.row}>
                                    <DefaultInput
                                        value={this.state.listItemCode}
                                        handlerFromParant={(value) => this.updateDataInState('listItemCode', value)}
                                        maxLength={50}
                                        ref={(el) => { this.inputRefs['listItemCode'] = el; }}
                                        name={'listItemCode'}
                                        moveFocusToNextField={() => {
                                            common.moveFocusToSpecificFeild(this.inputRefs, 'listItemDesc');
                                        }} />
                                </View>
                            </View>
                            <View style={styles.halfCol}>
                                <Text numberOfLines={1} style={styles.formLabel}>List Item</Text>
                                <View style={styles.row}>
                                    <DefaultInput
                                        value={this.state.listItemDesc}
                                        handlerFromParant={(value) => this.updateDataInState('listItemDesc', value)}
                                        maxLength={50}
                                        ref={(el) => { this.inputRefs['listItemDesc'] = el; }}
                                        name={'listItemDesc'}
                                        returnKeyType={'done'} />
                                </View>
                            </View>
                        </View>
                        <List style={styles.marginBottom}>
                            <ListItem noIndent style={styles.listHead}>
                                <Left>
                                    <Text style={styles.listHeading}>Code</Text>
                                </Left>
                                <Body>
                                    <Text style={styles.listHeading}>Description</Text>
                                </Body>
                            </ListItem>
                            <FlatList
                                data={this.props.dropdownsList}
                                renderItem={({ item, index }) =>
                                    <ListItem>
                                        <Left>
                                            <Text style={styles.listText}>{item.Item}</Text>
                                        </Left>
                                        <Body>
                                            <Text style={[styles.listText, { paddingLeft: 5 }]}>{item.Notes}</Text>
                                        </Body>
                                    </ListItem>

                                }
                                scrollEnabled={false}
                                keyExtractor={(item, index) => index.toString()}
                                extraData={this.props.dropdownsList}
                                ListEmptyComponent={<ErrorMessage />}
                            />
                        </List>
                    </ScrollView>
                    <View style={[modalStyles.modalFooter]}>
                        <Button style={[modalStyles.btnDefault, modalStyles.buttonDark, { marginRight: 20 }]}
                            onPress={() => this.saveNewListData('Save')} >
                            <Text style={[modalStyles.btnDefaultText, modalStyles.buttonDarkText]}>Save</Text>
                        </Button>
                        <Button style={[modalStyles.btnDefault, modalStyles.buttonLight, { marginRight: 20 }]} onPress={this.onClickCancelButton}>
                            <Text style={[modalStyles.btnDefaultText, modalStyles.buttonLightText]}>Cancel</Text>
                        </Button>
                        <Button style={[modalStyles.btnDefault, modalStyles.buttonLight]} onPress={this.onClickCloseCheckCodeInInput}>
                            <Text style={[modalStyles.btnDefaultText, modalStyles.buttonLightText]}>Close</Text>
                        </Button>
                    </View>
                </View>

                {/* Common validations info modal start */}
                <CommonInfoModal
                    iconType={this.state.modalIconType}
                    modalHeading={this.state.modalHeading}
                    isOpenCommonInfoModal={this.state.isOpenCommonInfoModal}
                    displayMessage={this.state.infoMessage}
                    hideCommonInfoModal={this.hideCommonInfoModal}
                    modalAction={this.state.modalAction}
                    confirmYesAction={this.state.modalAction && this.state.modalAction == 'Confirm' ?
                        this.onClickConfirmYesAction : ''}
                    confirmActionBody={this.state.confirmActionBody}
                />
                {/* common validations info modal end */}

            </Modal>




        )
    }
}

export default CommonListPopupModal;