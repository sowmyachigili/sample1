/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import React, { Component } from 'react';
import {
    View, TouchableOpacity, FlatList, Image, Alert
} from 'react-native';
import { Avatar } from 'react-native-elements';
import {
    Container, Header, Left, Body, Right,
    Text, List, ListItem, Title
} from 'native-base';

import RNPickerSelect from '../../../utility/RNPickerSelect';
import { Navigation } from "react-native-navigation";
import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-community/async-storage';

import fetchMethodRequest from '../../../config/service';
import config from '../../../config/config';
import api from '../../../config/apiCalls';
import configMessage from '../../../config/configMessages';
import configImage from '../../../config/configImages';
import navigation from '../../../utility/Navigation';

// Realm
import realm from '../../../Realm/realm';
import schema from '../../../Realm/schemaNames';
import realmController from '../../../Realm/realmController';

// Components
import MainHeader from "../../../components/mainHeader/mainHeader";
import Loader from '../../../components/common/loaderNew';
import dateFormats from '../../../utility/formatDate';
import CommonSearchBarPopover from '../../Common/CommonSearchBarPopover';
import ErrorMessage from '../../../components/common/ErrorMessage';
import CommonInfoModal from '../../Common/CommonInfoModal';
import common from '../../../utility/commonFun';

// styles
import styles from '../../../components/styles/liststyles';
import pickerSelectStyles from '../../../components/styles/pickerSelectStyles';
import headerStyles from '../../../components/styles/headerStyles';
import style from '../../../components/styles/styles';

class CaseListing extends Component {

    constructor(props) {
        super(props);
        // this.CaseListingPrivilege = common.getUserPrivillageType(this.props.loggedInUserDetails.CaseListingPrivilege, 'caselisting');
        this.state = {
            isOpenCommonInfoModal: false,
            infoMessage: '',
            modalIconType: '',
            modalAction: '',
            confirmActionBody: '',
            modalHeading: '',

            isLoading: true, // loader

            loggedInUserDetails: this.props.loggedInUserDetails,
            buttons: [
                { label: 'Add' },
                { label: 'Delete' },
                { label: 'Admit' },
                { label: 'Don\'t Admit' },
                { label: 'Pre-admit Document' },
                { label: 'Task Complete' },
            ],
            searchFilterList: [
                { label: 'Patient Name', value: 'Patient Name' },
                { label: 'Patient ID', value: 'Patient Id' },
                { label: 'Admission Date', value: 'Admission Date', type: 'Date' }
            ],
            changedDropdownValue: '',
            selectedOption: 'Open cases and discharges in last 30 days',
            List: [
                // { label: 'All', value: 'All' },
                { label: 'Open cases and discharges in last 30 days', value: 'Open cases and discharges in last 30 days' },
                { label: 'Open cases and discharges in last 60 days', value: 'Open cases and discharges in last 60 days' },
                { label: 'Open cases and discharges in last 90 days', value: 'Open cases and discharges in last 90 days' },
                { label: 'Open cases and discharges in last 120 days', value: 'Open cases and discharges in last 120 days' }
            ],
            cases: [],
            allCases: [],

            // Pagination options
            page: 0,
            per_page: 0,
            pre_page: null,
            next_page: null,
            total: 0,
            total_pages: 0,
            scrollBegin: false,
            // autoSyncProgress: !this.props.onlineSyncAutoSuccess ? 100 : this.props.autoSyncProgress,
            isInternetConnected: true
        }

        // if you want to listen on navigator events, set this up
        Navigation.events().bindComponent(this);
        // this.props.getIsLoading(false);
    }

    componentDidAppear = async () => {
        // this.getNotifications();
        AsyncStorage.setItem('screenName', JSON.stringify(navigation.CASE_LISTING_SCREEN));
        this.mainHeaderRef.sessionInterval();

        // for data in search bar when screen changed
        if (this.searchBarRef) {
            this.searchBarRef.updateDataInState('searchCriteria', []);
        }
        this.setState({ isLoading: true }, () => {
            this.getCasesRecords();
        });
    }

    componentDidDisappear = async () => {
        this.mainHeaderRef.clearIntervel();
    }

    componentDidMount = async () => {
        // check internet connection
        // NetInfo.addEventListener(state => {
        //     this.handleNetConnection(state);
        // });
        // NetInfo.fetch().then(state => {
        //     this.handleNetConnection(state);
        // });
        // this.checkAutoSyncProgress();
    }

    // handleNetConnection = (state) => {
    //     if (state && state.type) {
    //         this.setState({
    //             isInternetConnected: state.isConnected
    //         })
    //     }
    // }

    // check and update autoSync progress status in mainheader
    // checkAutoSyncProgress = (type, syncProgressInOffline) => {
    //     let autoSyncProgress = this.props.checkAutoSyncProgressForStatus(type, syncProgressInOffline);
    //     if (type) {
    //         this.setState({
    //             autoSyncProgress: syncProgressInOffline
    //         })
    //         this.updateMainHeaderProgress(autoSyncProgress);
    //         return autoSyncProgress

    //     } else {
    //         if (autoSyncProgress) {
    //             this.setState({
    //                 autoSyncProgress: autoSyncProgress
    //             })
    //             this.updateMainHeaderProgress(autoSyncProgress);
    //             return autoSyncProgress;
    //         }
    //     }
    // }

    // updateMainHeaderProgress = (autoSyncProgress) => {
    //     if (this.mainHeaderRef) {
    //         this.mainHeaderRef.updateSyncProgress(autoSyncProgress);
    //     }
    // }

    // checkAutoSyncProgressForStatus = () => {
    //     this.updateMainHeaderProgress(this.state.autoSyncProgress);
    //     return this.state.autoSyncProgress;
    // }
    checkPrevilegesAndGrantPermissions = async (patientDetails) => {
        if (!this.state.isLoading) {
            this.setState({ 'isLoading': true }, async () => {
                let screenName;
                screenName = navigation.COMPARE_CASELISTING_CONFLICTS;
                let props = {
                    loggedInUserDetails: this.props.loggedInUserDetails,
                    patID: patientDetails.PatID,
                    patientDetails: patientDetails,
                    // caseListingSaveWithMofiedDetails: this.props.caseListingSaveWithMofiedDetails,
                    // generateGuidAndSaveInStore: this.props.generateGuidAndSaveInStore,
                    // homeScreenNavigatorId: this.props.homeScreenNavigatorId,// for home button
                    // autoSyncProgress: this.state.autoSyncProgress,
                    // checkAutoSyncProgressForStatus: this.checkAutoSyncProgress,
                }
                navigation.pushScreenNavigator(this.props.componentId, screenName, props);
            });
        }
    }

    // For getting common filter query
    prepareFilterQuery = () => {
        if (this.state.loggedInUserDetails && this.state.loggedInUserDetails.FacilityCode) {
            return {
                basicFilterQuery: 'FacilityCode ==[c] ' + '"' + this.state.loggedInUserDetails.FacilityCode + '" AND DeletedBy == null AND IsAged == null',
                sortFilterQuery: [['AdmissionDate', true], ['InsertedOn', true]]

            }
        }
    }

    // Get all cases records
    getCasesRecords = () => {
        this.setState({ next_page: null, isLoading: true }, () => {
            // if (this.state.selectedOption) {
            //     if (this.state.selectedOption === 'All') {
            //         this.filterCasesWithSelectedDate();
            //     } else {
            //         this.onSelectFilterOption();
            //     }
            // } else {
                let filterQuery = this.prepareFilterQuery();
                if (filterQuery) {
                    // let allCases = realm.objects(schema.CASELISTING_SCHEMA).filtered(filterQuery.basicFilterQuery)
                    //     .sorted(filterQuery.sortFilterQuery);

                    let apiUserBody = {
                        FacilityCode: this.props.loggedInUserDetails && this.props.loggedInUserDetails.FacilityCode ? this.props.loggedInUserDetails.FacilityCode : null,
                        UserID: this.props.loggedInUserDetails && this.props.loggedInUserDetails.UserID ? this.props.loggedInUserDetails.UserID : null,
                    }
                    let data = {
                        ...apiUserBody,
                    }
                    fetchMethodRequest('POST', api.getCaseListingData, data)
                        .then(async (response) => {
                            if (response && response.length && response.length > 0) {
                                // let allCases = response;
                                let allCases = [];
                                for (let i = 0; i < response.length; i++) {
                                    const element = response[i].Patient;
                                    allCases.push(element)
                                }
                                this.setState({
                                    allCases: allCases,
                                });
                                this.loadMoreCasesWithPagination(allCases, 'Initial Load');
                            }
                        }).catch(err => {
                            ErrorLogHandler(err, true);
                            return;
                        });
                }
            // }
        });
    }

    // To apply pagination to cases
    loadMoreCasesWithPagination = (allCases, type) => {
        this.setState({ isLoading: true }, () => {
            if ((this.state.cases.length < this.state.total) || (type && type === 'Initial Load')) {
                let allCaseRecords = allCases ? [...allCases] : this.state.allCases;
                let casesPaginationInfo = '';


                if (type && type === 'Initial Load') {
                    casesPaginationInfo = common.Paginator(allCaseRecords);
                } else if (this.state.next_page) {
                    casesPaginationInfo = common.Paginator(allCaseRecords, this.state.next_page);
                } else {
                    casesPaginationInfo = common.Paginator(allCaseRecords);
                }

                if (casesPaginationInfo && casesPaginationInfo.data) {
                    this.setState({
                        page: casesPaginationInfo.page,
                        per_page: casesPaginationInfo.per_page,
                        pre_page: casesPaginationInfo.pre_page,
                        next_page: casesPaginationInfo.next_page,
                        total: casesPaginationInfo.total,
                        total_pages: casesPaginationInfo.total_pages,
                        cases: casesPaginationInfo.page === 1
                            ? casesPaginationInfo.data : [...this.state.cases, ...casesPaginationInfo.data]
                    });
                }
                setTimeout(() => {
                    if (this.state.page == 1) {
                        this.flatListRef.scrollToOffset({ animated: true, x: 0, y: 0 })
                    }
                    this.setState({ isLoading: false });
                }, config.caselistLoaderTimeout);
            } else {
                setTimeout(() => {
                    this.setState({ isLoading: false });
                }, config.caselistLoaderTimeout);
            }
        });
    }

    // To load more records with pagination when end reached
    loadMoreCasesWithPaginationOnEndReached = async () => {
        await this.loadMoreCasesWithPagination();
        await this.setState({ scrollBegin: false })
    }

    setScrollBegin = async () => {
        await this.setState({ scrollBegin: true })
    }

    // When click on search display filters
    prepareQueryForSearch = (selectedSearchInfo) => {
        this.setState({ isLoading: true }, () => {
            let initialilteruery = this.prepareFilterQuery();
            if (selectedSearchInfo && selectedSearchInfo.length && selectedSearchInfo.length > 0 && initialilteruery) {
                let searchCriteria = [...selectedSearchInfo];
                let filterQuery = initialilteruery.basicFilterQuery;
                let date = '';
                searchCriteria.forEach((seachField) => {
                    if (seachField && seachField.label) {
                        filterQuery += ' AND '
                        if (seachField.label === 'Patient Name') {
                            let name = seachField.value.trim();
                            if (name.includes(" ")) {
                                let names = seachField.value.split(' ');
                                if (names && names.length == 2) {
                                    filterQuery += '(FirstName CONTAINS[c] ' + '"' + names[0] + '" AND LastName CONTAINS[c] ' + '"' + names[1] + '")';
                                } else {
                                    filterQuery += 'FirstName CONTAINS[c] ' + '"' + name + '"';
                                }
                            } else {
                                filterQuery += '(FirstName CONTAINS[c] ' + '"' + name + '" OR LastName CONTAINS[c] ' + '"' + name + '")';
                            }
                        } else if (seachField.label === 'Admission Date') {
                            filterQuery += "AdmissionDate == $1";
                            date = dateFormats.formatDate(seachField.value, 'YYYY-MM-DD');
                        } else if (seachField.label === 'Patient Id') {
                            filterQuery += 'PatientID CONTAINS[c] ' + '"' + seachField.value.trim() + '"';
                        }
                    }
                });

                let isDateThere = searchCriteria.findIndex(query => query.label === 'Admission Date');
                let dateAvailable;
                if (isDateThere != -1) {
                    dateAvailable = date;
                } else {
                    dateAvailable = '';
                }
                this.onSelectFilterOption(filterQuery, dateAvailable);
            } else {
                this.getCasesRecords();
            }
        });
    }

    // Filter with dates
    filterCasesWithSelectedDate = (lastDate, searchQuery, dateAvailable) => {
        let filterQuery = this.prepareFilterQuery();
        if (filterQuery) {
            let query = searchQuery ? searchQuery : filterQuery.basicFilterQuery;
            if (lastDate) {
                query += " AND ((CompleteCMS == 0 OR CompleteUDS == 0) OR (DischargeDate != null AND DischargeDate >= $0))";
            }
            let allCases = [];
            if (lastDate) { //filtering based on selected date
                if (dateAvailable) { // if two dates are selected
                    allCases = realm.objects(schema.CASELISTING_SCHEMA).filtered(query, lastDate, dateAvailable).sorted(filterQuery.sortFilterQuery);
                } else {
                    allCases = realm.objects(schema.CASELISTING_SCHEMA).filtered(query, lastDate).sorted(filterQuery.sortFilterQuery);
                }
            } else { // all case
                if (dateAvailable) {
                    let dateQuery = query.replace("$1", "$0");
                    allCases = realm.objects(schema.CASELISTING_SCHEMA).filtered(dateQuery, dateAvailable).sorted(filterQuery.sortFilterQuery);
                } else {
                    allCases = realm.objects(schema.CASELISTING_SCHEMA).filtered(query).sorted(filterQuery.sortFilterQuery);
                }
            }

            this.setState({
                allCases: allCases,
            });
            this.loadMoreCasesWithPagination(allCases, 'Initial Load');
        }
    }

    // On select filter dropdown option
    onSelectFilterOption = (searchQuery, dateAvailable) => {
        if (this.state.selectedOption) {
            let currentDate = dateFormats.formatDate('todayDate', 'YYYY-MM-DD');
            let lastDate = '';

            if (this.state.selectedOption === 'All') {
                if (searchQuery) {
                    this.filterCasesWithSelectedDate('', searchQuery, dateAvailable);
                } else {
                    this.getCasesRecords();
                }
            } else if (this.state.selectedOption === 'Open cases and discharges in last 30 days') {
                lastDate = dateFormats.subtractDaysFromDate(currentDate, 30);
                this.filterCasesWithSelectedDate(lastDate, searchQuery, dateAvailable);
            } else if (this.state.selectedOption === 'Open cases and discharges in last 60 days') {
                lastDate = dateFormats.subtractDaysFromDate(currentDate, 60);
                this.filterCasesWithSelectedDate(lastDate, searchQuery, dateAvailable);
            } else if (this.state.selectedOption === 'Open cases and discharges in last 90 days') {
                lastDate = dateFormats.subtractDaysFromDate(currentDate, 90);
                this.filterCasesWithSelectedDate(lastDate, searchQuery, dateAvailable);
            } else if (this.state.selectedOption === 'Open cases and discharges in last 120 days') {
                lastDate = dateFormats.subtractDaysFromDate(currentDate, 120);
                this.filterCasesWithSelectedDate(lastDate, searchQuery, dateAvailable);
            } else {
                setTimeout(() => {
                    this.setState({ isLoading: false });
                }, config.caselistLoaderTimeout);
            }
        } else {
            setTimeout(() => {
                this.setState({ isLoading: false });
            });
        }
    }

    // On select record from image in a row
    onSelectRecordFromImage = (selectedCase, index) => {
        if (selectedCase && selectedCase.AdmissionDate) {
            let allCases = [...this.state.cases];
            if (selectedCase.selected) {
                selectedCase.selected = false;
            } else {
                selectedCase.selected = true;
            }
            allCases[index] = selectedCase;
            this.updateDataInState('cases', allCases);
        }
    }

    // update data in state
    updateDataInState = (fieldName, value) => {
        this.setState({ [fieldName]: value });
    }


    // back button handler
    backBtnHandler = () => {
        navigation.popScreenNavigator(this.props.componentId);
    }

    // open Case listing screen handler
    // openAddCaseForm = (patientDetails) => {
    //     let props = {
    //         'loggedInUserDetails': this.state.loggedInUserDetails,
    //         homeScreenNavigatorId: this.props.homeScreenNavigatorId,// for home button
    //         listingNavigatorId: this.props.componentId,
    //         getIsLoading: this.props.getIsLoading,
    //         autoSyncProgress: this.state.autoSyncProgress,
    //         checkAutoSyncProgressForStatus: this.checkAutoSyncProgress,
    //         onlineSyncAutoSuccess: this.props.onlineSyncAutoSuccess,
    //         caseListingSaveWithMofiedDetails: this.props.caseListingSaveWithMofiedDetails

    //     };
    //     if (patientDetails && patientDetails.patIPADID) {
    //         props.patIPADID = patientDetails.patIPADID;
    //         props.LockDownStatus = patientDetails.LockDownStatus;
    //     }
    //     AsyncStorage.setItem('screenName', JSON.stringify(navigation.ADD_CASE_SCREEN));
    //     navigation.pushScreenNavigator(this.props.componentId, navigation.ADD_CASE_SCREEN, props);
    // }

    // get selected case records
    getSelectedCaseRecords = () => {
        let allCases = [...this.state.cases];
        let selectedCases = [];

        allCases.forEach(selectedCase => {
            if (selectedCase.selected) {
                selectedCases.push(selectedCase);
            }
        });
        return selectedCases;
    }

    // Open common info modal
    openCommonInfoModal = (modalHeading, infoMessage, iconType, modalAction, confirmActionBody) => {
        setTimeout(() => {
            this.setState({
                isOpenCommonInfoModal: true,
                modalIconType: iconType,
                modalAction: modalAction ? modalAction : '',
                confirmActionBody: confirmActionBody ? confirmActionBody : '',
                modalHeading: modalHeading ? modalHeading : '',
                infoMessage: infoMessage ? infoMessage : ''
            });
        }, 10);
    }

    // Hide succes modal
    hideCommonInfoModal = (body) => {
        this.setState({
            isOpenCommonInfoModal: false
        }, () => {
            if (body == 'MoveToLogin' || body == 'dbVersionChange' || body == 'appVersionChange') {
                navigation.popToTopScreenNavigator(this.props.componentId);
            }
        });
    }

    // open delete case modal
    onClickDeleteCase = () => {
        let selectedCases = this.getSelectedCaseRecords();
        if (selectedCases && selectedCases.length === 0) {
            this.setState({ isLoading: false }, () => {
                this.openCommonInfoModal(config.informationMsg, configMessage.patientAlteastOneRecord, 'Validation');
                return;
            });
        } else {
            if (selectedCases) {
                let message = configMessage.preadmissionSingleDeleteConfirmation;
                let singleCaseWithLock = false;

                if (selectedCases.length == 1) {
                    // can not delete locked patient
                    if (selectedCases[0] && selectedCases[0].LockDownStatus == 1) {
                        singleCaseWithLock = true;
                        message = configMessage.cannotDeleteLockedPatient;
                    }
                } else if (selectedCases.length > 1) {
                    let isAnyCaseWithLock = false;
                    selectedCases.forEach(selectedCase => {
                        if (selectedCase && selectedCase.LockDownStatus && selectedCase.LockDownStatus == 1) {
                            isAnyCaseWithLock = true;
                        }
                    });
                    // selected any case with lock
                    if (isAnyCaseWithLock) {
                        message = configMessage.deletemultipleCasesWithLockMsg;
                    } else {
                        message = message.replace('?', 's?');
                    }
                }
                this.setState({ isLoading: false }, () => {
                    if (singleCaseWithLock) {
                        this.openCommonInfoModal(config.informationMsg, message, 'Validation');
                    } else {
                        this.openCommonInfoModal(config.confirmMsg, message, 'Confirm', 'Confirm', selectedCases);
                    }
                });
            } else {
                this.updateDataInState('isLoading', false);
            }
        }
    }

    // on click delete action
    // onClickDeleteConfirmAction = (selectedCases) => {
    //     if (selectedCases && selectedCases.length && selectedCases.length > 0) {
    //         this.updateDataInState('isLoading', true);
    //         selectedCases.forEach(selectedCase => {
    //             if (selectedCase.LockDownStatus && selectedCase.LockDownStatus == 1) {
    //                 // locked patient
    //             } else {
    //                 let patientObj = {
    //                     patIPADID: selectedCase.patIPADID,
    //                     DeletedBy: this.state.loggedInUserDetails.UserID,
    //                     DeletedOn: dateFormats.formatDate('todayDate'),
    //                     UpdatedOn: dateFormats.formatDate('todayDate'),
    //                     UpdatedBy: this.props.loggedInUserDetails.UserID
    //                 };
    //                 realmController.insertOrUpdateRecord(schema.CASELISTING_SCHEMA, patientObj, true);

    //                 // change patient admit status in preadmission after deleting patient in case
    //                 let filterQuery = 'patIPADID == ' + selectedCase.patIPADID;
    //                 let preadmissionAdmittedRecord = realmController.getAllRecordsWithFiltering(schema.PREADMISSION_SCHEMA, filterQuery);
    //                 if (preadmissionAdmittedRecord && preadmissionAdmittedRecord.length) {
    //                     let preAdmissionObject = {
    //                         PreAdmissionIPADID: preadmissionAdmittedRecord[0].PreAdmissionIPADID,
    //                         patIPADID: null,
    //                         PatID: null,
    //                         UpdatedOn: dateFormats.formatDate('todayDate'),
    //                         UpdatedBy: this.state.loggedInUserDetails.UserID
    //                     }
    //                     realmController.insertOrUpdateRecord(schema.PREADMISSION_SCHEMA, preAdmissionObject, true);
    //                 }
    //             }
    //         });
    //         this.setState({ isLoading: false }, () => {
    //             this.openCommonInfoModal(configMessage.multiplePatientsDeleteSuccessMsg, '', 'Success');
    //             this.getCasesRecords();
    //         });
    //     }
    // }

    displayValidationForVersionChange = (validationType) => {
        if (validationType == 'databaseChanged') {
            this.openCommonInfoModal(configMessage.databaseChangeErrorMessage, '', 'Warning', '', 'dbVersionChange');
            return;
        } else if (validationType == 'appVersionChanged') {
            this.openCommonInfoModal(configMessage.appVersionChangeErrorMessage, '', 'Warning', '', 'appVersionChange');
            return;
        }
    }

    // getNotifications = () => {
        // if (this.mainHeaderRef) {
        //     this.mainHeaderRef.getAppNotificationsWhenSync();
        // }
    // }

    render() {
        return (
            <Container onTouchStart={() => common.activateSession()}>
                <Text style={style.topStatusBarColor} />
                <Loader loading={this.state.isLoading} />

                {/* header1 */}
                <MainHeader homeScreenNavigatorId={this.props.homeScreenNavigatorId} navigatorProps={this.props.componentId}
                    onRef={ref => (this.mainHeaderRef = ref)}
                    // autoSyncProgress={this.state.autoSyncProgress}
                    // checkAutoSyncProgressForStatus={this.checkAutoSyncProgress}
                    // autoSync={true}
                    userInfo={this.props.loggedInUserDetails}
                    page={'CaseListing'}
                    // onlineSyncAutoSuccess={this.state.isInternetConnected}
                    displayValidationForDBOrAppVersionChange={this.displayValidationForVersionChange}
                />

                {/* header2 */}
                {/* <Header style={headerStyles.subHeader}>
                    <Left>
                        <TouchableOpacity onPress={this.backBtnHandler} >
                            <Image source={configImage.backButtonIcon} style={headerStyles.backBtn} />
                        </TouchableOpacity>
                    </Left>
                    <Right style={styles.buttonTopBottomSpace}>
                        <TouchableOpacity onPress={() => this.checkPrevilegesAndGrantPermissions('Add')} >
                            <Image source={configImage.addButtonIcon} style={[headerStyles.buttonRightSpace, headerStyles.addBtn]} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.checkPrevilegesAndGrantPermissions('Delete')} >
                            <Image source={configImage.deleteButtonIcon} style={[headerStyles.deleteBtn]} />
                        </TouchableOpacity>
                    </Right>
                </Header> */}

                {/* <Header style={headerStyles.headSpaceRemove}>
                    <Left>
                        {this.CaseListingPrivilege && (this.CaseListingPrivilege == config.readWrite ||
                            this.CaseListingPrivilege == config.readWriteLocking) ?
                            <Text style={headerStyles.privilegesLabel}>{config.readWrite}</Text> :
                            this.CaseListingPrivilege && (this.CaseListingPrivilege == config.readOnly ||
                                this.CaseListingPrivilege == config.readOnlyLocking) ?
                                <Text style={headerStyles.privilegesLabel}>{config.readOnly}</Text> :
                                <Text style={headerStyles.privilegesViewLabel}>{config.viewPermission}</Text>}
                    </Left>
                    <Body>
                        <Title style={headerStyles.listHeading}>Case Listing</Title>
                    </Body>
                    <Right>
                        <Text>{this.state.allCases.length}</Text>
                    </Right>
                </Header> */}

                {/* Search header start */}
                <View style={headerStyles.searchHeaderContainer}>
                    <View style={{ width: '34.5%', paddingLeft: 10 }}>
                        <RNPickerSelect
                            hideDoneBar={false}
                            placeholder={{
                                label: 'All',
                                value: 'All',
                            }}
                            placeholderTextColor={'black'}
                            items={this.state.List}
                            onValueChange={(value) => {
                                this.setState({
                                    selectedOption: value,
                                });
                            }}
                            style={{
                                ...pickerSelectStyles,
                                inputIOS: {
                                    ...pickerSelectStyles.inputIOS,
                                    height: 40
                                }
                            }}
                            value={this.state.selectedOption}
                            onOpen={() => this.setState({ changedDropdownValue: this.state.selectedOption })}
                            onClose={(this.state.changedDropdownValue && this.state.selectedOption != this.state.changedDropdownValue)
                                ? this.prepareQueryForSearch : null}
                        />
                    </View>

                    {/* start common search bar */}
                    <View style={{ width: '65%' }}>
                        <CommonSearchBarPopover
                            onRef={ref => (this.searchBarRef = ref)}
                            searchFilterList={this.state.searchFilterList}
                            prepareQueryForSearch={this.prepareQueryForSearch}
                            searchType={'listing'}
                        />
                    </View>
                    {/* end of common search bar */}

                </View>
                {/* End of search header */}

                {/*List Display section start  */}
                <List style={[styles.listSpace, { flex: 1 }]}>

                    {/* <ListItem avatar style={styles.lists} >
                        <Left style={styles.list}>
                            <Text style={[styles.headingList]}>Name/Patient ID</Text>
                        </Left>
                        <Body style={styles.list}>
                        </Body>
                        <Body style={[styles.listNoMargin, styles.dateWidth]}>
                            <Text style={styles.headingList}>Admission Date</Text>
                        </Body>

                        <Body style={[styles.listNoMargin, styles.dateWidth]}>
                            <Text style={styles.headingList}>Discharge Date</Text>
                        </Body>
                        <Body style={[styles.listNoMargin, styles.impairment]}>
                            <Text style={styles.headingList}>CMG</Text>
                        </Body>
                        <Body style={[styles.listNoMargin, styles.impairment]}>
                            <Text style={styles.headingList}>ELOS</Text>
                        </Body>
                        <Body style={[styles.listNoMargin, styles.locked]}>
                            <Text style={styles.headingList}>Locked</Text>
                        </Body>
                        <Right style={styles.list}></Right>
                    </ListItem> */}
                    <FlatList
                        ref={(ref) => this.flatListRef = ref}
                        data={this.state.cases}
                        onEndReached={this.state.scrollBegin ? this.loadMoreCasesWithPaginationOnEndReached : null}
                        onEndReachedThreshold={0.1}
                        onScrollBeginDrag={() => this.setScrollBegin()}
                        renderItem={({ item, index }) =>
                            <ListItem avatar style={styles.lists} 
                            onPress={() => this.checkPrevilegesAndGrantPermissions(item)}
                             >
                                <Left style={[styles.list]}>
                                    {item.selected ?
                                        <Avatar
                                            width={40}
                                            height={40}
                                            overlayContainerStyle={{ backgroundColor: config.buttonTextColor }}
                                            icon={{ name: 'check', color: config.whiteColor, type: 'font-awesome' }}
                                            onPress={() => this.onSelectRecordFromImage(item, index)}
                                            activeOpacity={0.7}
                                        />
                                        :
                                        <TouchableOpacity style={styles.thumbShortcut} onPress={() => this.onSelectRecordFromImage(item, index)}>
                                            <Text style={styles.thumbText}>
                                                {common.prepareIconTitleFromUsername(item.FirstName + ' ' + item.LastName)}
                                            </Text>
                                        </TouchableOpacity>}
                                </Left>

                                <Body style={styles.list}>
                                    <Text style={styles.name}>{item.FirstName} {item.LastName} </Text>
                                    <Text style={styles.phone}>{item.PatientID}</Text>
                                    <View style={styles.rowDivide}>
                                        <View style={styles.dateItem}>
                                            <Text style={styles.labelDate}>
                                                {item.AdmissionDate ? dateFormats.formatDate(item.AdmissionDate, config.dateFormat) : ''}
                                            </Text>
                                            <Text style={styles.labelDataLabel}>Admission Date</Text>
                                        </View>
                                        <View style={styles.dateItem}>
                                            <Text style={styles.labelDate}>
                                                {item.DischargeDate ? dateFormats.formatDate(item.DischargeDate, config.dateFormat) : ''}
                                            </Text>
                                            <Text style={styles.labelDataLabel}>Discharge Date</Text>
                                        </View>
                                    </View>
                                </Body>
                                {/* <Body style={[styles.listNoMargin, styles.dateWidth]}>
                                    <View style={styles.borderRight}>
                                        <Body>
                                            <Text style={styles.assDate}>
                                                {item.AdmissionDate ? dateFormats.formatDate(item.AdmissionDate, config.dateFormat) : ''}
                                            </Text>
                                        </Body>
                                    </View>
                                </Body> */}
                                {/* <Body style={[styles.listNoMargin, styles.dateWidth]}>
                                    <View style={styles.borderRight}>
                                        <Body>
                                            <Text style={styles.assDate}>
                                                {item.DischargeDate ? dateFormats.formatDate(item.DischargeDate, config.dateFormat) : ''}
                                            </Text>
                                        </Body>
                                    </View>
                                </Body> */}
                                {/* <Body style={[styles.listNoMargin, styles.impairment]}>
                                    <View style={styles.borderRight}>
                                        <Body>
                                            <Text style={styles.assDate}>{item.PCMG}</Text>
                                        </Body>
                                    </View>
                                </Body> */}
                                {/* <Body style={[styles.listNoMargin, styles.impairment]}>
                                    <View style={styles.borderRight}>
                                        <Body>
                                            <Text style={styles.assDate}>{common.getELOSFromCaseMixGroup(item.PCMG, item.PCMGVersion)}</Text>
                                        </Body>
                                    </View>
                                </Body> */}
                                <Right style={[styles.list, styles.lockImageStatus]}>
                                    {item.LockDownStatus == 1 ? <Image source={configImage.lockButtonIcon} style={styles.unlock} /> :
                                        <Image source={configImage.unlockButtonIcon} style={styles.lock} />}
                                </Right>

                                {/* <Right style={{paddingRight:10}}/> */}
                            </ListItem>
                        }
                        keyExtractor={(item, index) => index.toString()}
                        extraData={this.state.cases}
                        ItemSeparatorComponent={this.renderSeparator}
                        ListEmptyComponent={<ErrorMessage />}
                    />
                </List>

                {/* Common info modal start */}
                <CommonInfoModal
                    iconType={this.state.modalIconType}
                    isOpenCommonInfoModal={this.state.isOpenCommonInfoModal}
                    displayMessage={this.state.infoMessage ? this.state.infoMessage : ''}
                    modalHeading={this.state.modalHeading ? this.state.modalHeading : ''}
                    hideCommonInfoModal={this.hideCommonInfoModal}
                    modalAction={this.state.modalAction}
                    confirmYesAction={this.state.modalAction && this.state.modalAction == 'Confirm' ?
                        this.onClickDeleteConfirmAction : ''}
                    confirmActionBody={this.state.confirmActionBody} />
                {/* common info modal end */}


            </Container >
        );
    }
}
export default CaseListing;