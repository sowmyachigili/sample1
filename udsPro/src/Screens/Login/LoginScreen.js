/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import React, { Component } from 'react';
import {
    View, Text,
    Keyboard, TouchableWithoutFeedback,
    StyleSheet, TouchableOpacity, Image, KeyboardAvoidingView, Platform, AppState, Linking
} from 'react-native';
import { Form, Item, Input, Label, Button } from 'native-base';

import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from '@react-native-community/netinfo';

import Modal from "react-native-modal";
import DeviceInfo from 'react-native-device-info';

// Config
import fetchMethodRequest from '../../config/service';
import api from '../../config/apiCalls';
import config from '../../config/config';
import configMessage from '../../config/configMessages';
import configImage from '../../config/configImages';

import KeyboardManager from 'react-native-keyboard-manager';
import navigation from "../../utility/Navigation";
import ErrorLogHandler from '../../utility/ErrorLogAlert';

// Realm
import realmController from '../../Realm/realmController';
import realm from '../../Realm/realm';
import schema from '../../Realm/schemaNames';

// Components
import LogoImage from "../../components/common/logo";
import BackgroundImage from "../../components/backgroundImage/backgroundImage";
import dateFormats from '../../utility/formatDate';
import LoaderNew from '../../components/common/loaderNew';
import common, { getFacilityExpDetails } from '../../utility/commonFun';

// Styles
import modalStyles from '../../components/styles/modalStyles';
import localStyles from '../../components/styles/styles';

// PasscodeAlert
import PasscodeAlert from '../../utility/PasscodeAlert';
import AuditLogFile from '../../utility/AuditLogFile';

// Generate tocken ID
import UUIDGenerator from 'react-native-uuid-generator';

import Synchronization from '../Synchronization/Synchronization';

let loginUserId, loginSyncDetails, loginFacilityCode;
class LoginScreen extends Component {
    constructor(props) {
        super(props);
        this.inputRefs = {};
        this.state = {
            isLoading: true,
            userName: '',
            facilityCode: '',
            password: '',
            guid: '',
            isInternetConnected: true,

            // Validation Modal
            validationModal: false,
            validationMessages: [],
            validationHeading: '',

            disableLoginButton: false,
            // isNewVersionAvailable: false,
            displayValidationStar: false,
            loggedInUserDetails: '',
            onlineSyncAutoSuccess: false,
            appState: AppState.currentState,
            appStoreURL: '',
            isDbVersionChange: false,
            isAppVersionChange: false,
            isNewVersionAvalilable: false,
            Users: [],
        }
    }

    componentDidMount = async () => {
        // check the app status in background
        if (this.state.appState == 'active') {
            let passcodeStatus = await PasscodeAlert.displayPasscodeErrorAlert(this.state.appState);
            if (passcodeStatus == 'passcodeDisable') {
                return;
            } else {
                AppState.addEventListener('change', this._handleAppStateChange);
                // Clears async storage if anything in asyncstore
                AsyncStorage.getAllKeys().then(AsyncStorage.multiRemove);

                // check internet connection
                NetInfo.addEventListener(state => {
                    this.handleNetConnection(state);
                });
                NetInfo.fetch().then(state => {
                    this.handleNetConnection(state);
                });

                this.updateInputStateHandler('isLoading', false);
                KeyboardManager.setEnable(false);
            }
        } else {
            this.updateInputStateHandler('isLoading', false);
        }
    }

    handleNetConnection = (state) => {
        if (state && state.type) {
            this.setState({
                isInternetConnected: state.isConnected
            })
        }
    }

    componentWillUnmount() {
        // check the app status in background
        AppState.removeEventListener('change', this._handleAppStateChange);
    }

    // generate tocken id for SessionID
    generateTokenID = () => {
        UUIDGenerator.getRandomUUID((uuid) => {
            AsyncStorage.setItem('SessionID', uuid);
            return uuid;
        });
    }

    // handle the app status in background
    _handleAppStateChange = async (nextAppState) => {
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            // check and display passcode alert when passcode is disabled
            await PasscodeAlert.displayPasscodeErrorAlert(nextAppState);
        }
        await this.setState({ appState: nextAppState });
        return;
    }

    // auto sync in home screen only when user is in online
    setOnlineSyncMsgDisplay = () => {
        this.setState({
            onlineSyncAutoSuccess: false
        });
    }

    // to handle user entered data
    updateInputStateHandler = (fieldName, value) => {
        if (fieldName == 'facilityCode') {
            value = value ? value.toUpperCase() : null;
        }
        this.setState({ [fieldName]: value });
    }

    // Move user to home scree
    moveUserToHomeScreen = async (userId, loginFacilityCode) => {
        await this.saveSettingsInStore();
        let props = {
            FacilityCode: loginFacilityCode,
            UserID: userId,


            loggedInUserDetails: this.state.loggedInUserDetails,
            homeScreenNavigatorId: this.props.componentId, // for home button
        };
        AsyncStorage.setItem('screenName', JSON.stringify(navigation.CASE_LISTING_SCREEN));
        navigation.pushScreenNavigator(this.props.componentId, navigation.CASE_LISTING_SCREEN, props);
    }

    // display validation model based on custom model
    displayOrHideValidationMessage = (isDisplay, heading, messages, type) => {
        let allMessages = [], appStoreURL = '', isDbVersionChange = false, isAppVersionChange = false;
        if (typeof messages === 'string') {
            allMessages.push(messages);
        } else {
            allMessages = messages;
        }

        if (type == 'appChange') {
            appStoreURL = common.getConfigurationDetails('APPUPDATEIOSURL', 'versions');
            isAppVersionChange = true;
        }
        if (type == 'dbChange') {
            isDbVersionChange = true;
        }

        if (isDisplay) {
            setTimeout(() => {
                this.setState({
                    appStoreURL: appStoreURL,
                    isDbVersionChange: isDbVersionChange,
                    isAppVersionChange: isAppVersionChange,
                    validationModal: true,
                    validationHeading: heading,
                    validationMessages: allMessages,
                    displayValidationStar: typeof messages === 'string' ? false : true
                });
            }, 300)
        } else {
            this.setState({ validationModal: false }, () => {
                if (this.state.isAppVersionChange) {
                    setTimeout(() => {
                        if (this.state.appStoreURL) {
                            Linking.openURL(this.state.appStoreURL);
                        }
                    }, 90);
                }
                if (this.state.isNewVersionAvalilable) {
                    setTimeout(() => {
                        this.navigateToHomeScreen();
                    }, 90);
                }
            });
        }
    }

    // Clear state for invalid credentials
    clearStateForInvalidCredentials = (actionType) => {
        this.setState({
            userName: '',
            facilityCode: '',
            password: '',
            disableLoginButton: false
        });

        if (!actionType) {
            // For focusing facility code field
            this.moveFocusToNextField('facilityCode');
        }
    }

    // Save session expiration time,pagination limit in asyncstorage
    saveSettingsInStore = () => {
        // Session expiry time
        let expTime = common.getConfigurationDetails('SESSIONTIMEOUT'); // Ex: 20 min
        AsyncStorage.setItem('sessionExpTimeInMin', JSON.stringify(expTime));
        let expiryTime = common.getExpirationtime(expTime); // Present time + 20
        AsyncStorage.setItem('sessionExpTime', JSON.stringify(expiryTime));

        // For settign page limit in store
        let limitPerPage = common.getConfigurationDetails(schema.DEFAULT_RECORDS_PERPAGE);
        AsyncStorage.setItem('DEFAULT_RECORDS_PERPAGE', JSON.stringify(limitPerPage));
    }

    // Login handler
    userLoginHandler = async () => {
        if (this.state.userName && this.state.facilityCode && this.state.password) {
            await this.updateInputStateHandler('isLoading', true);
            if (this.state.isInternetConnected) {
                await this.submitUserDetailsToServer();
            } else {
                await this.updateInputStateHandler('isLoading', false);
                await this.displayOrHideValidationMessage(true, config.validationSummary, 'No internet connection. Connect to the internet and try again.');
            }

        } else {
            if (!this.state.facilityCode) {
                this.displayOrHideValidationMessage(true, config.validationSummary, configMessage.enterFacilityCode);
                return;
            }
            if (!this.state.userName) {
                this.displayOrHideValidationMessage(true, config.validationSummary, configMessage.enterUserName);
                return;
            }
            if (!this.state.password) {
                this.displayOrHideValidationMessage(true, config.validationSummary, configMessage.loginNoPassword);
                return;
            }
        }
    }

    // prepare data for password expiration Message
    prepareOfflineValidationMessage = (type) => {
        if (type) {
            this.updateInputStateHandler('isLoading', false);

            if (type === 'Pasword Expiry') {
                this.displayOrHideValidationMessage(true, configMessage.passwordExpHeading, configMessage.passwordExpMsg);
            } else if (type === 'Inactive User') {
                this.displayOrHideValidationMessage(true, config.validationSummary, configMessage.inactiveUserMsg);
            } else if (type === 'Locked User') {
                this.displayOrHideValidationMessage(true, config.validationSummary, configMessage.userLockedMsg);
            } else if (type === 'Facility Expired') {
                let message = `Facility ${this.state.facilityCode} registration has expired Please call UDSMR Technical Support to renew the registration.`
                this.displayOrHideValidationMessage(true, configMessage.facilityExpHeading, message);
            }
            this.clearStateForInvalidCredentials();
        }
    }

    // Update user info in audit log
    updateAuditLogInfo = async (loginUserDetails) => {
        await this.generateTokenID();
        await AuditLogFile.updateAuditLogInfoOnLogin(loginUserDetails)
    }

    // if user is online - submit details to server
    submitUserDetailsToServer = () => {
        let userData = {
            UserName: this.state.userName,
            FacilityCode: this.state.facilityCode,
            Password: this.state.password
        }
        this.setState({ disableLoginButton: true });
        fetchMethodRequest('POST', api.loginValidateNew, userData)
            .then(async (response) => {
                if (response && response.ResponseCode) {
                    if (response.ResponseCode === 'Fail') {
                        if (response.ResponseMessage) {
                            await this.updateInputStateHandler('isLoading', false);
                            await this.clearStateForInvalidCredentials();
                            await this.displayOrHideValidationMessage(true, config.validationSummary, response.ResponseMessage);
                        }
                    } else if (response.ResponseCode === 'Success') {
                        await this.setState({
                            guid: response.GUID,
                            onlineSyncAutoSuccess: true
                        })

                        // set user details in async store
                        let loginUserDetails = {
                            LoginName: userData.UserName,
                            FacilityCode: userData.FacilityCode,
                            Password: userData.password,
                            GUID: response.GUID,
                            UserID: response.UserID
                        }
                        await common.setLoggedInUserDetailsInAsyncStore(loginUserDetails);
                        let syncDetails = await this.getLastSyncDate(response.UserID);
                        loginUserId = response.UserID;
                        loginFacilityCode = this.state.facilityCode;
                        loginSyncDetails = syncDetails;
                        await this.getMasterDataRecords(response.UserID, syncDetails);

                    }
                } else {
                    await this.displayOrHideValidationMessage(true, configMessage.serverCallErrMsg, '');
                    await this.clearStateForInvalidCredentials();
                    await this.updateInputStateHandler('isLoading', false);
                }

            }).catch(error => {
                this.updateInputStateHandler('isLoading', false);
                this.displayOrHideValidationMessage(true, configMessage.serverCallErrMsg, '');
                this.clearStateForInvalidCredentials();
                // save error details in realm
                let errorDetails = { ...error, ...userData };
                ErrorLogHandler(errorDetails, true, '', 'Hide Alert', 'ServerError');
            });
    }

    // Get last synschronize date time
    getLastSyncDate = async (userId) => {
        let syncDetails = await common.getLastSyncDate('User', userId, this.state.facilityCode);
        if (syncDetails && syncDetails.length && syncDetails.length == 1) {
            return syncDetails[0];
        } else {
            return null;
        }
    }

    // To get master data records
    getMasterDataRecords = async (userID, syncDetails) => {
        let masterDataBody = {
            FacilityCode: this.state.facilityCode,
            UserID: userID,
            LastSyncDate: syncDetails && syncDetails.LastSynDate ? syncDetails.LastSynDate : null
        }

        fetchMethodRequest('POST', api.loginMasterData, masterDataBody)
            .then(async (response) => {
                if (response) {
                    if (response == config.invalidGUIDCode) { //401 error
                        this.displayOrHideValidationMessage(true, config.validationSummary, configMessage.invalidTokenMsgLogin);
                    } else if (response == config.generateGuidErrorCode) { //guid fail
                        this.displayOrHideValidationMessage(true, config.validationSummary, configMessage.unableToGenerateGUIDLogin);
                    } else if ((response.Facility && response.Facility.length && response.Facility.length > 0) ||
                        (response.Users && response.Users.length && response.Users.length > 0)) {
                        if (response.Users[0] && response.Users[0].UserID) {
                            let userDatFromRealm = realmController.getSingleRecord(schema.USERS_SCHEMA, `${response.Users[0].UserID}-${this.state.facilityCode}`);
                            userDatFromRealm = { ...userDatFromRealm };
                            // Delete all data if Database version changed after validation display in all screen from login
                            if (userDatFromRealm && userDatFromRealm.IsDbChanged) {
                                realm.write(() => {
                                    realm.deleteAll();
                                })
                            }
                        }
                        if (response.Facility && response.Facility.length && response.Facility.length > 0) {
                            // Insert facility
                            response.Facility.forEach((facility) => {
                                if (facility && facility.FacilityCode) {
                                    realmController.insertOrUpdateRecord(schema.Facility_SCHEMA, facility, true);
                                }
                            });
                        }

                        // Insert PROi App settings
                        let isDatabaseChanged = await Synchronization.syncPROiAppSettingsData({ FacilityCode: this.state.facilityCode, UserID: userID });
                        if (isDatabaseChanged && isDatabaseChanged == 'databaseChanged') {
                            await this.displayOrHideValidationMessage(true, configMessage.databaseChangeErrorMessage, '', 'dbChange');
                            await this.clearStateForInvalidCredentials();
                        } else {
                            if (response.Users && response.Users.length && response.Users.length > 0) {
                                this.setState({
                                    Users: response.Users
                                });

                                // check app new version is available or not
                                let deviceAppversion = DeviceInfo.getVersion();
                                let appStoreVersion = common.getConfigurationDetails('APPVERSION', 'versions');
                                if (!this.state.isNewVersionAvalilable && deviceAppversion && appStoreVersion && appStoreVersion != deviceAppversion) {
                                    let allowedAppVersionsList = common.getConfigurationDetails('ALLOWEDAPPVERSIONS', 'versions');
                                    if (allowedAppVersionsList) {
                                        allowedAppVersionsList = allowedAppVersionsList.split(',');
                                        let existingIndex = allowedAppVersionsList.findIndex((allowedVersion) => allowedVersion.trim() == deviceAppversion);
                                        if (existingIndex == -1) {
                                            await this.clearStateForInvalidCredentials();
                                            await this.displayOrHideValidationMessage(true, configMessage.appVersionChangeErrorMessage, '', 'appChange');
                                            await this.setState({ disableLoginButton: false, isLoading: false });
                                            return;
                                        } else {
                                            AsyncStorage.setItem('availableLatestVersion', JSON.stringify(true));
                                            this.setState({
                                                isNewVersionAvalilable: true,
                                                isLoading: false,
                                                disableLoginButton: false
                                            }, async () => {
                                                await this.displayOrHideValidationMessage(true, configMessage.appNewVersionAvailableMessage, '');
                                            });
                                            return;
                                        }
                                    }
                                } else {
                                    AsyncStorage.setItem('availableLatestVersion', JSON.stringify(false));
                                }
                                await this.navigateToHomeScreen();
                            }
                        }

                        await this.setState({ disableLoginButton: false, isLoading: false });
                    } else {
                        await this.displayOrHideValidationMessage(true, configMessage.serverCallErrMsg, '');
                        await this.setState({ isLoading: false });
                    }
                }
            }).catch(error => {
                let errorDetails = { ...error, ...masterDataBody };
                ErrorLogHandler(errorDetails, true, '', 'Hide Alert');
            });
    }

    navigateToHomeScreen = async () => {
        // Insert USER Info
        if (this.state.Users && this.state.Users.length > 0) {
            for (let user of this.state.Users) {
                // if user data matches add user with user entered password
                let userId = user.UserID;
                await this.setState({
                    loggedInUserDetails: user
                });
                if (user && user.UserID === userId) {
                    await this.saveUserInfoInLocalStore(user);
                } else {
                    // Just insert all users
                    await common.saveUserInfoInLocalRealm(user);
                }
                await this.updateAuditLogInfo(user);
            }
            
            await this.moveUserToHomeScreen(loginUserId, loginFacilityCode);
            if (loginFacilityCode && loginUserId) {
                common.updateSynchronizeDetails('User', loginFacilityCode, loginUserId, [loginSyncDetails]);
            }
            this.setState({
                isNewVersionAvalilable: false
            })
        }
    }

    // Save user details into local realm db
    saveUserInfoInLocalStore = (userInfo) => {
        let userDetails = common.saveUserInfoInLocalRealm(userInfo, this.state.password);
        userDetails.GUID = this.state.guid;
        common.saveCurrentLoggedUserInAsyncStorage(userDetails, 'online');
    }

    // Check user details in store 
    // checkUserCredentialsInStore = async () => {
    //     this.setState({ disableLoginButton: true });

    //     let filterCriteria = 'LoginName ==[c] ' + '"'
    //         + this.state.userName + '"' + 'AND FacilityCode ==[c] '
    //         + '"' + this.state.facilityCode + '"' + 'AND Password = '
    //         + '"' + this.state.password + '"';

    //     let users = realmController.getAllRecordsWithFiltering(schema.USERS_SCHEMA, filterCriteria);
    //     if (users && users.length && users.length >= 1) {

    //         let loggedInUser = users[0];
    //         if (loggedInUser) {
    //             let currentDate = dateFormats.formatDate('todayDate', 'YYYY-MM-DD HH:mm:ss');
    //             let isFacilityExpired = false;

    //             // Check for user facility expiration
    //             if (loggedInUser.FacilityCode) {
    //                 let facilityDetails = getFacilityExpDetails(loggedInUser.FacilityCode);
    //                 let facilityExpDate;
    //                 if (facilityDetails && facilityDetails.ExpiryDate) {
    //                     facilityExpDate = facilityDetails.ExpiryDate;
    //                 }
    //                 if (facilityExpDate) {
    //                     isFacilityExpired = dateFormats.datesComparisionBefore(facilityExpDate, currentDate);
    //                 }
    //             }

    //             // Check for password expired or not
    //             let isPwdExpired = loggedInUser.ForcePasswordChange && loggedInUser.ForcePasswordChange === 1;
    //             let pwdExpDate, isPwdExpWithTime;
    //             if (loggedInUser.PasswordExpirationDays != 0) {
    //                 pwdExpDate = dateFormats.addDaysToDate(loggedInUser.LastPWDChangeDate, parseInt(loggedInUser.PasswordExpirationDays));
    //                 isPwdExpWithTime = dateFormats.datesComparisionBefore(pwdExpDate, currentDate);
    //             }

    //             if (isPwdExpired || isPwdExpWithTime) {
    //                 this.prepareOfflineValidationMessage('Pasword Expiry');
    //             } else if (loggedInUser.IsActive === 0) {
    //                 this.prepareOfflineValidationMessage('Inactive User');
    //             } else if (loggedInUser.IsLocked === 1) {
    //                 this.prepareOfflineValidationMessage('Locked User');
    //             } else if (isFacilityExpired) {
    //                 this.prepareOfflineValidationMessage('Facility Expired');
    //             } else {
    //                 common.saveCurrentLoggedUserInAsyncStorage(loggedInUser);
    //                 await this.updateAuditLogInfo(loggedInUser);
    //                 loginUserId = loggedInUser.UserID ? loggedInUser.UserID : null;
    //                 loginFacilityCode = loggedInUser.FacilityCode ? loggedInUser.FacilityCode : null;
    //                 this.moveUserToHomeScreen(loginUserId, loginFacilityCode);
    //             }
    //         }
    //         this.setState({ disableLoginButton: false, isLoading: false });
    //     } else {
    //         // check user name exists in realm or user provided invalid credentials
    //         let filterCriteria = 'FacilityCode==[c] ' + '"' + this.state.facilityCode + '" AND LoginName ==[c] ' + '"' + this.state.userName + '"';
    //         let users = realmController.getAllRecordsWithFiltering(schema.USERS_SCHEMA, filterCriteria);
    //         this.updateInputStateHandler('isLoading', false);
    //         if (users && users.length === 0) {
    //             this.displayOrHideValidationMessage(true, config.validationSummary, configMessage.connectOnlineMsg);
    //         } else {
    //             this.displayOrHideValidationMessage(true, config.validationSummary, configMessage.invalidLoginMsg);
    //         }
    //         this.clearStateForInvalidCredentials();
    //     }
    // }

    // Move focus to next field
    moveFocusToNextField = (fieldName) => {
        if (fieldName && this.inputRefs && this.inputRefs[fieldName]
            && this.inputRefs[fieldName]._root && this.inputRefs[fieldName]._root.focus) {
            this.inputRefs[fieldName]._root.focus();
        }
    }

    render() {
        // For displaying validation messages in the modal
        const validationMessage = this.state.validationMessages.map((data, i) => {
            return (
                data ?
                    <Text style={[modalStyles.validationText, { flexDirection: 'row', alignItems: 'center' }]} key={i}>
                        {this.state.displayValidationStar ?
                            <Text style={modalStyles.bigStar}>* </Text> : null}
                        <Text style={{ textAlign: 'center' }}>{data}</Text>
                    </Text> : null
            )
        });

        return (

            <BackgroundImage imageName='login'>
                <Text style={[localStyles.topStatusBarColorForHomeLogin]}></Text>
                <LoaderNew loading={this.state.isLoading} />
                <KeyboardAvoidingView behavior='padding'
                    keyboardVerticalOffset={
                        Platform.select({
                            ios: () => 0,
                            android: () => 200
                        })()
                    }
                    style={styles.container}>
                    <View style={styles.container}>
                        <LogoImage />
                        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                            <Form style={styles.formContainer}>
                                <View style={styles.view}>
                                    <Label style={styles.formLabel}>FACILITY CODE</Label>
                                    <Item style={styles.item}>
                                        <Input style={styles.input}
                                            // ref='facilityCode'
                                            maxLength={10}
                                            value={this.state.facilityCode}
                                            onChangeText={(val) => this.updateInputStateHandler('facilityCode', val)}
                                            ref={(el) => { this.inputRefs['facilityCode'] = el; }}
                                            returnKeyType='next'
                                            onSubmitEditing={() => this.moveFocusToNextField('userName')}
                                            blurOnSubmit={false}
                                        // autoCapitalize={'characters'}
                                        />
                                        <Image
                                            source={configImage.facilityCodeIcon}
                                            style={styles.codeimage} />
                                    </Item>
                                </View>
                                <View style={styles.view}>
                                    <Label style={styles.formLabel}>USER NAME</Label>
                                    <Item style={styles.item}>
                                        <Input style={styles.input} value={this.state.userName}
                                            maxLength={20}
                                            onChangeText={(val) => this.updateInputStateHandler('userName', val)}
                                            ref={(el) => { this.inputRefs['userName'] = el; }}
                                            returnKeyType='next'
                                            onSubmitEditing={() => this.moveFocusToNextField('password')}
                                            blurOnSubmit={false} />
                                        <Image source={configImage.userIcon} style={styles.userimage} />
                                    </Item>
                                </View>
                                <View style={styles.view}>
                                    <Label style={styles.formLabel}>PASSWORD</Label>
                                    <Item style={styles.item}>
                                        <Input style={styles.input} secureTextEntry
                                            value={this.state.password}
                                            onSubmitEditing={this.userLoginHandler}
                                            onChangeText={(val) => this.updateInputStateHandler('password', val)}
                                            ref={(el) => { this.inputRefs['password'] = el; }} />
                                        <Image source={configImage.passwordIcon} style={styles.pwdimage} />
                                    </Item>
                                </View>
                            </Form>

                        </TouchableWithoutFeedback>
                        {/* Based on disable button - display login button or disable login button */}
                        {/* <View style={{ flexDirection: 'row' }}> */}
                        {
                            this.state.disableLoginButton ?
                                <Button block disabled
                                    style={styles.loginDisabledButton} >
                                    <Text style={styles.loginButtonText}>LOGIN</Text>
                                </Button>
                                :
                                <Button block
                                    onPress={this.userLoginHandler}
                                    style={styles.loginButton} >
                                    <Text style={styles.loginButtonText}>LOGIN</Text>
                                </Button>
                        }
                    </View>

                    {/* Validation Modal Start */}
                </KeyboardAvoidingView>
                <View style={[styles.footer]}>
                    <Text style={[{
                        fontFamily: 'Roboto-Bold',
                        fontSize: 15,
                        color: config.whiteColor,
                        textDecorationLine: 'underline'
                    }]}
                        onPress={common.handlePrivacyPolicyDocument}>Privacy Policy</Text>
                </View>

                <Modal
                    isVisible={this.state.validationModal}
                    backdropOpacity={0.5}
                    onBackButtonPress={() => this.displayOrHideValidationMessage(false)}
                    style={modalStyles.modalSmall}>
                    <View style={modalStyles.modalContent}>
                        <View style={modalStyles.modalSmallBody}>
                            <View style={{ alignItems: 'center' }}>
                                <Image style={[modalStyles.warning]}
                                    source={this.state.isNewVersionAvalilable ? configImage.warningIcon : configImage.errorIcon} />
                                <Text style={[modalStyles.informationText, { marginBottom: 10 }]}>{this.state.validationHeading}</Text>
                                {this.state.validationMessages && this.state.validationMessages.length
                                    && this.state.validationMessages.length > 0 ?
                                    <View>
                                        {validationMessage}
                                    </View>
                                    : null}
                            </View>
                        </View>
                        <View style={[modalStyles.modalFooter]}>
                            <Button style={[modalStyles.btnDefault, modalStyles.buttonDark]}
                                onPress={() => this.displayOrHideValidationMessage(false)}>
                                <Text style={[modalStyles.btnDefaultText, modalStyles.buttonDarkText]}>
                                    {this.state.isDbVersionChange ? 'Restore' :
                                        this.state.isAppVersionChange ? 'Go To AppStore' : 'Ok'}</Text>
                            </Button>
                        </View>
                    </View>
                </Modal>
            </BackgroundImage>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        justifyContent: "center",
        alignItems: "center",
        flex: 1,
    },
    formContainer: {
        width: 300.5,
    },
    view: {
        borderBottomWidth: 0,
        marginLeft: 0,
        marginTop: 41.5
    },
    item: {
        marginLeft: 0,
        borderColor: '#E4E4E4',
        borderBottomWidth: 1,
    },
    input: {
        color: '#E4E4E4',
        height: 39.5,
        fontSize: 14,
    },
    formLabel: {
        color: '#E4E4E4',
        fontSize: 14,
        padding: 0,
        margin: 0,
        fontFamily: 'Roboto-Regular',
    },
    codeimage: {
        width: 20,
        height: 13,
        marginRight: 18.5,
    },
    userimage: {
        width: 13,
        height: 15,
        marginRight: 18.5,
    },
    pwdimage: {
        width: 11,
        height: 16,
        marginRight: 18.5,
    },
    forgotlabelMargin: {
        marginTop: 21.5
    },
    forgotPwd: {
        color: '#ffffff',
        fontSize: 14,
        textDecorationLine: "underline",
    },
    loginDisabledButton: {
        marginTop: 35,
        backgroundColor: '#83e4f0',
    },
    loginButton: {
        marginTop: 35,
        backgroundColor: '#4dd0e1',
    },
    loginButtonText: {
        color: '#ffffff',
        fontSize: 14,
        fontFamily: 'Roboto-Regular',
    },
    // footer 
    footer: {
        height: 40,
        marginBottom: 15,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    footerLeftContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
    footerRightContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    comodoSecure: {
        marginRight: 30,
        width: 70,
        height: 40,
    },
    footerText: {
        color: '#ffffff',
        fontSize: 14,
        fontFamily: 'Roboto-Regular',
    }
})
export default LoginScreen;