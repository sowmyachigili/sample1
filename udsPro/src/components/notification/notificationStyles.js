/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import { StyleSheet } from 'react-native';
import config from '../../config/config';

export default StyleSheet.create({
    modal: {
        width: 300,
        marginLeft: 0,
        marginRight: 0,
        marginBottom: 0,
        marginTop: 0,
        top: 64,
        right: 15,
        position: 'absolute',
    },
    modalContent: {
        backgroundColor: '#fff',
        width: '100%',
        borderWidth: 1,
        borderColor: config.tabBarBorderColor,
        borderRadius: 15,
    },
    moreText: {
        fontSize: 17,
        color: config.buttonTextColor,
        fontFamily: 'Roboto-Regular',
    },
    moreTextOnClick: {
        fontSize: 17,
        fontFamily: 'Roboto-Regular',
    },
    alignItems: {
        justifyContent: 'center',
    },
    scrollableModalContent1: {
        height: 200,
        alignItems: "center",
        justifyContent: "center"
    },
    scrollableModal: {
        height: 900
    }

});