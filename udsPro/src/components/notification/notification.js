/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import React, { Component } from 'react';
import { View, Image, TouchableOpacity, StyleSheet, Linking } from 'react-native';
import { Text, ListItem, Badge } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import Modal from "react-native-modal";

import config from '../../config/config';
import configImage from '../../config/configImages';

// Styles
import styles from './notificationStyles';

// Common Function
import commonFun from '../../utility/commonFun';

class Notification extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isModalVisible: false,
            availableLatestVersion: false,
        }
    }

    componentDidMount() {
        this.props.onRef(this);
    }

    componentWillUnmount() {
        this.props.onRef(null);
    }

    getAppNotifications = () => {
        let isAvailable = false;
        AsyncStorage.getItem('availableLatestVersion').then(availableLatestVersion => {
            availableLatestVersion = JSON.parse(availableLatestVersion);
            if (availableLatestVersion) {
                isAvailable = true;
            }
            this.setState({
                availableLatestVersion: isAvailable,
            })
        });
    }

    // Get AppStore URL from PROiApp_Settings Table
    onClickOptions = () => {
        let appStoreURL = commonFun.getConfigurationDetails('APPUPDATEIOSURL', 'URL');
        if (appStoreURL) {
            Linking.canOpenURL(appStoreURL).then((isSupported) => {
                if (isSupported) {
                    Linking.openURL(appStoreURL);
                    this.setState({
                        isModalVisible: false
                    })
                } else {
                    console.log('Available URL is not supported');
                }
            })
        }
    }

    render() {
        return (
            <View {...this.props}>

                <TouchableOpacity
                    onPress={(value) => {
                        this.setState({
                            isModalVisible: this.props.SyncStatus ? false : true,
                        });
                    }}
                >
                    <Image style={this.props.style} source={configImage.notificationIcon} />
                    {this.state.availableLatestVersion ?
                        <Badge style={{ position: "absolute", top: -8, left: 15, width: 20, height: 20, paddingLeft: 3, paddingRight: 3, paddingTop: 4 }}>
                            <Text style={{ fontFamily: 'Roboto-Regular', fontSize: 14, lineHeight: 14 }}>1</Text>
                        </Badge>
                        : null}
                </TouchableOpacity>

                <Modal
                    onBackdropPress={() => this.setState({ isModalVisible: false })}
                    isVisible={this.state.isModalVisible}
                    animationIn='slideInRight'
                    animationOut='slideOutRight'
                    // animationInTiming={100}
                    // animationOutTiming={10}
                    backdropOpacity={0}
                    onBackButtonPress={() => this.setState({ isModalVisible: false })}
                    style={styles.modal} >
                    <View style={styles.modalContent}>
                        {this.state.availableLatestVersion ?
                            <ListItem style={styles.alignItems}
                                onPress={() => this.onClickOptions()}
                            >
                                <Text style={styles.moreText}>New Version Available</Text>
                            </ListItem>
                            :
                            <ListItem style={styles.alignItems} >
                                <Text style={styles.moreTextOnClick}>No new notifications</Text>
                            </ListItem>
                        }
                    </View>
                </Modal>

            </View>
        )
    }
}
export default Notification;