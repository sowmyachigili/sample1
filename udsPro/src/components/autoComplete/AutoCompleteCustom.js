/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import React, { Component } from 'react';
import { View, Image, Text, Keyboard, FlatList, TouchableOpacity } from 'react-native';
import { Item, Input } from 'native-base';
import KeyboardManager from 'react-native-keyboard-manager'

import configImage from '../../config/configImages';
import config from '../../config/config';

// Styles
import styles from '../input/inputStyles';


class AutoComplete extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [] = this.props.items,
            value: this.props.value,
            searchResults: [],
        };
    }

    sendDataToParent = (value) => {
        if (this.props.handlerFromParant && !this.props.getBlurValue) {
            // send value to parent
            this.props.handlerFromParant(value, this.props.name, this.props.tabName);
        }
    }

    handleChange = (value) => {
        this.sendDataToParent(value);

        var searchResults = [];
        if (value) {
            let setSort = '';
            if (!setSort && typeof setSort !== 'function') {
                setSort = (item, searchedText) => {
                    return item.name.toLowerCase().indexOf(searchedText.toLowerCase()) > -1
                };
            }
            searchResults = this.props.items.filter((item) => {
                return setSort(item, value);
            });
        } else {
            searchResults = [];
        }
        this.setState({
            searchResults: searchResults,
            value: value
        });
    }

    selectListItem = (value) => {
        this.sendDataToParent(value);
        this.setState({
            value: value,
            searchResults: [],
        });
    }

    // For validating input feilds
    validateInputField = (text) => {
        if (this.props.handlerFromParant && this.props.getBlurValue) {
            // send value to parent
            this.props.handlerFromParant(text, this.props.name, this.props.tabName);
        }

        if (this.props.validateInputFeild) {
            this.props.validateInputFeild(this.props.name, this.props.getBlurValue ? text : null);
        }
    }

    render() {

        return (
            <View {...this.props} style={{ width: '100%' }}>
                <Item regular
                    style={[styles.inputBox, this.props && this.props.disabled ? styles.disabled : null,
                    this.props.backgroundColor ? styles.yellowBackground : null]}
                    disabled={this.props && this.props.disabled ? true : false}>

                    {/* in input label */}
                    {this.props.label ? <Text active style={styles.label}>{this.props.label}</Text> : null}

                    <Input
                        ref={this.props.name}
                        disabled={this.props && this.props.disabled ? true : false}
                        placeholder={this.props.placeholder ? this.props.placeholder : null}
                        style={[styles.input, this.props.style]}
                        value={this.state.value}
                        autoCorrect={false}
                        maxLength={this.props.maxLength ? this.props.maxLength : null}
                        onChangeText={(text) => this.handleChange(text)}
                        // onBlur={this.validateInputField}
                        onBlur={(e) => this.validateInputField(e.nativeEvent.text)}

                        keyboardType={config.numbericInputFields.includes(this.props.name) ? 'number-pad' : 'ascii-capable'} // default

                        secureTextEntry={this.props.secureTextEntry ? this.props.secureTextEntry : false}
                        onLayout={() => {
                            // When the input size changes, it updates the keyboard position.
                            KeyboardManager.reloadLayoutIfNeeded();
                        }}
                        returnKeyType={this.props.returnKeyType ? "default" : "next"} // {this.props.returnKeyType ? this.props.returnKeyType : "next"}
                        onSubmitEditing={this.props.moveFocusToNextField ? this.props.moveFocusToNextField : Keyboard.dismiss}
                        blurOnSubmit={false}
                        onFocus={this.props.onFocus ? this.props.onFocus : null}
                        autoCapitalize={'none'}
                    />

                    {/* input icon */}
                    {/* {this.props.image == 'lock' ? <Image source={configImage.readOnlyLock} style={styles.inputlockimage} /> : null} */}
                    {this.props.image == 'search' ? <Image source={configImage.searchIcon} style={styles.inputsearchimage} /> : null}

                </Item >

                <FlatList
                    style={{ 
                        position: 'relative',
                        maxHeight: 170,
                        width: '100%',
                        borderRadius: 5,
                        backgroundColor: '#00000005',
                    }}
                    data={this.state.searchResults}
                    renderItem={({ item, index }) =>
                        <TouchableOpacity
                        style={{
                            paddingVertical: 8,
                            paddingHorizontal: 10,
                            borderBottomColor: '#00000040', // 25% opacity applied in 000000 color
                            borderBottomWidth: 0.5,
                            zIndex:99999999,
                        }}
                        onPress={() => { this.selectListItem(item.label) }}
                        >
                            <Text>
                                {item.label}
                            </Text>
                        </TouchableOpacity>
                    }
                    keyExtractor={(item, index) => index.toString()}
                    extraData={this.state}
                    ItemSeparatorComponent={this.renderSeparator}
                />
            </View>
        )
    }
}

export default AutoComplete;

