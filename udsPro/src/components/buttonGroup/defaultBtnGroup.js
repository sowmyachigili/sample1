/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import React, { Component } from 'react';
import {
    Text, View, TouchableOpacity
} from 'react-native';

// Styles
import styles from './defaultBtnGroupStyles';

class DefaultBtnGroup extends Component {

    constructor(props) {
        super(props);
    }
    render() {
        const {
            tabName,
            name,
            buttons,
            onPress,
            selectedIndex,
            innerBorderStyle,
            lastBorderStyle,
            underlayColor,
            disableSelected,
            ...attributes
        } = this.props;

        return (
            <View
                style={styles.container}
            >
                {buttons.map((button, i) => {
                    const containerRadius = name == 'Gender' ? null : 4;
                    return (
                        <TouchableOpacity
                            underlayColor={underlayColor || 'green'}
                            disabled={disableSelected ? true : false}
                            onPress={onPress ? () => onPress(button, name, tabName, 'btnGroup') : () => { }}
                            key={i}
                            style={[
                                styles.button,
                                i < buttons.length - 1 && {
                                    borderRightWidth:
                                        i === 0
                                            ? 0
                                            : (innerBorderStyle && innerBorderStyle.width) || 1,
                                    borderRightColor:
                                        (innerBorderStyle && innerBorderStyle.color) || '#00000040',
                                },
                                i === 1 && {
                                    borderLeftWidth:
                                        (innerBorderStyle && innerBorderStyle.width) || 1,
                                    borderLeftColor:
                                        (innerBorderStyle && innerBorderStyle.color) || '#00000040',
                                },
                                i === buttons.length - 1 && {
                                    ...lastBorderStyle,
                                    borderTopRightRadius: containerRadius,
                                    borderBottomRightRadius: containerRadius,
                                },
                                i === 0 && {
                                    borderTopLeftRadius: containerRadius,
                                    borderBottomLeftRadius: containerRadius,
                                },
                                selectedIndex === i && {
                                    backgroundColor: 'white',
                                },
                                disableSelected ? styles.disableSelected : null
                            ]}
                        >
                            <View
                                style={[
                                    styles.textContainer,
                                    selectedIndex === button &&
                                    { backgroundColor: '#6781B7' }, // selected btn color
                                ]}
                            >
                                {button.element ? (
                                    <button.element />
                                ) : (
                                        <Text
                                            style={[
                                                styles.buttonText,
                                                selectedIndex === button && { color: '#fff' }, // selected text color
                                            ]}
                                        >
                                            {button}
                                        </Text>
                                    )}
                            </View>
                        </TouchableOpacity>
                    );
                })}
            </View>
        )
    }
}
export default DefaultBtnGroup;