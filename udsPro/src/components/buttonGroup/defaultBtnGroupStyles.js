/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import { StyleSheet, Platform } from 'react-native';

export default StyleSheet.create({
    button: {
        flex: 1,
    },
    textContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    container: {
        borderColor: '#00000040',
        borderWidth: 1,
        flexDirection: 'row',
        borderRadius: 4,
        backgroundColor: '#fff',
        height: 37,
    },
    disableSelected: {
        backgroundColor: '#EFEFEF',
        borderColor: "#D6D6D6",
    },
    buttonText: {
        fontSize: 13,
        fontFamily: 'Roboto-Regular',
        color: '#80848F',
        ...Platform.select({
            ios: {
                fontWeight: '500',
            },
        }),
    },

});