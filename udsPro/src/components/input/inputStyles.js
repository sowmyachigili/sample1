/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    inputBox: {
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#00000040', // 25% opacity applied in 000000 color
    },
    input: {
        height: 35,
        paddingTop: 5,
        paddingBottom: 5,
    },
    invalid: {
        backgroundColor: '#f9c0c0',
        borderColor: 'red'
    },
    disabled: {
        backgroundColor: "#EFEFEF",
        borderColor: "#D6D6D6",
    },
    yellowBackground: {
        backgroundColor: "#fff26d"
    },
    inputlockimage: {
        width: 13,
        height: 18,
        marginRight: 9.9,
    },
    inputsearchimage: {
        width: 16,
        height: 16,
        marginRight: 9.9,
    },
    dropdownImage: {
        width: 10,
        height: 13,
        marginRight: 9.9,
    },
    calendar: {
        width: 25,
        height: 25,
        marginRight: 9.9,
    },
    label: {
        backgroundColor: '#EFEFEF',
        height: 35,
        paddingLeft: 10,
        paddingRight: 12,
        paddingTop: 7,
        fontFamily: 'Roboto-Regular',
        color: '#000',
        fontSize: 14,
    },

});