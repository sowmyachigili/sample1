/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import React, { Component } from 'react';
import { View, Image, Text, Keyboard } from 'react-native';
import { Item, Input } from 'native-base';

import configImage from '../../config/configImages';

// Styles
import styles from './inputStyles';

class DefaultNumberInput extends Component {
    constructor(props) {
        super(props);
    }

    // handle change
    handleChange = (inputField) => {
        if (this.props && this.props.handlerFromParant) {
            this.props.handlerFromParant(inputField, this.props.name, this.props.tabName);
        }
    }

    // For validating input feilds
    validateInputFeild = () => {
        if (this.props.validateInputFeild) {
            this.props.validateInputFeild(this.props.name);
        }
    }

    render() {
        return (
            <View {...this.props} style={{ width: '100%' }}>
                <Item regular style={[styles.inputBox, this.props && this.props.disabled ? styles.disabled : null]} disabled={this.props && this.props.disabled ? true : false}>

                    {this.props.label ? <Text active style={styles.label}>{this.props.label}</Text> : null}

                    <Input
                        autoCorrect={false}
                        ref={this.props.name}
                        maxLength={this.props.maxLength ? this.props.maxLength : null}
                        disabled={this.props && this.props.disabled ? true : false}
                        placeholder={this.props.placeholder ? this.props.placeholder : null}
                        placeholderTextColor='#cacaca'
                        style={[styles.input, this.props.style]}
                        keyboardType={this.props.name == 'FirstName' || this.props.name == 'LastName' || this.props.name == 'PatientID' ? 'default' : 'number-pad'}
                        value={this.props.value}
                        onChange={(e) => this.handleChange(e.nativeEvent.text)}
                        onBlur={this.validateInputFeild}
                        returnKeyType={this.props.returnKeyType ? "default" : "next"} // {this.props.returnKeyType ? this.props.returnKeyType : "next"}
                        onSubmitEditing={this.props.moveFocusToNextField ? this.props.moveFocusToNextField : Keyboard.dismiss}
                        blurOnSubmit={false}
                    />
                    {this.props.image == 'lock' ? <Image source={configImage.readOnlyLock} style={styles.inputlockimage} /> : null}
                    {this.props.image == 'search' ? <Image source={configImage.searchIcon} style={styles.inputsearchimage} /> : null}
                </Item >
            </View>
        )
    }
}

export default DefaultNumberInput;