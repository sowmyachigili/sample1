/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import React, { Component } from 'react';
import { StyleSheet, View, Keyboard } from 'react-native';
import { Textarea } from 'native-base';

class DefaultTextArea extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            textField: ''
        };
    }

    handleChange(value) {
        this.setState({
            textField: value//event.target.value
        });

        // send value to parent
        this.props.handlerFromParant(value, this.props.name, this.props.tabName);
    }

    render() {

        return (
            <View {...this.props} style={{ width: '100%' }}>
                <Textarea style={[
                    styles.inputBox, this.props.style,
                    !this.props.valid && this.props.touched ? styles.invalid : null,
                    this.props.disabled ? styles.disabled : null]}
                    rowSpan={this.props.rowSpan}
                    value={this.props.value}
                    // onBlur={(e) => this.handleChange(e.nativeEvent.text)}
                    onChangeText={(text) => this.handleChange(text)}
                    ref={this.props.name}
                    maxLength={this.props.maxLength ? this.props.maxLength : null}
                    disabled={this.props && this.props.disabled ? true : false}
                    // returnKeyType={this.props.returnKeyType ? "default" : "next"} // {this.props.returnKeyType ? this.props.returnKeyType : "next"}
                    // onSubmitEditing={this.props.moveFocusToNextField ? this.props.moveFocusToNextField : Keyboard.dismiss}
                    blurOnSubmit={false}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    inputBox: {
        borderWidth: 1,
        borderColor: '#00000040',
        borderRadius: 5,
        width: '100%'
    },
    invalid: {
        backgroundColor: '#f9c0c0',
        borderColor: 'red'
    },
    disabled: {
        backgroundColor: "lightgray",
        borderColor: "#e5e5e5"
    }
})

export default DefaultTextArea;