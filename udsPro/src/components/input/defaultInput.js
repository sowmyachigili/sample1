/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import React, { Component } from 'react';
import { View, Image, Text, Keyboard } from 'react-native';
import { Item, Input } from 'native-base';
import KeyboardManager from 'react-native-keyboard-manager'

import configImage from '../../config/configImages';
import config from '../../config/config';

// Styles
import styles from './inputStyles';

class DefaultInput extends Component {
    constructor(props) {
        super(props);
    }

    handleChange = (value) => {
        if (this.props.handlerFromParant && !this.props.getBlurValue) {
            // send value to parent
            this.props.handlerFromParant(value, this.props.name, this.props.tabName);
        }
    }

    // For validating input feilds
    validateInputField = (text) => {
        if (this.props.handlerFromParant && this.props.getBlurValue) {
            // send value to parent
            this.props.handlerFromParant(text, this.props.name, this.props.tabName);
        }

        if (this.props.validateInputFeild) {
            this.props.validateInputFeild(this.props.name, this.props.getBlurValue ? text : null);
        }
    }

    render() {

        return (
            <View {...this.props} style={{ width: '100%' }}>
                <Item regular
                    style={[styles.inputBox, this.props && this.props.disabled ? styles.disabled : null,
                    this.props.backgroundColor ? styles.yellowBackground : null]}
                    disabled={this.props && this.props.disabled ? true : false}>

                    {/* in input label */}
                    {this.props.label ? <Text active style={styles.label}>{this.props.label}</Text> : null}

                    <Input
                        ref={this.props.name}
                        disabled={this.props && this.props.disabled ? true : false}
                        placeholder={this.props.placeholder ? this.props.placeholder : null}
                        style={[styles.input, this.props.style]}
                        value={this.props.value}
                        autoCorrect={false}
                        maxLength={this.props.maxLength ? this.props.maxLength : null}
                        onChangeText={(text) => this.handleChange(text)}
                        // onBlur={this.validateInputField}
                        onBlur={(e) => this.validateInputField(e.nativeEvent.text)}

                        keyboardType={config.numbericInputFields.includes(this.props.name) ? 'number-pad' : 'ascii-capable'} // default

                        secureTextEntry={this.props.secureTextEntry ? this.props.secureTextEntry : false}
                        onLayout={() => {
                            // When the input size changes, it updates the keyboard position.
                            KeyboardManager.reloadLayoutIfNeeded();
                        }}
                        returnKeyType={this.props.returnKeyType ? "default" : "next"} // {this.props.returnKeyType ? this.props.returnKeyType : "next"}
                        onSubmitEditing={this.props.moveFocusToNextField ? this.props.moveFocusToNextField : Keyboard.dismiss}
                        blurOnSubmit={false}
                        onFocus={this.props.onFocus ? this.props.onFocus : null}
                        autoCapitalize={'none'}
                    />

                    {/* input icon */}
                    {/* {this.props.image == 'lock' ? <Image source={configImage.readOnlyLock} style={styles.inputlockimage} /> : null} */}
                    {this.props.image == 'search' ? <Image source={configImage.searchIcon} style={styles.inputsearchimage} /> : null}

                </Item >
                {/* <Text>Visible in child: {this.props.value}</Text> */}
            </View>
        )
    }
}

export default DefaultInput;

