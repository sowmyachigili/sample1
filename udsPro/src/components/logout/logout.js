/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import React, { Component } from 'react';
import { View, Image, TouchableOpacity, AppState, Alert } from 'react-native';
import { Text, Button } from 'native-base';
import Modal from "react-native-modal";
import AsyncStorage from '@react-native-community/async-storage';

import config from '../../config/config';
import configMessage from '../../config/configMessages';
import configImage from '../../config/configImages';
import navigation from "../../utility/Navigation";
import Loader from '../../components/common/loaderNew';

// Components
import dateFormats from '../../utility/formatDate';
import common from "../../utility/commonFun";

//Styles
import modalStyles from './../styles/modalStyles';

import AuditLogFile from '../../utility/AuditLogFile';

class Logout extends Component {

    constructor(props) {
        super(props);
        this.interval;
        this.ActivateSessionInterval;
        this.isModifiedRecord = null;
        this.state = {
            isLoading: false,
            appState: AppState.currentState,
            isLogoutModalVisible: false,
            isSessionExpiredModal: false,
            isSessionExpired: false,
            sessionTimeOut: config.activeSessionTimeSec

        }
    }


    // App has come to the foreground!
    _handleAppStateChange = (nextAppState) => {
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            this.checkLogouttime();
        }
        this.setState({ appState: nextAppState });
    }

    // Check User Active time with current time
    checkLogouttime = async () => {
        let sessionExpTime = await AsyncStorage.getItem('sessionExpTime');
        // AsyncStorage.getItem('sessionExpTime').then(sessionExpTime => {
        sessionExpTime = JSON.parse(sessionExpTime);
        let sessionLogoutTime = dateFormats.addSecondsToDate(sessionExpTime, config.activeSessionTimeSec);;
        let currentTime = dateFormats.currentDateTime();
        if (dateFormats.datesComparisionWithSameOrAfter(sessionLogoutTime, currentTime)) {
            this.setState({
                isSessionExpiredModal: true,
                isSessionExpired: true,
            });
            clearInterval(this.interval);
            clearInterval(this.ActivateSessionInterval);
        }
        // });
    }

    componentDidMount() {
        if (this.props.onRef) {
            this.props.onRef(this);
        }

        // check logout condition - App has come to the foreground!
        AppState.addEventListener('change', this._handleAppStateChange);

        // this.sessionIntervel();

    }

    sessionInterval = () => {
        this.interval = setInterval(async () => {
            if (!this.state.isSessionExpiredModal && !this.props.SyncStatus) {
                // AsyncStorage.getItem('sessionExpTime').then(sessionExpTime => {
                let sessionExpTime = await AsyncStorage.getItem('sessionExpTime');
                sessionExpTime = JSON.parse(sessionExpTime);
                let sessionLogoutTime = dateFormats.addSecondsToDate(sessionExpTime, config.activeSessionTimeSec);;
                let currentTime = dateFormats.currentDateTime();
                // check current time with timer time
                if (!this.state.isSessionExpiredModal && dateFormats.datesComparisionWithSameOrAfter(sessionLogoutTime, currentTime)) {
                    this.setState({
                        isSessionExpiredModal: true,
                        isSessionExpired: true,
                    });
                    clearInterval(this.interval);
                } else if (!this.state.isSessionExpiredModal && dateFormats.datesComparisionWithSameOrAfter(sessionExpTime, currentTime)) {

                    // display session time modal
                    this.setState({
                        isSessionExpiredModal: true,
                        isSessionExpired: false,
                    });

                    // Session Expire Timer start with 300 sec 
                    this.ActivateSessionInterval = setInterval(() => {
                        if (dateFormats.datesComparisionWithSameOrAfter(sessionLogoutTime, currentTime)) {
                            this.setState({
                                // isSessionExpiredModal: true,
                                isSessionExpired: true,
                            });
                            clearInterval(this.interval);
                            clearInterval(this.ActivateSessionInterval);
                        } else if (this.state.sessionTimeOut > 0 && this.state.isSessionExpiredModal) {
                            // Session Expire Timer start with 300 sec display
                            this.setState({
                                sessionTimeOut: this.state.sessionTimeOut - 1,
                            });
                        } else { // Session expire modal
                            this.setState({
                                isSessionExpiredModal: true,
                                isSessionExpired: true,
                            });
                            clearInterval(this.interval);
                            clearInterval(this.ActivateSessionInterval)
                        }
                    }, 1000); // Interval for 1 second
                }
                // });
            } else {
                clearInterval(this.interval);
            }
        }, 10000); // Interval for 1 minute - 60000
    }

    // Save session expiration time in asyncstorage
    activateSession = async () => {
        this.setState({
            isLoading: true,
        }, async () => {
            let isActiveSessionTime = await common.activateSession(); // Activate user session Time
            if (isActiveSessionTime) {
                setTimeout(() => {
                    clearInterval(this.ActivateSessionInterval)
                    this.setState({
                        isLoading: false,
                        isSessionExpiredModal: false,
                        sessionTimeOut: config.activeSessionTimeSec,
                    }, () => {
                        this.sessionInterval();
                    });
                }, 500);
            }
        })
    }

    // For removing interval listener
    componentWillUnmount() {
        if (this.props.onRef) {
            this.props.onRef(null);
        }
        this.clearIntervel();
    }

    clearIntervel = () => {
        if (this.interval) clearInterval(this.interval);
        if (this.ActivateSessionInterval) clearInterval(this.ActivateSessionInterval);

    }

    //on click button open logout modal box
    onClickLogout = (type) => {
        this.isModifiedRecord = type;
        // if (this.props.autoSyncProgress == 100 || !this.props.SyncStatus) {
            this.setState({
                isLogoutModalVisible: true,
            });
        // }
    }

    // Check Preadmission or case list editable changes modal box display
    onClickLogoutCheckvalidation = () => {
        if (this.props.page && (this.props.page == 'AddPreadmission' || this.props.page == 'AddCase'
            || this.props.page == 'AdvancedComorbidScreen' || this.props.page == 'ICDCodesScreen')) {
            this.props.clickLogout();
        } else {
            this.onClickLogout();
        }
    }


    // Logout button handler
    logoutButtonHandler = async (modalname) => {
        this.clearIntervel();
        // Clears async storage if anything in asyncstore
        await this.setState({
            [modalname]: false,
        });

        setTimeout(async () => {
            await this.updateAuditLogInfo(this.props.userInfo);
            AsyncStorage.getAllKeys().then(AsyncStorage.multiRemove);
            navigation.popToTopScreenNavigator(this.props.componentId);
        }, 100)

    }

    // Update audit log info when click on logout
    updateAuditLogInfo = async (loginUserDetails) => {
        if (loginUserDetails) {
            await AuditLogFile.updateAuditLogInfoOnLogout(loginUserDetails)
        }
    }

    render() {
        return (
            <View {...this.props}>

                <TouchableOpacity onPress={this.onClickLogoutCheckvalidation} >
                    <Image style={this.props.style} source={configImage.LogOutIcon} />
                </TouchableOpacity>

                {/* Logout modal start */}
                <Modal
                    isVisible={this.state.isLogoutModalVisible}
                    backdropOpacity={0.5}
                    onBackButtonPress={() => this.setState({ isLogoutModalVisible: false })}
                    style={modalStyles.modalSmall} >
                    <View style={modalStyles.modalContent}>
                        <Loader loading={this.state.isLoading} />
                        <View style={modalStyles.modalSmallBody}>
                            <View style={{ alignItems: 'center' }}>
                                <Image style={[modalStyles.logoutIcon]} source={configImage.powerButtonOffIcon} />
                                <Text style={modalStyles.informationText}>{this.isModifiedRecord == 'modified' ?
                                    configMessage.logoutConfirmForModifiedRecord : configMessage.logout}</Text>
                            </View>
                        </View>
                        <View style={[modalStyles.modalFooter]}>
                            <Button style={[modalStyles.btnDefault, modalStyles.buttonDark, { marginRight: 20 }]} onPress={() => this.logoutButtonHandler('isLogoutModalVisible')}>
                                <Text style={[modalStyles.btnDefaultText, modalStyles.buttonDarkText]}>Yes</Text>
                            </Button>
                            <Button style={[modalStyles.btnDefault, modalStyles.buttonLight]} onPress={() => this.setState({ isLogoutModalVisible: false })}>
                                <Text style={[modalStyles.btnDefaultText, modalStyles.buttonLightText]}>No</Text>
                            </Button>
                        </View>
                    </View>
                </Modal>
                {/* Logout modal start */}

                {/* Session modal start */}
                <Modal
                    isVisible={this.state.isSessionExpiredModal}
                    backdropOpacity={0.5}
                    style={modalStyles.modalSmall} >
                    <View style={modalStyles.modalContent}>
                        <View style={modalStyles.modalSmallBody}>
                            <View style={{ alignItems: 'center' }}>
                                <Image style={[modalStyles.warning]} source={configImage.errorIcon} />

                                {this.state.isSessionExpired ?
                                    <Text style={modalStyles.informationText}>{configMessage.sessionExpMsg}</Text>
                                    :
                                    <View>
                                        <Text style={modalStyles.informationText}>Your session will be timed out in {this.state.sessionTimeOut} seconds due to inactivity. </Text>
                                        <Text style={modalStyles.informationText}>{configMessage.reactivateYourSession}</Text>
                                    </View>
                                }
                            </View>
                        </View>
                        <View style={[modalStyles.modalFooter]}>
                            {this.state.isSessionExpired ?
                                <Button style={[modalStyles.btnDefault, modalStyles.buttonDark, { marginRight: 20 }]} onPress={() => this.logoutButtonHandler('isSessionExpiredModal')}>
                                    <Text style={[modalStyles.btnDefaultText, modalStyles.buttonDarkText]}>OK</Text>
                                </Button>
                                :
                                <Button style={[modalStyles.btnDefault, modalStyles.buttonDark, { marginRight: 20 }]} onPress={this.activateSession}>
                                    <Text style={[modalStyles.btnDefaultText, modalStyles.buttonDarkText]}>Click here to activate your session</Text>
                                </Button>
                            }
                        </View>
                    </View>
                </Modal>
                {/* Session modal start */}


            </View>
        )
    }
}

export default Logout;