/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  // header1 css
  header: {
    backgroundColor: '#1e7a92',
    opacity: 100,
    height: 50,
    paddingLeft: 12.5,
    paddingRight: 12.5,
  },
  logo: {
    width: 175,
    height: 20,
    // marginTop: 14.84,
    marginBottom: 14.84,
  },
  headerOptions: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginBottom: 14.84,
  },
  iconSpace: {
    marginRight: 21,
  },
  logoutIcon: {
    width: 23,
    // height: 24,
  },
  notification: {
    width: 22,
    height: 22,
  },
  logoutBtn: {
    width: 23,
    height: 24,
  },
  userPic: {
    width: 31.99,
    height: 31.99,
  },

  // header2 css
  subHeader: {
    backgroundColor: '#fff',
    paddingTop: 12,
    paddingBottom: 12,
    paddingLeft: 22.5,
    paddingRight: 22.5,
    height: 53,
  },
  buttonTopBottomSpace: {
    marginTop: 14,
    marginBottom: 14,
  },
  buttonRightSpace: {
    marginRight: 15,
  },
  backBtn: {
    width: 53,
    height: 42,
  },
  saveBtn: {
    width: 80,
    height: 32,
  },
  addBtn: {
    width: 75,
    height: 32,
  },
  signinBtn: {
    width: 75,
    height: 32,
  },
  admitBtn: {
    width: 85,
    height: 32,
  },
  dontAdmitBtn: {
    width: 136,
    height: 32,
  },
  moreBtn: {
    width: 32,
    height: 32,
  },

  listHeading: {
    fontFamily: 'Muli-SemiBold',
    fontSize: 14,
    color: '#007AFF',
  },
  headSpaceRemove: {
    paddingTop: 0,
    height: 50
  },
  searchHeaderContainer: {
    height: 60,
    backgroundColor: '#BBC2CF',
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center'
  },
  privilegesLabel: {
    fontWeight: 'bold',
    fontSize: 15,
    color: '#2E8B57',
  },
  privilegesViewLabel: {
    fontWeight: 'bold',
    fontSize: 15,
    color: 'red',
  },

});