/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import { StyleSheet } from 'react-native';

// date style start
const dateStyles = StyleSheet.create({
    dateText: {

    },
    dateIcon: {
        position: 'absolute',
        right: 0,
        top: 1,
        paddingRight: 9.9,
    },

    onlyDateIcon: {
        width: 17,
        height: 17,
        marginVertical: 8,
        marginRight: 8
    },
    closeIconView: {
        borderLeftColor: '#00000040',
        borderLeftWidth: 1,
        justifyContent: 'center',
        height: '100%',
    },

    dateInput: {
        paddingLeft: 10,
        height: 35,
        top: -2.5,
    },

    dateInputWidth: {
        width: '100%'
    },
    dateClearWidth: {
        width: '12%'
    },
    dateMaxClearWidth: {
        width: '17%'
    },
    smallDateInputWidth: {
        width: '100%',
    },
    smallDateClearWidth: {
        width: '20%'
    },
    clearBtn: {
        width: '100%',
        height: 35,
        alignItems: 'center',
        justifyContent: 'center',
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderRightWidth: 1,
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
        borderColor: '#00000040',
    },
    clearIcon: {
        width: 13,
        height: 13
    },
    btnTextConfirm: {
        color: "black",
        fontSize: 18,
        fontWeight: "500",
        padding: 10
    },
    btnTextNext: {
        color: "gray",
        fontSize: 18,
        padding: 10
    }
});



export default dateStyles;