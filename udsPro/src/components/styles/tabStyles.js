/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import { StyleSheet } from 'react-native';
import config from '../../config/config';

export default StyleSheet.create({
    // new tabs style start
    newTabs: {
        flexDirection: 'row',
        marginTop: 5,
        marginLeft: 9,
        marginRight: 7.5,
        borderColor: config.tabBarBorderColor,
        borderWidth: 1,
        backgroundColor: config.tabbarBackgroungdColor,
        minHeight: 60,
        position: 'absolute',
    },
    newTab: {
        alignItems: 'center',
        justifyContent: 'center',
        fontFamily: 'Roboto-Regular',
        paddingLeft: 20,
        paddingRight: 20,
    },
    tabActive: {
        borderBottomWidth: 4,
        borderBottomColor: '#027BFF',
    },
    tabTextInActive: {
        color: '#36363e',
        fontSize: 16,
    },
    tabTextActive: {
        marginTop: 5,
        fontFamily: 'Roboto-Bold',
        fontSize: 16,
        color: config.blueTextColor,
    },
    tabTextDanger: {
        marginTop: 5,
        fontFamily: 'Roboto-Bold',
        fontSize: 16,
        color: config.dangerColor,
    },
    leftRightAction: {
        marginLeft: 11,
        marginRight: 11,
        marginTop: 13,
        marginBottom: 13,
        width: 34,
        height: 34,
    },

    // tabs style start
    tabs: {
        marginTop: 5,
        marginLeft: 9,
        marginRight: 9,
        borderColor: config.tabBarBorderColor,
        borderWidth: 1,
    },
    tab: {
        backgroundColor: config.tabbarBackgroungdColor,
    },
    tabTextStyle: {
        color: config.tabBarInactiveTextColor,
        fontSize: 16,
        lineHeight: 18,
        fontFamily: 'Roboto-Regular',
    },
    activeTabTextStyle: {
        color: config.blueTextColor,
        fontSize: 16,
        lineHeight: 18,
        fontFamily: 'Roboto-Bold',
    },
    // tabs style end

    // footer css
    footer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        paddingTop: 20,
        paddingBottom: 35,
        paddingRight: 13,
        marginBottom: 120,

    },
    footerText: {
        fontSize: 12,
        color: '#000',
        opacity: 0.5,
        fontFamily: 'Muli-SemiBold'
    },
});