/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import { StyleSheet } from 'react-native';
import config from '../../config/config';

export default StyleSheet.create({
    width100: {
        width: '100%',
    },
    slash: {
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
        paddingTop: 8,
        paddingRight: 10,
        alignItems: 'center'
    },
    button: {
        height: 35,
        backgroundColor: config.blueButtonColor,
        borderRadius: 5,
    },
    buttonSuccess: {
        height: 35,
        backgroundColor: '#2FB285',
        borderRadius: 5,
    },
    btnDefault: {
        borderRadius: 4,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 26,
        paddingRight: 26,
    },
    buttonLight: {
        backgroundColor: '#C1D5FF',
    },
    buttonDark: {
        backgroundColor: '#007AFF',
    },
    btnDefaultText: {
        fontSize: 16,
        lineHeight: 26,
        fontFamily: 'Roboto-Medium',
    },
    buttonLightText: {
        color: '#2F3946',
    },
    buttonDarkText: {
        color: '#ffffff',
    },
    btnText: {
        color: '#ffffff',
        fontSize: 16,
        // lineHeight: 26,
        fontFamily: 'Roboto-Medium',
        paddingLeft: 12,
        paddingRight: 12,
    },
    marginBottom: {
        marginBottom: 30,
    },
    marginBottom13: {
        marginBottom: 13,
    },
    marginBottom20: {
        marginBottom: 20,
    },
    marginTop15: {
        marginTop: 15,
    },
    marginLeft20: {
        marginLeft: 20
    },
    marginLeft15: {
        marginLeft: 65
    },
    margin20: {
        marginBottom: 20,
        marginTop: 20
    },
    listHead: {
        backgroundColor: "#cde1f9"
    },
    listHeading: {
        fontSize: 15,
        fontFamily: 'Roboto-Bold',
    },
    listText: {
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
    },
    tabContent: {
        marginTop: 35.5,
        marginBottom: 25.5,
        marginLeft: 20,
        marginRight: 25.5,
    },
    heading: {
        fontSize: 20,
        lineHeight: 28,
        color: config.textFontColor,
        fontFamily: 'Roboto-Regular',
        paddingBottom: 30,
    },
    subHeading: {
        fontSize: 16,
        lineHeight: 26,
        color: '#000',
        fontFamily: 'Roboto-Bold',
    },
    totalHeading: {
        fontSize: 16,
        color: '#000',
        fontFamily: 'Roboto-Regular',
        lineHeight: 26,

    },
    fimScreen: {
        fontSize: 16,
        lineHeight: 20,
        color: '#000',
        fontWeight: 'bold',
        fontFamily: 'Muli-Regular',
    },
    marginRight10: {
        marginRight: 10,
    },
    marginRight20: {
        marginRight: 20,
    },
    sample: {
        fontSize: 12,
        color: '#717171',
    },
    row: {
        // alignItems: 'center',
        width: '100%',
        flexDirection: 'row',
    },
    threeColumnsInRow: {
        // alignItems: 'center',
        width: '33%',
        flexDirection: 'column',
    },
    twoColumnsInRow: {
        // alignItems: 'center',
        width: '50%',
        flexDirection: 'column',
    },
    advancedRow: {
        // alignItems: 'center',
        width: '100%',
        flexDirection: 'row',
        marginBottom: 26
    },
    advancedColumn: {
        // alignItems: 'center',
        width: '100%',
        flexDirection: 'column',
        marginBottom: 26
    },
    formHeading: {
        fontSize: 16,
        lineHeight: 19,
        color: config.labelFontColor,
        fontFamily: 'Roboto-Regular',
        paddingBottom: 10
    },
    formLabel: {
        fontSize: 14,
        lineHeight: 17,
        color: config.labelFontColor,
        fontFamily: 'Roboto-Regular',
        marginBottom: 7,
    },
    cmsFieldLabelColor: {
        color: '#007AFF'
    },
    ItalicText: {
        fontSize: 14,
        lineHeight: 17,
        color: config.labelFontColor,
        fontFamily: 'Verdana-Italic',
        marginBottom: 7,
    },
    genderFormLabel: {
        fontSize: 14,
        lineHeight: 17,
        color: config.labelFontColor,
        fontFamily: 'Roboto-Regular',
        marginBottom: 3,
    },
    formLabelBold: {
        fontSize: 14,
        lineHeight: 17,
        color: config.labelFontColor,
        fontFamily: 'Roboto-Bold',
        marginBottom: 7,
    },
    formLabel1: {
        fontSize: 14,
        lineHeight: 17,
        color: config.labelFontColor,
        fontFamily: 'Roboto-Regular',
        marginBottom: 7,
        minHeight: 34,

    },
    formLabel2: {
        fontSize: 14,
        lineHeight: 17,
        color: config.labelFontColor,
        fontFamily: 'Roboto-Regular',
        marginBottom: 7,
        minHeight: 51,

    },
    halfCol: {
        // flexDirection: 'column',
        width: '50%',
        marginBottom: 26,
        paddingRight: 40,
    },
    halfColNew: {
        width: '30%',
        marginBottom: 26,
        paddingRight: 40,
    },
    halfFull: {
        // flexDirection: 'column',
        width: '70%',
        marginBottom: 26,
    },
    fullCol: {
        // flexDirection: 'column',
        width: '100%',
        marginBottom: 26,
    },
    halfCol1: {
        width: '20%',
        marginBottom: 26,
        // paddingRight: 40,
    },
    width25: {
        width: '25%',
        marginBottom: 15,
        paddingRight: 10,
    },
    width15: {
        width: '19%',
        marginBottom: 15,
        paddingRight: 10,
    },
    width35: {
        width: '31%',
        marginBottom: 15,
    },
    half: {
        width: '50%',
    },
    halfRight: {
        width: '50%',
        justifyContent: 'flex-end',
        flexDirection: 'row'
    },
    halfRightStart: {
        width: '50%',
        justifyContent: 'flex-start',
        flexDirection: 'row'
    },

    colRight: {
        width: '100%',
        justifyContent: 'flex-end',
        flexDirection: 'row',
    },
    rightAlign: {
        width: '100%',
        justifyContent: 'flex-end',
        flexDirection: 'row',
        marginTop: 8,
        marginBottom: 8,
    },
    textRightAlign: {
        width: '100%',
        justifyContent: 'flex-end',
        flexDirection: 'row',
        marginTop: 8,
    },
    superscriptText: {
        fontSize: 8,
        lineHeight: 18,
        fontWeight: 'bold'
    },
    rightLabel: {
        marginTop: 10,
        marginRight: 10,
    },
    inputWidth: {
        width: 90,
    },
    directionRowWithWidth: {
        paddingBottom: 26,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    width3ItemsPadding: {
        width: '33%',
        paddingRight: 40,
        marginBottom: 26,
    },
    width2ItemsPadding: {
        width: '50%',
        paddingRight: 40,
        marginBottom: 26,
    },
    widthForcolumns: {
        paddingRight: 40,
        marginBottom: 26,
    },
    halfWithMargin: {
        width: '40%',
        marginBottom: 26,
    },
    halfWithMargin40: {
        width: '40%',
        marginBottom: 26,
        marginRight: 30,
    },
    center: {
        alignItems: 'center',
        justifyContent: "center"
    },
    width72: {
        width: "72.8%",
    },
    half2WithMargin: {
        width: '28%',
        marginBottom: 26,
        marginLeft: 15,
    },

    half3WithMargin: {
        width: '25%',
        marginBottom: 26,
        marginLeft: 15,
    },
    FIMsubHeading: {
        width: '100%',
        marginBottom: 26,
        paddingRight: 40,
    },
    FIMCalHeading: {
        width: '30%',
        marginBottom: 26,
        // paddingRight: 40,
    },
    FIMCalHeadingNew: {
        width: '60%',
        marginBottom: 26,
        // paddingRight: 40,
    },
    FIMCalBoxNew: {
        width: '40%',
        marginBottom: 26,
        marginLeft: 15,
    },
    FIMCalBox: {
        width: '20%',
        marginBottom: 26,
        marginLeft: 15,
    },

    FIMCalBoxWidth30: {
        width: '30%',
        marginBottom: 26,
        marginLeft: 15,
    },
    half5WithMargin: {
        width: '10%',
        marginBottom: 26
    },
    input60: {
        width: 60,
    },
    input65: {
        width: 67,
    },
    input48: {
        width: 48,
    },
    input50: {
        width: 50,
    },
    input100: {
        width: 90,
    },
    input54: {
        width: 54,
    },
    input52: {
        width: 52,
    },
    selectWithList: {
        width: '73%',
        paddingRight: 20,
    },
    withList: {
        width: '27%',
    },
    width4ItemsPadding: {
        width: '25%',
        paddingRight: 5,
        marginBottom: 26,
    },
    width3ItemsNomargin: {
        width: '33%',
        paddingRight: 40,
    },
    width3Items: {
        width: '33%',
        paddingRight: 40,
        marginBottom: 20,
    },
    width66: {
        width: '66%',
        marginBottom: 20,
    },
    width30: {
        // marginBottom: 20,
        width: '90%',
    },
    width33: {
        width: '33%',
        marginBottom: 20,
    },
    // checkbox css start
    checkBoxTextStyle: {
        color: '#000000',
        fontSize: 14,
        lineHeight: 17,
        fontFamily: 'Roboto-Regular',
        // marginLeft: -3,
    },
    preadmissionHomeCheckBoxTextStyle: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
        lineHeight: 18,
        fontFamily: 'Roboto-Regular',
    },
    preadmissionHomeCheckBoxStyle: {
        width: 25,
        height: 25,
        alignSelf: "flex-start"
    },
    checkBoxStyle: {
        width: 26,
        height: 26,
        alignSelf: "flex-start"
    },
    smallCheckBoxStyles: {
        width: 20,
        height: 20
    },
    // checkbox css end

    // button group start
    transparent: {
        backgroundColor: 'transparent',
    },
    btnGroup: {
        borderColor: '#00000040', // 25% opacity applied in 000000 color
        width: '50%',
        justifyContent: "center",
        height: 35,
    },
    genderbtnGroup: {
        borderColor: '#00000040', // 25% opacity applied in 000000 color
        width: '50%',
        justifyContent: "center",
        height: 36,
    },
    btnGroupText: {
        // color: '#80848F',
        fontSize: 13,
        lineHeight: 15,
        fontFamily: 'Roboto-Regular',
    },
    borderLeftRadius: {
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5,
    },
    borderRightRadius: {
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
    },
    // button group end

    importantText: {
        color: config.importantText,
        fontSize: 12,
        fontFamily: 'Roboto-Regular',
    },
    importantTextInFim: {
        color: '#a02b27',
        fontSize: 17,
        fontFamily: 'Roboto-Regular',
        fontWeight: 'bold',
    },
    fimHeading: {
        fontSize: 20,
        lineHeight: 28,
        color: config.textFontColor,
        fontFamily: 'Roboto-Regular',
        paddingBottom: 10,
    },
    inputHead: {
        fontSize: 14,
        color: config.blueTextColor,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 21,
        paddingRight: 21,
        borderColor: '#7070701f',
        borderWidth: 1,
    },
    textStrong: {
        fontSize: 15,
        color: '#000',
        fontFamily: 'Roboto-Regular',
    },
    textSmall: {
        fontSize: 12,
        color: '#000',
        fontFamily: 'Roboto-Regular',
        color: config.labelFontColor,
    },


    thirdCol: {
        width: '30%',
        marginBottom: 26,
        paddingRight: 40,

    },
    fifthCol: {
        width: '20%',
        marginBottom: 26,
        paddingRight: 40,

    },
    //End Add Patient Tab Styles




    redStar: {
        color: config.dangerStartColor,
    },
    badge: {
        backgroundColor: config.buttonTextColor,
    },
    badgeText: {
        color: '#fff',
        paddingLeft: 10,
        paddingRight: 10,
        fontSize: 14,
        fontFamily: 'Roboto-Bold',
    },
    commonText: {
        fontSize: 14,
        fontFamily: 'Roboto-Regular',
        color: '#000',
        marginTop: -6,
    },
    infoBtn: {
        width: 75,
        height: 32,
    },
    halfOne: {
        width: '50%',
        paddingRight: 10,
    },
    threeFouthOne: {
        width: '80%',
        paddingRight: 10,
    },
    quaterOne: {
        width: '20%',
        paddingRight: 10,
    },
    requiredText: {
        fontSize: 14,
        fontFamily: 'Roboto-Regular',
    },
    advancedHeadingswidth2ItemsPadding: {
        width: '50%',
        // paddingRight: 40,
        // paddingLeft: 10,
        marginTop: 10
    },
    advancedWidth3ItemsPadding: {
        width: '33%',
        paddingRight: 40,
        // marginBottom: 26,
    },
    advancedWidth2ItemsPadding: {
        width: '50%',
        paddingRight: 40,
        // marginBottom: 26,
    },
    AdditionalInAdvancedwidth3ItemsPadding: {
        width: '50%',
        // paddingRight: 40,
        marginBottom: 26,
    },
    goalsListBorder: {
        borderColor: '#00000030',
        borderRadius: 5,
        borderWidth: 1,
        // padding:10,
        paddingTop: 10,
        paddingLeft: 10,
        marginBottom: 20,
    },
    textContainer: {
        textAlign: 'center'
    },
    tabHeading: {
        fontSize: 18,
        lineHeight: 20,
        // color: '#000',
        fontFamily: 'Roboto-Regular',
        fontWeight: '600',
    },
    radarChartheight: {
        flex: 1,
        height: 670,
        width: 670
    },
    radraChartContainer: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
    },
    topStatusBarColor: {
        backgroundColor: '#000000',
        opacity: 0.89,
        paddingTop: 8,
    },
    topStatusBarColorForHomeLogin: {
        backgroundColor: '#000000',
        opacity: 0.5,
        paddingTop: 8,
    },
    bmiCalculatorPopupFieldsWidth1: {
        width: '20%',
        paddingRight: 10
    },
    bmiCalculatorPopupFieldsWidth2: {
        width: '40%',
        paddingRight: 20
    },
    bmiCalculatorPopupFieldsWidth3: {
        width: '40%',
        paddingRight: 30
    },
    bmiCalculatorTextRadius: {
        width: 50,
        borderRadius: 10
    },

    border: {
        borderColor: '#00000040',
        borderWidth: 1,
    },
    boldText: {
        fontWeight: 'bold'
    },
    interimPaginationAlign: {
        flexDirection: 'row',
        marginBottom: 22
    },
    caseFimPACLabelAlign: {
        width: '65%',
    },
    PACToolLabelAlign: {
        width: '35%',
        alignItems: 'flex-end'
    },
    caseFimPACLabelAlignForInterim: {
        width: '67%',
    },
    PACToolLabelAlignForInterim: {
        width: '33%',
        alignItems: 'flex-end'
    },
    logoutimgaeWidth: {
        width: 23,
        height: 24,
        marginTop: 6,
        marginRight: 20,
    },
    notification: {
        width: 22,
        height: 22,
    },
});