/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import { StyleSheet } from 'react-native';

import config from '../../config/config';

export default StyleSheet.create({
    row: {
        flexDirection: 'row',
        width: '100%',
    },
    rowBottomSpace: {
        marginBottom: 35,
    },
    rowBottomSpaceDashBorad: {
        marginBottom: 10,
    },
    rowBottomSpacePortrait: {
        marginBottom: 9,
        marginTop: 10,
    },
    rowBottomSpaceSmall: {
        marginBottom: 17,
    },
    rowBottomSpaceSmallNew: {
        marginBottom: 10,
    },
    rowBottomSpaceSmallLandscape: {
        marginBottom: 7,
    },
    alignItemsCenter: {
        alignItems: 'center'
    },
    verticalCenter: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    pageContent: {
        paddingLeft: 22.5,
        paddingRight: 22.5,
        paddingTop: 10,
        // paddingBottom: 10,
    },
    statusPartion: {
        width: 225,
        flex: 0,
    },
    statusPartionLandscape: {
        width: 80,
        flex: 0,
    },
    accessMargin: {
        marginRight: 38.5,
        marginTop: 5
    },
    access: {
        color: '#2FB285',
        fontSize: 20,
        lineHeight: 20,
        marginBottom: 4,
    },
    switch: {
        height: 40,
    },
    switchLandscape: {
        height: 5,
        marginBottom: 8
    },
    accessLandscape: {
        color: '#2FB285',
        fontSize: 20,
    },
    accessMarginLandscape: {
        marginRight: 38.5,
        marginTop: 5,
        marginBottom: 15
    },
    accessLabel: {
        fontSize: 12,
        color: '#808080',
        fontFamily: 'Roboto-Regular',
        textAlign: 'center'
    },
    pendingIconIndicator: {
        fontSize: 12,
        color: '#DC5959',
        marginRight: 6
    },
    statusfont: {
        color: '#36363E',
        fontSize: 12,
        lineHeight: 14
    },

    rowDivideBy4: {
        width: '25%',
    },
    rowDivideBy6: {
        width: '15%',
    },
    rowDivideBy6By2: {
        width: '17%',
    },
    rowWidth16: {
        width: '16%',
    },
    rowDivideBy7: {
        width: '12.5%',
    },
    rowDivideBy7New: {
        width: '13%',
    },
    rowDivideBy5: {
        width: '20%',
    },
    rowWidth60: {
        width: '60%'
    },
    rowWidth90: {
        width: '94%'
    },
    rowWidth10: {
        width: '6%'
    },
    rowWidth40: {
        width: '40%'
    },
    rowWidth80: {
        width: '80%'
    },
    rowWidth70: {
        width: '70%'
    },
    rowWidth30: {
        width: '30%'
    },
    rowWidth20: {
        width: '15%'
    },
    rowWidth85: {
        width: '85%'
    },
    rowWidth12: {
        width: '12%'
    },

    labelCssWithLeftAlignment: {
        fontSize: 13,
        color: config.labelFontColor,
        lineHeight: 15,
        marginLeft: 3,
    },
    labelCss: {
        fontSize: 13,
        color: config.labelFontColor,
        lineHeight: 15,
    },
    labelCss1: {
        fontSize: 13,
        color: config.labelFontColor,
        lineHeight: 15,
        minHeight: 30,
    },
    lableCssLandScape: {
        fontSize: 13,
        color: config.labelFontColor,
        lineHeight: 15,
    },
    textcssLink: {
        color: config.blueTextColor,
        fontSize: 14,
        textDecorationLine: 'underline',
        height: 25
    },
    textDangerLink: {
        color: config.dangerColor,
        fontSize: 14,
        textDecorationLine: 'underline',
        height: 25
    },
    fimModifiersLinkText: {
        fontWeight: 'bold',
        color: config.blueTextColor,
        fontSize: 16,
        textDecorationLine: 'underline',
        height: 25
    },
    textCss: {
        fontSize: 16,
        color: config.textFontColor,
        lineHeight: 18,
        paddingTop: 6,
        height: 25
    },
    unlockedImage: {
        width: 54,
        height: 49,
    },
    lockedImage: {
        width: 40,
        height: 48,
    },
    textcssLinkDanger: {
        color: config.dangerStartColor,
    },
    navigationbuttonsrow: {
        flexDirection: 'row',
        width: '100%',
        zIndex: 1
    },
    navigationbuttons: {
        alignSelf: "stretch",
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        zIndex: 10
    },
    navigationnextbutton: {
        position: 'absolute',
        top: 10,
        left: 5,

    },
    navigationprevbutton: {
        position: 'absolute',
        top: 10,
        right: 10,
    },
    NotQualifyingpopoverStyle: {
        backgroundColor: '#f8f5bd',
        borderColor: '#E1E1E1',
        borderBottomColor: '#E1E1E1',
        borderWidth: 1,
        borderRadius: 15,
        paddingHorizontal: 20,
        paddingVertical: 20,
        width: 470
    },
    popoverStyle: {
        backgroundColor: '#fbfaff',
        borderColor: '#E1E1E1',
        borderBottomColor: '#E1E1E1',
        borderWidth: 1,
        borderRadius: 15,
        paddingHorizontal: 20,
        paddingVertical: 20,
        width: 470
    },
    mmtpopoverStyle: {
        backgroundColor: '#fbfaff',
        borderColor: '#E1E1E1',
        borderBottomColor: '#E1E1E1',
        borderWidth: 1,
        borderRadius: 15,
        paddingHorizontal: 20,
        paddingVertical: 20,
        width: 300
    },
    textInPopover: {
        fontSize: 15,
        flex: 1,
    },
    NotQualifyingLinkText: {
        fontSize: 15,
        color: 'blue',
    },
    width90WithTopAlignment: {
        width: '90%',
        marginTop: 7,
    },
    inputBoxText: {
        borderRadius: 6,
        borderWidth: 1,
        // borderColor: '#00000040', // 25% opacity applied in 000000 color
        height: 28,
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 5,
        backgroundColor: "#EFEFEF",
        borderColor: "#D6D6D6",
    },
    admitStatusText: {
        color: '#000',
        fontWeight: 'bold'
    },
    complianceTextCss: {
        color: '#000',
        fontWeight: 'bold',
        fontSize: 18,
        justifyContent: 'center'
    }

});