/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import { StyleSheet } from 'react-native';
import config from '../../config/config';

export default StyleSheet.create({

    namesDisplayAlignment: {
        borderWidth: 1,
        padding: 10,
        flexDirection: "column",
        marginRight: 14
    },

    row: {
        // alignItems: 'center',
        width: '100%',
        flexDirection: 'row',
    },
    rowDiff: {
        backgroundColor: 'lightgreen',
        width: '100%',
        flexDirection: 'row',
    },
    rowDiff1: {
        backgroundColor: 'lightgreen',
        width: '93%',
        flexDirection: 'row',
    },
    halfOne: {
        width: '50%',
        paddingRight: 20,
    },
    quaterOne: {
        width: '25%',
        paddingRight: 10,
    },
    requiredText: {
        fontSize: 14,
        fontFamily: 'Roboto-Regular',
    },
    half2: {
        width: '18%',
    },
    quarter: {
        width: '25%',
    },

    caseTableCellWidth: {
        width: '50%',
    },

    half1: {
        width: '30%',
    },

    halfTwo: {
        width: '45%',
        paddingRight: 10,
        paddingLeft: 21
    },
    width40: {
        width: '40%'
    },

    labelStyle: {
        color: '#696969',
        fontSize: 18,
        fontFamily: 'Times New Roman',
        textAlignVertical: 'top',

    },
    tablelabelStyle: {
        fontSize: 18,
        fontFamily: 'Times New Roman'

    },
    tabletextStyle: {
        fontSize: 16,
        fontFamily: 'Times New Roman',
    },
    tableTextDiff: { backgroundColor: 'lightgreen' },

    labelStyleDiff: {
        // backgroundColor: 'lightgreen',
        fontSize: 18,
        fontFamily: 'Times New Roman',
        textAlignVertical: 'top',
    },

    textStyle: {
        color: 'black',
        fontSize: 16,
        fontFamily: 'Times New Roman',
        textAlignVertical: 'top'
    },
    textStyleDiff: {
        backgroundColor: 'lightgreen',
        fontSize: 16,
        fontFamily: 'Times New Roman',
        textAlignVertical: 'top'
    },
    noteTextStyle: {
        justifyContent: "space-between",
        flexDirection: 'row',
        borderTopWidth: 1,
        borderBottomWidth: 1,
        marginTop: -9.85,
        width: '100%'
    },

    noteCommentsTextStyle: {
        width: '100%',
        paddingLeft: 5,
        marginTop: 5,
        textAlign: 'left',
    },
    marginBottom10: {
        marginBottom: 10
    },
    marginTop10: {
        marginTop: 10
    },
    margin: {
        marginBottom: 20,
        marginTop: 20,
        marginLeft: 10
    },
    marginLeft15: {
        marginLeft: 15
    },
    HeadingColor: {
        paddingLeft: 10,
        fontSize: 14,
        fontWeight: 'bold',
        color: config.buttonTextColor,
        fontFamily: 'Times New Roman',
    },
    HeadingAdmin: {
        fontSize: 14,
        fontWeight: 'bold',
        fontFamily: 'Times New Roman',
    },
    halfOneNew: {
        width: '100%',
        // paddingLeft: 15
    },
    borderLeft: {
        borderColor: '#70707033', // 0.2 opacity applied
        borderLeftWidth: 1
    },

    container: { flex: 1, paddingLeft: 5, paddingRight: 5 },
    wrapper: { flexDirection: 'row' },
    // head: { height: 40 },
    text: { margin: 6 },
    bold: {
        fontWeight: 'bold'
    },
    bold300: {
        fontWeight: '400',
        fontFamily: 'Times New Roman',

    },
    bold600: {
        fontWeight: '600',
        fontSize: 18,
        fontFamily: 'Times New Roman',
    },
    boldNew: {
        fontWeight: '900'
    },
    tableHeadingAlign: {
        // flexDirection:'row',
        textAlign: 'center',
        paddingTop: 7,
        paddingBottom: 7
    },
    superscriptText: {
        fontSize: 10,
        lineHeight: 18,
    },
    borderRight1: {
        borderRightWidth: 0.9,
    },

    marginLeft5: {
        marginLeft: -4
    },
    notesSeperator: {
        borderLeftWidth: 1,
        borderRightWidth: 1,
        paddingTop: 10,
        paddingBottom: 5,
        width: '100%',
        flexDirection: "column",
    },
    halftable: {
        width: '33.35%'
    },
    topAlign: {
        alignSelf: 'flex-start'
    }
});