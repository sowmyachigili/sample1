/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import { StyleSheet } from 'react-native';
import config from '../../config/config';

export default StyleSheet.create({
    // buttons
    btnDefault: {
        borderRadius: 4,
        paddingTop: 10,
        paddingBottom: 11,
        paddingLeft: 10,
        paddingRight: 10,
        minWidth: 100,
        height: 40,
        justifyContent: 'center',
    },
    buttonLight: {
        backgroundColor: '#88edfb',
    },
    buttonDisableLight: {
        backgroundColor: 'lightgray',
    },
    buttonDark: {
        backgroundColor: '#4dd0e1',
    },
    btnDefaultText: {
        fontSize: 16,
        lineHeight: 26,
        fontFamily: 'Roboto-Medium',
        height: 19,
        lineHeight: 19,
    },
    buttonLightText: {
        color: '#2F3946',
    },

    buttonDarkText: {
        color: '#ffffff',
    },
    buttonGreen: {
        backgroundColor: '#4dd0e1',
    },

    // modal box start
    modal: {
        borderRadius: 15,
        backgroundColor: '#fff',
        paddingLeft: 10,
        paddingRight: 10,
    },
    modalHead: {
        borderBottomWidth: 1,
        borderColor: '#00000033',
        paddingTop: 20,
        paddingBottom: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    modalHeading: {
        color: '#000',
        fontSize: 22,
        fontFamily: 'Roboto-Medium',
    },
    leftContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
    rightContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    closeIcon: {
        color: '#707070',
        fontSize: 36,
        paddingTop: 0,
    },
    smallCloseIcon: {
        color: '#707070',
        fontSize: 24,
        padding: 0,
        alignSelf: 'flex-end'
    },
    modalBody: {
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 20,
        paddingBottom: 20,
    },
    modalFooterForText: {
        marginBottom: 20,
        marginTop: 10,
        flexDirection: 'column',
        justifyContent: 'center',

    },
    modalFooter: {
        marginBottom: 20,
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    // modal box end

    informationText: {
        fontSize: 20,
        fontFamily: 'Roboto-Regular',
        textAlign: 'center',
        color: '#000',
        fontWeight: '500'
    },
    smallInformationText: {
        fontSize: 16,
        fontFamily: 'Roboto-Regular',
        marginBottom: 20,
        textAlign: 'center'
    },
    bigStar: {
        fontSize: 16,
    },
    validationText: {
        fontSize: 16,
        marginBottom: 2,
        fontFamily: 'Roboto-Regular',
        color: '#868686',
    },
    tickIcon: {
        width: 88,
        height: 97,
        marginBottom: 20,
    },
    warning: {
        width: 64,
        height: 64,
        marginBottom: 20,
    },
    deleteIcon: {
        width: 84,
        height: 92,
        marginBottom: 20,
    },
    logoutIcon: {
        width: 64,
        height: 64,
        marginBottom: 20,
    },
    modalSmall: {
        width: '85%',
        alignSelf: 'center',
    },
    modalContent: {
        borderRadius: 15,
        backgroundColor: '#fff',
        paddingLeft: 5,
        paddingRight: 5,
        paddingTop: 5,
        paddingBottom: 15,
    },
    modalSmallBody: {
        paddingTop: 30,
        paddingBottom: 40,
        paddingLeft: 10,
        paddingRight: 10,
    },
    modalSubHeading: {
        fontSize: 16,
        lineHeight: 19,
        color: config.labelFontColor,
        fontFamily: 'Roboto-Regular',
    },
});