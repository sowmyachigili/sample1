/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import { StyleSheet } from 'react-native';
import config from '../../config/config';

export default StyleSheet.create({
    // header3 css
    searchHeader: {
        backgroundColor: '#bbc2cf',
        paddingLeft: 9,
        paddingRight: 9,
        paddingTop: 9,
        paddingBottom: 9,
    },
    searchItem: {
        height: 32,
        paddingTop: 7,
        paddingBottom: 7,
        fontSize: 16,
        lineHeight: 18,
        fontFamily: 'Roboto-Regular',
        backgroundColor: '#fff',
    },
    pickerback: {
        width: '19%',
        marginLeft: '1%',
        borderRadius: 3,
        height: 32,
        paddingTop: 7,
        paddingBottom: 7,
    },
    dropDownList: {
        height: 40,
    },

    // list css
    thumbShortcut: {
        borderRadius: 8,
        width: 40,
        height: 40,
        backgroundColor: '#6075af',
        marginTop: 10,
    },
    thumbText: {
        fontSize: 15,
        lineHeight: 15,
        letterSpacing: 0.10,
        fontFamily: 'Roboto-Regular',
        color: '#FFFFFF',
        alignItems: 'center',
        paddingTop: 12,
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    pendingIconIndicator: {
        fontSize: 12,
        color: '#DC5959',
        marginRight: 6,
        lineHeight: 14,
    },
    statusfont: {
        fontSize: 12,
        color: '#36363E',
        fontFamily: 'Roboto-Regular',
        lineHeight: 14,
        // width: 204,
    },
    // admited: {
    //     fontSize: 14,
    //     color: '#36363E',
    //     fontFamily: 'Roboto-Medium',
    //     marginTop: 10,
    //     marginLeft: 10,
    //     opacity: 0.5,
    // },
    lock: {
        height: 34,
        width: 37,
        paddingLeft: 10,
        justifyContent: 'center'
    },
    unlock: {
        width: 30,
        height: 36,
        paddingLeft: 10,
    },
    arrow: {
        marginTop: 14,
        width: 12,
        height: 18,
    },
    dateWidth: {
        width: 110,
        flex: 0,
    },
    admittedWidth: {
        width: 80,
        flex: 0,
    },
    width70: {
        width: 0,
        flex: 0
    },
    width90: {
        width: 90,
        flex: 0
    },
    width100: {
        width: 100,
        flex: 0
    },
    width120: {
        width: 120,
        flex: 0
    },
    width130: {
        width: 130,
        flex: 0
    },
    width60: {
        width: 60,
        flex: 0
    },
    width50: {
        width: 50,
        flex: 0
    },
    width30: {
        width: 30,
        flex: 0
    },
    impairment: {
        width: 80,
        flex: 0,
    },
    locked: {
        width: 95,
        flex: 0,
    },
    assDateLabel: {
        fontSize: 12,
        color: '#8C8C8c',
        fontFamily: 'Roboto-Regular',
        lineHeight: 14,
        marginBottom: 5.5
    },
    assDate: {
        fontSize: 14,
        lineHeight: 18,
        color: '#000',
        fontFamily: 'Roboto-Regular',
    },
    assDateColor: {
        lineHeight: 18,
        fontFamily: 'Roboto-Regular',
        color: config.blueTextColor,
        fontSize: 14,
        textDecorationLine: 'underline'
    },
    rowDivide: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start' // if you want to fill rows left to right
    },
    dateItem: {
        width: '50%' // is 50% of container width
    },
    name: {
        fontSize: 14,
        lineHeight: 14,
        color: '#000',
        fontFamily: 'Roboto-Medium',
        fontWeight: "bold",
        marginBottom: 1,
        paddingTop: 8,
    },
    phone: {
        fontSize: 11,
        lineHeight: 11,
        color: '#9d9d9d',
        fontFamily: 'Roboto-Regular',
        marginBottom: 5,

    },
    labelDate: {
        fontSize: 11,
        lineHeight: 11,
        color: '#000000',
        fontFamily: 'Roboto-Regular',
    },
    labelDataLabel: {
        fontSize: 9,
        lineHeight: 9,
        color: '#9d9d9d',
        fontFamily: 'Roboto-Regular',
        paddingBottom: 8
    },
    list: {
        borderBottomWidth: 0,
        paddingBottom: 0,
        paddingTop: 0,
    },
    listNoMargin: {
        borderBottomWidth: 0,
        marginLeft: 0,
    },
    borderRight: {
        borderColor: '#70707033', // 0.2 opacity applied
        borderRightWidth: 1,
        // marginRight: 15,
    },
    borderLeft: {
        borderColor: '#70707033', // 0.2 opacity applied
        borderLeftWidth: 1,
        paddingLeft: 15,
    },
    paddingRight: {
        paddingRight: 0,
    },
    listData: {
        marginTop: 15,
        marginBottom: 15,
        // paddingRight: 16,
    },
    lists: {
        borderColor: '#70707026',  // 0.15 opacity applied
        borderBottomWidth: 0.5,
        paddingLeft: 22.5, // already margin left 18px applied
        paddingRight: 22.5,
        marginLeft: 0,
        marginRight: 0,

    },
    seperator: {
        borderWidth: 1,
        paddingTop: 10,
        paddingBottom: 10,
        width: '100%',
        flexDirection: "column",
    },
    dash: {
        color: '#8C8C8c',
    },
    sigatureImage: {
        width: 35,
        height: 29,
    },
    noteImage: {
        width: 31,
        height: 31,
        marginTop: 10,
    },
    deleteImage: {
        width: 30,
        height: 32,
        marginTop: 10,
    },
    deleteText: {
        fontSize: 12,
        color: '#0000004d',
        fontFamily: 'Roboto-Bold',
        opacity: 0.5,
        justifyContent: 'center',
    },


    listSpace: {
        marginTop: 10,
        marginBottom: 10,
    },
    modalLists: {
        borderColor: '#70707026',  // 0.15 opacity applied
        borderBottomWidth: 0.5,
        marginLeft: 0,
        paddingRight: 0,
    },
    normalText: {
        fontSize: 16,
        fontFamily: 'Roboto-Regular',
    },
    headingList: {
        fontSize: 15,
        fontFamily: 'Roboto-Bold',
        color: '#000',
        opacity: 0.6,
        // marginLeft: 0,
        // marginRight: 0,
    },


    numberWidth: {
        width: 70,
        flex: 0,
    },
    TierWidth: {
        width: 55,
        flex: 0,
    },
    ruleWidth: {
        width: 105,
        flex: 0,
    },
    btnWidth: {
        width: 85,
        flex: 0
    },

    descWidth: {
        width: '60%',
        flex: 0,
    },
    groupWidth: {
        width: '10%',
        flex: 0,
    },
    selectWidth: {
        width: '10%',
        flex: 0,
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    buttons: {
        position: 'absolute',
        flexDirection: 'row',
        flex: 1,
        top: 0,
        left: 0,
        marginTop: 20,
    },
    popoverContainer: {
        backgroundColor: '#114B5F',
        padding: 8,
        borderRadius: 5,
    },
    popoverText: {
        color: '#E4FDE1',
    },

    expandIconWidth: {
        width: '30%'
    },

    commonIcon: {
        width: 20,
        height: 20
    },
    width50Per: {
        width: '50%'
    },
    width100Per: {
        width: '100%'
    },
    lockImageStatus: {
        width: 40,
        flex: 0,
        // marginLeft: 15,
        // marginRight: 15,
        alignItems: 'center',
        justifyContent: 'center'
    }

});