/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import { StyleSheet } from 'react-native';

// dropdown list start
const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        fontFamily: 'Roboto-Regular',
        lineHeight: 19,
        paddingTop: 9,
        paddingHorizontal: 7,
        paddingBottom: 8,
        color: 'black',
        backgroundColor: '#fff',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#00000040',  // 25% opacity applied in 000000 color
        height: 35,
    },
    icon: {
        color: '#00000099',
        borderRightWidth: 5,
        borderRightColor: 'transparent',
        borderLeftWidth: 5,
        borderLeftColor: 'transparent',
        width: 0,
        height: 0,
        top: 14,
        right: 15,
    },
});

export const disableickerSelectStyles = StyleSheet.create({
    ...pickerSelectStyles,
    inputIOS: {
        ...pickerSelectStyles.inputIOS,
        backgroundColor: '#EFEFEF'
    }
});

// dropdown list start
export default pickerSelectStyles;