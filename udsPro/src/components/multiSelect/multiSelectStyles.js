/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import { StyleSheet } from 'react-native';

// Components
import config from '../../config/config';

export default StyleSheet.create({
    row: {
        width: '100%',
        flexDirection: 'row',
    },
    // modal box start
    modal: {
        width: '100%',
        marginLeft: 0,
        marginRight: 0,
        marginBottom: 0,
        marginTop: 0,
        bottom: 0,
        position: 'absolute',
    },
    modalContent: {
        backgroundColor: '#fff',
        width: '100%',
        borderTopWidth: 1,
        borderColor: config.tabBarBorderColor,
    },
    modalBody: {
        height: 270
    },

    doneBar: {
        backgroundColor: '#EFEFEF',
        marginTop: 0,
        paddingTop: 0,
        height: 45,
        borderTopColor: '#c9c9c9',
        borderWidth: 1
    },
    doneText: {
        fontSize: 18,
        color: 'black', //config.blueTextColor
        fontWeight: '500',
        fontFamily: 'Roboto-Regular',
        // paddingRight: 15,
    },
    disableNextText: {
        fontSize: 18,
        color: 'gray', //config.blueTextColor
        fontWeight: '500',
        fontFamily: 'Roboto-Regular',
        // paddingRight: 15,
    },
    containerStyle: {
        width: '100%',
        paddingTop: 6.5,
        paddingBottom: 6.5,
    }

});