/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import React, { Component } from 'react';
import { View, Image, Text, FlatList, TouchableOpacity, StyleSheet, Keyboard, TouchableWithoutFeedback, Alert } from 'react-native';
import { Item, Input, Button, List, ListItem, Header } from 'native-base';
import Modal from "react-native-modal";

import configImage from '../../config/configImages';

// Components
import inputStyles from './../input/inputStyles';
import CheckBox from 'react-native-modest-checkbox';

// Styles
import styles from '../styles/styles';
import selectStyles from './multiSelectStyles';

class MultiSelect extends Component {
    constructor(props) {
        super(props);
        this.inputRefs = {};

        this.state = {
            isMutiSelectVisible: false,
            value: this.props.field,
            checkboxes: [] = this.props.lists,
        };
    }

    // select item
    toggleCheckbox = (index) => {
        // update list
        this.state.checkboxes[index].checked = !this.state.checkboxes[index].checked;
        this.setState({
            checkboxes: this.state.checkboxes
        });

        let arrayPush = this.loadMultiselectData();

        // send data to parent
        this.props.handlerFromParant(arrayPush.toString(), this.props.name, this.props.tabName);

    }
    // Will Receive Props
    componentDidUpdate(prevProps) {
        if (prevProps.lists !== this.props.lists) {
            this.loadMultiselectData();
        }
    }

    // load and Update multi select data
    loadMultiselectData = () => {
        // generate DB format data
        let arrayPush = [];

        if (this.props.lists) {
            this.setState({
                checkboxes: this.props.lists
            })
        }

        // calculate total selected inputs
        if (this.state.checkboxes) {
            var count = 0;
            var text = undefined;
            var lists = this.state.checkboxes;
            for (let i = 0; i < lists.length; i++) {
                if (lists[i].checked == true) {
                    arrayPush.push('"' + this.state.checkboxes[i].label + '"');
                    if (count == 0) {
                        text = lists[i].label;
                    }
                    count++;
                }
            }
            this.assignInputCountOrText(count, text);
        }
        return arrayPush;
    }

    // Multi select input field text change
    assignInputCountOrText = (count, text) => {
        //  assign data to input
        if (count > 1) {
            this.setState({
                value: count + ' items selected',
            });
        } else if (text) {
            this.setState({
                value: text,
            });
        } else {
            this.setState({
                value: undefined,
            });
        }
    }

    // Open or hide multiselect modal
    togglePicker = () => {
        Keyboard.dismiss();
        if (!this.props.disabled && this.state.checkboxes.length > 0) {
            this.setState({
                isMutiSelectVisible: !this.state.isMutiSelectVisible,
            });
        }

        if (this.inputRef && this.inputRef._root && this.inputRef._root.focus && this.inputRef._root.blur) { // to move dropdown to upward 
            this.inputRef._root.focus();
            this.inputRef._root.blur();
        }
    }

    // move focus to next field
    moveFocusToNextField = (actionType) => {
        if (actionType) {
            Keyboard.dismiss();
            this.setState({
                isMutiSelectVisible: false,
            }, () => {
                if (this.props.onDownArrow && actionType == 'Next') {
                    setTimeout(() => {
                        this.props.onDownArrow();
                    }, 500);
                }
            });
        }
    }

    setInputRef = (ref) => {
        this.inputRef = ref;
    }

    render() {
        return (
            <TouchableWithoutFeedback onPress={this.togglePicker}>
                <View {...this.props} style={{ width: '100%' }} pointerEvents="box-only">

                    {/*  Select Box Start */}
                    <Item regular style={[inputStyles.inputBox, this.props && this.props.disabled ? inputStyles.disabled : null]} disabled={this.props && this.props.disabled ? true : false} >
                        {this.props.label ? <Text active style={inputStyles.label}>{this.props.label}</Text> : null}
                        <Input
                            // ref={this.props.name}
                            ref={(ref) => this.setInputRef(ref)}
                            // editable={false}
                            // onTouchStart={() => {
                            //     this.togglePicker();
                            // }}
                            placeholder={this.props.placeholder ? this.props.placeholder : null}
                            placeholderTextColor='#cacaca'
                            style={[inputStyles.input, this.props.style]}
                            value={this.state.value}
                        />


                        <TouchableOpacity onPress={() => { this.togglePicker() }} >
                            <Image source={configImage.dropDownIcon} style={inputStyles.dropdownImage} />
                        </TouchableOpacity>

                        {/* {this.props.image == 'lock' ? <Image source={configImage.readOnlyLock} style={styles.inputlockimage} /> : null} */}

                    </Item >
                    {/*  Select Box End */}


                    {/*  Select List Box Start */}
                    <Modal
                        isVisible={this.state.isMutiSelectVisible}

                        backdropOpacity={0}
                        onBackdropPress={() => this.setState({ isMutiSelectVisible: false })}
                        onBackButtonPress={() => this.setState({ isMutiSelectVisible: false })}
                        style={selectStyles.modal} >
                        <View style={selectStyles.modalContent}>
                            <Header style={selectStyles.doneBar}>
                                <View style={defaultStyles.nextView}>
                                    <Button transparent onPress={() => this.moveFocusToNextField('Next')}>
                                        <Text style={this.props.onDownArrow ? selectStyles.doneText : selectStyles.disableNextText}>Next</Text>
                                    </Button>
                                </View>
                                {/* For displaying selected field name */}
                                <View style={defaultStyles.displayView}>
                                    <Text style={[defaultStyles.done, defaultStyles.labelText]} numberOfLines={1}>
                                        {this.props.displayName}
                                    </Text>
                                </View>
                                <View style={[defaultStyles.doneView]}>
                                    <Button transparent
                                        onPress={() => this.moveFocusToNextField('Done')}
                                    >
                                        <Text style={selectStyles.doneText}>Done</Text>
                                    </Button>
                                </View>
                            </Header>
                            <View style={selectStyles.modalBody}>
                                <List>
                                    <FlatList
                                        data={this.state.checkboxes}
                                        renderItem={({ item, index }) =>
                                            <ListItem itemHeader first style={{ paddingTop: 0, paddingBottom: 0 }}>
                                                <CheckBox checked={this.state.checkboxes[index].checked}
                                                    label={item.label}
                                                    numberOfLabelLines={2}
                                                    checkedImage={require('../../assets/icons/Check-Box-Selected.png')}
                                                    uncheckedImage={require('../../assets/icons/Check-box.png')}
                                                    // onChange={(checked) =>  this.toggleCheckbox(item.id)  }
                                                    onChange={(checked) => this.toggleCheckbox(index)}
                                                    checkboxStyle={styles.checkBoxStyle}
                                                    labelStyle={styles.checkBoxTextStyle}
                                                    containerStyle={selectStyles.containerStyle}
                                                />
                                            </ListItem>
                                        }
                                        keyExtractor={(item, index) => index.toString()}
                                        extraData={this.state}
                                        ItemSeparatorComponent={this.renderSeparator}
                                    />
                                </List>
                            </View>
                        </View>
                    </Modal>
                    {/*  Select List Box Start */}

                </View>
            </TouchableWithoutFeedback>
        )
    }
}

const defaultStyles = StyleSheet.create({
    done: {
        color: "black",
        fontWeight: "500",
        padding: 10,
        fontSize: 18
    },
    labelText: {
        fontWeight: 'normal',
        color: '#007AFE'
    },
    nextView: {
        width: '10%',
        alignItems: 'flex-start',
        marginLeft: 0,
        paddingLeft: 0
    },
    doneView: {
        width: '10%',
        alignItems: 'flex-end',
    },
    displayView: {
        width: '80%',
        alignItems: 'center'
    }
});

export default MultiSelect;