/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import React, { Component } from 'react';
import { View, Image, TouchableOpacity, StatusBar, Alert } from 'react-native';
import { Header, Left, Right, Text, Icon } from 'native-base';
import ProgressCircle from 'react-native-progress-circle';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from '@react-native-community/netinfo';

import configImage from '../../config/configImages';
import config from '../../config/config';

import navigation from '../../utility/Navigation';

// Components
import Logout from '../logout/logout';
import Notification from './../notification/notification';

//Styles
import headerStyles from './../styles/headerStyles';
import styles from './../styles/styles';


import synchronization from '../../Screens/Synchronization/Synchronization';
import commonFun from '../../utility/commonFun';

// let eventsCount = 0;
// let checkOfflineFromLogin = '';
class MainHeader extends Component {

    constructor(props) {
        super(props);
        this.progressBarInterval;
        this.offline = false;
        this.eventsCountSync = 0;
        // this.state = {
        //     isInternetConnected: true,
        //     syncProgress: this.props.autoSyncProgress,
        // }
        // AsyncStorage.getItem('checkOffline').then((checkOffline) => {
        //     if (checkOffline) {
        //         checkOfflineFromLogin = JSON.parse(checkOffline);
        //     }
        // })
    }

    componentWillUnmount() {
        if (this.props.onRef) {
            this.props.onRef(null);
        }
    }

    componentDidMount() {
        if (this.props.onRef) {
            this.props.onRef(this);
        }
        // NetInfo.addEventListener(state => {
        //     let isInternetConnected = false;
        //     if (state && state.type && state.isConnected && state.isInternetReachable) {
        //         isInternetConnected = true;
        //         AsyncStorage.getItem('screenName').then(async (screenName) => {

        //             let screen = screenName;
        //             // set autoSync status progress 
        //             if (this.progressBarInterval) {
        //                 // clear interval for evary screen change
        //                 clearInterval(this.progressBarInterval);
        //             }
        //             if (screen && JSON.parse(screen) === this.props.page && eventsCount == 0) {
        //                 eventsCount = eventsCount + 1;
        //                 await this.synchronizeFullData(this.props.userInfo);
        //             }
        //         }).catch((err) => {
        //             commonFun.errorLogForStartAndEndTimeOfAPICall('errSYnc When Offline to online', true, err)
        //         })
        //     } else {
        //         this.offline = true;
        //         AsyncStorage.setItem('checkOffline', JSON.stringify('offline'));
        //         isInternetConnected = false;
        //         eventsCount = 0;
        //     }
        //     this.setState({
        //         isInternetConnected: isInternetConnected
        //     })
        // });
        // NetInfo.fetch().then(state => {
        //     if (state && state.type) {
        //         this.setState({
        //             isInternetConnected: state.isConnected
        //         })
        //     }
        // });
    }

    sessionInterval = () => {
        this.logoutRef.sessionInterval();

        // set autoSync status progress 
        if (this.progressBarInterval) {
            // clear interval for evary screen change
            clearInterval(this.progressBarInterval);
        }
        // this.setInterval();
    }

    // setInterval = () => {
    //     this.progressBarInterval = setInterval(() => {
    //         if (this.props.autoSyncProgress < 100) {
    //             this.props.checkAutoSyncProgressForStatus();
    //         } else {
    //             if (this.progressBarInterval) {
    //                 // clear interval for evary screen change
    //                 clearInterval(this.progressBarInterval);
    //             }
    //         }
    //     }, 1000);
    // }

    clearIntervel = () => {
        this.logoutRef.clearIntervel();
    }

    // home button handler
    // homeButtonHandler = () => {
    //     if (this.props.page && (this.props.page == 'AddPreadmission' || this.props.page == 'AddCase'
    //         || this.props.page == 'AdvancedComorbidScreen' || this.props.page == 'ICDCodesScreen')) {
    //         this.props.clickHomeButton('Home');
    //     } else {
    //         navigation.popToSpecificRouteNavigator(this.props.homeScreenNavigatorId);
    //     }
    // }

    handleLogoutHandler = () => {
        // if (this.props.page && (this.props.page == 'AddPreadmission' || this.props.page == 'AddCase'
        //     || this.props.page == 'AdvancedComorbidScreen' || this.props.page == 'ICDCodesScreen')) {
            this.props.clickHomeButton('Logout');
        // }
    }

    onClickLogout = (type) => {
        // if (this.props.autoSyncProgress === 100) {
            this.logoutRef.onClickLogout(type);
        // }
    }

    // synchronizeFullData = async (userData) => {
    //     let responseFromApi, conflictPatIdsArray;
    //     if (userData && userData.UserID && userData.FacilityCode &&
    //         (this.offline || checkOfflineFromLogin == 'offline') && this.eventsCountSync == 0) {
    //         // sync uds User Mata
    //         let apiUserBody = {
    //             FacilityCode: userData.FacilityCode,
    //             UserID: userData.UserID,
    //         }
    //         setTimeout(async () => {
    //             this.eventsCountSync++;
    //             responseFromApi = await synchronization.syncUDSUserData(apiUserBody, userData, 'autoSync');
    //             await this.props.checkAutoSyncProgressForStatus('offline', 5);
    //             if (responseFromApi != 'databaseChanged' && responseFromApi != 'appVersionChanged' && responseFromApi != 'serverError') {
    //                 if (this.notificationRef) {
    //                     this.notificationRef.getAppNotifications();
    //                 }
    //                 if (responseFromApi && (responseFromApi != 'FailGUID' || responseFromApi != 'invalidGUIDCode'
    //                     || responseFromApi != 'serverError')) {
    //                     userData = responseFromApi.loggedInUserDetails;
    //                 } else {
    //                     return;
    //                 }

    //                 // sync UDS Master Mata
    //                 if (responseFromApi != 'serverError') {
    //                     responseFromApi = await synchronization.syncUDSMasterData(apiUserBody);
    //                 } else {
    //                     return;
    //                 }
    //                 // sync ICD Data
    //                 if (responseFromApi != 'serverError') {
    //                     await this.props.checkAutoSyncProgressForStatus('offline', 10);
    //                     responseFromApi = await synchronization.syncICDData(apiUserBody);
    //                 } else {
    //                     return;
    //                 }

    //                 // sync ICD10 Full Data
    //                 if (responseFromApi != 'serverError') {
    //                     await this.props.checkAutoSyncProgressForStatus('offline', 20);
    //                     responseFromApi = await synchronization.syncICD10FullData(apiUserBody);
    //                 } else {
    //                     return;
    //                 }

    //                 // sync ICD9 Full Data
    //                 if (responseFromApi != 'serverError') {
    //                     await this.props.checkAutoSyncProgressForStatus('offline', 30);
    //                     responseFromApi = await synchronization.syncICD9FullData(apiUserBody);
    //                 } else {
    //                     return;
    //                 }

    //                 // sync Facility Common Data
    //                 if (responseFromApi != 'serverError') {
    //                     await this.props.checkAutoSyncProgressForStatus('offline', 40);
    //                     responseFromApi = await synchronization.syncFacilityCommonData(apiUserBody);
    //                 } else {
    //                     return;
    //                 }

    //                 // sync Facility Data
    //                 if (responseFromApi != 'serverError') {
    //                     await this.props.checkAutoSyncProgressForStatus('offline', 50);
    //                     responseFromApi = await synchronization.syncFacilityData(apiUserBody);
    //                 } else {
    //                     return;
    //                 }

    //                 // sync Category Data
    //                 if (responseFromApi != 'serverError') {
    //                     await this.props.checkAutoSyncProgressForStatus('offline', 60);
    //                     responseFromApi = await synchronization.syncCategoryData(apiUserBody);
    //                 } else {
    //                     return;
    //                 }

    //                 // sync Facility Category Data
    //                 if (responseFromApi != 'serverError') {
    //                     responseFromApi = await synchronization.syncFacilityCategoryData(apiUserBody);
    //                 } else {
    //                     return;
    //                 }


    //                 //only if user has write permissions for Preadmission sync the data
    //                 if (!config.preadmOrCaseNonSyncPrivileges.includes(userData.PreadmissionPrivilege)
    //                     && responseFromApi != 'serverError') {
    //                     // sync Preadmission Data

    //                     await this.props.checkAutoSyncProgressForStatus('offline', 70);
    //                     responseFromApi = await synchronization.syncPreadmissionData(apiUserBody);
    //                     await this.props.checkAutoSyncProgressForStatus('offline', 80);

    //                     if (responseFromApi != 'serverError') {
    //                         conflictPatIdsArray = responseFromApi.conflictPatIdsArray;
    //                         // sync Notification Data
    //                         responseFromApi = await synchronization.syncNotificationData(apiUserBody);
    //                     } else {
    //                         return;
    //                     }

    //                     // sync CaseListing Data
    //                     // only if user has write permissions for CaseListing sync the data
    //                     if (!config.preadmOrCaseNonSyncPrivileges.includes(userData.CaseListingPrivilege) &&
    //                         responseFromApi != 'serverError') {
    //                         await this.props.checkAutoSyncProgressForStatus('offline', 90);
    //                         responseFromApi = await synchronization.syncCaseListingData(apiUserBody, conflictPatIdsArray);
    //                         if (responseFromApi != 'serverError') {
    //                             // Insert Tier and Compliance
    //                             responseFromApi = await synchronization.syncTierAndComplianceData(apiUserBody);
    //                             if (responseFromApi != 'serverError') {
    //                                 responseFromApi = await synchronization.syncAuditLogData();
    //                             }
    //                             // sync errorLog Data
    //                             if (responseFromApi != 'serverError') {
    //                                 await synchronization.syncErrorLogData();
    //                             }
    //                             await this.props.checkAutoSyncProgressForStatus('offline', 100);
    //                             await this.setSyncEventValuesToZero();
    //                         } else {
    //                             return;
    //                         }
    //                     } else if (responseFromApi != 'serverError') {
    //                         return;
    //                     } else {
    //                         responseFromApi = await synchronization.syncAuditLogData();
    //                         // sync errorLog Data
    //                         if (responseFromApi != 'serverError') {
    //                             await synchronization.syncErrorLogData();
    //                         }
    //                         await this.props.checkAutoSyncProgressForStatus('offline', 100);
    //                         await this.setSyncEventValuesToZero();
    //                     }
    //                 } else if (!config.preadmOrCaseNonSyncPrivileges.includes(userData.CaseListingPrivilege) &&
    //                     responseFromApi != 'serverError') { //only if user has write permissions for CaseListing sync the data
    //                     // sync CaseListing Data
    //                     await this.props.checkAutoSyncProgressForStatus('offline', 90);
    //                     responseFromApi = await synchronization.syncCaseListingData(apiUserBody, conflictPatIdsArray);

    //                     // for updating conflict count when navigate from login to home screen (handle sync in login)
    //                     if (responseFromApi != 'serverError') {
    //                         // Insert Tier and Compliance
    //                         responseFromApi = await synchronization.syncTierAndComplianceData(apiUserBody);
    //                         if (responseFromApi != 'serverError') {
    //                             responseFromApi = await synchronization.syncAuditLogData();
    //                         }
    //                         // sync errorLog Data
    //                         if (responseFromApi != 'serverError') {
    //                             await synchronization.syncErrorLogData();
    //                         }
    //                         await this.props.checkAutoSyncProgressForStatus('offline', 100);
    //                         await this.setSyncEventValuesToZero();
    //                     } else {
    //                         await this.props.checkAutoSyncProgressForStatus('offline', 100);
    //                         await this.setSyncEventValuesToZero();
    //                     }
    //                 } else if (responseFromApi != 'serverError') {
    //                     return;
    //                 } else {
    //                     responseFromApi = await synchronization.syncAuditLogData();
    //                     // sync errorLog Data
    //                     if (responseFromApi != 'serverError') {
    //                         await synchronization.syncErrorLogData();
    //                     }
    //                     await this.props.checkAutoSyncProgressForStatus('offline', 100);
    //                     await this.setSyncEventValuesToZero();
    //                 }
    //             } else {
    //                 this.props.displayValidationForDBOrAppVersionChange(responseFromApi);
    //             }
    //         }, 0);
    //     }
    // }

    // set all the values
    // setSyncEventValuesToZero = () => {
    //     this.eventsCountSync = 0;
    //     this.offline = false;
    //     eventsCount = 0;
    //     AsyncStorage.setItem('checkOffline', JSON.stringify('online'));
    // }

    // updateSyncProgress = async (syncProgress) => {
    //     if (syncProgress) {
    //         await this.setState({
    //             syncProgress: syncProgress
    //         });
    //     }
    // }

    // error log display if there are any errors
    // onClickErrorLogAction = (screenName) => {
    //     let props = {
    //         loggedInUserDetails: this.props.userInfo,
    //         homeScreenNavigatorId: this.props.componentId, // for home button
    //         getIsLoading: this.props.getIsLoading,
    //         syncProgress: false
    //     };
    //     this.props.getIsLoading(true);
    //     navigation.pushScreenNavigator(this.props.componentId, screenName, props);
    // }

    // getAppNotificationsWhenSync = () => {
    //     if (this.notificationRef) {
    //         this.notificationRef.getAppNotifications();
    //     }
    // }

    render() {
        StatusBar.setBarStyle('light-content', true);
        return (
            <View {...this.props} pointerEvents={this.props.pointerEvents ? this.props.pointerEvents : 'auto'}>
                
                    <Header style={headerStyles.header}>
                        <Left>
                            <Image source={configImage.logoIcon} style={headerStyles.logo} />
                        </Left>
                        <Right style={headerStyles.headerOptions}>
                            {/* {this.state.syncProgress == 100 || !this.state.syncProgress || this.props.logScreen ? null :
                                <TouchableOpacity style={[headerStyles.iconSpace]} disabled={true}>
                                    <ProgressCircle
                                        percent={this.state.syncProgress}
                                        radius={17}
                                        borderWidth={7}
                                        color="#007aff"
                                        shadowColor="#999"
                                        bgColor="#fff"
                                    >
                                        <Text style={{ fontSize: 10 }}>{this.state.syncProgress}</Text>
                                    </ProgressCircle>
                                </TouchableOpacity>

                            }
                            <TouchableOpacity onPress={this.homeButtonHandler} >
                                <Image source={configImage.homeIcon} style={[headerStyles.iconSpace, headerStyles.notification]} />
                            </TouchableOpacity>

                            <Notification style={[headerStyles.iconSpace, headerStyles.notification]}
                                onRef={ref => (this.notificationRef = ref)}
                            /> */}
                            <Logout style={headerStyles.logoutIcon}
                                page={this.props.page}
                                userInfo={this.props.userInfo}
                                clickLogout={this.handleLogoutHandler}
                                onRef={ref => (this.logoutRef = ref)}
                                componentId={this.props.navigatorProps}
                                // autoSyncProgress={this.state.syncProgress}
                            />
                        </Right>
                    </Header>
            </View>
        )
    }
}

export default MainHeader;