/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import React from 'react';
import { StyleSheet, ImageBackground } from 'react-native';

import loginbackground from "../../assets/icons/Login-BG.png";

const BackgroundImage = props => (
    <ImageBackground
        source={props.imageName ? loginbackground : null}
        style={props.style ? props.style : styles.backgroundImage} >
        {props.children}
    </ImageBackground >
)

const styles = StyleSheet.create({
    backgroundImage: {
        width: "100%",
        flex: 1
    }
})

export default BackgroundImage;