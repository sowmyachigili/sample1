/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import React from 'react';
import { Image } from 'react-native';
import logoImage from "../../assets/Logo/Logo.png";

const LogoImage = () => (
    <Image source={logoImage} style={{ marginBottom: 30, width: 258, height: 29 }} />
)

export default LogoImage;