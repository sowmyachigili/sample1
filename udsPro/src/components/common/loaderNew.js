/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import React from 'react';
import {
    StyleSheet, ActivityIndicator
} from 'react-native';
const LoaderNew = props => {
    const { loading } = props;
    return (
        <ActivityIndicator animating={loading} size='large'
            color="#007aff" hidesWhenStopped={!loading}
            style={loading ? styles.loader : styles.noloader} />
    )
}

const styles = StyleSheet.create({
    loader: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        opacity: 1,
        backgroundColor: '#0000004d', // 5d59591a
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 999,

    },
    noloader: {
        height: 0,
    }
});

export default LoaderNew;