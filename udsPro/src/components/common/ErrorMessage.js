/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import configMessage from '../../config/configMessages';

const ErrorMessage = (props) => (
    <View style={localStyles.noRecordsView}>
        <Text style={localStyles.noRecordsText}>
            {props.message ? props.message : configMessage.noRecordsMsg}
        </Text>
    </View>
)

const localStyles = StyleSheet.create({
    noRecordsView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        minHeight: '88%',
    },
    noRecordsText: {
        fontSize: 17,
        fontFamily: 'Roboto-Regular',
        color: '#9A9A9A',
    }
});

export default ErrorMessage;