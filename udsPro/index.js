/**
 * CSSI Jayeesha
 *
 * @package	CSSI
 * @author	Jayeesha Dev Team
 */

import { AppRegistry, AppState } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import './crashlyticsSetup';

import { setJSExceptionHandler, setNativeExceptionHandler } from 'react-native-exception-handler';
import ErrorLogHandler from './src/utility/ErrorLogAlert';
import PasscodeAlert from './src/utility/PasscodeAlert';
import KeyboardManager from 'react-native-keyboard-manager';
var appState = AppState.currentState;

KeyboardManager.setEnable(true);
KeyboardManager.setEnableDebugging(false);
KeyboardManager.setKeyboardDistanceFromTextField(10);
KeyboardManager.setPreventShowingBottomBlankSpace(true);
KeyboardManager.setEnableAutoToolbar(false);
// KeyboardManager.setToolbarDoneBarButtonItemText("Done");
KeyboardManager.setToolbarManageBehaviour(0);
// KeyboardManager.setToolbarPreviousNextButtonEnable(false);
KeyboardManager.setShouldToolbarUsesTextFieldTintColor(false);
KeyboardManager.setShouldShowTextFieldPlaceholder(true); // deprecated, use setShouldShowToolbarPlaceholder
KeyboardManager.setShouldShowToolbarPlaceholder(true);
KeyboardManager.setOverrideKeyboardAppearance(true);
KeyboardManager.setShouldResignOnTouchOutside(true);
KeyboardManager.resignFirstResponder();
KeyboardManager.isKeyboardShowing()
    .then((isShowing) => {
        // ...
    });


AppState.addEventListener('change', _handleAppStateChange);
AppState.removeEventListener('change', _handleAppStateChange);
// handle the app status in background
const _handleAppStateChange = (nextAppState) => {
    if (appState.match(/inactive|background/) && nextAppState === 'active') {
        // check and display passcode alert when passcode is disabled
        PasscodeAlert.displayPasscodeErrorAlert(nextAppState);
        return;
    }
    appState = nextAppState;
}

const errorHandler = (e, isFatal) => {
    ErrorLogHandler(e, isFatal);
};

setJSExceptionHandler(errorHandler, true);
// setNativeExceptionHandler(errorHandler, true);

AppRegistry.registerComponent(appName, () => App);