import React, { useState, useEffect, useRef, useLayoutEffect, TouchableWithoutFeedback } from 'react';
import { FlatList, ScrollView, Text, TouchableOpacity, View, Alert, BackHandler } from 'react-native';
import * as Animatable from 'react-native-animatable';
import styles from './styles';

function ProfileScreen(props) {
    handleViewRef = ref => this.view = ref;
  
    bounce = () => this.view.bounce(800).then(endState => console.log(endState.finished ? 'bounce finished' : 'bounce cancelled'));
  

    return (
        <View style={ styles.container }>
            <Animatable.Text animation="slideInDown" iterationCount={25} direction="alternate">Up and down you go</Animatable.Text>
            <Animatable.Text animation="pulse" easing="ease-out" iterationCount="infinite" style={{ textAlign: 'center' }}>❤️</Animatable.Text>

            <Text onPress={this.bounce}>Click me</Text>

            <Animatable.View ref={this.handleViewRef}>
                <Text>Bounce me!</Text>
            </Animatable.View>
        </View>
    );
}

export default ProfileScreen;
