import React, { useState, useEffect, useRef, useLayoutEffect } from 'react';
import { FlatList, ScrollView, TouchableOpacity, View, Alert, BackHandler, StyleSheet } from 'react-native';
import { Button, Card, Icon, Text, AirbnbRating, Rating } from 'react-native-elements';
import * as Animatable from 'react-native-animatable';
import styles from './styles';

function HomeScreen(props) {
    // const dispatch = useDispatch();
    const urlImage = "https://images.unsplash.com/photo-1595526114035-0d45ed16cfbf?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80";
    function setRating(rating) {
        Alert.alert("Rating is: " + rating);
    }
    
    return (
        <View style={ styles.container }>
            <Text>Home!</Text>
            <Button title='Welcome'/>
            <Card>
                <Card.Title>Property</Card.Title>
                <Card.Divider />
                <Card.Image source={{ uri: urlImage }} />
                <Text style={{ marginBottom: 10, marginTop: 10 }}>
                    Norem Norem Norem Norem Norem Norem Norem Norem Norem Norem Norem Norem 
                    Norem Norem Norem Norem Norem Norem Norem Norem Norem Norem Norem Norem 
                    Norem Norem Norem Norem 😎
                </Text>
                <Button title="MORE INFO" />
            </Card>

            <AirbnbRating
                count={5}
                reviews={["Terrible", "Meh", "Good", "Very Good", "Amazing"]}
                defaultRating={5}
                size={20}
                onFinishRating={setRating}
            />

        </View>
    );
}

export default HomeScreen;
