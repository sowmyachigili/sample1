import { StyleSheet } from 'react-native';
// import { Configuration } from '../../Configuration';

export default StyleSheet.create({
    container: {
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center'
    },
});