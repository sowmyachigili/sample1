import React, { useState, useEffect, useRef, useLayoutEffect } from 'react';
import { FlatList, ScrollView, Text, TouchableOpacity, View, Alert, BackHandler } from 'react-native';
import styles from './styles';
import { Input, Icon } from "react-native-elements";

function NotificationsScreen(props) {
    // const dispatch = useDispatch();

    return (
        <View style={ styles.container }>
            <Input
                label="What's your email address?"
                placeholder="Email"
                leftIcon={<Icon size={24} name="email" />}
                errorMessage="I pop up when something goes wrong"
                />
            <Input
                secureTextEntry
                label="Type it, don't tell me"
                placeholder="Password"
                leftIcon={{ name: "lock", size: 24, color: "purple" }}
                labelStyle={{ color: "orange" }}
                inputContainerStyle={{ borderBottomColor: "red" }}
                errorMessage="I pop up when something goes wrong"
                />
        </View>
    );
}

export default NotificationsScreen;
