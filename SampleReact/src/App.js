import * as React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome';
import HomeScreen from './screen/HomeScreen/HomeScreen';
import NotificationsScreen from './screen/NotificationsScreen/NotificationsScreen';
import SettingsScreen from './screen/SettingsScreen/SettingsScreen';
import ProfileScreen from './screen/ProfileScreen/ProfileScreen';

const Tab = createBottomTabNavigator();

function MyTabs() {
  return (
    <Tab.Navigator>
        <Tab.Screen 
            name="Home" 
            component={HomeScreen}
            options={{
            tabBarLabel: 'Home',
            tabBarIcon: ({ color, size }) => (
                <Icon name="home" color={color} size={size} />
            ),
            }} />
        <Tab.Screen 
            name="Notifications" 
            component={NotificationsScreen}
            options={{
            tabBarLabel: 'Notifications',
            tabBarIcon: ({ color, size }) => (
                <Icon name="bell" color={color} size={size} />
            ),
            }} />
        <Tab.Screen 
            name="Settings" 
            component={SettingsScreen}
            options={{
            tabBarLabel: 'Settings',
            tabBarIcon: ({ color, size }) => (
                <Icon name="cog" color={color} size={size} />
            ),
            }} />
        <Tab.Screen
            name="Profile"
            component={ProfileScreen}
            options={{
            tabBarLabel: 'Profile',
            tabBarIcon: ({ color, size }) => (
                <Icon name="user" color={color} size={size} />
            ),
            }} />
    </Tab.Navigator>
  );
}

const App = props => {
    return (
        <NavigationContainer>
            <MyTabs />
        </NavigationContainer>
    );
}

export default App;